package es.sadesi.webmovil;

public class ConstantesWebmovil {
	
	public static final String LISTA_CARPETAS = "listaCarpetas";
	public static final String LISTA_CARPETAS_STATUS = "listaCarpetasStatus";
	
	public static final String SECCION = "seccion";
	
	public static final String TIPO_NUMERO_NACIONAL = "nacional";
	public static final String TIPO_NUMERO_INTERNACIONAL = "internacional";
	public static final String TIPO_NUMERO_CORPORATIVO = "corporativo";
	
	public static final String ADMINISTRACION_ID_EMISOR_ACTIVO = "administracionIdEmisorActivo";
	public static final String ADMINISTRACION_NUM_PAGINA_ACTIVO = "administracionNumPaginaActivo";
	public static final String ADMINISTRACION_NUM_PAGINA_EMISORES_ACTIVO = "administracionNumPaginasEmisoresActivo";
	public static final String ADMINISTRACION_LISTA_USUARIOS_TEMP = "administracionListaUsuariosTemp";
	public static final String ADMINISTRACION_FACTURACION_PERIODO_ALMACENADO_MES = "administracionFacturacionPeriodoAlmacenadoMes";
	public static final String ADMINISTRACION_FACTURACION_PERIODO_ALMACENADO_ANYO = "administracionFacturacionPeriodoAlmacenadoAnyo";
	public static final String ADMINISTRACION_FACTURACION_RESULTADOS_ALMACENADOS = "administracionFacturacionResultadosAlmacenados";
}
