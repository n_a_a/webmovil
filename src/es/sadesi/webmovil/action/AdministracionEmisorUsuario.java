package es.sadesi.webmovil.action;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SManagerImpl;

public class AdministracionEmisorUsuario extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionEmisorUsuario.class);
	
	private String formUsuario;
	private String formIdEmisor;
	private String descripcion;
	private String textoResultado;
	private String textoError;
	private String accion;
	
	public String execute() throws Exception {
		logger.info("Ejecutando acción AdministracionEmisorUsuario");
		
		setSeccion("administracion");
		
		// ¿Se envió un nombre de usuario?
		if ("Encontrar".equals(accion)) {
			if (formUsuario == null || "".equals(formUsuario)) {
				setTextoError("Por favor, introduzca un nombre de usuario.");
			} else {
				// Obtenemos los datos del usuario del WS
				try {
					formIdEmisor = getP3sManager().emisorUsuario(formUsuario);
					
					// Obtenemos ahora la descripción del emisor usando infoEmisor
					Map <String,Object> datosEmisor = getP3sManager().infoEmisor(formIdEmisor);
					
					textoResultado = (String) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ID);
				} catch (Exception e) {
					setTextoError("Nombre de usuario incorrecto o usuario no dado de alta en la pasarela.");
				}
			}
		} else {
			textoResultado = "";
		}
		
		return SUCCESS;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTextoResultado() {
		return textoResultado;
	}

	public void setTextoResultado(String textoResultado) {
		this.textoResultado = textoResultado;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setFormUsuario(String formUsuario) {
		this.formUsuario = formUsuario;
	}

	public String getFormUsuario() {
		return formUsuario;
	}

	public void setFormIdEmisor(String formIdEmisor) {
		this.formIdEmisor = formIdEmisor;
	}

	public String getFormIdEmisor() {
		return formIdEmisor;
	}

	public void setTextoError(String textoError) {
		this.textoError = textoError;
	}

	public String getTextoError() {
		return textoError;
	}
}
