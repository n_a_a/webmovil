package es.sadesi.webmovil.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.xwork.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.sms.P3SProperties;
import es.sadesi.sms.ws.cliente.SmsWS;
import es.sadesi.sms.ws.cliente.SmsWSServiceLocator;
import es.sadesi.webmovil.model.Attachment;
import es.sadesi.webmovil.model.DefItem;
import es.sadesi.webmovil.model.OptionItem;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilWebmovil;

public class NotificacionesJDAComponer extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog (NotificacionesJDAComponer.class);
	
	private static String rutaArchivosTemporales = P3SProperties.getInstance ().getProperty (P3SProperties.RUTA_ARCHIVOS_TEMPORALES);
	private static String claveEmisor = P3SProperties.getInstance().getProperty(P3SProperties.CLAVE_EMISOR);
	
	private String accion;
	private List<Attachment> adjuntos;
	private int numAdjuntos;
	private String nuevoTipo;
	private String texto;
	private String tlfnoDestino;
	private List<DefItem> listaTipos;
	private String lineAccionValue;
	private String someError;
	private List<OptionItem> opciones;
	private int numOpciones;
	private int flags;
	private boolean respuestaFlagFeedback;
	private boolean opcionesOn;
	private boolean ackOn;
	private Vector<String> optionsSyntaxErrors = null;
	private boolean noSendSMS;
	private String idContacto;
	private String idGrupo;
	
	private int page;
	private String comesFrom;
	
	private String [] binaryTypes = {
		"image/jpg", "image/png", "image/gif", "application/pdf", "application/msword", 
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.oasis.opendocument.text",
		"audio/mpeg"
	};
	
	private File [] files;
	private String [] contentTypes;
	private String [] fileNames;
	
	public File [] getUpload () { return this.files; }
	public void setUpload (File [] file) { this.files = file; }
	public String [] getUploadContentType () { return this.contentTypes; }
	public void setUploadContentType (String [] contentType) { this.contentTypes = contentType; }
	public String [] getUploadFileName () { return this.fileNames; }
	public void setUploadFileName (String [] fileName) { this.fileNames = fileName; }
	
	private boolean empty (String s) {
		return (s == null || "".equals (s));
	}
	
	private boolean checkDateTime (String s) {
		//return (s.matches("\\d{2}-\\d{2}-\\d{4} \\d{2}:\\d{2}:\\d{2}"));
		return (s.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"));
	}
	
	private boolean checkDouble (String s) {
		try {
			Double.parseDouble(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public String execute() throws Exception {
		logger.info ("NotificacionesJDA - 2 - Componer [" + accion + "] [" + lineAccionValue + "]");
		someError = null;
		setSeccion("notificacionesjda");
		
		// Antes que nada...
		if ("Cancelar".equals (accion)) {
			if ("ver".equals (comesFrom)) {
				return "tolist";
			} else {
				return "cancel";
			}
		}
		
		// Setup
		listaTipos = new ArrayList<DefItem> ();
		listaTipos.add (new DefItem ("p3s/url", "Url"));
		listaTipos.add (new DefItem ("p3s/event", "Cita"));
		/*listaTipos.add (new DefItem ("p3s/loc", "Localización"));*/
		listaTipos.add (new DefItem ("p3s/video", "Enlace a video"));
		listaTipos.add (new DefItem ("image/jpg", "Imagen JPG"));
		listaTipos.add (new DefItem ("image/png", "Imagen PNG"));
		listaTipos.add (new DefItem ("image/gif", "Imagen GIF"));
		listaTipos.add (new DefItem ("application/pdf", "Documento PDF"));
		listaTipos.add (new DefItem ("application/msword", "Documento DOC"));
		listaTipos.add (new DefItem ("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "Documento DOCX"));
		listaTipos.add (new DefItem ("application/vnd.oasis.opendocument.text", "Documento ODT"));
		listaTipos.add (new DefItem ("audio/mpeg", "Audio MP3"));
		listaTipos.add (new DefItem ("p3s/option", "Respuesta"));
		listaTipos.add (new DefItem ("p3s/ack", "Solicitud de confirmación"));
		
		// Instanciar cliente de Webservices
		SmsWSServiceLocator locator;
		SmsWS smsWS = null;
		try {
			locator = new SmsWSServiceLocator ();
			smsWS = locator.getP3SWebmovilWS ();
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción al crear instancia del cliente de WS!");
			someError = "Error fatal. Contacte con su administrador: " + e.getMessage ();
		}
		
		// Preparamos ruta de archivos temporales
		Usuario usuario = getUsuarioRemoto();
		String rutaArchivos = rutaArchivosTemporales + usuario.getId ();
		UtilWebmovil.compruebaSiExisteYSiNoCrea (rutaArchivos);
		
		// ¿Entramos por primera vez?
		if ((accion == null || "".equals (accion) || "Cancelar".equals (accion)) && (lineAccionValue == null || "".equals (lineAccionValue))) {
			logger.debug ("Entrando por primera vez. Creando vector de adjuntos y vaciando carpeta temporal."); 
			adjuntos = new ArrayList<Attachment> ();
			UtilWebmovil.vaciaDirectorio (rutaArchivos);
			opcionesOn = false;
			ackOn = false;
			comesFrom = "";
		} else {
			adjuntos = getAdjuntos ();
		}
		
		for (Attachment adjunto : adjuntos) {
			adjunto.setLastError (null);
		}
		
		// Manejar subidas de archivos (like a boss)
		if (files != null && !"Cancelar".equals (accion)) {
			for (int i = 0; i < files.length; i ++) {
				// Search which - ¿Seguro que no hay una forma mejor de hacer esto :-/
				for (Attachment adjunto : adjuntos) {
					adjunto.setLastError(null);
					String fileName = adjunto.getFilename ();
					if (fileName != null && fileName.contains (fileNames [i])) {
						// Generamos un hash para el nombre de archivo temporal, copiamos el archivo
						// al directorio temporal del usuario, y finalmente poblamos los datos necesarios
						// en el adjunto (enlace al archivo, básicamente).
						logger.debug ("Pertenece a la linea " + adjunto.getId());
						logger.debug ("Content type == " + contentTypes [i]);
						
						// Algunos navegadores (ejem... de Linux... ejem) no son capaces de
						// rellenar bien el content type en algunos (ejem... muchos) casos.
						// En ese caso, intentamos que Java lo resuelva. 
						if ("binary/octet-stream".equals (contentTypes [i])) {
							MimetypesFileTypeMap mtm = new MimetypesFileTypeMap ();
							contentTypes [i] = mtm.getContentType(files [i]);
							logger.info ("Content type (recalculado) == " + contentTypes [i]);
						}
						
						// Check content type. People are AIVOL
						boolean doMe = true;
						logger.info ("Usuario tratando de subir " + adjunto.getType () + ", contentTypes [i] = " + contentTypes [i]);
						if (
							contentTypes [i] == null ||
							!(
								("image/jpg".equals (adjunto.getType ()) && (contentTypes [i].contains ("jpeg") || contentTypes [i].contains ("jpg"))) ||
								("image/png".equals (adjunto.getType ()) && contentTypes [i].contains ("png")) ||
								("image/gif".equals (adjunto.getType ()) && contentTypes [i].contains ("gif")) ||
								("application/pdf".equals (adjunto.getType ()) && contentTypes [i].contains ("pdf")) || 
								("application/msword".equals (adjunto.getType ()) && contentTypes [i].contains ("msword")) || 
								("application/vnd.openxmlformats-officedocument.wordprocessingml.document".equals (adjunto.getType ()) && contentTypes [i].contains ("vnd.openxmlformats-officedocument.wordprocessingml.document")) ||
								("application/vnd.oasis.opendocument.text".equals (adjunto.getType ()) && contentTypes [i].contains ("vnd.oasis.opendocument.text")) ||
								("audio/mpeg".equals (adjunto.getType ()) && contentTypes [i].contains ("mpeg")) ||
								("audio/mp3". equals (adjunto.getType ()) && contentTypes [i].contains ("mp3"))
							)
						) {
							logger.warn ("El usuario intentó subir un archivo de tipo incorrecto. adjunto.getType=" + adjunto.getType () + ", contentTypes [i]=" + contentTypes [i]);
							adjunto.setLastError ("¡El archivo que intentó subir no es del tipo especificado!");
							doMe = false;
						}
					
						if (doMe) {
							String hashedFilename = UUID.randomUUID ().toString () + fileNames [i].substring (fileNames [i].lastIndexOf ('.'));
							
							logger.debug ("Copiando " + fileNames [i] + " a " + rutaArchivos + "/" + hashedFilename);
							File newFile = new File (rutaArchivos, hashedFilename);
							FileUtils.copyFile(files [i], newFile);
							adjunto.setFullPathToFile(rutaArchivos + "/" + hashedFilename);
							adjunto.setAlreadyUploaded (true);
							adjunto.setJustFilename (fileNames [i]);
						}
					}
				}
			}
		}
	
		// Añadir una linea
		if ("Aceptar".equals (accion)) {
			logger.info ("Añadir " + nuevoTipo);
			if ("p3s/option".equals(nuevoTipo)) {
				opcionesOn = true;
				numOpciones = 1;
				opciones = new ArrayList<OptionItem> ();
				opciones.add (new OptionItem ("", ""));
			} else if ("p3s/ack".equals(nuevoTipo)){
				ackOn = true;
			} else {
				Attachment attachment = new Attachment (adjuntos.size ());
				attachment.setType (nuevoTipo);
				String partIconName = nuevoTipo.substring (1 + nuevoTipo.lastIndexOf("/"));
				if("msword".equals(partIconName)) {
					partIconName = "doc";
				} else if("vnd.openxmlformats-officedocument.wordprocessingml.document".equals(partIconName)) {
					partIconName = "docx";
				} else if("vnd.oasis.opendocument.text".equals(partIconName)) {
					partIconName = "odt";
				}
				attachment.setIconName ("ft-" + partIconName + ".png");
				attachment.setAlreadyUploaded (false);
				for (DefItem item : listaTipos) {
					if (item.getKey ().equals (nuevoTipo)) {
						attachment.setTypeName (item.getValue ());
						break;
					}
				}
				adjuntos.add (attachment);
			}
		}
		
		// Añadir una opción
		if ("Nueva linea".equals (accion)) {
			logger.info ("Añadir opción");
			opciones.add (new OptionItem ("", ""));
			numOpciones ++;
		}
		
		// Orden de los adjuntos
		if (lineAccionValue != null && !"".equals (lineAccionValue)) {
			if ("deAttOp".equals (lineAccionValue)) {
				logger.info ("Line accion [borrar opciones]");
				opcionesOn = false;
				flags = 0;
			} else if ("deAttAck".equals(lineAccionValue)) {
				logger.info ("Line accion [borrar ack]");
				ackOn = false;
			} else {
				String whatDo = lineAccionValue.substring (0, 2);
				String whereDo = lineAccionValue.substring (2, 5);
				int id = Integer.parseInt (lineAccionValue.substring(5));
				if ("Opc".equals (whereDo)) {
					logger.info ("Borrar opción id = " + id);
					opciones.remove (id);
				} else {
					logger.info ("Line accion [" + whatDo + "] al id = " + id);
					if ("up".equals (whatDo)) {
						if (id > 0) {
							Collections.swap (adjuntos, id, id - 1);
						}
					} else if ("dw".equals (whatDo)) {
						if (id < adjuntos.size () - 1) {
							Collections.swap (adjuntos, id, id + 1);
						}
					} else if ("de".equals (whatDo)) {
						adjuntos.remove (id);
					}
				}
			}
		}
		
		// Enviar!
		if ("Enviar".equals (accion) && smsWS != null) {
			boolean keepGoing = true;
			
			// Comprobar los adjuntos
			logger.debug ("Primero comprobamos los adjuntos...");
			
			for (Attachment adjunto : adjuntos) {
				adjunto.setSyntaxErrors(null);
				if (adjunto.getLastError() != null) {
					adjunto.addSyntaxError(adjunto.getLastError());
					adjunto.setLastError(null);
				}
				if ("p3s/url".equals (adjunto.getType ())) {
					if (empty (adjunto.getUrl ()) || empty (adjunto.getDesc ())) {
						adjunto.addSyntaxError("Debe rellenar todos los campos");
						keepGoing = false;
					}
				} else if ("p3s/event".equals (adjunto.getType ())) {
					if (empty (adjunto.getStart ())) {
						adjunto.addSyntaxError ("Debe especificar fecha/hora de inicio");
						keepGoing = false;
					}
					if (empty (adjunto.getEnd ())) {
						adjunto.addSyntaxError ("Debe especificar fecha/hora de finalización");
						keepGoing = false;
					}
					if (!checkDateTime (adjunto.getStart ())) {
						adjunto.addSyntaxError ("Error de sintaxis en fecha/hora de inicio");
						keepGoing = false;
					}
					if (!checkDateTime (adjunto.getEnd ())) {
						adjunto.addSyntaxError ("Error de sintaxis en fecha/hora de finalización");
						keepGoing = false;
					}
					/*
					if (!checkDouble (adjunto.getLon())) {
						adjunto.addSyntaxError ("La longitud debe tener un valor numérico");
						keepGoing = false;
					}
					if (!checkDouble (adjunto.getLat())) {
						adjunto.addSyntaxError ("La latitud debe tener un valor numérico");
						keepGoing = false;
					}
					*/
				} else if ("p3s/option".equals (adjunto.getType ())) {

				} else if ("p3s/loc".equals (adjunto.getType ())) {
					if (empty (adjunto.getDesc ())) {
						adjunto.addSyntaxError("Debe rellenar todos los campos");
						keepGoing = false;
					}
					if (!checkDouble (adjunto.getLon())) {
						adjunto.addSyntaxError ("La longitud debe tener un valor numérico");
						keepGoing = false;
					}
					if (!checkDouble (adjunto.getLat())) {
						adjunto.addSyntaxError ("La latitud debe tener un valor numérico");
						keepGoing = false;
					}
				} else if ("p3s/video".equals (adjunto.getType ())) {
					if (empty (adjunto.getUrl ()) || empty (adjunto.getTit ())) {
						adjunto.addSyntaxError("Debe rellenar todos los campos");
						keepGoing = false;
					}
					if (!adjunto.getUrl ().contains ("youtube") && !adjunto.getUrl ().contains ("youtu.be")) {
						adjunto.addSyntaxError("Debe especificar una URL de youtube válida");
						keepGoing = false;
					}
				} else {
					if (!adjunto.isAlreadyUploaded ()) {
						adjunto.addSyntaxError("Debe especificar un archivo válido o eliminar este adjunto de la lista.");
						keepGoing = false;
					}
				}
			}
			
			if (opcionesOn) {
				optionsSyntaxErrors = null;
				int nOp = 1;
				for (OptionItem opcion : opciones) {
					if (empty (opcion.getTexto())) {
						if (optionsSyntaxErrors == null) optionsSyntaxErrors = new Vector<String> ();
						optionsSyntaxErrors.add ("Falta texto en opción #" + nOp);
						keepGoing = false;
					}
					if (empty (opcion.getUrl())) {
						if (optionsSyntaxErrors == null) optionsSyntaxErrors = new Vector<String> ();
						optionsSyntaxErrors.add ("Falta url en opción #" + nOp);
						keepGoing = false;
					}
					nOp ++;
				}
			}
			
			if (ackOn) {
				// Para un futuro...
			}
			
			// Intentamos enviar
			
			if (keepGoing) {
				if (tlfnoDestino == null || "".equals (tlfnoDestino)) {
					someError = "Debe especificar al menos un número de teléfono";
				} else {
					logger.debug ("Comprobaciones pasadas con éxito. ¡A enviar!");
					try {
						// Subir todos los adjuntos
						Vector<String> binaryTypesV = UtilWebmovil.makeVectorFromStringArray(binaryTypes);
						Vector<Integer> uploadedIds = new Vector<Integer> ();
						
						// Adjuntos especiales (generados)
						// p3s/config
						if (noSendSMS) {
							uploadedIds.add (
								smsWS.p3SPushUploadTextAttachment(
										"p3s/config",
										"{\"sendsms\":\"false\"}",
										usuario.getEmisor ().getEmisor (),
										usuario.getUsuario (),
										claveEmisor
										)
								);
						}
						
						// Adjuntos normales
						for (Attachment adjunto : adjuntos) {
							logger.debug ("Procesando adjunto de tipo " + adjunto.getType ());
							if (binaryTypesV.contains (adjunto.getType ())) {
								logger.debug ("Adjunto Binario. File: " + adjunto.getFullPathToFile ());
								// Adjunto binario. Tenemos que enviar el contenido del archivo
								if (adjunto.isAlreadyOnServer()) {
									uploadedIds.add (adjunto.getIdOnServer());
								} else {
									logger.debug("Transfiriendo a p3s archivo en " + adjunto.getFullPathToFile ());
									logger.debug("Creando el objeto a partir de un File (" + adjunto.getFullPathToFile () + ")");
									File fi = new File (adjunto.getFullPathToFile ());
									
									logger.debug("Creando FileDataSource");
									FileDataSource fds = new FileDataSource (fi);
									logger.debug("Creando DataHandler");
									DataHandler dh = new DataHandler (fds);
									logger.debug("Enviando bytes...");
									uploadedIds.add (
											smsWS.p3SPushUploadBinaryAttachment(
													adjunto.getType (),
													//new DataHandler (new FileDataSource (adjunto.getFullPathToFile ())),
													dh,
													usuario.getEmisor ().getEmisor (),
													usuario.getUsuario (),
													claveEmisor
													)
											);
									logger.debug("Subido OK!");
								}
							} else {
								// Adjunto json. Tenemos que construir el (pseudo)json custom con los datos almacenados
								logger.debug ("Adjunto JSON.");
								String json = "";
								if ("p3s/url".equals (adjunto.getType ())) {
									json = "{\"url\":\"" + UtilWebmovil.fixURL(adjunto.getUrl ()) + "\",\"desc\":\"" + adjunto.getDesc () + "\"}";
								} else if ("p3s/event".equals (adjunto.getType ())) {
									adjunto.fixStart ();
									adjunto.fixEnd ();
									json = "{\"title\":\"" + adjunto.getTitle () + "\",\"allday\":";
									if (adjunto.isAllDay()) json = json + "\"true\","; else json = json + "\"false\",";
									json = json + "\"start\":\"" + adjunto.getStart () + "\",";
									json = json + "\"end\":\"" + adjunto.getEnd () + "\",";
									json = json + "\"url\":\"" + UtilWebmovil.fixURL(adjunto.getUrl ()) + "\"";
									/*
									json = json + ",\"loc\":{";
									json = json + "\"desc\":\"" + adjunto.getDesc () + "\",";
									json = json + "\"lon\":\"" + adjunto.getLon () + "\",\"lat\":\"" + adjunto.getLat () + "\"}";
									*/
									json = json + "}";
								} else if ("p3s/option".equals (adjunto.getType ())) {

								} else if ("p3s/loc".equals (adjunto.getType ())) {
									json = "{\"desc\":\"" + adjunto.getDesc () + "\",";
									json = json + "\"lon\":\"" + adjunto.getLon () + "\",\"lat\":\"" + adjunto.getLat () + "\"}";
								} else if ("p3s/video".equals (adjunto.getType ())) {
									json = "{\"tit\":\"" + adjunto.getTit () + "\",\"url\":\"" + UtilWebmovil.fixURL(adjunto.getUrl ()) + "\"}";
								}

								logger.debug ("JSON: " + json);
								uploadedIds.add (
										smsWS.p3SPushUploadTextAttachment(
												adjunto.getType (),
												json,
												usuario.getEmisor ().getEmisor (),
												usuario.getUsuario (),
												claveEmisor
												)
										);
							}
						}
						// Añadir opción
						if (opcionesOn) {
							flags = 0;
							if (respuestaFlagFeedback) flags |= 1;
							String json = "{\"num\":\"" + opciones.size () + "\",";
							json = json + "\"opciones\":{";
							json = json + "\"opcion\":[";
							int ctr = 0;
							for (OptionItem opcion : opciones) {
								json = json + "{\"texto\":\"" + opcion.getTexto() + "\",";
								json = json + "\"url\":\"" + UtilWebmovil.fixURL(opcion.getUrl ()) + "\"}";
								ctr ++; if (ctr < opciones.size ()) json = json + ",";
							}
							json = json + "]";
							json = json + "},";
							json = json + "\"flags\":\"" + flags +"\"}";
							logger.debug ("JSON: " + json);
							uploadedIds.add (
									smsWS.p3SPushUploadTextAttachment(
											"p3s/option",
											json,
											usuario.getEmisor ().getEmisor (),
											usuario.getUsuario (),
											claveEmisor
											)
									);
						}
						
						// Añadir ack
						if (ackOn) {
							String json = "{\"type\":\"default\"}";
							logger.debug ("JSON: " + json);
							uploadedIds.add (
									smsWS.p3SPushUploadTextAttachment(
											"p3s/ack",
											json,
											usuario.getEmisor ().getEmisor (),
											usuario.getUsuario (),
											claveEmisor
											)
									);
						}

						// Enviar
						// Obtenemos la lista de destinatarios
						StringTokenizer tokenizer = new StringTokenizer (tlfnoDestino, ", \n-;.");
						while (tokenizer.hasMoreElements()) {
							String tlfnoToken = tokenizer.nextElement().toString().trim();
							// Validar / convertir número de teléfono
							try {
								String tlfnoValidate [] = UtilWebmovil.validaNumeroTelefono(tlfnoToken);
								tlfnoToken = tlfnoValidate [0];
							} catch (Exception e) { }
							try {
								tlfnoToken = UtilWebmovil.cleanUpTlfno (tlfnoToken);
							} catch (Exception e) { }
							logger.info ("Enviando mensaje a " + tlfnoToken);
							int id = smsWS.p3SPushSendMultimediaMessage (
									tlfnoToken,
									texto,
									uploadedIds,
									usuario.getEmisor ().getEmisor (),
									usuario.getUsuario (),
									claveEmisor
									);
							logger.info ("Mensaje con adjuntos enviado correctamente. ID = " + id);
						}
						
						return "sent";
					} catch (Exception e) {
						logger.error("Error componiendo y enviando el archivo: " + e.getMessage ());
						someError = "Error componiendo y enviando el archivo. Contacte con su administrador: " + e.getMessage ();
					}
				}
			}
		}
		
		// Store
		logger.debug ("Guardando tiestos en la sesión");
		remapAdjuntos ();
		ActionContext.getContext ().getSession ().put ("adjuntos", adjuntos);
		numAdjuntos = adjuntos.size ();
		
		logger.debug ("All done");
		return SUCCESS;
	}
	
	public String cargarDestinariosNotificacion() {
		String resultado = SUCCESS;
		
		try {
			final List<String[]> destinos = UtilWebmovil.validaDestinatario("", idContacto, idGrupo, getUsuarioRemoto());
			
			if(destinos != null && destinos.size() > 0) {
				if(StringUtils.isEmpty(tlfnoDestino)) {
					tlfnoDestino = "";
				} else {
					tlfnoDestino += ",";
				}
				final Iterator<String[]> destino = destinos.iterator();
				while (destino.hasNext()) {
					final String tlfnDestinoComprobar = UtilWebmovil.cleanUpTlfno (destino.next()[0]);
					if(!tlfnoDestino.contains(tlfnDestinoComprobar)) {
						tlfnoDestino += tlfnDestinoComprobar + ",";
					}
				}
				//Eliminar ultima coma
				tlfnoDestino = tlfnoDestino.substring(0, tlfnoDestino.length() - 1);
			}
		} catch (Exception e) {
			LOG.error("Error al obtener los destinatarios del grupo o del contacto de la agenda", e);
			addActionError("Error al obtener los destinatarios de la agenda");
			resultado = ERROR;
		}
		
		return resultado;
	}

	private void remapAdjuntos () {
		for (int i = 0; i < adjuntos.size (); i ++) {
			adjuntos.get (i).setId (i);
		}
	}
	
	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<Attachment> getAdjuntos() {
		@SuppressWarnings("unchecked")
		List<Attachment> result = (List<Attachment>) ActionContext.getContext ().getSession ().get ("adjuntos");
		if (result == null) {
			adjuntos = new ArrayList<Attachment> ();
		}
		return result;
	}

	public void setAdjuntos(List<Attachment> adjuntos) {
		this.adjuntos = adjuntos;
	}

	public String getNuevoTipo() {
		return nuevoTipo;
	}

	public void setNuevoTipo(String nuevoTipo) {
		this.nuevoTipo = nuevoTipo;
	}
	
	public List<DefItem> getListaTipos() {
		return listaTipos;
	}

	public void setListaTipos(List<DefItem> listaTipos) {
		this.listaTipos = listaTipos;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTlfnoDestino() {
		return tlfnoDestino;
	}

	public void setTlfnoDestino(String tlfnoDestino) {
		this.tlfnoDestino = tlfnoDestino;
	}

	public int getNumAdjuntos() {
		return numAdjuntos;
	}

	public void setNumAdjuntos(int numAdjuntos) {
		this.numAdjuntos = numAdjuntos;
	}

	public String getLineAccionValue() {
		return lineAccionValue;
	}

	public void setLineAccionValue(String lineAccionValue) {
		this.lineAccionValue = lineAccionValue;
	}
	public String getSomeError() {
		return someError;
	}
	public void setSomeError(String someError) {
		this.someError = someError;
	}
	public List<OptionItem> getOpciones() {
		return opciones;
	}
	public void setOpciones(List<OptionItem> opciones) {
		this.opciones = opciones;
	}
	public int getNumOpciones() {
		return numOpciones;
	}
	public void setNumOpciones(int numOpciones) {
		this.numOpciones = numOpciones;
	}
	public int getFlags() {
		return flags;
	}
	public void setFlags(int flags) {
		this.flags = flags;
	}
	public boolean isOpcionesOn() {
		return opcionesOn;
	}
	public void setOpcionesOn(boolean opcionesOn) {
		this.opcionesOn = opcionesOn;
	}
	public boolean isRespuestaFlagFeedback() {
		return respuestaFlagFeedback;
	}
	public void setRespuestaFlagFeedback(boolean respuestaFlagFeedback) {
		this.respuestaFlagFeedback = respuestaFlagFeedback;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getComesFrom() {
		return comesFrom;
	}
	public void setComesFrom(String comesFrom) {
		this.comesFrom = comesFrom;
	}
	public Vector<String> getOptionsSyntaxErrors() {
		return optionsSyntaxErrors;
	}
	public void setOptionsSyntaxErrors(Vector<String> optionsSyntaxErrors) {
		this.optionsSyntaxErrors = optionsSyntaxErrors;
	}
	public boolean isAckOn() {
		return ackOn;
	}
	public void setAckOn(boolean ackOn) {
		this.ackOn = ackOn;
	}
	public boolean isNoSendSMS() {
		return noSendSMS;
	}
	public void setNoSendSMS(boolean noSendSMS) {
		this.noSendSMS = noSendSMS;
	}
	public String getIdContacto() {
		return idContacto;
	}
	public void setIdContacto(String idContacto) {
		this.idContacto = idContacto;
	}
	public String getIdGrupo() {
		return idGrupo;
	}
	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}
}
