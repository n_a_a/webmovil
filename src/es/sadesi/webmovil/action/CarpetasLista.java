package es.sadesi.webmovil.action;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.sms.P3SProperties;
import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.Carpeta;
import es.sadesi.webmovil.model.Usuario;

public class CarpetasLista extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private Vector<Carpeta> carpetasListaStore = null;
	private static final Log logger = LogFactory.getLog(CarpetasLista.class);
	
	private static String rutaExcels = P3SProperties.getInstance().getProperty(P3SProperties.RUTA_EXCELS);
	
	private String accion;
	private String accionIdCarpeta;
	private String accionNombreCarpeta;
	private boolean usuarioEspecial;
	
	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		
		logger.info("Ejecutando acción CarpetasLista");
		
		setSeccion("SMS");
		
		// Obtenemos los datos necesarios del usuario de la sesión:
		Map<String, Object> session = (Map<String, Object>) ActionContext
				.getContext().get("session");
		Usuario user = getUsuarioRemoto();
		String usuario = user.getUsuario();
		String emisor = user.getEmisor().getEmisor();

		// Primero tenemos que atender a las posibles acciones 
		// que podemos realizar
		
		if ("eliminar".equals(accion)) {
			if (accionIdCarpeta != null) {
				// Llamamos al WS para eliminar la carpeta referida
				getP3sManager().eliminaCarpetaUsuario(usuario, emisor, accionIdCarpeta);
				// Invalidamos la lista de carpetas en la sesión (si existiera)
				session.put(ConstantesWebmovil.LISTA_CARPETAS_STATUS, "refresh");
			}
		} else if ("Crear carpeta".equals(accion)) {
			if (accionNombreCarpeta != null) {
				// Llamamos al WS para crear la carpeta referida
				getP3sManager().creaCarpetaUsuario(usuario, emisor, accionNombreCarpeta);
				// Invalidamos la lista de carpetas en la sesión (si existiera)
				session.put(ConstantesWebmovil.LISTA_CARPETAS_STATUS, "refresh");
			} 
		}
		
		// Inicializamos nuestro modelo:
		
		// ¿Es un usuario "especial"?
		usuarioEspecial = false;
		try {
			FileReader fr = new FileReader (rutaExcels + "lista-usuarios.txt");
			BufferedReader textReader = new BufferedReader (fr);
			String lineFromFile = "";
			while ((lineFromFile = textReader.readLine ())!= null) {
				if (usuario.trim ().equals(lineFromFile.trim ())) {
					logger.info (usuario + " es un usuario especial. Tiene excels precalculados.");
					usuarioEspecial = true;
				}
			}
			textReader.close ();
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción al buscar al usuario en " + rutaExcels + "lista-usuarios.txt : " + e.toString ());
		}
		
		// Comprobamos si carpetasListaStore está creada en la 
		// sesión. Si no está, nos la traemos del webservice y la
		// almacenamos en la sesión.
		
		carpetasListaStore = (Vector<Carpeta>) session.get(ConstantesWebmovil.LISTA_CARPETAS);
		String carpetasListaStatus = (String) session.get(ConstantesWebmovil.LISTA_CARPETAS_STATUS);
		
		if (carpetasListaStore == null || "refresh".equals(carpetasListaStatus)) {
		
			logger.info("Obteniendo carpetasListaStore del webservice");
						
			carpetasListaStore = new Vector<Carpeta>();
			
			// Siempre añadimos por defecto una carpeta "virtual
			// "Enviados" y otra "Recibidos"
			
			// Obtenemos el número de mensajes enviados y recibidos
			
			//Map<String, Object> informacionUsuario = getP3sManager().infoUsuario(usuario, emisor, "00001231000000", "99991231000000");
			
			int numMensajesEnviados = 0;
			/*if (informacionUsuario.get ("total_carpeta_enviados") != null)
				numMensajesEnviados = ((Integer) informacionUsuario.get ("total_carpeta_enviados")).intValue();
			*/
			if (!usuarioEspecial) {
				numMensajesEnviados = getP3sManager().estadisticasUsuario(usuario, emisor, "enviadosNoEliminados", "00001231000000", "99991231000000");
			}
			
			int numMensajesRecibidos = 0;
			/*if (informacionUsuario.get ("total_carpeta_recibidos") != null)
				numMensajesRecibidos = ((Integer) informacionUsuario.get ("total_carpeta_recibidos")).intValue();
			*/
			
			numMensajesRecibidos = getP3sManager().estadisticasUsuario(usuario, emisor, "recibidosNoEliminados", "00001231000000", "99991231000000");
			
			// Creamos un objeto carpeta para cada una, con los datos
			
			Carpeta carpetaEnviados = new Carpeta ();
			carpetaEnviados.setIdCarpeta("-1");
			carpetaEnviados.setNombreCarpeta("Enviados");
			carpetaEnviados.setNumMensajes(numMensajesEnviados);
			
			Carpeta carpetaRecibidos = new Carpeta ();
			carpetaRecibidos.setIdCarpeta("-2");
			carpetaRecibidos.setNombreCarpeta("Recibidos");
			carpetaRecibidos.setNumMensajes(numMensajesRecibidos);
			
			// Los añadimos a nuestro modelo.
			
			carpetasListaStore.add (carpetaEnviados);
			carpetasListaStore.add (carpetaRecibidos);
			
			// Obtenemos la lista de carpetas del usuario en un vector
			// de HashMaps
	
			try {			
				Vector<HashMap<String, Object>> listaCarpetas = getP3sManager().listaCarpetasUsuario(usuario, emisor);
				
				// Recorremos listaCarpetas para poblar nuestro carpetasListaStore
	
				if (listaCarpetas != null) {
					for (int i = 0; i < listaCarpetas.size(); i++) {
						Carpeta carpeta = new Carpeta();
						carpeta.setIdCarpeta((String) listaCarpetas.get(i).get("id_carpeta"));
						carpeta.setNombreCarpeta((String) listaCarpetas.get(i).get("nombre"));
						carpeta.setNumMensajes(((Integer) listaCarpetas.get(i).get("numero_mensajes")).intValue());
						carpetasListaStore.add (carpeta);
					}
				}
			} catch (Exception e) {
				logger.error("Ocurrió un error llamando a listaCarpetas(" + usuario	+ "," + emisor + ")");
				throw new Exception(e);
			}
			
			// Ponemos la carpeta en la sesión.
			
			session.put(ConstantesWebmovil.LISTA_CARPETAS, carpetasListaStore);
			session.put(ConstantesWebmovil.LISTA_CARPETAS_STATUS, "ok");
		} else {
			logger.info("Obteniendo carpetasListaStore de la sesión");
		}

		return SUCCESS;
	}

	public Vector<Carpeta> getCarpetasListaStore() {
		return carpetasListaStore;
	}

	public void setCarpetasListaStore(Vector<Carpeta> carpetasListaStore) {
		this.carpetasListaStore = carpetasListaStore;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccionIdCarpeta(String accionIdCarpeta) {
		this.accionIdCarpeta = accionIdCarpeta;
	}

	public String getAccionIdCarpeta() {
		return accionIdCarpeta;
	}

	public void setAccionNombreCarpeta(String accionNombreCarpeta) {
		this.accionNombreCarpeta = accionNombreCarpeta;
	}

	public String getAccionNombreCarpeta() {
		return accionNombreCarpeta;
	}

	public boolean isUsuarioEspecial() {
		return usuarioEspecial;
	}

	public void setUsuarioEspecial(boolean usuarioEspecial) {
		this.usuarioEspecial = usuarioEspecial;
	}
	
	
}
