package es.sadesi.webmovil.action;

import java.util.HashMap;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import es.sadesi.webmovil.model.FuenteSuscripcion;
import es.sadesi.webmovil.model.Usuario;

public class AdministracionFuentesSuscripcion extends WebmovilAction {
	private static final long serialVersionUID = -9598098863074758L;
	private static final Log logger = LogFactory
			.getLog(AdministracionEditaEmisor.class);

	private Vector<FuenteSuscripcion> listaFuentes;
	private String idEmisor;
	private String lastError;
	private int numFuentes;

	private String formId;
	private String formDesc;
	private String formTipo;
	private String formUrlAlta;
	private String formUrlBaja;
	private String formAction;

	public String execute() throws Exception {
		setSeccion("administracion");
		logger.info("AdministracionFuentesSuscripcion");
		lastError = "";

		// Básico...
		Usuario user = getUsuarioRemoto();
		idEmisor = user.getEmisor().getEmisor();

		// Crear
		if ("Crear".equals(formAction)) {
			logger.info("AdministracionFuentesSuscripcion :: crear");
			try {
				if ((formDesc == null || "".equals(formDesc))
						|| (formUrlAlta == null || "".equals(formUrlAlta))
						|| ("completa".equals(formTipo) && (formUrlBaja == null || ""
								.equals(formUrlBaja)))) {
					lastError = "Debe rellenar todos los campos.";
				} else {
					getP3sManager().nuevaFuenteSuscripcion(formTipo, idEmisor,
							formDesc, formUrlAlta, formUrlBaja, "GET");
				}
			} catch (Exception e) {
				lastError = "Error creando nueva fuente de suscripción. Pruebe de nuevo más tarde."
						+ e.getMessage();
				logger.error("Error creando nueva fuente de suscripción.", e);
			}
		}

		// Editar
		if ("Editar".equals(formAction)) {
			logger.info("AdministracionFuentesSuscripcion :: editar");

			// formId...
			int id = -1;
			try {
				id = Integer.parseInt(formId);
			} catch (Exception e) {
				lastError = "Error editando fuente de suscripción. Pruebe de nuevo más tarde."
						+ e.getMessage();
				logger.error(
						"Error editando fuente de suscripción (obteniendo id).",
						e);
			}

			if (id > 1) {
				try {
					if ((formDesc == null || "".equals(formDesc))
							|| (formUrlAlta == null || "".equals(formUrlAlta))
							|| ("completa".equals(formTipo) && (formUrlBaja == null || ""
									.equals(formUrlBaja)))) {
						lastError = "Debe rellenar todos los campos.";
					} else {
						getP3sManager().eliminaFuenteSuscripcion(id);
						getP3sManager().nuevaFuenteSuscripcion(formTipo,
								idEmisor, formDesc, formUrlAlta, formUrlBaja,
								"GET");
					}
				} catch (Exception e) {
					lastError = "Error editando fuente de suscripción. Pruebe de nuevo más tarde."
							+ e.getMessage();
					logger.error("Error editando fuente de suscripción.", e);
				}
			} else {
				lastError = "Error editando fuente de suscripción. Pruebe de nuevo más tarde.";
				logger.error("¡ID INCORRECTO!");
			}
		}
		
		// Eliminar
		if ("Eliminar".equals (formAction)) {
			logger.info("AdministracionFuentesSuscripcion :: eliminar");

			// formId...
			int id = -1;
			try {
				id = Integer.parseInt(formId);
			} catch (Exception e) {
				lastError = "Error eliminando fuente de suscripción. Pruebe de nuevo más tarde."
						+ e.getMessage();
				logger.error(
						"Error eliminando fuente de suscripción (obteniendo id).",
						e);
			}

			if (id > 1) {
				try {
					getP3sManager().eliminaFuenteSuscripcion(id);
					lastError = "Fuente eliminada con éxito.";					
				} catch (Exception e) {
					lastError = "Error eliminando fuente de suscripción. Pruebe de nuevo más tarde."
							+ e.getMessage();
					logger.error("Error eliminando fuente de suscripción.", e);
				}
			} else {
				lastError = "Error eliminando fuente de suscripción. Pruebe de nuevo más tarde.";
				logger.error("¡ID INCORRECTO!");
			}
		}

		// Ejecutar siempre: obtener lista para el emisor.
		try {
			Vector<HashMap<String, String>> listaFuentesVectHash = getP3sManager()
					.getFuentesEmisor(idEmisor);

			listaFuentes = null;
			numFuentes = 0;

			if (listaFuentesVectHash != null && listaFuentesVectHash.size() > 0) {
				listaFuentes = new Vector<FuenteSuscripcion>();
				for (HashMap<String, String> fuenteHash : listaFuentesVectHash) {
					if ("completa".equals(fuenteHash.get("tipo"))) {
						FuenteSuscripcion fuente = new FuenteSuscripcion(
								Integer.parseInt(fuenteHash.get("id")),
								fuenteHash.get("tipo"), fuenteHash.get("ide"),
								fuenteHash.get("desc"),
								fuenteHash.get("url_alta"),
								fuenteHash.get("url_baja"),
								fuenteHash.get("method"));
						listaFuentes.add(fuente);
						numFuentes++;
					} else if ("simple".equals(fuenteHash.get("tipo"))) {
						FuenteSuscripcion fuente = new FuenteSuscripcion(
								Integer.parseInt(fuenteHash.get("id")),
								fuenteHash.get("tipo"), fuenteHash.get("ide"),
								fuenteHash.get("desc"), fuenteHash.get("url"),
								"", "");
						listaFuentes.add(fuente);
						numFuentes++;
					}
				}
			}
		} catch (Exception e) {
			lastError = "Error obteniendo la listas de fuentes del servidor. Pruebe de nuevo más tarde."
					+ e.getMessage();
			logger.error(
					"Error obteniendo la listas de fuentes del servidor. Pruebe de nuevo más tarde.",
					e);
		}

		if ("".equals (lastError)) return SUCCESS; else return INPUT;
	}

	public Vector<FuenteSuscripcion> getListaFuentes() {
		return listaFuentes;
	}

	public void setListaFuentes(Vector<FuenteSuscripcion> listaFuentes) {
		this.listaFuentes = listaFuentes;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public int getNumFuentes() {
		return numFuentes;
	}

	public void setNumFuentes(int numFuentes) {
		this.numFuentes = numFuentes;
	}

	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}

	public String getFormTipo() {
		return formTipo;
	}

	public void setFormTipo(String formTipo) {
		this.formTipo = formTipo;
	}

	public String getFormUrlAlta() {
		return formUrlAlta;
	}

	public void setFormUrlAlta(String formUrlAlta) {
		this.formUrlAlta = formUrlAlta;
	}

	public String getFormUrlBaja() {
		return formUrlBaja;
	}

	public void setFormUrlBaja(String formUrlBaja) {
		this.formUrlBaja = formUrlBaja;
	}

	public String getFormAction() {
		return formAction;
	}

	public void setFormAction(String formAction) {
		this.formAction = formAction;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}
}
