package es.sadesi.webmovil.action;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.security.InvalidCredentialsException;
import es.sadesi.sms.LimitExceededException;
import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.sms.SyntaxErrorException;
import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.Envio;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilWebmovil;

public class EnviarSMS extends WebmovilAction {

	/**
	 * Auto-generado.
	 */ 
	private static final long serialVersionUID = -6844039497715218612L;

	private String[] tiposDestinatario = { "Nacional", "Internacional" };
	private String[] vigencias = { "1 hora", "12 horas", "1 día", "1 semana" };
	private Envio envio;
	private int smsEnviadosInternacionales;
	private int smsEnviadosNacionales;
	private int restoSmsInternacionales;
	private int restoSmsNacionales;
	private int smsProgramados;
	private int smsEnviadosTotales;
	private int contCaracteres;
	private int contMensajes;
	private int totalNacionales;
	private int totalInternacionales;
	private boolean limiteExcedido = false;
	private String alfa = null;
	private boolean canSetAlfa = false;
	
	private static final Log logger = LogFactory.getLog(EnviarSMS.class);

	public void validate() {
		
		logger.info("Ejecutando acción EnviarSMS");

		setSeccion("SMS");
		String name = ActionContext.getContext().getName();
		if (envio != null && !("enviarSMSNuevo".equals(name))) {
			
			Usuario usuario = getUsuarioRemoto();

			String tipoDestinatario = envio.getTipoDestinatario();
			String destinatario = envio.getDestinatario();
			String idContacto = envio.getIdContacto();
			String idGrupo = envio.getIdGrupo();
			
			logger.debug ("Tipo Destinatario: " + tipoDestinatario + ", destinatario: " + destinatario + ", idContacto: " + idContacto + ", idGrupo: " + idGrupo);
			if ("Nacional".equals(tipoDestinatario)) {
				try {
					//Object[] destino = UtilWebmovil.validaDestinatario(destinatario, idContacto, idGrupo, false, usuario);
					List<String[]> destino = UtilWebmovil.validaDestinatario(destinatario, idContacto, idGrupo, usuario);
					
					for(int i = 0; i < destino.size(); i ++) {
						if (ConstantesWebmovil.TIPO_NUMERO_NACIONAL.equals(destino.get(i)[1]) && tienePermiso(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_SOLO_CORPORATIVO))
							addFieldError("envio.destinatario", "Sólo puede enviar mensajes a móviles corporativos.");
					}

					envio.setDestinatarios((List<String[]>)destino);
				} catch (NumberFormatException nfe) {
					addFieldError("envio.destinatario", nfe.getMessage());
				} catch (Exception e) {
					addFieldError("errorEnvio", e.getMessage());
				}
			} else if ("Internacional".equals(envio.getTipoDestinatario())) {
				try {
					// Object[] destino = UtilWebmovil.validaDestinatario(destinatario, idContacto, idGrupo, true, usuario);
					List<String[]> destino = UtilWebmovil.validaDestinatario(destinatario, idContacto, idGrupo, usuario);					
					
					for(int i = 0; i < destino.size(); i ++) {
						if (ConstantesWebmovil.TIPO_NUMERO_INTERNACIONAL.equals(destino.get(i)[1]) && tienePermiso(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_SOLO_CORPORATIVO))
							addFieldError("envio.destinatario", "Sólo puede enviar mensajes a móviles corporativos.");
					}
					
					envio.setDestinatarios((List<String[]>)destino);
				} catch (NumberFormatException nfe) {
					addFieldError("envio.destinatario", nfe.getMessage());
				} catch (Exception e) {
					addFieldError("errorEnvio", e.getMessage());
				}
			} else
				addFieldError("envio.tipoDestinatario",
						"Tipo de destinatario incorrecto.");
		}
	}

	@Override
	public String execute() throws Exception {

		Usuario usuario = getUsuarioRemoto();
		String destino = "";

		Map<String, Object> resultEnvio = null;
		int numEnviados = 0;
		try {
			//if (UtilWebmovil.esCadenaNumerica(envio.getDestinatario())) {
			List<String[]> destinatarios = envio.getDestinatarios();
			Iterator<String[]> iter = destinatarios.iterator();
			while (iter.hasNext()) {
				destino = iter.next()[0];
				logger.debug("enviando a " + destino);
				logger.debug("tipo: " + Integer.parseInt (envio.getTipo ()));
				int push = Integer.parseInt (envio.getTipo ());
				resultEnvio = getP3sManager().SMSEx(usuario.getUsuario(),
						usuario.getEmisor().getEmisor(),
						destino, envio.getMensaje(),
						(envio.isPrioridadAlta() ? 1 : 0),
						(envio.isAcuseRecibo() ? 1 : 0),
						(envio.isRemitenteAlfanumerico() ? 1 : 0),
						getFechaCaducidad(),
						null,
						push);

				numEnviados ++;
				int codigo = ((Integer) resultEnvio
						.get(P3SManagerImpl.KEY_SMS_STATUS_CODE)).intValue();
				String status = (String) resultEnvio
						.get(P3SManagerImpl.KEY_SMS_STATUS_STRING);
				if (codigo == 0)
					addActionMessage("Su mensaje al " + destino
							+ " ha sido enviado.");
				else
					addActionMessage("El estado del envío de su mensaje al "
							+ destino + " es el siguiente: "
							+ codigo + " - " + status);
			}
			
		} catch (LimitExceededException lee) {
			addFieldError("errorEnvio", "ERROR: Ha alcanzado el límite de mensajes que tiene permitido enviar al mes. Se enviaron " + numEnviados + " mensajes.");
			return INPUT;
		} catch (InvalidCredentialsException ice) {
			addFieldError("errorEnvio", "ERROR: Credenciales incorrectas.");
			return INPUT;
		} catch (SyntaxErrorException see) {
			addFieldError("errorEnvio", "ERROR: El destinatario " + destino + " no tiene instalada la aplicación Avisos Junta.");
			return INPUT;
		} catch (Exception e) {
			addFieldError("errorEnvio", "ERROR GRAVE: No se pudo completar el envío. Ocurrió un error grave en la pasarela. Consulte a su administrador.");
			return INPUT;
		}
		
		//***!!!
		//envio.setDestinatario("");
		return SUCCESS;
	}

	@Override
	public String input() throws Exception {

		setSeccion("SMS");
		Usuario usuario = getUsuarioRemoto();

		logger.debug("calculando estadísticas del usuario");

		smsEnviadosInternacionales = getP3sManager().estadisticasUsuario(
				usuario.getUsuario(), usuario.getEmisor().getEmisor(),
				"enviadosInternacionalesContador", "", "");
		logger.debug("smsEnviadosInternacionales: " + smsEnviadosInternacionales);

		smsEnviadosTotales = getP3sManager().estadisticasUsuario(
				usuario.getUsuario(), usuario.getEmisor().getEmisor(),
				"enviadosTotalesContador", "", "");		
		logger.debug("smsEnviadosTotales: " + smsEnviadosTotales);
		
		smsEnviadosNacionales = smsEnviadosTotales - smsEnviadosInternacionales;
		
		// TODO Cálculo de los mensajes programados.
		smsProgramados = 0;
		// Possibly TODO forever.
		
		restoSmsInternacionales = usuario.getMaxInternacional() - smsEnviadosInternacionales;
		restoSmsNacionales = usuario.getMaxNacional() - smsEnviadosNacionales;
		
		totalNacionales = usuario.getMaxNacional();
		totalInternacionales = usuario.getMaxInternacional();
		
		if (smsEnviadosTotales + smsProgramados >= usuario.getMaxNacional () && usuario.getMaxNacional () != -1)
			limiteExcedido = true;
		
		if (envio == null) {
			envio = new Envio();

			String firmaPersonal = usuario.getFirmaPersonal();
			if (firmaPersonal != null && firmaPersonal.length() > 0) {
				envio.setFirmaPersonal(firmaPersonal);
				envio.setConFirmaPersonal(true);
			}

			envio.setFirmaCorporativa(usuario.getEmisor().getFirma());
		}
		
		alfa = null;
		canSetAlfa = false;
		try {
			String operacion = "obtieneAlfanumericoNew";
			HashMap<String,Object> args = new HashMap<String,Object>();
			args.put("idEmisor", usuario.getEmisor().getEmisor());
			HashMap<String,Object> lecturaBD = null;
			lecturaBD = getP3sManager().generalReadBD(operacion, args);
			if (lecturaBD != null) {
				alfa = (String)lecturaBD.get("resultado");
				canSetAlfa = (alfa != null && !"".equals(alfa));
			}
		} catch (Exception e) {
			logger.warn("Ocurrió una excepción al tratar de obtener el alfanumérico asociado al emisor del usuario", e);
		}
		
		cuentaCaracteres();

		return INPUT;

	}

	public String preview() throws Exception {
		
		setSeccion("SMS");
		
		return SUCCESS;
	}
	
	public String nuevo() throws Exception {
		
		setSeccion("SMS");
		envio = null;

		return input();
	}

	public int getSmsEnviadosInternacionales() {
		return smsEnviadosInternacionales;
	}

	public void setSmsEnviadosInternacionales(int smsEnviadosInternacionales) {
		this.smsEnviadosInternacionales = smsEnviadosInternacionales;
	}

	public int getSmsEnviadosNacionales() {
		return smsEnviadosNacionales;
	}

	public void setSmsEnviadosNacionales(int smsEnviadosNacionales) {
		this.smsEnviadosNacionales = smsEnviadosNacionales;
	}

	public int getSmsProgramados() {
		return smsProgramados;
	}

	public void setSmsProgramados(int smsProgramados) {
		this.smsProgramados = smsProgramados;
	}

	public int getRestoSmsInternacionales() {
		return restoSmsInternacionales;
	}

	public void setRestoSmsInternacionales(int restoSmsInternacionales) {
		this.restoSmsInternacionales = restoSmsInternacionales;
	}

	public int getRestoSmsNacionales() {
		return restoSmsNacionales;
	}

	public void setRestoSmsNacionales(int restoSmsNacionales) {
		this.restoSmsNacionales = restoSmsNacionales;
	}

	public int getSmsEnviadosTotales() {
		return smsEnviadosTotales;
	}

	public void setSmsEnviadosTotales(int smsEnviadosTotales) {
		this.smsEnviadosTotales = smsEnviadosTotales;
	}

	public String[] getTiposDestinatario() {
		return tiposDestinatario;
	}

	public void setTiposDestinatario(String[] tiposDestinatario) {
		this.tiposDestinatario = tiposDestinatario;
	}

	public String[] getVigencias() {
		return vigencias;
	}

	public void setVigencias(String[] vigencias) {
		this.vigencias = vigencias;
	}

	public Envio getEnvio() {
		return envio;
	}

	public void setEnvio(Envio envio) {
		this.envio = envio;
	}

	public int getContCaracteres() {
		return contCaracteres;
	}

	public void setContCaracteres(int contCaracteres) {
		this.contCaracteres = contCaracteres;
	}

	public int getContMensajes() {
		return contMensajes;
	}

	public void setContMensajes(int contMensajes) {
		this.contMensajes = contMensajes;
	}

	public boolean isLimiteExcedido() {
		return limiteExcedido;
	}

	public void setLimiteExcedido(boolean limiteExcedido) {
		this.limiteExcedido = limiteExcedido;
	}

	private String getFechaCaducidad() {
		String fecha = null;
		if (envio != null) {
			GregorianCalendar gc = new GregorianCalendar();

			String vigencia = envio.getVigencia();
			if ("1 hora".equals(vigencia)) {
				gc.add(GregorianCalendar.HOUR, 1);
			} else if ("12 horas".equals(vigencia)) {
				gc.add(GregorianCalendar.HOUR, 12);
			} else if ("1 día".equals(vigencia)) {
				gc.add(GregorianCalendar.DAY_OF_MONTH, 1);
			} else if ("1 semana".equals(vigencia)) {
				gc.add(GregorianCalendar.DAY_OF_MONTH, 7);
			}
			fecha = P3SManager.P3S_DATE_FORMAT.format(gc.getTime());
		}
		return fecha;
	}
	
	private void cuentaCaracteres() {
		int caracteres = 0;
		if (envio.isConFirmaPersonal()) {
			if (envio.getFirmaPersonal() != null)
				caracteres += envio.getFirmaPersonal().length();
		}
		
		if (envio.isConFirmaCorporativa()) {
			if (envio.getFirmaCorporativa() != null)
				caracteres += envio.getFirmaCorporativa().length();
		}
		
		if (envio.getTexto() != null)
			caracteres += envio.getTexto().length();
		
		contMensajes = 1 + (caracteres - 1)/160;
		contCaracteres = 160 - caracteres%160;
	}

	public void setTotalNacionales(int totalNacionales) {
		this.totalNacionales = totalNacionales;
	}

	public int getTotalNacionales() {
		return totalNacionales;
	}

	public void setTotalInternacionales(int totalInternacionales) {
		this.totalInternacionales = totalInternacionales;
	}

	public int getTotalInternacionales() {
		return totalInternacionales;
	}

	public String getAlfa() {
		return alfa;
	}

	public void setAlfa(String alfa) {
		this.alfa = alfa;
	}

	public boolean isCanSetAlfa() {
		return canSetAlfa;
	}

	public void setCanSetAlfa(boolean canSetAlfa) {
		this.canSetAlfa = canSetAlfa;
	}

	/*
	 * private int obtenerProgramados(String usuario) { // Variable que controla
	 * el número de teléfonos $nums_tlfn = 0; // Obtener los envíos programados
	 * en la cola $path_queu = WEBMOVIL_HOME.RAW_PATH_QUEU;
	 * $envios=listFiles2($path_queu,$id_usuario,$AllFiles=array());
	 * 
	 * // Obtener los nombres de los ficheros programados foreach ($envios as
	 * $envio){ $archivo = file($path_queu.$envio);
	 * 
	 * if($archivo){ $nombre_archivo = trim($archivo[2],"\n"); $ruta =
	 * WEBMOVIL_HOME.RAW_PATH_USERS.$id_usuario."/dest/".$nombre_archivo;
	 * $nums_tlfn += lineas_fichero($ruta); } } return $nums_tlfn; }
	 */
}
