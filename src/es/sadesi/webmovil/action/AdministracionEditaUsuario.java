package es.sadesi.webmovil.action;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SManagerImpl;

public class AdministracionEditaUsuario extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionEditaUsuario.class);
	
	private String idUsuario = "";
	private String idEmisor = "";
	private String formUsuario;
	private String formLimite;
	private String formLimiteBlando;
	private String formLimiteInternacional;
	private String formLimiteInternacionalBlando;
	private String formTlfnoMovil;
	private String formNivel;
	
	private boolean formPerPrioridad;
	private boolean formPerRecibo;
	private boolean formPerSolocorp;
	private boolean formPerFirma;
	private boolean formPerRecepcion;
	private boolean formPerMasivo;
	
	private String accion;
	private int numPagina;
	
	private String mensajeEstado = "";
	
	public void validate() {
		setSeccion("administracion");
		
		if ("Enviar".equals(accion)) {
			// Campos requeridos
			if (formLimite == null || "".equals(formLimite) || 
				formLimiteBlando == null || "".equals(formLimiteBlando) ||
				formLimiteInternacional == null || "".equals(formLimiteInternacional) ||
				formLimiteInternacionalBlando == null || "".equals(formLimiteInternacionalBlando)) {
				addFieldError ("editaUsuario.limites", "Debe rellenar correctamente todos los límites de mensajes.");
			}
			
			// Limitaciones de rango 
			try {
				Integer.parseInt(formLimite);
				Integer.parseInt(formLimiteBlando);
				Integer.parseInt(formLimiteInternacional);
				Integer.parseInt(formLimiteInternacionalBlando);
			} catch (Exception e) {
				addFieldError ("editaUsuario.limites", "Valores incorrectos. Debe introducir un valor menor que 2147483648");
			}
			
			if (formUsuario == null || "".equals(formUsuario)) {
				addFieldError ("editaUsuario.general", "Debe especificar un nombre de usuario LDAP válido");
			}
		}
	}
	
	public String execute() throws Exception {
		
		setSeccion("administracion");
		
		logger.info("Ejecutando acción AdministracionEditaUsuario");
		
		if (null == formTlfnoMovil)
			formTlfnoMovil = "34600123456";
		
		// Acciones: Actualizar
		if ("Enviar".equals(accion)) {
			// Precalculamos todo esto:
			String per_prioridad = formPerPrioridad ? "S" : "N";
			String per_recibo = formPerRecibo ? "S" : "N";
			String per_solocorp = formPerSolocorp ? "S" : "N";
			String per_firma = formPerFirma ? "S" : "N";
			String per_recepcion = formPerRecepcion ? "S" : "N";
			String per_masivo = formPerMasivo ? "S" : "N";
			
			// Vemos si es edición o creación
			if ("NUEVO".equals(idUsuario) || ("".equals(idUsuario))) {
				// Crear un usuario nuevo con los valores del formulario
				
				try {
					getP3sManager().creaUsuario(formUsuario.trim(), idEmisor, 
							formLimite.trim(), formLimiteBlando.trim(), formLimiteInternacional.trim(), formLimiteInternacionalBlando.trim(), 
							formTlfnoMovil.trim(), formNivel, per_prioridad, per_recibo, per_solocorp, per_firma, per_recepcion, per_masivo);
				} catch (Exception e) {
					String mensajeDeError = e.toString (); 
					mensajeEstado = "Ocurrió un error al crear el usuario " + formUsuario + " en la base de datos." + mensajeDeError.substring(mensajeDeError.indexOf("Exception") + 10);
					logger.error (mensajeEstado);
					return INPUT;
				}
				
				return "usuarioCreado";
			} else {
				// Editar el usuario idUsuario con los valores del formulario.
				// Hay que llamar a modificaAtributoUsuario con cada valor.
				try {
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "limite", formLimite.trim());
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "limite_blando", formLimiteBlando.trim());
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "limite_internacional", formLimiteInternacional.trim());
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "limite_internacional_blando", formLimiteInternacionalBlando.trim());
					
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "tlfno_movil", formTlfnoMovil.trim());
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "nivel", formNivel.trim());
					
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "per_prioridad", per_prioridad);
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "per_recibo", per_recibo);
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "per_solocorp", per_solocorp);
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "per_firma", per_firma);
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "per_recepcion", per_recepcion);
					getP3sManager().modificaAtributoUsuario(formUsuario, idEmisor, "per_masivo", per_masivo);
	
					mensajeEstado = "Los atributos del usuario " + formUsuario + " fueron editados correctamente";
					return INPUT;
				} catch (Exception e) {
					String mensajeDeError = e.toString ();
					mensajeEstado = "Ocurrió un error al editar un atributo del usuario " + formUsuario + " en la base de datos. " + mensajeDeError.substring(mensajeDeError.indexOf("Exception") + 10);
					logger.error (mensajeEstado);
				}
			}
		}
		
		// Mostrar: Si idUsuario != "NUEVO", tenemos que leer los
		// datos del WS y poblar los controles del formulario.
		if (!("NUEVO".equals(idUsuario)) && !("".equals(idUsuario))) {
			try {
				Map <String,Object> datosUsuario = getP3sManager().infoUsuarioEstaticos(formUsuario, idEmisor);
				formLimite = ((String) "" + datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_MAXIMO_NACIONALES)).trim(); 
				formLimiteBlando = ((String) "" + datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_MAXIMO_NACIONALES_BLANDO)).trim();
				formLimiteInternacional = ((String) "" + datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_MAXIMO_INTERNACIONALES)).trim();
				formLimiteInternacionalBlando = ((String) "" + datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_MAXIMO_INTERNACIONALES_BLANDO)).trim();
				formTlfnoMovil = ((String) datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_TLF_MOVIL));
				if (formTlfnoMovil != null) formTlfnoMovil = formTlfnoMovil.trim(); else formTlfnoMovil = "";
				formNivel = ((String) datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_NIVEL));
				if (formNivel != null) formNivel = formNivel.trim(); else formNivel = "";
				
				formPerPrioridad = (Boolean) ("S".equals((String)datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_PRIORIDAD)));
				formPerRecibo = (Boolean) ("S".equals((String)datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_RECIBO)));
				formPerSolocorp = (Boolean) ("S".equals((String)datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_SOLO_CORPORATIVO)));
				formPerFirma = (Boolean) ("S".equals((String)datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_FIRMA)));
				formPerRecepcion = (Boolean) ("S".equals((String)datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_RECEPCION)));
				formPerMasivo = (Boolean) ("S".equals((String)datosUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_PERMISO_MASIVO)));
			} catch (Exception e) {
				String mensajeDeError = e.toString ();
				mensajeEstado = "Ocurrió un error inesperado al intentar obtener los datos del usuario. " +
						"Por favor, inténtelo de nuevo pasados unos minutos. " +
						"Si el problema persiste, reporte el problema indicando el emisor y el usuario implicados y el siguiente texto: " + mensajeDeError;
				logger.error (mensajeEstado);
			}
		}

		return SUCCESS;
	}

	public String getFormLimite() {
		return formLimite;
	}

	public void setFormLimite(String formLimite) {
		this.formLimite = formLimite;
	}

	public String getFormLimiteBlando() {
		return formLimiteBlando;
	}

	public void setFormLimiteBlando(String formLimiteBlando) {
		this.formLimiteBlando = formLimiteBlando;
	}

	public String getFormLimiteInternacional() {
		return formLimiteInternacional;
	}

	public void setFormLimiteInternacional(String formLimiteInternacional) {
		this.formLimiteInternacional = formLimiteInternacional;
	}

	public String getFormLimiteInternacionalBlando() {
		return formLimiteInternacionalBlando;
	}

	public void setFormLimiteInternacionalBlando(
			String formLimiteInternacionalBlando) {
		this.formLimiteInternacionalBlando = formLimiteInternacionalBlando;
	}

	public String getFormTlfnoMovil() {
		return formTlfnoMovil;
	}

	public void setFormTlfnoMovil(String formTlfnoMovil) {
		this.formTlfnoMovil = formTlfnoMovil;
	}

	public String getFormNivel() {
		return formNivel;
	}

	public void setFormNivel(String formNivel) {
		this.formNivel = formNivel;
	}

	public boolean getFormPerPrioridad() {
		return formPerPrioridad;
	}

	public void setFormPerPrioridad(boolean formPerPrioridad) {
		this.formPerPrioridad = formPerPrioridad;
	}

	public boolean getFormPerRecibo() {
		return formPerRecibo;
	}

	public void setFormPerRecibo(boolean formPerRecibo) {
		this.formPerRecibo = formPerRecibo;
	}

	public boolean getFormPerSolocorp() {
		return formPerSolocorp;
	}

	public void setFormPerSolocorp(boolean formPerSolocorp) {
		this.formPerSolocorp = formPerSolocorp;
	}

	public boolean getFormPerFirma() {
		return formPerFirma;
	}

	public void setFormPerFirma(boolean formPerFirma) {
		this.formPerFirma = formPerFirma;
	}

	public boolean getFormPerRecepcion() {
		return formPerRecepcion;
	}

	public void setFormPerRecepcion(boolean formPerRecepcion) {
		this.formPerRecepcion = formPerRecepcion;
	}

	public boolean getFormPerMasivo() {
		return formPerMasivo;
	}

	public void setFormPerMasivo(boolean formPerMasivo) {
		this.formPerMasivo = formPerMasivo;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public int getNumPagina() {
		return numPagina;
	}
	
	public String getFormUsuario() {
		return formUsuario;
	}

	public void setFormUsuario(String formUsuario) {
		this.formUsuario = formUsuario;
	}

	public void setMensajeEstado(String mensajeEstado) {
		this.mensajeEstado = mensajeEstado;
	}

	public String getMensajeEstado() {
		return mensajeEstado;
	}
}
