package es.sadesi.webmovil.action;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.FacturacionEmisor;
import es.sadesi.webmovil.model.Usuario;

public class AdministracionFacturacionResultados extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionFacturacionResultados.class);
	
	private String factMonth = "";
	private String factYear = "";
	private String showPeriod = "";
	private Vector<FacturacionEmisor> facturacionStore = null;
	private String trimestreMes1 = "";
	private String trimestreMes2 = "";
	private String trimestreMes3 = "";
	private String tipoVista = "";
	
	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		logger.info("Ejecutando acción AdministracionFacturacionResultados");
		
		setSeccion("administracion");
		
		// Obtenemos el usuario de la sesión
		
		Usuario user = getUsuarioRemoto();
		String idEmisor = user.getEmisor().getEmisor();
		
		// Construimos el texto showPeriod para mostrar en la vista.
		
		String [] meses = {	"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
				"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
		int mes1 = 0, mes2 = 0, mes3 = 0, anyo1 = 0, anyo2 = 0, anyo3 = 0;
		
		if ("0".equals(factMonth) || "0".equals(factYear)) {
			showPeriod = "Datos del trimestre actual";
			tipoVista = "trimestral";
			
			// Calculamos los tres meses para mostrar.
			
			Calendar cal = Calendar.getInstance ();
			mes3 = 1 + cal.get(Calendar.MONTH);
			anyo3 = cal.get(Calendar.YEAR);
			mes2 = mes3 - 1;
			anyo2 = anyo3;
			if (mes2 == 0) {
				mes2 = 12;
				anyo2 --;
			}
			mes1 = mes2 - 1;
			anyo1 = anyo2;
			if (mes1 == 0) {
				mes1 = 12;
				anyo1 --;
			}
			trimestreMes1 = meses[mes1 - 1];
			trimestreMes2 = meses[mes2 - 1];
			trimestreMes3 = meses[mes3 - 1];
		} else {
			showPeriod = "Datos de " + meses[Integer.parseInt(factMonth) - 1] + " de " + factYear;
			tipoVista = "mensual";
		}
		
		// Comprobamos si en la sesión hay almacenado un período y si este coincide
		// con el que se pide. En ese caso, obtenemos los datos de la sesión. En otro
		// caso, habrá que pedirlos a Webservice (y sentarse a esperar).
		
		Map<String,Object> session = (Map<String,Object>) ActionContext.getContext().get("session");
		
		String monthInSession = (String) session.get (ConstantesWebmovil.ADMINISTRACION_FACTURACION_PERIODO_ALMACENADO_MES);
		String yearInSession = (String) session.get (ConstantesWebmovil.ADMINISTRACION_FACTURACION_PERIODO_ALMACENADO_ANYO);
		
		if (monthInSession == null || "".equals(monthInSession) || !factMonth.equals(monthInSession) ||
			yearInSession == null || "".equals(yearInSession) || !factYear.equals(yearInSession)) {
			// Calcular
			
			// Primero obtenemos una lista de emisores del webservice
			Vector<HashMap<String,String>> listaEmisoresTemp = getP3sManager().listaEmisores (idEmisor);
			
			// Actuamos diferentemente si necesitamos mostrar la información de un mes o la de tres
			if ("0".equals(factMonth) || "0".equals(factYear)) {
				Calendar cal = Calendar.getInstance ();
				String desde, hasta;
				String myMonth;
				int ultimoDiaDelMes;
				String ultimoDiaDelMesS;
				facturacionStore = new Vector<FacturacionEmisor> ();
				
				// Trimestre. Pedir tres períodos
				
				for (int i = 0; i < listaEmisoresTemp.size (); i ++) {
					FacturacionEmisor facturacionEmisor = new FacturacionEmisor ();
					
					// Primer mes
					myMonth = mes1 < 10 ? "0" + mes1 : "" + mes1;
					desde = anyo1 + myMonth + "01000000";
					cal.set(Calendar.MONTH, mes1 - 1);
					cal.set(Calendar.YEAR, anyo1);
					ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
					hasta = anyo1 + myMonth + ultimoDiaDelMesS + "235959";
					
					logger.debug ("[" + desde + ":" + hasta + "]");
					
					String actualEmisor = "";
					
					try {
						actualEmisor = (String) listaEmisoresTemp.get (i).get(P3SManagerImpl.KEY_INFO_EMISOR_ID);
						Map <String,Object> datosEmisor = getP3sManager().infoEmisor(actualEmisor);
						String descripcion = (String) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_DESCRIPCION);
						String navision = (String) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_NAVISION);
						if (descripcion == null || "".equals (descripcion))
							descripcion = (String) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ID);
						facturacionEmisor.setIdEmisor(descripcion);
						facturacionEmisor.setNavision(navision);
						
						//int enviadosTotales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS));
						//int enviadosInternacionales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_INTERNACIONALES));
						//int enviadosCorporativos = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_CORPORATIVOS));
						
						int enviadosTotales = getP3sManager().estadisticasEmisor(actualEmisor , "enviados", desde, hasta);
						int enviadosInternacionales = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosInternacionales", desde, hasta);
						int enviadosCorporativos = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosCorporativos", desde, hasta);
						
						facturacionEmisor.setEnviadosCorporativos1(enviadosCorporativos);
						facturacionEmisor.setEnviadosInternacionales1(enviadosInternacionales);
						facturacionEmisor.setEnviadosNoCorporativos1(enviadosTotales - enviadosCorporativos - enviadosInternacionales);
						
						logger.debug (">>" + facturacionEmisor.getIdEmisor() + " C:" + facturacionEmisor.getEnviadosCorporativos1() + " NC:" + facturacionEmisor.getEnviadosNoCorporativos1() + " I:" + facturacionEmisor.getEnviadosInternacionales1());
					} catch (Exception e) {}	

					// Segundo mes
					myMonth = mes2 < 10 ? "0" + mes2 : "" + mes2;
					desde = anyo2 + myMonth + "01000000";
					cal.set(Calendar.MONTH, mes2 - 1);
					cal.set(Calendar.YEAR, anyo2);
					ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
					hasta = anyo2 + myMonth + ultimoDiaDelMesS + "235959";
				
					logger.debug ("[" + desde + ":" + hasta + "]");
				
					try {
						//Map <String,Object> datosEmisor = getP3sManager().infoEmisor((String) listaEmisoresTemp.get (i).get(P3SManagerImpl.KEY_INFO_EMISOR_ID), desde, hasta);
						
						//int enviadosTotales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS));
						//int enviadosInternacionales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_INTERNACIONALES));
						//int enviadosCorporativos = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_CORPORATIVOS));
						
						int enviadosTotales = getP3sManager().estadisticasEmisor(actualEmisor , "enviados", desde, hasta);
						int enviadosInternacionales = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosInternacionales", desde, hasta);
						int enviadosCorporativos = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosCorporativos", desde, hasta);
						
						facturacionEmisor.setEnviadosCorporativos2(enviadosCorporativos);
						facturacionEmisor.setEnviadosInternacionales2(enviadosInternacionales);
						facturacionEmisor.setEnviadosNoCorporativos2(enviadosTotales - enviadosCorporativos - enviadosInternacionales);
						
						logger.debug (">>" + facturacionEmisor.getIdEmisor() + " C:" + facturacionEmisor.getEnviadosCorporativos2() + " NC:" + facturacionEmisor.getEnviadosNoCorporativos2() + " I:" + facturacionEmisor.getEnviadosInternacionales1());
					} catch (Exception e) {}
				
					// Tercer mes
					myMonth = mes3 < 10 ? "0" + mes3 : "" + mes3;
					desde = anyo3 + myMonth + "01000000";
					cal.set(Calendar.MONTH, mes3 - 1);
					cal.set(Calendar.YEAR, anyo3);
					ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
					ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
					hasta = anyo3 + myMonth + ultimoDiaDelMesS + "235959";
					
					logger.debug ("[" + desde + ":" + hasta + "]");
				
					try {
						//Map <String,Object> datosEmisor = getP3sManager().infoEmisor((String) listaEmisoresTemp.get (i).get(P3SManagerImpl.KEY_INFO_EMISOR_ID), desde, hasta);
						
						//int enviadosTotales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS));
						//int enviadosInternacionales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_INTERNACIONALES));
						//int enviadosCorporativos = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_CORPORATIVOS));
						
						int enviadosTotales = getP3sManager().estadisticasEmisor(actualEmisor , "enviados", desde, hasta);
						int enviadosInternacionales = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosInternacionales", desde, hasta);
						int enviadosCorporativos = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosCorporativos", desde, hasta);
						
						facturacionEmisor.setEnviadosCorporativos3(enviadosCorporativos);
						facturacionEmisor.setEnviadosInternacionales3(enviadosInternacionales);
						facturacionEmisor.setEnviadosNoCorporativos3(enviadosTotales - enviadosCorporativos - enviadosInternacionales);
						
						logger.debug (">>" + facturacionEmisor.getIdEmisor() + " C:" + facturacionEmisor.getEnviadosCorporativos3() + " NC:" + facturacionEmisor.getEnviadosNoCorporativos3() + " I:" + facturacionEmisor.getEnviadosInternacionales3());
						
					} catch (Exception e) {}
					
					facturacionStore.add (facturacionEmisor);
				}
			} else {
				String desde = factYear + factMonth + "01000000";
				Calendar cal = Calendar.getInstance ();
				cal.set(Calendar.MONTH, Integer.parseInt(factMonth) - 1);
				cal.set(Calendar.YEAR, Integer.parseInt(factYear));
				int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				String ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
				String hasta = factYear + factMonth + ultimoDiaDelMesS + "235959";
				
				// Mes. Pedir mes actual
				facturacionStore = new Vector<FacturacionEmisor> ();
				for (int i = 0; i < listaEmisoresTemp.size (); i ++) {
					try {
						String actualEmisor = (String) listaEmisoresTemp.get (i).get(P3SManagerImpl.KEY_INFO_EMISOR_ID);
						Map <String,Object> datosEmisor = getP3sManager().infoEmisor(actualEmisor);
						FacturacionEmisor facturacionEmisor = new FacturacionEmisor ();
						String descripcion = (String) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_DESCRIPCION);
						String navision = (String) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_NAVISION);
						if (descripcion == null || "".equals (descripcion))
							descripcion = (String) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ID);
						facturacionEmisor.setIdEmisor(descripcion);
						facturacionEmisor.setNavision(navision);
						
						//int enviadosTotales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS));
						//int enviadosInternacionales = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_INTERNACIONALES));
						//int enviadosCorporativos = Integer.parseInt((String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_CORPORATIVOS));
						
						int enviadosTotales = getP3sManager().estadisticasEmisor(actualEmisor , "enviados", desde, hasta);
						int enviadosInternacionales = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosInternacionales", desde, hasta);
						int enviadosCorporativos = getP3sManager().estadisticasEmisor(actualEmisor , "enviadosCorporativos", desde, hasta);
						
						facturacionEmisor.setEnviadosCorporativos1(enviadosCorporativos);
						facturacionEmisor.setEnviadosInternacionales1(enviadosInternacionales);
						facturacionEmisor.setEnviadosNoCorporativos1(enviadosTotales - enviadosCorporativos - enviadosInternacionales);
						
						logger.debug (">>" + facturacionEmisor.getIdEmisor() + " T:" + enviadosTotales + " C:" + facturacionEmisor.getEnviadosCorporativos1() + " NC:" + facturacionEmisor.getEnviadosNoCorporativos1() + " I:" + facturacionEmisor.getEnviadosInternacionales1());
						
						// Añadimos al store
						facturacionStore.add (facturacionEmisor);
					} catch (Exception e) {}
				}
			}
			
			// Guardamos todo en la sesión
			session.put (ConstantesWebmovil.ADMINISTRACION_FACTURACION_PERIODO_ALMACENADO_MES, factMonth);
			session.put (ConstantesWebmovil.ADMINISTRACION_FACTURACION_PERIODO_ALMACENADO_ANYO, factYear);
			session.put (ConstantesWebmovil.ADMINISTRACION_FACTURACION_RESULTADOS_ALMACENADOS, facturacionStore);
		} else {
			// Obtener de la sesión
			facturacionStore = (Vector<FacturacionEmisor>) session.get (ConstantesWebmovil.ADMINISTRACION_FACTURACION_RESULTADOS_ALMACENADOS);
		}	
		// Hecho.

		return SUCCESS;
	}

	public void setFactMonth(String factMonth) {
		this.factMonth = factMonth;
	}

	public String getFactMonth() {
		return factMonth;
	}

	public void setFactYear(String factYear) {
		this.factYear = factYear;
	}

	public String getFactYear() {
		return factYear;
	}

	public void setShowPeriod(String showPeriod) {
		this.showPeriod = showPeriod;
	}

	public String getShowPeriod() {
		return showPeriod;
	}

	public void setFacturacionStore(Vector<FacturacionEmisor> facturacionStore) {
		this.facturacionStore = facturacionStore;
	}

	public Vector<FacturacionEmisor> getFacturacionStore() {
		return facturacionStore;
	}

	public String getTrimestreMes1() {
		return trimestreMes1;
	}

	public void setTrimestreMes1(String trimestreMes1) {
		this.trimestreMes1 = trimestreMes1;
	}

	public String getTrimestreMes2() {
		return trimestreMes2;
	}

	public void setTrimestreMes2(String trimestreMes2) {
		this.trimestreMes2 = trimestreMes2;
	}

	public String getTrimestreMes3() {
		return trimestreMes3;
	}

	public void setTrimestreMes3(String trimestreMes3) {
		this.trimestreMes3 = trimestreMes3;
	}

	public void setTipoVista(String tipoVista) {
		this.tipoVista = tipoVista;
	}

	public String getTipoVista() {
		return tipoVista;
	}
}
