package es.sadesi.webmovil.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.model.UsuarioEstadistico;

public class AdministracionEstadisticasEmisor extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionEstadisticasEmisor.class);
	
	private String accion;
	private int numPaginaEmisor;
	private String idEmisor;
	
	// Estadísticas del emisor
	private String limiteMensual = "";
	private String mensajesEnviados = "";
	private String mensajesRecibidos = "";
	private String mensajesEnviadosCorporativos = "";
	private String mensajesPendientes = "";
	private String mensajesError = "";
	private String mensajesAcuseRecibo = "";
	private String mensajesPrioridadAlta = "";
	private Vector<UsuarioEstadistico> usuariosMasActivosStore = null;
	
	private String queIntervalo = null;
	private Map<String,String> intervalos;

	private String mesN, anyoN;
	
	private static final String [] mesNames = {
		"Enero", "Febrero", "Marzo", "Abril",
		"Mayo", "Junio", "Julio", "Agosto",
		"Septiembre", "Octubre", "Noviembre", "Diciembre"
	};
	
	private String title = null;
	
	private List<String> anios;
	
	private String mes;
	
	private String anio;

	public String execute() throws Exception {    
		setSeccion("administracion");
		String resultado = SUCCESS;
		
		logger.info("Ejecutando acción AdministracionEstadisticasEmisor");
				
		if (queIntervalo == null || "".equals (queIntervalo)) {
			queIntervalo = "actual";
			title = null;
		} else {
			String anyoDesdeS = "", mesDesdeS = "", anyoHastaS = "", mesHastaS = "", ultimoDiaDelMesS = "";
			
			// Calculamos desde y hasta en base al mes actual (primer y último días del mes actual)
			Calendar cal = Calendar.getInstance ();
			
			if (queIntervalo.equals ("actual")) {
				// Mes actual
				int mesActual = 1 + cal.get(Calendar.MONTH);
				mesN = mesNames [cal.get(Calendar.MONTH)];
				int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				mesDesdeS = mesActual < 10 ? "0" + mesActual : "" + mesActual;
				mesHastaS = mesDesdeS;
				ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
				anyoDesdeS = "" + cal.get(Calendar.YEAR); anyoHastaS = anyoDesdeS;
				anyoN = anyoDesdeS; 
				
				if (queIntervalo.equals ("actual"))
					title = "durante el mes actual (" + mesN + " de " + anyoN + ")";
				else 
					title = "durante el mes anterior (" + mesN + " de " + anyoN + ")";
			} else if (queIntervalo.equals ("anterior")) {
				// Mes anterior
				cal.add (Calendar.MONTH, -1);
				int mesDesde = 1 + cal.get(Calendar.MONTH);
				mesN = mesNames [cal.get(Calendar.MONTH)];
				int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				mesDesdeS = mesDesde < 10 ? "0" + mesDesde : "" + mesDesde;
				mesHastaS = mesDesdeS;
				ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
				anyoDesdeS = "" + cal.get(Calendar.YEAR); anyoHastaS = anyoDesdeS;
				anyoN = anyoDesdeS; 
				
				if (queIntervalo.equals ("actual"))
					title = "durante el mes actual (" + mesN + " de " + anyoN + ")";
				else 
					title = "durante el mes anterior (" + mesN + " de " + anyoN + ")";
			} else {
				// Intervalo de 3 meses anteriores
				cal.add (Calendar.MONTH, -3);
				int mesDesde = 1 + cal.get(Calendar.MONTH);
				mesN = mesNames [cal.get(Calendar.MONTH)];
				mesDesdeS = mesDesde < 10 ? "0" + mesDesde : "" + mesDesde;
				anyoDesdeS = "" + cal.get(Calendar.YEAR); 
				anyoN = anyoDesdeS;
				title = "durante los tres meses anteriores (" + mesN + " de " + anyoN + " a ";
							
				cal.add (Calendar.MONTH, 2);
				int mesHasta = 1 + cal.get(Calendar.MONTH);
				mesN = mesNames [cal.get(Calendar.MONTH)];
				mesHastaS = mesHasta < 10 ? "0" + mesHasta : "" + mesHasta;
				anyoHastaS = "" + cal.get(Calendar.YEAR);
				anyoN = anyoHastaS;
				int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
				ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
				
				title = title + mesN + " de " + anyoN + ")";
			}
			
			String desde = anyoDesdeS + mesDesdeS + "01000000";
			String hasta = anyoHastaS + mesHastaS + ultimoDiaDelMesS + "235959";
			logger.info ("Desde: " + desde + ", Hasta: " + hasta);
			
			if (queIntervalo.equals ("actual")) {
				// Poblamos nuestro store
				//mensajesEnviados = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS);
				mensajesEnviados = (String) "" + getP3sManager().estadisticasEmisor(idEmisor, "enviados", desde, hasta);
				logger.debug("Mensajes Enviados: " + mensajesEnviados);
				//mensajesRecibidos = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_RECIBIDOS);
				mensajesRecibidos = (String) "" + getP3sManager().estadisticasEmisor(idEmisor, "recibidos", desde, hasta);
				logger.debug("Mensajes Recibidos: " + mensajesRecibidos);
				//mensajesEnviadosCorporativos = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ENVIADOS_CORPORATIVOS);
				mensajesEnviadosCorporativos = (String) "" + getP3sManager().estadisticasEmisor(idEmisor, "enviadosCorporativos", desde, hasta);
				logger.debug("Mensajes Enviados Corporativos:" + mensajesEnviadosCorporativos);
				//mensajesPendientes = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_PENDIENTES);
				mensajesPendientes = (String) "" + getP3sManager().estadisticasEmisor(idEmisor, "pendientes", desde, hasta);
				logger.debug("Mensajes Pendientes:" + mensajesPendientes);
				//mensajesError = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ERROR);
				mensajesError = (String) "" + getP3sManager().estadisticasEmisor(idEmisor, "error", desde, hasta);
				logger.debug("Mensajes Error:" + mensajesError);
				//mensajesAcuseRecibo = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_ACUSE);
				mensajesAcuseRecibo = (String) "" + getP3sManager().estadisticasEmisor(idEmisor, "acuse", desde, hasta);
				logger.debug("Mensajes Acuse Recibo:" + mensajesAcuseRecibo);
				//mensajesPrioridadAlta = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_PRIORIDAD_ALTA);
				mensajesPrioridadAlta = (String) "" + getP3sManager().estadisticasEmisor(idEmisor, "prioridadAlta", desde, hasta);
				logger.debug("Mensajes Prioridad Alta:" + mensajesPrioridadAlta);
							
				//Vector<HashMap<String,Object>> usuariosMasActivos = (Vector<HashMap<String,Object>>) datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS);
				Vector<HashMap<String,Object>> usuariosMasActivos = null;
				try {
					// OJO! desde y hasta se ignoran. Los usuarios más activos sólo se calculan para el mes en curso.
					usuariosMasActivos = (Vector<HashMap<String,Object>>) getP3sManager ().usuariosMasActivosEmisor(idEmisor, desde, hasta);
				} catch (Exception e) {
					addActionError("Error al obtener las estad\u00edsticas de usuarios con m\u00e1s env\u00edos");
					logger.warn("Ocurrió un timeout al calcular los usuarios más activos para el emisor " + idEmisor);
				}
				
				usuariosMasActivosStore = new Vector<UsuarioEstadistico> ();
				if (usuariosMasActivos != null) {
					for (int i = 0; i < usuariosMasActivos.size (); i ++) {
						UsuarioEstadistico usuarioEstadistico = new UsuarioEstadistico ();
						HashMap<String,Object> usuarioSimple = (HashMap<String,Object>)usuariosMasActivos.get (i);
						//logger.info ("[" + usuarioSimple.get(P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO) + "]");
						if (usuarioSimple.get(P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO) != null && !"null".equals (P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO)) {
							usuarioEstadistico.setNumEnviados((String) "" + usuarioSimple.get(P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_NUM_MENSAJES));
							usuarioEstadistico.setUsuario((String) "" + usuarioSimple.get(P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO));
							usuariosMasActivosStore.add (usuarioEstadistico);
						}
					}
				}
			} else {
				resultado = calcularEstadisticas(desde, hasta);
			}
		}
		
		return resultado;
	}
	
	public String consultarFecha() { 
		setSeccion("administracion");
		String resultado = SUCCESS;
		
		logger.info("Metodo consultarFecha");
		logger.info("Mes: " + mes + ". Año: " + anio);
		
		if(StringUtils.isEmpty(mes) || StringUtils.isEmpty(anio)) {
			addActionError("Debe seleccionar el mes y el a\u00f1o para realizar la consulta por fechas");
			resultado = INPUT;
		} else {
			int mesInt = 0;
			try {
				mesInt = Integer.parseInt (mes);
			} catch (Exception e) {
				addActionError ("mes no válido");
				return INPUT;
			}
			
			final Calendar cal = Calendar.getInstance ();
			cal.set (Calendar.MONTH, mesInt - 1);
			
			final String fechaDesde = anio + mes + "01000000";
			final String fechaHasta = anio + mes + cal.getActualMaximum(Calendar.DAY_OF_MONTH) + "235959";
			
			logger.info ("Intervalo: " + fechaDesde + " - " + fechaHasta);
			
			resultado = calcularEstadisticas(fechaDesde, fechaHasta);
						
			// titulo a mostrar en los resultados de las estadisticas
			title = "durante el mes de " + mesNames [Integer.valueOf(mes) - 1] + " de " + anio;
		}

		return resultado;
	}

	public Map<String, String> getIntervalos() {
		// Populate "intervalos" 
		String mes, anyo, bs;
		Calendar calI = Calendar.getInstance ();
		intervalos = new HashMap<String,String> ();
		mes = mesNames [calI.get (Calendar.MONTH)]; anyo = "" + calI.get (Calendar.YEAR);
		intervalos.put ("actual", "Mes en curso (" + mes + " de " + anyo + ")");
		calI.add (Calendar.MONTH, -1);
		mes = mesNames [calI.get (Calendar.MONTH)]; anyo = "" + calI.get (Calendar.YEAR);
		intervalos.put ("anterior", "Mes anterior (" + mes + " de " + anyo + ")");
		//calI.add (Calendar.MONTH, -1); 
		mes = mesNames [calI.get (Calendar.MONTH)]; anyo = "" + calI.get (Calendar.YEAR);
		bs = mes + " de " + anyo;
		calI.add (Calendar.MONTH, -2);
		mes = mesNames [calI.get (Calendar.MONTH)]; anyo = "" + calI.get (Calendar.YEAR);
		bs = mes + " de " + anyo + " a " + bs;
		intervalos.put ("3anteriores", "Meses anteriores (" + bs + ")");
				
		return intervalos;
	}
	
	public List<String> getAnios() {
		final Calendar calendario = Calendar.getInstance ();
		anios = new ArrayList<String>();
		
		// Se aniaden los anios de 2010 al actual
		for(int i = 2017; i <= calendario.get (Calendar.YEAR); i++) {
			anios.add (String.valueOf(i));
		}
				
		return anios;
	}
	
	private String calcularEstadisticas (final String fechaDesde, final String fechaHasta) {
		String resultado = SUCCESS;
		
		try {
			final Map<String, Object> mapaEstadisticas = getP3sManager().obtenerEstadisticasAgrupadasPorEmisor(idEmisor, fechaDesde, fechaHasta);
			if(mapaEstadisticas == null) {
				mensajesEnviados = "0";
				mensajesRecibidos = "0";
				mensajesEnviadosCorporativos = "0";
				mensajesPendientes = "0";
				mensajesError = "0";
				mensajesAcuseRecibo = "0";
				mensajesPrioridadAlta = "0";
			} else {
				mensajesEnviados = String.valueOf(mapaEstadisticas.get("n_mensajes_enviados"));
				mensajesRecibidos = String.valueOf(mapaEstadisticas.get("n_mensajes_recibidos"));
				mensajesEnviadosCorporativos = String.valueOf(mapaEstadisticas.get("n_mensajes_corp_enviados"));
				mensajesPendientes = String.valueOf(mapaEstadisticas.get("n_mensajes_pendientes"));
				mensajesError = String.valueOf(mapaEstadisticas.get("n_mensajes_error"));
				mensajesAcuseRecibo = String.valueOf(mapaEstadisticas.get("n_mensajes_acuse_recibo"));
				mensajesPrioridadAlta = String.valueOf(mapaEstadisticas.get("n_mensajes_prioridad_alta"));
			}
		} catch (Exception e) {
			LOG.error("Error al obtener las estad\u00edsticas", e);
			addActionError("Error al obtener las estad\u00edsticas");
			resultado = ERROR;
		}
		
		Vector<Map<String,Object>> estadisticasUsuarios = null;
		try {				
			estadisticasUsuarios = getP3sManager ().obtenerEstadisticasUsuariosAgrupadasPorUsuario(idEmisor, fechaDesde, fechaHasta);
		} catch (Exception e) {
			addActionError("Error al obtener las estad\u00edsticas de usuarios con m\u00e1s env\u00edos");
			LOG.error("Error al obtener las estad\u00edsticas de usuarios (m\u00e1s activos) para el emisor: " + idEmisor, e);
		}
		
		usuariosMasActivosStore = new Vector<UsuarioEstadistico> ();
		if (estadisticasUsuarios != null) {
			for(Map<String, Object> estadisticaUsuario : estadisticasUsuarios) {
				final UsuarioEstadistico usuarioEstadistico = new UsuarioEstadistico ();
				if (estadisticaUsuario.get(P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO) != null && !"null".equals (P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO)) {
					usuarioEstadistico.setNumEnviados("" + (Integer) estadisticaUsuario.get(P3SManagerImpl.KEY_INFO_EMISOR_ESTADISTICAS_USUARIOS_NUM_MENSAJES));
					usuarioEstadistico.setUsuario((String) estadisticaUsuario.get(P3SManagerImpl.KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO));
					usuariosMasActivosStore.add (usuarioEstadistico);
				}
			}
		}
		
		return resultado;
	}

	public void setIntervalos(Map<String, String> intervalos) {
		this.intervalos = intervalos;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public int getNumPaginaEmisor() {
		return numPaginaEmisor;
	}

	public void setNumPaginaEmisor(int numPaginaEmisor) {
		this.numPaginaEmisor = numPaginaEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public String getMensajesEnviados() {
		return mensajesEnviados;
	}

	public void setMensajesEnviados(String mensajesEnviados) {
		this.mensajesEnviados = mensajesEnviados;
	}

	public String getMensajesRecibidos() {
		return mensajesRecibidos;
	}

	public void setMensajesRecibidos(String mensajesRecibidos) {
		this.mensajesRecibidos = mensajesRecibidos;
	}

	public String getMensajesEnviadosCorporativos() {
		return mensajesEnviadosCorporativos;
	}

	public void setMensajesEnviadosCorporativos(String mensajesEnviadosCorporativos) {
		this.mensajesEnviadosCorporativos = mensajesEnviadosCorporativos;
	}

	public String getMensajesPendientes() {
		return mensajesPendientes;
	}

	public void setMensajesPendientes(String mensajesPendientes) {
		this.mensajesPendientes = mensajesPendientes;
	}

	public String getMensajesError() {
		return mensajesError;
	}

	public void setMensajesError(String mensajesError) {
		this.mensajesError = mensajesError;
	}

	public String getMensajesAcuseRecibo() {
		return mensajesAcuseRecibo;
	}

	public void setMensajesAcuseRecibo(String mensajesAcuseRecibo) {
		this.mensajesAcuseRecibo = mensajesAcuseRecibo;
	}

	public String getMensajesPrioridadAlta() {
		return mensajesPrioridadAlta;
	}

	public void setMensajesPrioridadAlta(String mensajesPrioridadAlta) {
		this.mensajesPrioridadAlta = mensajesPrioridadAlta;
	}

	public void setLimiteMensual(String limiteMensual) {
		this.limiteMensual = limiteMensual;
	}

	public String getLimiteMensual() {
		
		try {
			// Obtenemos todos los datos
			Map <String,Object> datosEmisor = getP3sManager().infoEmisor(idEmisor);
			limiteMensual = (String) "" + datosEmisor.get(P3SManagerImpl.KEY_INFO_EMISOR_LIMITE);
		} catch(final Exception e) {
			LOG.error("Error al obtener el limite de mensajes del emisor: " + idEmisor, e);
		}
		
		return limiteMensual;
	}

	public void setUsuariosMasActivosStore(Vector<UsuarioEstadistico> usuariosMasActivosStore) {
		this.usuariosMasActivosStore = usuariosMasActivosStore;
	}

	public Vector<UsuarioEstadistico> getUsuariosMasActivosStore() {
		return usuariosMasActivosStore;
	}

	public String getMesN() {
		return mesN;
	}

	public void setMesN(String mesN) {
		this.mesN = mesN;
	}

	public String getAnyoN() {
		return anyoN;
	}

	public void setAnyoN(String anyoN) {
		this.anyoN = anyoN;
	}

	public String getQueIntervalo() {
		return queIntervalo;
	}

	public void setQueIntervalo(String queIntervalo) {
		this.queIntervalo = queIntervalo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}
	
}
