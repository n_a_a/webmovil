package es.sadesi.webmovil.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.Carpeta;
import es.sadesi.webmovil.model.Mensaje;
import es.sadesi.webmovil.model.Usuario;

public class CarpetasObtieneContenido extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(CarpetasObtieneContenido.class);

	private Vector<Mensaje> mensajesStore = null;
	private Vector<Carpeta> miListaCarpetas;
	private List<String> formDiaList;
	private int numPagina;
	private int maxPagina;
	private int mensajesPorPagina;
	private int mensajesTotales;
	private String accion;
	private String desde = "";
	private String hasta = "";
	private String formDia = "";
	private String formMes = "";
	private String formAnyo = "";
	private String nombreCarpeta;
	private String idCarpeta;
	private String mensajesSeleccionados;
	private String carpetaDestinoOperacion;
	private boolean excelAvailable;
	
	private InputStream inputStream;

	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		int num_vez = 1000;
		
		// failsafe
		
		if (formDia == null || "".equals(formDia))
			formDia = "Todos";
		
		// Rellenar select de los días in a cutre way.
		
		formDiaList = new ArrayList<String> ();
		formDiaList.add ("Todos");
		for (int i = 1; i <= 31; i ++) {
			String representacion;
			if (i < 10)
				representacion = "0" + i;
			else
				representacion = "" + i;
			formDiaList.add (representacion);
		}
		
		// Ya.
		
		logger.info("Ejecutando acción CarpetasObtieneEnviados " + accion);
		
		setSeccion("SMS");
		
		Calendar cal = Calendar.getInstance ();
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyy");
		String esteAnyo = sdf.format (cal.getTime()); 
		SimpleDateFormat sdm = new SimpleDateFormat ("MM");
		String esteMes = sdm.format(cal.getTime());
		/*SimpleDateFormat sdd = new SimpleDateFormat ("dd");
		String esteDia = sdd.format(cal.getTime());
		*/
		
		// Si el mes viene vacío, establecemos el mes actual
		
		if (formMes == null || "".equals(formMes)) {
			formMes = esteMes;
		}
		
		// Si el año viene vacío o no es un año válido (entre 2000 y el actual)
		// establecemos el año actual
		
		
		if (formAnyo == null || "".equals(formAnyo)) {
			formAnyo = esteAnyo; 
		} else {
			int formAnyoInteger = Integer.parseInt(formAnyo);
			int esteAnyoInteger = Integer.parseInt(esteAnyo);
			if (formAnyoInteger < 2000 || formAnyoInteger > esteAnyoInteger) {
				formAnyo = esteAnyo;
			}
		}

		// Obtenemos los datos necesarios del usuario de la sesión:
		
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
		Usuario user = getUsuarioRemoto();
		String usuario = user.getUsuario();
		String emisor = user.getEmisor().getEmisor();

		// Obtenemos mensajesPorPagina
		
		mensajesPorPagina = user.getItemPorPagina();

		// Obtenemos la lista de carpetas de la sesión

		Vector<Carpeta> tmpListaCarpetas = (Vector<Carpeta>) (session.get(ConstantesWebmovil.LISTA_CARPETAS));
		miListaCarpetas = (Vector<Carpeta>) tmpListaCarpetas.clone();

		// Pero no nos valen "Enviados" y "Recibidos", así que las eliminamos.
		// Siempre son las dos primeras:

		miListaCarpetas.remove(0);
		miListaCarpetas.remove(0);
		
		// Calcularmos desde y hasta.
		
		if ("00".equals(formMes)) {
			desde = formAnyo + "0101000000";
			hasta = formAnyo + "1231235959";
		} else {
			if ("Todos".equals(formDia)) {
				
				// Calculamos el último día del mes elegido
				
				String ultimoDia;
				Calendar calCurr = Calendar.getInstance();
				calCurr.set(Integer.parseInt(formAnyo), Integer.parseInt(formMes) - 1, 1);
				sdf = new SimpleDateFormat ("dd");
				int ultimoDiaInt = calCurr.getActualMaximum(Calendar.DAY_OF_MONTH);
				if (ultimoDiaInt < 10) {
					ultimoDia = "0" + ultimoDiaInt;
				} else {
					ultimoDia = "" + ultimoDiaInt;
				}
				
				desde = formAnyo + formMes + "01000000";
				hasta = formAnyo + formMes + ultimoDia + "235959";
				
			} else {
				
				// Corregir la posible fecha incorrecta:
				logger.debug ("[" + formDia + "]");
				int formDiaInt = Integer.parseInt(formDia);
				Calendar calCurr = Calendar.getInstance();
				calCurr.set(Integer.parseInt(formAnyo), Integer.parseInt(formMes) - 1, 1);
				sdf = new SimpleDateFormat ("dd");
				int ultimoDiaInt = calCurr.getActualMaximum(Calendar.DAY_OF_MONTH);
				if (formDiaInt > ultimoDiaInt) {
					formDia = "" + ultimoDiaInt;
				}
				
				// Solo el dia seleccionado.
				
				desde = formAnyo + formMes + formDia + "000000";
				hasta = formAnyo + formMes + formDia + "235959";
				
			}
		}
		
		// Vemos la acción que tenemos que hacer
		// antes de obtener los mensajes.

		if ("paginaAnterior".equals(accion)) {
			numPagina--;
		} else if ("paginaSiguiente".equals(accion)) {
			numPagina++;
		} else if ("Mover seleccionados a".equals(accion)
				|| "Borrar seleccionados".equals(accion)) {
			if (mensajesSeleccionados != null) {
				
				// Tokenizamos "mensajesSeleccionados" y realizamos la misma
				// acción sobre todos
				
				StringTokenizer mensajesSeleccionadosTokens = new StringTokenizer(mensajesSeleccionados, ",");
				while (mensajesSeleccionadosTokens.hasMoreTokens()) {
					if ("Mover seleccionados a".equals(accion)) {
						getP3sManager().mueveMensajeCarpeta(usuario, emisor, mensajesSeleccionadosTokens.nextToken().trim(), buscaIdCarpeta(carpetaDestinoOperacion));
						
						// Invalidamos la lista de carpetas de la sesión.
						
						session.put(ConstantesWebmovil.LISTA_CARPETAS_STATUS, "refresh");
					} else if ("Borrar seleccionados".equals(accion)) {
						getP3sManager().mueveMensajeCarpeta(usuario, emisor, mensajesSeleccionadosTokens.nextToken().trim(), "trash");
						
						// Invalidamos la lista de carpetas de la sesión.
						
						session.put(ConstantesWebmovil.LISTA_CARPETAS_STATUS, "refresh");
					}
				}
			}
		} else if ("Actualizar intervalo".equals(accion)) {
			// No hay gran cosa que hacer, así que para
			// eliminar problemas nos vamos a la primera página.
			
			numPagina = 0;
		} else if ("Exportar Excel".equals (accion)) {
			// Las peticiones se hacen en orden inverso, para aprovechar
			// el cacheo de Oracle.
			
			logger.info("Generando EXCEL para exportar ["+usuario+","+emisor+","+idCarpeta+","+desde+","+hasta+"]"); 
			
			mensajesTotales = getP3sManager().obtieneNumMensajesCarpetaUsuario (usuario, emisor, idCarpeta, desde, hasta);
			maxPagina = (mensajesTotales - 1) / num_vez;
			logger.info ("Se hará un total de " + (maxPagina+1) + " llamadas para obtener los " + mensajesTotales + ".");

			String textHdr = "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>\n<body><table>";
			textHdr += "<tr><th>Telefono</th><th>Texto</th><th>Fecha de creacion</th><th>Fecha de recepcion</th><th>Estado</ht></tr>";
			
			String textBody = "";
			
			for (int i = maxPagina; i >= 0; i --) {
				int regIni = 1 + (num_vez * i);
				int regFin = (i + 1) * num_vez;
				logger.info("Obteniendo registros " + regIni + " hasta " + regFin + " de " + mensajesTotales);
				Vector<HashMap<String, Object>> mensajesTemp = getP3sManager().obtieneMensajesCarpetaUsuario(usuario, emisor, idCarpeta, desde, hasta, regIni, regFin);
				String textItem = "";
				if (mensajesTemp != null) {
					for (int j = 0; j < mensajesTemp.size(); j++) {
						textItem += "<tr>";
						textItem += "<td>" + mensajesTemp.get(j).get("telefono") + "</td>";
						textItem += "<td>" + mensajesTemp.get(j).get("texto") + "</td>";
						textItem += "<td>" + mensajesTemp.get(j).get("fecha_emision") + "</td>";
						textItem += "<td>" + mensajesTemp.get(j).get("fecha_recepcion") + "</td>";
						textItem += "<td>" + mensajesTemp.get(j).get("estado") + "</td>";
						String tipoMensaje = "SMS";
						try {
							if (Integer.parseInt ((String) mensajesTemp.get(j).get("tipo_mensaje")) >= 128) {
								tipoMensaje = "PUSH";
							}
						} catch (Exception e) { ; }
						textItem += "<td>" + tipoMensaje + "</td>";
						textItem += "</tr>";
					}
				}
				textBody = textItem + textBody;
			}
			
			String text = textHdr + textBody + "</table></body>";
			
			// Generar excel para descarga:
						
			try {
				setInputStream(new ByteArrayInputStream(text.getBytes("UTF-8")));
			} catch (Exception e) {
				logger.error ("Ocurrió una excepción " + e);
			}
			
			return SUCCESS;
		}

		// Tenemos que traer del WS los mensajes enviados
		// desde 1 + (mensajesPorPagina * numPagina) hasta + (mensajesPorPagina * numPagina)
		
		int regIni = 1 + (mensajesPorPagina * numPagina);
		int regFin = (numPagina + 1) * mensajesPorPagina;
		logger.debug ("usuario="+usuario+", emisor="+emisor+", idCarpeta="+idCarpeta+", desde="+desde+", hasta="+hasta+", regIni="+regIni+", regFin="+regFin+".");
		Vector<HashMap<String, Object>> mensajesTemp = getP3sManager().obtieneMensajesCarpetaUsuario(usuario, emisor, idCarpeta, desde, hasta, regIni, regFin);

		// Obtenemos maxPagina
		
		mensajesTotales = getP3sManager().obtieneNumMensajesCarpetaUsuario (usuario, emisor, idCarpeta, desde, hasta);
		maxPagina = (mensajesTotales - 1) / mensajesPorPagina;
		
		// Posibilidad de obtener o no el excel
		
		excelAvailable = (mensajesTotales <= 120000);
		
		// recorremos mensajesTemp y rellenamos nuestro store:
		
		mensajesStore = new Vector<Mensaje>();
		if (mensajesTemp != null) {
			for (int i = 0; i < mensajesTemp.size(); i++) {
				Mensaje mensaje = new Mensaje();
				mensaje.setEstado((String) mensajesTemp.get(i).get("estado"));
				if ("N/A".equals(mensajesTemp.get(i).get("fecha_recepcion"))) {
					mensaje.setFecha((String) mensajesTemp.get(i).get("fecha_emision"));
				} else {
					mensaje.setFecha((String) mensajesTemp.get(i).get("fecha_recepcion"));
				}
				mensaje.setIdEsme((String) mensajesTemp.get(i).get("id_esme"));
				mensaje.setTelefono((String) mensajesTemp.get(i).get("telefono"));
				mensaje.setTexto((String) mensajesTemp.get(i).get("texto"));
				mensaje.setIdCarpeta((String) mensajesTemp.get(i).get("id_carpeta"));
				mensaje.setNombreCarpeta(buscaNombreCarpeta(mensaje.getIdCarpeta()));
				try {
					mensaje.setTipoMensaje(Integer.parseInt ((String) mensajesTemp.get(i).get("tipo_mensaje")));
					if (mensaje.getTipoMensaje() >= 128) mensaje.setPush(true); else mensaje.setPush(false);
				} catch (Exception e) {
					mensaje.setPush (false);
				}
				mensajesStore.add(mensaje);
			}
		}

		// Hecho.

		return SUCCESS;
	}

	private String buscaNombreCarpeta(String idCarpeta) {
		String resultado = "N/A";
		for (int i = 0; i < miListaCarpetas.size(); i++) {
			Carpeta carpeta = miListaCarpetas.get(i);
			if (idCarpeta != null && idCarpeta.equals(carpeta.getIdCarpeta())) {
				resultado = carpeta.getNombreCarpeta();
				break;
			}
		}
		return resultado;
	}

	private String buscaIdCarpeta(String nombreCarpeta) {
		String resultado = "0";
		for (int i = 0; i < miListaCarpetas.size(); i++) {
			Carpeta carpeta = miListaCarpetas.get(i);
			if (nombreCarpeta != null
					&& nombreCarpeta.equals(carpeta.getNombreCarpeta())) {
				resultado = carpeta.getIdCarpeta();
				break;
			}
		}
		return resultado;
	}

	public void setMensajesStore(Vector<Mensaje> mensajesStore) {
		this.mensajesStore = mensajesStore;
	}

	public Vector<Mensaje> getMensajesStore() {
		return mensajesStore;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public int getNumPagina() {
		return numPagina;
	}

	public void setMensajesPorPagina(int mensajesPorPagina) {
		this.mensajesPorPagina = mensajesPorPagina;
	}

	public int getMensajesPorPagina() {
		return mensajesPorPagina;
	}

	public void setMensajesTotales(int mensajesTotales) {
		this.mensajesTotales = mensajesTotales;
	}

	public int getMensajesTotales() {
		return mensajesTotales;
	}

	public String getDesde() {
		return desde;
	}

	public void setDesde(String desde) {
		this.desde = desde;
	}

	public String getHasta() {
		return hasta;
	}

	public void setHasta(String hasta) {
		this.hasta = hasta;
	}

	public void setNombreCarpeta(String nombreCarpeta) {
		this.nombreCarpeta = nombreCarpeta;
	}

	public String getNombreCarpeta() {
		return nombreCarpeta;
	}

	public void setMaxPagina(int maxPagina) {
		this.maxPagina = maxPagina;
	}

	public int getMaxPagina() {
		return maxPagina;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setIdCarpeta(String idCarpeta) {
		this.idCarpeta = idCarpeta;
	}

	public String getIdCarpeta() {
		return idCarpeta;
	}

	public void setMiListaCarpetas(Vector<Carpeta> miListaCarpetas) {
		this.miListaCarpetas = miListaCarpetas;
	}

	public Vector<Carpeta> getMiListaCarpetas() {
		return miListaCarpetas;
	}

	public void setMensajesSeleccionados(String mensajesSeleccionados) {
		this.mensajesSeleccionados = mensajesSeleccionados;
	}

	public String getMensajesSeleccionados() {
		return mensajesSeleccionados;
	}

	public void setCarpetaDestinoOperacion(String carpetaDestinoOperacion) {
		this.carpetaDestinoOperacion = carpetaDestinoOperacion;
	}

	public String getCarpetaDestinoOperacion() {
		return carpetaDestinoOperacion;
	}

	public void setFormMes(String formMes) {
		this.formMes = formMes;
	}

	public String getFormMes() {
		return formMes;
	}

	public void setFormAnyo(String formAnyo) {
		this.formAnyo = formAnyo;
	}

	public String getFormAnyo() {
		return formAnyo;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setFormDia(String formDia) {
		this.formDia = formDia;
	}

	public String getFormDia() {
		return formDia;
	}

	public void setFormDiaList(List<String> formDiaList) {
		this.formDiaList = formDiaList;
	}

	public List<String> getFormDiaList() {
		return formDiaList;
	}

	public boolean isExcelAvailable() {
		return excelAvailable;
	}

	public void setExcelAvailable(boolean excelAvailable) {
		this.excelAvailable = excelAvailable;
	}
}
