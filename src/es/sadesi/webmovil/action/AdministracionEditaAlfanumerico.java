package es.sadesi.webmovil.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.Alfanumerico;

public class AdministracionEditaAlfanumerico extends WebmovilAction {

	private static final long serialVersionUID = 45927304856981043L;
	private static final Log logger = LogFactory.getLog(AdministracionEditaAlfanumerico.class);

	private String idEmisor;
	private Alfanumerico alfanumerico;
	private String accion;
	
	public void validate() {
		setSeccion("administracion");
		
		if ("Enviar".equals(accion)) {
			if (alfanumerico.getAlfa () == null || "".equals (alfanumerico.getAlfa ())) {
				addFieldError ("editaAlfanumerico.general", "Debe rellenar el campo Alfanumérico");
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String execute () throws Exception {
		setSeccion("administracion");
		
		logger.info ("Obteniendo lista de alfanuméricos");
		
		String operacion = "listaAlfanumericosNew";
		HashMap<String,Object> args = null;		
		HashMap<String,Object> lecturaBD = null;
		
		try {
			lecturaBD = getP3sManager().generalReadBD(operacion, args);
		} catch (Exception e) {
			logger.error ("Ocurrió un error al intentar obtener la lista de alfanuméricos." + e);
			addFieldError ("listaAlfanumericos.general", "Ocurrió un error al intentar obtener la lista de alfanuméricos. Contacte con el ardministrador");
			return INPUT;
		}
		
		Vector<HashMap<String,Object>> resultados = (Vector<HashMap<String,Object>>) lecturaBD.get("resultado");
				
		if ("Enviar".equals(accion)) {
			// Almacenar cambios/nuevo
			idEmisor = alfanumerico.getIdEmisor ();

			try {
				logger.info ("Actualizando datos del emisor " + idEmisor);
				
				operacion = "escribeAlfanumericoNew";
				args = new HashMap<String,Object> ();
				args.put ("id_emisor", idEmisor);
				args.put ("alfa", alfanumerico.getAlfa());
				getP3sManager().generalWriteBD(operacion, args);
				
				return "alfanumericoCreado";
			} catch (Exception e) {
				logger.error ("Ocurrió un error al intentar actualizar el emisor " + idEmisor, e);
				addFieldError ("listaAlfanumericos.general", "Ocurrió un error al intentar actualizar el emisor " + idEmisor + ". Contacte con el ardministrador");
				return INPUT;
			}
		}
		
		if (!"NUEVO".equals (idEmisor)) {
			// Extraer los valores del alfanumérico referenciado.
			try {
				alfanumerico = new Alfanumerico ();
				Iterator<HashMap<String,Object>> iter = resultados.iterator ();
				while (iter.hasNext()) {
					HashMap<String,Object> resultado = (HashMap<String,Object>) iter.next();
					String curIdEmisor = (String) resultado.get("id_emisor");
					if (curIdEmisor != null && curIdEmisor.equals(idEmisor)) {
						alfanumerico.setIdEmisor ((String) resultado.get("id_emisor"));
						alfanumerico.setDescripcion ((String) resultado.get ("descripcion"));
						alfanumerico.setAlfa ((String) resultado.get ("alfa"));
						break;
					}					
				}
			} catch (Exception e) {
				logger.error ("Ocurrió un error al intentar obtener los valores del alfanumérico requerido." + e);
				addFieldError ("listaAlfanumericos.general", "Ocurrió un error al intentar obtener los valores del alfanumérico requerido. Contacte con el ardministrador");
				return INPUT;
			}
		}
		return SUCCESS;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public Alfanumerico getAlfanumerico() {
		return alfanumerico;
	}

	public void setAlfanumerico(Alfanumerico alfanumerico) {
		this.alfanumerico = alfanumerico;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}
