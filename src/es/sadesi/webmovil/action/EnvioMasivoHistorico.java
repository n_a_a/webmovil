package es.sadesi.webmovil.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SProperties;
import es.sadesi.webmovil.model.EnvioMasivoAlmacenado;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilWebmovil;

public class EnvioMasivoHistorico extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(EnvioMasivoHistorico.class);

	private static String rutaEnvioMasivo = P3SProperties.getInstance().getProperty(P3SProperties.RUTA_ENVIO_MASIVO);
	
	private Vector<EnvioMasivoAlmacenado> envioMasivoStore = null;
	private int numPagina = 0;
	private int maxPagina = 0;
	private int numAlmacenados = 0;
	private String accion; 
	private String nombreArchivo;

	public String execute() throws Exception {
		logger.info("Ejecutando acción EnvioMasivoHistorico");
		
		setSeccion("SMS");
		
		// Rellenamos campos automáticos
		
		Usuario usuario = getUsuarioRemoto();
		
		// Primero, por robustez, comprobamos que existen todos los directorios
		// en juego: queue, users, users/<employeenumber>/dest y users/<employeenumber>/log
		// Creamos los que no existan.
		
		UtilWebmovil.compruebaSiExisteYSiNoCrea (rutaEnvioMasivo + "queue");
		UtilWebmovil.compruebaSiExisteYSiNoCrea (rutaEnvioMasivo + "users");
		UtilWebmovil.compruebaSiExisteYSiNoCrea (rutaEnvioMasivo + "users/" + usuario.getId());
		UtilWebmovil.compruebaSiExisteYSiNoCrea (rutaEnvioMasivo + "users/" + usuario.getId() + "/dest");
		UtilWebmovil.compruebaSiExisteYSiNoCrea (rutaEnvioMasivo + "users/" + usuario.getId() + "/log");
		
		// Ahora vemos si nos han mandado borrar algún archivo
		
		if ("delete".equals(accion)) {
			File corredorDeLaMuerte = new File(rutaEnvioMasivo + "users/" + usuario.getId() + "/log", nombreArchivo);
			corredorDeLaMuerte.delete ();
		}
		
		// Poblamos envioMasivoStore
		
		envioMasivoStore = new Vector<EnvioMasivoAlmacenado> ();
		
		// Vemos qué archivos hay en el directorio de log del usuario:
		
		File dir = new File (rutaEnvioMasivo + "users/" + usuario.getId() + "/log");
		String[] children = dir.list ();
		
		// Ordenamos children por fecha (contenida en el propio texto)
		// TODO: Buscar un algoritmo de ordenación más eficiente.
		
		for (int i = 2; i <= children.length; i ++) {
			for (int j = 0; j <= children.length - i; j ++) {

				// Extraemos la fecha de los elementos que hay que comparar en cada iteración.
				
				int finSubstringJ = children [j].lastIndexOf("#");
				String elemJ = children [j].substring (finSubstringJ + 1);
				
				int finSubstringJmas1 = children [j + 1].lastIndexOf("#");
				String elemJmas1 = children [j + 1].substring(finSubstringJmas1 + 1);
				
				// Comparar, ¿elemJ > elemJ+1?
				if (elemJ.compareTo (elemJmas1) > 0) {
					
					// Intercambiar
					String aux = children [j];
					children [j] = children [j + 1];
					children [j + 1] = aux;
				}
			}
		}
		
		// Paginación.
		
		numAlmacenados = children.length;
		maxPagina = (numAlmacenados - 1) / usuario.getItemPorPagina();
		
		// Parsear acción (cambio de página)
		
		if ("paginaAnterior".equals(accion)) {
			numPagina--;
		} else if ("paginaSiguiente".equals(accion)) {
			numPagina++;
		} else if ("paginaPrimera".equals(accion)) {
			numPagina = 0;
		} else if ("paginaUltima".equals(accion)) {
			numPagina = maxPagina;
		} 
		
		// Calculamos qué registros vamos a mostrar
		
		int registroIni = numPagina * usuario.getItemPorPagina();
		int registroFin = (numPagina + 1) * usuario.getItemPorPagina() - 1;
		if (registroFin > numAlmacenados - 1) registroFin = numAlmacenados - 1;
		
		// children contiene una lista de archivos .log. Recorremos children poblando nuestro store
		
		for (int i = registroIni; i <= registroFin; i ++) {
			EnvioMasivoAlmacenado envioAlmacenado = new EnvioMasivoAlmacenado ();
			
			// Nombre delarchivo
			envioAlmacenado.setNombreArchivo(children [i]);
			
			// título (descripción) del envío
			int inicioSubstring = children [i].indexOf("#") + 1;
			int finSubstring = children [i].lastIndexOf("#");
			envioAlmacenado.setDescripcion(children [i].substring(inicioSubstring, finSubstring));
			
			// fecha del envío
			String fechaYHora = children [i].substring (finSubstring + 1);
			envioAlmacenado.setFecha (fechaYHora.substring(6, 8) + "/" + fechaYHora.substring(4, 6) + "/" + fechaYHora.substring (0, 4));
			envioAlmacenado.setHora(fechaYHora.substring(8,10) + ":" + fechaYHora.substring(10,12));
			
			// texto del envío. Abrimos el archivo children [i] y leemos la primera linea
			String textoEnvio = "";
			try {
				File archivoLog = new File(rutaEnvioMasivo + "users/" + usuario.getId() + "/log", children [i]);
				BufferedReader input = null;
				input = new BufferedReader (new FileReader(archivoLog));	
				
				try {
					textoEnvio = input.readLine ();
				} catch (IOException e) {
					logger.error ("Ocurrió un error leyendo el texto del envío en " + children [i] + ":" + e.toString ());
					textoEnvio = "(imposible leer el texto)";
				} finally {
					input.close ();
				}
			} catch (Exception e) {
				logger.error ("Ocurrió un error leyendo el archivo del envío en " + children [i] + ":" + e.toString ());
				textoEnvio = "(imposible leer el archivo)";
			}
			
			envioAlmacenado.setTexto(textoEnvio);
			
			// Añadir al store
			envioMasivoStore.add (envioAlmacenado);
		}
		
		return SUCCESS;
	}

	public void setEnvioMasivoStore(Vector<EnvioMasivoAlmacenado> envioMasivoStore) {
		this.envioMasivoStore = envioMasivoStore;
	}

	public Vector<EnvioMasivoAlmacenado> getEnvioMasivoStore() {
		return envioMasivoStore;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public int getNumPagina() {
		return numPagina;
	}

	public void setMaxPagina(int maxPagina) {
		this.maxPagina = maxPagina;
	}

	public int getMaxPagina() {
		return maxPagina;
	}

	public void setNumAlmacenados(int numAlmacenados) {
		this.numAlmacenados = numAlmacenados;
	}

	public int getNumAlmacenados() {
		return numAlmacenados;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}
}
