package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Test extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(Test.class);
	
	public String execute() throws Exception {
		logger.info("Ejecutando test");
		
		// Para probar que el WS está activo símplemente llamamos a 
		// la función dummy "version", que básicamente está diseñada para ello.
		
		try {
			logger.info("Resultado del test OK : " + getP3sManager().version());
		} catch (Exception e) {
			logger.error("¡El test falló! P3SWebmovilWS no está disponible.");
			return INPUT;
		}
		
		return SUCCESS;
	}
}
