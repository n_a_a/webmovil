package es.sadesi.webmovil.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import es.sadesi.security.SecurityManager;
import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.sms.P3SAgenManager;
import es.sadesi.sms.P3SAgenManagerImpl;
import es.sadesi.webmovil.manager.AgendaManager;
import es.sadesi.webmovil.manager.AgendaManagerImpl;
import es.sadesi.webmovil.manager.EmisorManager;
import es.sadesi.webmovil.manager.EmisorManagerImpl;
import es.sadesi.webmovil.manager.UsuarioManager;
import es.sadesi.webmovil.manager.UsuarioManagerImpl;
import es.sadesi.webmovil.model.Usuario;

public class WebmovilAction extends ActionSupport {

	/**
	 * Auto-generado.
	 */
	private static final long serialVersionUID = -4917902232234699719L;

	private P3SManager p3sManager;
	private P3SAgenManager p3sAgenManager;
	private UsuarioManager usuarioManager;
	private EmisorManager emisorManager;
	private AgendaManager agendaManager;
	
	private String seccion;
	private String version="3_3_0_0";
	
	public Usuario getUsuarioRemoto() {
		ActionContext context = ActionContext.getContext();
        return (Usuario) context.getSession().get(SecurityManager.USER_HANDLE);
    }

	public P3SManager getP3sManager() {	
		if (p3sManager == null) {
			p3sManager = new P3SManagerImpl();
		}
		return p3sManager;
	}
	
	public P3SAgenManager getP3sAgenManager() {
		if (p3sAgenManager == null) {
			p3sAgenManager = new P3SAgenManagerImpl();
		}
		return p3sAgenManager;
	}
	
	public UsuarioManager getUsuarioManager() {
		if (usuarioManager == null) {
			usuarioManager = new UsuarioManagerImpl(getP3sManager(), getEmisorManager());
		}
		return usuarioManager;
	}

	public EmisorManager getEmisorManager() {
		if (emisorManager == null) {
			emisorManager = new EmisorManagerImpl(getP3sManager());
		}
		return emisorManager;
	}

	public boolean tienePermiso(String permiso) {
		return getUsuarioManager().tienePermiso(permiso, getUsuarioRemoto());
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public AgendaManager getAgendaManager() {
		if (agendaManager == null)
			agendaManager = new AgendaManagerImpl(getP3sManager(), getP3sAgenManager ());
		return agendaManager;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
