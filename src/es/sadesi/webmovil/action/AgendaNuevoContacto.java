package es.sadesi.webmovil.action;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;

public class AgendaNuevoContacto extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory
			.getLog(AgendaNuevoContacto.class);

	private String nombreContacto;
	private String tlfMovil;
	private String tlfFijo;
	private String[] tiposTelefono = { "Nacional", "Internacional" };
	private String tipoTlfMovil = "Nacional";
	private String tipoTlfFijo = "Nacional";
	private String email;
	private String entidad;
	private String notas;
	private List<Contacto> grupos;
	private String idGrupoSel;
	private String accion;
	
	public void validate() {
		setSeccion("agenda");
		if ("Aceptar".equals(accion)) {
			if (nombreContacto == null || "".equals(nombreContacto)) {
				addFieldError("nombreContacto", "Introduzca el nombre del contacto.");
			}
			
			if (tlfMovil == null || "".equals(tlfMovil)) {
				addFieldError("tlfMovil", "Introduzca un número de teléfono móvil.");
			}
			
			if (tlfFijo != null && !"".equals(tlfFijo)) {
				/*
				if ("Nacional".equals(tipoTlfFijo)) {
					try {
						UtilWebmovil.validaDestinatario(tlfFijo, false, "");
					} catch (NumberFormatException nfe) {
						addFieldError("tlfFijo", nfe.getMessage());
					}
				} else if ("Internacional".equals(tlfFijo)) {
					try {
						UtilWebmovil.validaDestinatario(tlfFijo, true, "");					
					} catch (NumberFormatException nfe) {
						addFieldError("tlfFijo", nfe.getMessage());
					}
				} else
					addFieldError("tlfFijo", "Tipo de teléfono incorrecto.");
				*/
			}
			if (hasFieldErrors())
				grupos = getAgendaManager().getGrupos(getUsuarioRemoto());
		}
	}

	public String execute() throws Exception {
		if (logger.isDebugEnabled())
			logger.debug("Ejecutando la acción AgendaNuevoContacto.");
		
		setSeccion("agenda");
		
		Usuario usuario = getUsuarioRemoto();

		if ("Volver".equals(accion)) {
			return "volver";
		} if ("Aceptar".equals(accion)) {
			try {
				creaContacto(usuario);
			} catch (Exception e) {
				logger.error("Error al crear el contacto '" + nombreContacto
						+ "' en el grupo con id '" + idGrupoSel
						+ "' para el usuario '" + usuario.getUsuario() + "'.",
						e);
				addActionError("Ocurrió un error al crear el contacto '"
						+ nombreContacto + "'.");
				return INPUT;
			}
			
			return "contactoCreado";
		}

		grupos = getAgendaManager().getGrupos(getUsuarioRemoto());

		return SUCCESS;
	}

	public List<Contacto> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Contacto> grupos) {
		this.grupos = grupos;
	}

	public String getIdGrupoSel() {
		return idGrupoSel;
	}

	public void setIdGrupoSel(String idGrupoSel) {
		this.idGrupoSel = idGrupoSel;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getTlfMovil() {
		return tlfMovil;
	}

	public void setTlfMovil(String tlfMovil) {
		this.tlfMovil = tlfMovil;
	}

	public String getTlfFijo() {
		return tlfFijo;
	}

	public void setTlfFijo(String tlfFijo) {
		this.tlfFijo = tlfFijo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public String[] getTiposTelefono() {
		return tiposTelefono;
	}

	public void setTiposTelefono(String[] tiposTelefono) {
		this.tiposTelefono = tiposTelefono;
	}

	public String getTipoTlfMovil() {
		return tipoTlfMovil;
	}

	public void setTipoTlfMovil(String tipoTlfMovil) {
		this.tipoTlfMovil = tipoTlfMovil;
	}

	public String getTipoTlfFijo() {
		return tipoTlfFijo;
	}

	public void setTipoTlfFijo(String tipoTlfFijo) {
		this.tipoTlfFijo = tipoTlfFijo;
	}

	private void creaContacto(Usuario usuario) throws Exception {
		if (email == null) email = "";
		if (tlfFijo == null) tlfFijo = "";
		if (entidad == null) entidad = "";
		if (notas == null) notas = "";
		
		// Remove all non-numeric characters
		tlfMovil = tlfMovil.replaceAll("[^\\d]", "");
		tlfFijo = tlfFijo.replaceAll("[^\\d]", "");
		
		// Trim a bit
		if (tlfMovil.length() > 20) tlfMovil = tlfMovil.substring(0, 19);
		if (tlfFijo.length() > 20) tlfFijo = tlfFijo.substring(0, 19);
		
		Emisor emisor = usuario.getEmisor();
		getP3sAgenManager().agendaCreaContacto(usuario.getId (),
				emisor.getEmisor(), idGrupoSel, nombreContacto, tlfMovil,
				email, tlfFijo, entidad, notas);
	}
}
