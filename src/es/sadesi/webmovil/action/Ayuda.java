package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Ayuda extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(Ayuda.class);
	public String execute () {
		setSeccion("ayuda");
		logger.info("Ejecutando acción PreferenciasUsuario");
		return SUCCESS;
	}
}
