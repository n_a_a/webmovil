package es.sadesi.webmovil.action;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilWebmovil;

public class AgendaContenidoCombo extends WebmovilAction {

	private static final long serialVersionUID = 17L;
	private static final Log logger = LogFactory.getLog(AgendaContenidoCombo.class);

	private Contacto grupoSel;
	private String idGrupoSel;
	
	private int numContactos;
	private Vector<Contacto> listaGrupos;
	private int numGrupos;
	private Vector<Contacto> listaContactos;
	private String accion;

	// Para la paginación
	private int numPagina = 0;
	private int maxPagina;
	private int regIni;
	private int regFin;

	// Para la eliminación
	private String contactosSeleccionados;
	private String gruposSeleccionados;

	// Para mover y copiar
	private List<Contacto> grupos;
	private String idGrupoMover;
	private String idGrupoCopiar;
	
	// Para importar
	private String textoImportar;
	private List<String> resultadoImportar;

	public String execute() throws Exception {
		if (idGrupoSel == null || "".equals (idGrupoSel)) {
			idGrupoSel = "0";
		}
		
		if (logger.isDebugEnabled())
			logger.debug("Ejecutando la acción AgendaContenido.");

		setSeccion("agenda");

		// Obtenemos el usuario y emisor de la sesión.
		Usuario usuario = getUsuarioRemoto();
		Emisor emisor = usuario.getEmisor();

		if ("Ver".equals(accion)) {
			numPagina = 0;
		} else if ("Eliminar".equals(accion)) {
			try {
				eliminaContactos(usuario);
			} catch (Exception e) {
				logger.error("Error al eliminar los contactos seleccionados "
						+ "' en el grupo con id '" + idGrupoSel
						+ "' de la agenda del usuario '" + usuario.getUsuario()
						+ "'.", e);
				addActionError("Ocurrió un error al eliminar los contactos.");
				return INPUT;
			}
		} else if ("Mover a".equals(accion)) {
			try {
				mueveContacto(usuario);
			} catch (Exception e) {
				logger.error("Error al mover los contactos seleccionados "
						+ "' del grupo con id '" + idGrupoSel
						+ "' al grupo con id '" + idGrupoMover
						+ "' de la agenda del usuario '" + usuario.getUsuario()
						+ "'.", e);
				addActionError("Ocurrió un error al mover los contactos.");
				return INPUT;
			}
		} else if ("Copiar a".equals(accion)) {
			try {
				copiaContacto(usuario);
			} catch (Exception e) {
				logger.error("Error al copiar los contactos seleccionados "
						+ "' del grupo con id '" + idGrupoSel
						+ "' al grupo con id '" + idGrupoMover
						+ "' de la agenda del usuario '" + usuario.getUsuario()
						+ "'.", e);
				addActionError("Ocurrió un error al copiar los contactos.");
				return INPUT;
			}
		} else if ("Importar".equals(accion)) {
			logger.info ("Parseando los contactos para importar");
			BufferedReader reader = new BufferedReader(new StringReader(textoImportar));
			String linea;
			resultadoImportar = new Vector<String> ();
			String nuevoContactoNombre = "";
			String nuevoContactoTlfno = "";
			String lineasErroneas = "";
			int numLin = 0;
			while ((linea = reader.readLine ()) != null) {
				numLin ++;
				if (linea.length() > 0) {
					// Obtener nombre y teléfono con un tokenizer
					StringTokenizer lineaTokens = new StringTokenizer(linea, ";");
					try {
						nuevoContactoNombre = lineaTokens.nextToken();
						nuevoContactoTlfno = lineaTokens.nextToken();
						
						logger.info ("Creando contacto de nombre = " + nuevoContactoNombre + " y teléfono " + nuevoContactoTlfno + ".");
						getP3sAgenManager().agendaCreaContacto(usuario.getId (),
								emisor.getEmisor(), idGrupoSel, nuevoContactoNombre, 
								nuevoContactoTlfno,	"", "", "", "");
						resultadoImportar.add ("Contacto [" + nuevoContactoNombre + " (" + nuevoContactoTlfno + ")] creado con exito.");
					} catch (Exception e) {
						logger.warn ("Ignorando la linea \"" + linea + "\" por error " + e);
						lineasErroneas += linea + "\n";
						resultadoImportar.add ("ERROR en la linea " + numLin +". Ignorando.\n");
					}
				}
			}
			textoImportar = lineasErroneas;
		}

		if (grupoSel == null) {
			grupoSel = new Contacto();
		}
		grupoSel.setIdGrupo(idGrupoSel);
		if (!"0".equals(idGrupoSel)) {
			grupoSel = getAgendaManager().getGrupo(idGrupoSel, usuario);
		}

		// Obtengo la página actual de contactos del grupo seleccionado.
		
		Vector<HashMap<String, Object>> listaContactosTmp = null;

		numContactos = getP3sAgenManager().cuentaContactosGrupo(
				usuario.getId(), emisor.getEmisor(),
				grupoSel.getIdGrupo());

		regIni = 1 + (usuario.getItemPorPagina() * numPagina);
		regFin = (numPagina + 1) * usuario.getItemPorPagina();
		if (regFin > numContactos)
			regFin = numContactos;
		listaContactosTmp = getP3sAgenManager().listaContactosGrupo(
				usuario.getId(), emisor.getEmisor(),
				grupoSel.getIdGrupo(), regIni, regFin);

		maxPagina = (numContactos-1) / usuario.getItemPorPagina();

		listaContactos = new Vector<Contacto>();
		if (listaContactosTmp != null) {
			for (int i = 0; i < listaContactosTmp.size(); i++) {
				Contacto contacto = new Contacto();
				contacto.setNombre((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_NOMBRE));
				contacto.setIdGrupo((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_ID_GRUPO));
				contacto.setIdContacto((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_ID_CONTACTO));
				contacto.setIdPadre((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_ID_PADRE));
				contacto.setEmail((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_EMAIL));
				contacto.setTlfMovil((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_TLF_MOVIL));
				contacto.setTlfFijo((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_TLF_FIJO));
				contacto.setEntidad((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_ENTIDAD));
				contacto.setNotas((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_NOTAS));
				
				// New: Mostrar si es cliente push. Esto necesita cierto maneje...
				// Validar / convertir número de teléfono
				boolean esPush = false;
				
				String tlfnoToken = contacto.getTlfMovil();
				String restoreMyCopy = tlfnoToken;
				try {
					String tlfnoValidate [] = UtilWebmovil.validaNumeroTelefono(tlfnoToken);
					tlfnoToken = tlfnoValidate [0];
				} catch (Exception e) {
					logger.warn ("Excepción validando teléfono " + tlfnoToken + ": " + e.getMessage ());
					tlfnoToken = restoreMyCopy;
				}
				try {
					tlfnoToken = UtilWebmovil.cleanUpTlfno (tlfnoToken);
				} catch (Exception e) { 
					logger.warn ("Excepción limpiando teléfono " + tlfnoToken + ": " + e.getMessage ());
					tlfnoToken = restoreMyCopy;
				}
				try {
					esPush = getP3sManager().esClientePush(tlfnoToken);					
				} catch (Exception e) {
					logger.warn ("Excepción comprobando si " + tlfnoToken + " es cliente push");
				}
				
				contacto.setClientePush(esPush);
				
				listaContactos.add(contacto);
			}
		}
		
		// Obtengo los subgrupos del contacto seleccionado.
		//TODO
		numGrupos = getP3sAgenManager().cuentaSubgruposGrupo(
				usuario.getId(), emisor.getEmisor(),
				grupoSel.getIdGrupo());

		
		listaContactosTmp = getP3sAgenManager().listaSubgruposGrupo(
				usuario.getId(), emisor.getEmisor(),
				grupoSel.getIdGrupo(), 1, numGrupos);
		
		listaGrupos = new Vector<Contacto>();
		if (listaContactosTmp != null) {
			for (int i = 0; i < listaContactosTmp.size(); i++) {
				Contacto contacto = new Contacto();
				contacto.setNombre((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_NOMBRE));
				contacto.setIdGrupo((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_ID_GRUPO));
				contacto.setIdContacto((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_ID_CONTACTO));
				contacto.setIdPadre((String) listaContactosTmp.get(i).get(
						P3SManagerImpl.KEY_CONTACTOS_ID_PADRE));
				listaGrupos.add(contacto);
			}
		}
		
		// Obtengo todos los grupos para los combos de mover, etc.

		grupos = getAgendaManager().getGrupos(getUsuarioRemoto());

		return SUCCESS;
	}

	public Contacto getGrupoSel() {
		return grupoSel;
	}

	public void setGrupoSel(Contacto grupoSel) {
		this.grupoSel = grupoSel;
	}

	public String getIdGrupoSel() {
		return idGrupoSel;
	}

	public void setIdGrupoSel(String idGrupoSel) {
		this.idGrupoSel = idGrupoSel;
	}

	public int getNumContactos() {
		return numContactos;
	}

	public void setNumContactos(int numContactos) {
		this.numContactos = numContactos;
	}

	public Vector<Contacto> getListaContactos() {
		return listaContactos;
	}

	public void setListaContactos(Vector<Contacto> listaContactos) {
		this.listaContactos = listaContactos;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public int getNumPagina() {
		return numPagina;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public int getMaxPagina() {
		return maxPagina;
	}

	public void setMaxPagina(int maxPagina) {
		this.maxPagina = maxPagina;
	}

	public int getRegIni() {
		return regIni;
	}

	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}

	public int getRegFin() {
		return regFin;
	}

	public void setRegFin(int regFin) {
		this.regFin = regFin;
	}

	public String getContactosSeleccionados() {
		return contactosSeleccionados;
	}

	public void setContactosSeleccionados(String contactosSeleccionados) {
		this.contactosSeleccionados = contactosSeleccionados;
	}

	public List<Contacto> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Contacto> grupos) {
		this.grupos = grupos;
	}

	public String getIdGrupoMover() {
		return idGrupoMover;
	}

	public void setIdGrupoMover(String idGrupoMover) {
		this.idGrupoMover = idGrupoMover;
	}

	public String getIdGrupoCopiar() {
		return idGrupoCopiar;
	}

	public void setIdGrupoCopiar(String idGrupoCopiar) {
		this.idGrupoCopiar = idGrupoCopiar;
	}

	private void eliminaContactos(Usuario usuario) throws Exception {
		if (contactosSeleccionados != null) {

			// Tokenizamos "contactosSeleccionados" y realizamos la misma acción
			// sobre todos
			StringTokenizer contactosSeleccionadosTokens = new StringTokenizer(
					contactosSeleccionados, ",");
			while (contactosSeleccionadosTokens.hasMoreTokens()) {
				String idContacto = contactosSeleccionadosTokens.nextToken();
				if (!"false".equals(idContacto))
					getP3sAgenManager().agendaEliminaContacto(usuario.getId(),
						usuario.getEmisor().getEmisor(), idContacto);
			}
		}
		
		if (gruposSeleccionados != null) {

			// Tokenizamos "gruposSeleccionados" y realizamos la misma acción
			// sobre todos
			StringTokenizer gruposSeleccionadosTokens = new StringTokenizer(
					gruposSeleccionados, ",");
			while (gruposSeleccionadosTokens.hasMoreTokens()) {
				String idContacto = gruposSeleccionadosTokens.nextToken();
				if (!"false".equals(idContacto))
					getP3sAgenManager().agendaEliminaContacto(usuario.getId(),
						usuario.getEmisor().getEmisor(), idContacto);
			}
		}
	}

	private void mueveContacto(Usuario usuario) throws Exception {
		if (contactosSeleccionados != null) {
			// Tokenizamos "contactosSeleccionados" y realizamos la misma acción
			// sobre todos
			StringTokenizer contactosSeleccionadosTokens = new StringTokenizer(
					contactosSeleccionados, ",");
			while (contactosSeleccionadosTokens.hasMoreTokens()) {
				String idContacto = contactosSeleccionadosTokens.nextToken();
				if (!"false".equals(idContacto))
					getP3sAgenManager().agendaMueveContacto(usuario.getId(),
						usuario.getEmisor().getEmisor(), idContacto,
						idGrupoMover, idGrupoSel);
			}
		}
		
		if (gruposSeleccionados != null) {
			// Tokenizamos "gruposSeleccionados" y realizamos la misma acción
			// sobre todos
			StringTokenizer gruposSeleccionadosTokens = new StringTokenizer(
					gruposSeleccionados, ",");
			while (gruposSeleccionadosTokens.hasMoreTokens()) {
				String idContacto = gruposSeleccionadosTokens.nextToken();
				if (!"false".equals(idContacto))
					getP3sAgenManager().agendaMueveContacto(usuario.getId(),
						usuario.getEmisor().getEmisor(), idContacto,
						idGrupoMover, idGrupoSel);
			}
		}
	}
	
	private void copiaContacto(Usuario usuario) throws Exception {
		if (contactosSeleccionados != null) {
			// Tokenizamos "contactosSeleccionados" y realizamos la misma acción
			// sobre todos
			StringTokenizer contactosSeleccionadosTokens = new StringTokenizer(
					contactosSeleccionados, ",");
			while (contactosSeleccionadosTokens.hasMoreTokens()) {
				String idContacto = contactosSeleccionadosTokens.nextToken();
				if (!"false".equals(idContacto))
					getP3sAgenManager().agendaCopiaContacto(usuario.getId(),
						usuario.getEmisor().getEmisor(), idContacto,
						idGrupoCopiar);
			}
		}
		
		if (gruposSeleccionados != null) {
			// Tokenizamos "gruposSeleccionados" y realizamos la misma acción
			// sobre todos
			StringTokenizer gruposSeleccionadosTokens = new StringTokenizer(
					gruposSeleccionados, ",");
			while (gruposSeleccionadosTokens.hasMoreTokens()) {
				String idContacto = gruposSeleccionadosTokens.nextToken();
				if (!"false".equals(idContacto))
					getP3sAgenManager().agendaCopiaContacto(usuario.getId(),
						usuario.getEmisor().getEmisor(), idContacto,
						idGrupoCopiar);
			}
		}
	}

	public void setTextoImportar(String textoImportar) {
		this.textoImportar = textoImportar;
	}

	public String getTextoImportar() {
		return textoImportar;
	}

	public void setResultadoImportar(List<String> resultadoImportar) {
		this.resultadoImportar = resultadoImportar;
	}

	public List<String> getResultadoImportar() {
		return resultadoImportar;
	}

	public void setListaGrupos(Vector<Contacto> listaGrupos) {
		this.listaGrupos = listaGrupos;
	}

	public Vector<Contacto> getListaGrupos() {
		return listaGrupos;
	}

	public void setNumGrupos(int numGrupos) {
		this.numGrupos = numGrupos;
	}

	public int getNumGrupos() {
		return numGrupos;
	}

	public void setGruposSeleccionados(String gruposSeleccionados) {
		this.gruposSeleccionados = gruposSeleccionados;
	}

	public String getGruposSeleccionados() {
		return gruposSeleccionados;
	}
}
