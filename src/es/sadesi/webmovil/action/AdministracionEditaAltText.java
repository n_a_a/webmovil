package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.Usuario;

public class AdministracionEditaAltText extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory
			.getLog(AdministracionEditaEmisor.class);

	private String idEmisor;
	private boolean formDisableInvitationSMS;
	private boolean formAltTextEnable = false;
	private String formAltText = "";
	private String accion;

	public String execute() throws Exception {
		setSeccion("administracion");
		logger.info("Ejecutando acción AdministracionEditaEmisor");
		Usuario user = getUsuarioRemoto();
		idEmisor = user.getEmisor().getEmisor();
		
		if ("Enviar".equals(accion)) {
			// Vemos si es edición o creación
			try {
				getP3sManager().setAltText(idEmisor, formAltText.trim());
				getP3sManager().setAltTextEnable(idEmisor, formAltTextEnable);
				getP3sManager().setDisableInvitationSMS(idEmisor, formDisableInvitationSMS);
			} catch (Exception e) {
				mensajeEstado = "Ocurrió un error. Revise sus datos: "
						+ e.toString();
				return INPUT;
			}
			mensajeEstado = "Los atributos del emisor " + idEmisor
					+ " fueron editados correctamente";
			return INPUT;
		}
		
		formAltText = getP3sManager().getAltText(idEmisor);
		if (formAltText != null)
			formAltText = formAltText.trim();
		else
			formAltText = "";
		formAltTextEnable = getP3sManager().getAltTextEnable(idEmisor);
		formDisableInvitationSMS = getP3sManager().getDisableInvitationSMS(idEmisor);
		
		return SUCCESS;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public boolean isFormDisableInvitationSMS() {
		return formDisableInvitationSMS;
	}

	public void setFormDisableInvitationSMS(boolean formDisableInvitationSMS) {
		this.formDisableInvitationSMS = formDisableInvitationSMS;
	}

	public boolean isFormAltTextEnable() {
		return formAltTextEnable;
	}

	public void setFormAltTextEnable(boolean formAltTextEnable) {
		this.formAltTextEnable = formAltTextEnable;
	}

	public String getFormAltText() {
		return formAltText;
	}

	public void setFormAltText(String formAltText) {
		this.formAltText = formAltText;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getMensajeEstado() {
		return mensajeEstado;
	}

	public void setMensajeEstado(String mensajeEstado) {
		this.mensajeEstado = mensajeEstado;
	}

	private String mensajeEstado = "";
}
