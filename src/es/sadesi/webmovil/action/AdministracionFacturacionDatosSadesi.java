package es.sadesi.webmovil.action;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.UsuarioEstadistico;

public class AdministracionFacturacionDatosSadesi extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionFacturacionDatosSadesi.class);
	
	Vector<UsuarioEstadistico> usuariosStore = null;
	private String statusMessage = "";
	private String diaIni = "", mesIni = "", anyoIni = "";
	private String diaFin = "", mesFin = "", anyoFin = "";
	private String idEmisor = "sadesi"; 		// Por si hay que cambiar esto en el futuro.
	private String accion;
	
	public void validate() {
		
		// Para validar las fechas intentamos construir un objeto GregorianCalendar 
		// con ellas. Una excepción indicará que no son correctas.
		
		setSeccion("administracion");
		
		if ("Mostrar".equals(accion)) {
			try {
				GregorianCalendar gc = new GregorianCalendar();
				gc.setLenient(false);        
				
				gc.set(GregorianCalendar.YEAR, Integer.parseInt (anyoIni));
				gc.set(GregorianCalendar.MONTH, Integer.parseInt (mesIni) - 1);
				gc.set(GregorianCalendar.DATE, Integer.parseInt (diaIni));
				gc.getTime(); 		// Lanza excepción si es inválida
			} catch (Exception e) {
				addFieldError ("datosSadesi.date", "Fecha inicial no válida");	
			}
			
			try {
				GregorianCalendar gc = new GregorianCalendar();
				gc.setLenient(false);        
				
				gc.set(GregorianCalendar.YEAR, Integer.parseInt (anyoFin));
				gc.set(GregorianCalendar.MONTH, Integer.parseInt (mesFin) - 1);
				gc.set(GregorianCalendar.DATE, Integer.parseInt (diaFin));
				gc.getTime(); 		// Lanza excepción si es inválida 
			} catch (Exception e) {
				addFieldError ("datosSadesi.date", "Fecha final no válida");	
			}			
		}
	}
	
	public String execute() throws Exception {
		logger.info("Ejecutando acción AdministracionFacturacionDatosSadesi");
		
		setSeccion("administracion");
		
		// Si diaIni, etc. son "", los poblamos con el inicio y el fin del mes actual
		if (diaIni == null || "".equals(diaIni) || diaFin == null || "".equals(diaFin) ||
			mesIni == null || "".equals(mesIni) || mesFin == null || "".equals(mesFin) ||
			anyoIni == null || "".equals(anyoIni) || anyoFin == null || "".equals(anyoFin)) {
			
			Calendar cal = Calendar.getInstance ();
			int mes = 1 + cal.get(Calendar.MONTH);
			mesIni = mes < 10 ? "0" + mes : "" + mes;
			mesFin = mes < 10 ? "0" + mes : "" + mes;
			int anyo = cal.get(Calendar.YEAR);
			anyoIni = "" + anyo;
			anyoFin = "" + anyo;
			diaIni = "01";
			int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			diaFin = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
		} else {
			// Fix del día a dos cifras
			int dia = Integer.parseInt (diaIni);
			diaIni = dia < 10 ? "0" + dia : "" + dia;
			dia = Integer.parseInt (diaFin);
			diaFin = dia < 10 ? "0" + dia : "" + dia;
		}
		
		// desde y hasta
		
		String desde = anyoIni + mesIni + diaIni + "000000";
		String hasta = anyoFin + mesFin + diaFin + "000000";
		
		// Teniendo las fechas, obtenemos una lista de usuarios:
		
		Vector<HashMap<String,String>> listaUsuariosTemp = getP3sManager().listaUsuariosEmisor(idEmisor);
		
		// Y recorriendo nuestra lista, poblamos nuestro store:
		
		usuariosStore = new Vector<UsuarioEstadistico> ();
		for (int i = 0; i < listaUsuariosTemp.size (); i ++) {
			UsuarioEstadistico usuario = new UsuarioEstadistico ();
			usuario.setUsuario(listaUsuariosTemp.get(i).get("uid"));
			
			logger.info ("Procesando " + usuario.getUsuario ());
			
			// Llamamos a infoUsuario para el usuario
			//Map<String,Object>infoUsuario = getP3sManager().infoUsuarioEstaticos (listaUsuariosTemp.get(i).get("uid"), idEmisor);
			
			// Obtenemos los valores pertinentes
			//usuario.setNumEnviados((String) "" + infoUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_ENVIADOS_NO_CORPORATIVOS));
			//usuario.setNumEnviadosCorporativos((String) "" + infoUsuario.get(P3SManagerImpl.KEY_INFO_USUARIO_ENVIADOS_CORPORATIVOS));
			
			usuario.setNumEnviados((String) "" + getP3sManager().estadisticasUsuario(listaUsuariosTemp.get(i).get("uid"), idEmisor, "enviados", desde, hasta));
			usuario.setNumEnviadosCorporativos((String) "" + getP3sManager().estadisticasUsuario(listaUsuariosTemp.get(i).get("uid"), idEmisor, "enviadosCorporativos", desde, hasta));
			
			// Añadimos
			usuariosStore.add(usuario);
		}
		
		// All done
		
		return SUCCESS;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public Vector<UsuarioEstadistico> getUsuariosStore() {
		return usuariosStore;
	}

	public void setUsuariosStore(Vector<UsuarioEstadistico> usuariosStore) {
		this.usuariosStore = usuariosStore;
	}

	public String getDiaIni() {
		return diaIni;
	}

	public void setDiaIni(String diaIni) {
		this.diaIni = diaIni;
	}

	public String getMesIni() {
		return mesIni;
	}

	public void setMesIni(String mesIni) {
		this.mesIni = mesIni;
	}

	public String getAnyoIni() {
		return anyoIni;
	}

	public void setAnyoIni(String anyoIni) {
		this.anyoIni = anyoIni;
	}

	public String getDiaFin() {
		return diaFin;
	}

	public void setDiaFin(String diaFin) {
		this.diaFin = diaFin;
	}

	public String getMesFin() {
		return mesFin;
	}

	public void setMesFin(String mesFin) {
		this.mesFin = mesFin;
	}

	public String getAnyoFin() {
		return anyoFin;
	}

	public void setAnyoFin(String anyoFin) {
		this.anyoFin = anyoFin;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}
