package es.sadesi.webmovil.action;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.imageio.ImageIO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SProperties;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilBD;

public class AdministracionCambioIconoEmisor extends WebmovilAction {

	private static final long serialVersionUID = -556864462779947330L;
	private static final Log logger = LogFactory.getLog(AdministracionCambioIconoEmisor.class);

	private String accion;
	private File formIcon;
	private String formIconFileName;
	private String uriIcon;
	private String idEmisor;
	private String mensajeEstado;
	
	public String execute() throws Exception {
		setSeccion("administracion");
		
		// Calculamos uriIcon
		Usuario user = getUsuarioRemoto();
		idEmisor = user.getEmisor().getEmisor();
		uriIcon = P3SProperties.getInstance().getProperty(P3SProperties.URI_P3S) + "IconoEmisor/" + idEmisor;
		
		// ¿Subir nuevo icono?
		if ("Aceptar".equals (accion)) {
			if (formIconFileName == null || "".equals(formIconFileName)) {
				mensajeEstado = "Debe especificar un archivo de imagen.";
				return INPUT;
			}
			try {
				mensajeEstado = "Icono cambiado con éxito";
				processIcon ();
			} catch (Exception e) {
				String mensajeDeError = e.toString ();
				mensajeEstado = mensajeDeError;
			}
		}
		
		return SUCCESS;
	}

	private void processIcon () throws Exception {
		// Aquí: Si hay algo en el campo formIconFile, subirlo a la BD para el emisor idEmisor.
		// Esto se hace a pelo a falta de mejores formas. Es guarrisucio, pero bueno.
		if (formIconFileName != null && !"".equals(formIconFileName) ) {
			LOG.info (formIconFileName);
			
			// Comprobar que es de 256x256
			int width, height;
			try {
				BufferedImage bimg = ImageIO.read(formIcon);
				width = bimg.getWidth ();
				height = bimg.getHeight ();
			} catch (Exception e) {
				logger.error (e);
				throw new Exception ("La imagen subida no es válida."); 
			}
			if (width != 256 || height != 256) throw new Exception ("La imagen debe ser de 256x256 píxels.");
			
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			UtilBD utilBD = null;
			try {
				utilBD = new UtilBD (
						P3SProperties.getInstance().getProperty(P3SProperties.BD_DRIVER),
						P3SProperties.getInstance().getProperty(P3SProperties.BD_URL),
						P3SProperties.getInstance().getProperty(P3SProperties.BD_USER),
						P3SProperties.getInstance().getProperty(P3SProperties.BD_PASS),
						Integer.parseInt(P3SProperties.getInstance().getProperty(P3SProperties.BD_MINLIMIT)),
						Integer.parseInt(P3SProperties.getInstance().getProperty(P3SProperties.BD_MAXLIMIT)),
						Long.parseLong(P3SProperties.getInstance().getProperty(P3SProperties.BD_TIME_BETWEEN_EVICTION_RUNS_MILLIS)),
						Integer.parseInt(P3SProperties.getInstance().getProperty(P3SProperties.BD_NUM_TESTS_PER_EVICTION)),
						Long.parseLong(P3SProperties.getInstance().getProperty(P3SProperties.BD_MIN_EVICTABLE_IDLE_TIME_MILLIS)),
						Long.parseLong(P3SProperties.getInstance().getProperty(P3SProperties.BD_MAX_WAIT))
				);

				con = utilBD.getConnection ();
				con.setAutoCommit(false);
				ps = con.prepareStatement ("UPDATE p3s_emisores SET icon = ? WHERE id_emisor = ?");
				logger.info("Almacenando el icono para el emisor " + idEmisor + " en la BD");
				ps.setString (2, idEmisor);
				InputStream input = new BufferedInputStream(new FileInputStream(formIcon));
				ps.setBinaryStream (1, input, (int) formIcon.length ());
				ps.executeUpdate();
				con.commit();
			} catch (Exception e) {
				throw e;
			} finally {
				if (utilBD != null) {
					utilBD.closeAll("processIcon", rs, ps, con);
				}
			}
		}
	}
	
	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public File getFormIcon() {
		return formIcon;
	}

	public void setFormIcon(File formIcon) {
		this.formIcon = formIcon;
	}

	public String getUriIcon() {
		return uriIcon;
	}

	public void setUriIcon(String uriIcon) {
		this.uriIcon = uriIcon;
	}

	public String getFormIconFileName() {
		return formIconFileName;
	}

	public void setFormIconFileName(String formIconFileName) {
		this.formIconFileName = formIconFileName;
	}

	public String getMensajeEstado() {
		return mensajeEstado;
	}

	public void setMensajeEstado(String mensajeEstado) {
		this.mensajeEstado = mensajeEstado;
	}
}
