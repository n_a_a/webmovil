package es.sadesi.webmovil.action;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.Usuario;

public class AdministracionListaUsuarios extends WebmovilAction {
	
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionListaUsuarios.class);
	//private static final int USUARIOS_PAGINA = 10;
	
	private boolean vieneDeListaEmisores = false;
	private int numPagina = -1;
	private int numPaginaEmisores = -1;
	private int maxPagina;
	private int numUsuariosEmisor;
	private String idEmisor = null;
	private String accion;
	private Vector<Usuario> listaUsuariosStore;
	private String usuariosSeleccionados;
	private Vector<String> mensajeEstado = null;
	private String activity;
	
	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		setSeccion("administracion");
		
		logger.info("Ejecutando acción AdministracionListaUsuarios");

		// Obtenemos datos de la sesión si no están establecidos por un setter.
		
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
				
		if (idEmisor == null) {
			idEmisor = (String) session.get (ConstantesWebmovil.ADMINISTRACION_ID_EMISOR_ACTIVO);	
		} else {
			session.put (ConstantesWebmovil.ADMINISTRACION_ID_EMISOR_ACTIVO, idEmisor);
		}
		
		if (numPagina == -1) {
			numPagina = Integer.parseInt((String)session.get (ConstantesWebmovil.ADMINISTRACION_NUM_PAGINA_ACTIVO));
		} else {
			session.put (ConstantesWebmovil.ADMINISTRACION_NUM_PAGINA_ACTIVO, "" + numPagina);
		}
		
		// Para la paginación de la lista de emisores... (para que todo quede consistente al navegar)
		
		if (vieneDeListaEmisores) {
			if (numPaginaEmisores == -1) {
				numPagina = Integer.parseInt((String)session.get (ConstantesWebmovil.ADMINISTRACION_NUM_PAGINA_EMISORES_ACTIVO));
			} else {
				session.put (ConstantesWebmovil.ADMINISTRACION_NUM_PAGINA_EMISORES_ACTIVO, "" + numPagina);
			}
		}
		
		// Obtenemos el usuario de la sesión
		
		Usuario user = getUsuarioRemoto();
		
		// Parsear acción 
		
		if ("Borrar seleccionados".equals(accion)) {
			StringTokenizer usuariosSeleccionadosTokens = new StringTokenizer(usuariosSeleccionados, ",");
			if (usuariosSeleccionadosTokens.hasMoreTokens ())
				mensajeEstado = new Vector<String> ();
			while (usuariosSeleccionadosTokens.hasMoreTokens ()) {
				String usuarioKaput = usuariosSeleccionadosTokens.nextToken().trim();
				try {
					getP3sManager().eliminaUsuario(idEmisor, usuarioKaput);
					mensajeEstado.add("El usuario " + usuarioKaput + " fue eliminado correctamente.");
					logger.info ("El usuario " + usuarioKaput + " fue eliminado correctamente.");
				} catch (Exception e) {
					mensajeEstado.add("Ocurri&oacute; un error al eliminar el usuario " + usuarioKaput + ".");
					logger.error ("Ocurri&oacute; un error al eliminar el usuario " + usuarioKaput + ".");
				}
			}
		}
		
		// Leemos nuestra lista de usuarios
		
		Vector<HashMap<String,String>> listaUsuariosTemp = getP3sManager().listaUsuariosEmisor(idEmisor);
		
		// Calculamos maxPagina. Mostraremos USUARIOS_PAGINA usuarios por página
		
		numUsuariosEmisor = listaUsuariosTemp.size();
		maxPagina = numUsuariosEmisor / user.getItemPorPagina();
		
		// Parsear accion (cambio de página)
		
		if ("paginaAnterior".equals(accion)) {
			numPagina--;
		} else if ("paginaSiguiente".equals(accion)) {
			numPagina++;
		} else if ("paginaPrimera".equals(accion)) {
			numPagina = 0;
		} else if ("paginaUltima".equals(accion)) {
			numPagina = maxPagina;
		}  
		
		// Poblamos nuestro store con la página de usuarios correcta.
		
		listaUsuariosStore = new Vector<Usuario> ();
		for (int i = numPagina * user.getItemPorPagina(); i < (numPagina + 1) * user.getItemPorPagina() && i < numUsuariosEmisor; i ++) {
			Usuario usuarioItem = new Usuario ();
			usuarioItem.setId(listaUsuariosTemp.get(i).get("id"));
			usuarioItem.setUsuario(listaUsuariosTemp.get(i).get("uid"));
			listaUsuariosStore.add(usuarioItem);
		} 
		
		// Listo.
		
		return SUCCESS;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public int getNumPagina() {
		return numPagina;
	}

	public void setMaxPagina(int maxPagina) {
		this.maxPagina = maxPagina;
	}

	public int getMaxPagina() {
		return maxPagina;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setListaUsuariosStore(Vector<Usuario> listaUsuariosStore) {
		this.listaUsuariosStore = listaUsuariosStore;
	}

	public Vector<Usuario> getListaUsuariosStore() {
		return listaUsuariosStore;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setNumUsuariosEmisor(int numUsuariosEmisor) {
		this.numUsuariosEmisor = numUsuariosEmisor;
	}

	public int getNumUsuariosEmisor() {
		return numUsuariosEmisor;
	}

	public void setUsuariosSeleccionados(String usuariosSeleccionados) {
		this.usuariosSeleccionados = usuariosSeleccionados;
	}

	public String getUsuariosSeleccionados() {
		return usuariosSeleccionados;
	}

	public void setMensajeEstado(Vector<String> mensajeEstado) {
		this.mensajeEstado = mensajeEstado;
	}

	public Vector<String> getMensajeEstado() {
		return mensajeEstado;
	}

	public void setNumPaginaEmisores(int numPaginaEmisores) {
		this.numPaginaEmisores = numPaginaEmisores;
	}

	public int getNumPaginaEmisores() {
		return numPaginaEmisores;
	}

	public void setVieneDeListaEmisores(boolean vieneDeListaEmisores) {
		this.vieneDeListaEmisores = vieneDeListaEmisores;
	}

	public boolean isVieneDeListaEmisores() {
		return vieneDeListaEmisores;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getActivity() {
		return activity;
	}
}
