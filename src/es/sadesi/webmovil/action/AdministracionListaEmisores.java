package es.sadesi.webmovil.action;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;

public class AdministracionListaEmisores extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionListaEmisores.class);
	
	private int numPaginaEmisores = -1;
	private int maxPagina;
	private int numEmisores;
	private String accion;
	private Vector<Emisor> listaEmisoresStore;
	private String emisoresSeleccionados;
	private Vector<String> mensajeEstado = null;
	private String activity;
	
	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		setSeccion("administracion");
		
		logger.info("Ejecutando acción AdministracionListaEmisores");
		logger.info("activity = " + activity);
		
		// Obtenemos datos de la sesión si no están establecidos por un setter.
		
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext().get("session");
				
		if (numPaginaEmisores == -1) {
			numPaginaEmisores = Integer.parseInt((String)session.get (ConstantesWebmovil.ADMINISTRACION_NUM_PAGINA_EMISORES_ACTIVO));
		} else {
			session.put (ConstantesWebmovil.ADMINISTRACION_NUM_PAGINA_EMISORES_ACTIVO, "" + numPaginaEmisores);
		}
		
		// Obtenemos el usuario de la sesión
		
		Usuario user = getUsuarioRemoto();
		String idEmisor = user.getEmisor().getEmisor();
		
		// Parsear acción 
		
		if ("Borrar seleccionados".equals(accion)) {
			logger.info ("Emisores seleccionados: " + emisoresSeleccionados);
			StringTokenizer emisoresSeleccionadosTokens = new StringTokenizer(emisoresSeleccionados, ",");
			if (emisoresSeleccionadosTokens.hasMoreTokens ()) {
				mensajeEstado = new Vector<String> ();
				while (emisoresSeleccionadosTokens.hasMoreTokens ()) {
					String emisorKaput = emisoresSeleccionadosTokens.nextToken().trim();
					try {
						getP3sManager().eliminaEmisor (idEmisor, emisorKaput); 
						mensajeEstado.add("El emisor " + emisorKaput + " fue eliminado correctamente.");
						logger.info ("El emisor " + emisorKaput + " fue eliminado correctamente.");
					} catch (Exception e) {
						mensajeEstado.add("Ocurri&oacute; un error al eliminar el emisor " + emisorKaput + ".");
						logger.error ("Ocurri&oacute; un error al eliminar el emisor " + emisorKaput + ".");
					}
				}
			}
		}
		
		// Leemos nuestra lista de usuarios
		
		Vector<HashMap<String,String>> listaEmisoresTemp = getP3sManager().listaEmisores (idEmisor);
		
		// Manual cheesy sort (o algo así). Como no son muchos, con una ordenación por intercambio me va.
		// Gurús del java, usen un Collections.sort y tal.

		int t = listaEmisoresTemp.size ();
		if (t > 1) {
			for (int i = 1; i < t; i ++) { 
				for (int j = t - 1; j >= i; j --) {
					String emisor_j1 = listaEmisoresTemp.get(j - 1).get("id_emisor");
					String emisor_j2 = listaEmisoresTemp.get(j).get("id_emisor");
					if (emisor_j1.compareToIgnoreCase(emisor_j2) > 0) {
						HashMap<String,String> e1 = listaEmisoresTemp.get(j - 1);
						HashMap<String,String> e2 = listaEmisoresTemp.get(j);
						listaEmisoresTemp.set(j - 1, e2);
						listaEmisoresTemp.set(j, e1);
					}
				}
			}
		}
		
		// Calculamos maxPagina. Mostraremos USUARIOS_PAGINA usuarios por página
		
		numEmisores = listaEmisoresTemp.size();
		maxPagina = (numEmisores - 1) / user.getItemPorPagina();
		
		// Parsear acción (cambio de página)
		
		if ("paginaAnterior".equals(accion)) {
			numPaginaEmisores--;
		} else if ("paginaSiguiente".equals(accion)) {
			numPaginaEmisores++;
		} else if ("paginaPrimera".equals(accion)) {
			numPaginaEmisores = 0;
		} else if ("paginaUltima".equals(accion)) {
			numPaginaEmisores = maxPagina;
		} 
		
		logger.info ("maxPagina = " + maxPagina + " numEmisore = " + numEmisores);
		logger.info ("numPaginaEmisores = " + numPaginaEmisores);
		// Poblamos nuestro store con la página de usuarios correcta.
		
		listaEmisoresStore = new Vector<Emisor> ();
		for (int i = numPaginaEmisores * user.getItemPorPagina(); i < (numPaginaEmisores + 1) * user.getItemPorPagina() && i < numEmisores; i ++) {
			Emisor emisorItem = new Emisor ();
			emisorItem.setEmisor(listaEmisoresTemp.get(i).get("id_emisor"));
			emisorItem.setDescripcion(listaEmisoresTemp.get(i).get("descripcion"));
			listaEmisoresStore.add(emisorItem);
		}

		// Listo
		
		return SUCCESS;
	}
	
	public int getNumPaginaEmisores() {
		return numPaginaEmisores;
	}
	public void setNumPaginaEmisores(int numPaginaEmisores) {
		this.numPaginaEmisores = numPaginaEmisores;
	}
	public int getMaxPagina() {
		return maxPagina;
	}
	public void setMaxPagina(int maxPagina) {
		this.maxPagina = maxPagina;
	}
	public int getNumEmisores() {
		return numEmisores;
	}
	public void setNumEmisores(int numEmisores) {
		this.numEmisores = numEmisores;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public Vector<Emisor> getListaEmisoresStore() {
		return listaEmisoresStore;
	}
	public void setListaEmisoresStore(Vector<Emisor> listaEmisoresStore) {
		this.listaEmisoresStore = listaEmisoresStore;
	}
	public String getEmisoresSeleccionados() {
		return emisoresSeleccionados;
	}
	public void setEmisoresSeleccionados(String emisoresSeleccionados) {
		this.emisoresSeleccionados = emisoresSeleccionados;
	}
	public Vector<String> getMensajeEstado() {
		return mensajeEstado;
	}
	public void setMensajeEstado(Vector<String> mensajeEstado) {
		this.mensajeEstado = mensajeEstado;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getActivity() {
		return activity;
	}
}
