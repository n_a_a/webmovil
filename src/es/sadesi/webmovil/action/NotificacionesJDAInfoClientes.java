package es.sadesi.webmovil.action;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.InfoClientePush;
import es.sadesi.webmovil.util.UtilWebmovil;

public class NotificacionesJDAInfoClientes extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog (NotificacionesJDAInfoClientes.class);
	
	private String tlfnoDestino;
	private String accion;
	private int numResultados;
	private Vector<InfoClientePush> resultados;
	
	public String execute() throws Exception {
		logger.info ("NotificacionesJDA");
		setSeccion("notificacionesjda");
		
		numResultados = 0;
		resultados = null;
		
		if ("Enviar".equals (accion)) {
			resultados = new Vector<InfoClientePush> ();
			StringTokenizer tokenizer = new StringTokenizer (tlfnoDestino, ", \n-;.");
			while (tokenizer.hasMoreElements()) {
				try {
					String tlfnoToken = tokenizer.nextElement().toString().trim();
					// Validar / convertir número de teléfono
					String restoreMyCopy = tlfnoToken;
					try {
						String tlfnoValidate [] = UtilWebmovil.validaNumeroTelefono(tlfnoToken);
						tlfnoToken = tlfnoValidate [0];
					} catch (Exception e) {
						logger.warn ("Excepción validando teléfono " + tlfnoToken + ": " + e.getMessage ());
						tlfnoToken = restoreMyCopy;
					}
					try {
						tlfnoToken = UtilWebmovil.cleanUpTlfno (tlfnoToken);
					} catch (Exception e) { 
						logger.warn ("Excepción limpiando teléfono " + tlfnoToken + ": " + e.getMessage ());
						tlfnoToken = restoreMyCopy;
					}
					
					InfoClientePush infoClientePush = new InfoClientePush (tlfnoToken);
					
					if (getP3sManager().esClientePush (tlfnoToken)) {
						logger.info ("esClientePush (" + tlfnoToken + ") == true");
						infoClientePush.setPush (true);
						
						HashMap<String,String> infoLoc = getP3sManager().getInfoLocClientePush (tlfnoToken);
						
						logger.info ("getInfoLocClientePush (" + tlfnoToken + ") == " + infoLoc.toString ());
						
						infoClientePush.setLat (Double.parseDouble (infoLoc.get ("lat")));
						infoClientePush.setLon (Double.parseDouble (infoLoc.get ("lon")));
						infoClientePush.setTs (Long.parseLong (infoLoc.get ("ts")));
					} else logger.info ("esClientePush (" + tlfnoToken + ") == false");
					
					resultados.add (infoClientePush);
					numResultados ++;
				} catch (Exception e) {
					logger.warn ("Excepción extrayendo tokens de la lista o cualquier otra mierda... : " + e.getMessage ());
				}
			}
		}
		
		return SUCCESS;
	}

	public String getTlfnoDestino() {
		return tlfnoDestino;
	}

	public void setTlfnoDestino(String tlfnoDestino) {
		this.tlfnoDestino = tlfnoDestino;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public int getNumResultados() {
		return numResultados;
	}

	public void setNumResultados(int numResultados) {
		this.numResultados = numResultados;
	}

	public Vector<InfoClientePush> getResultados() {
		return resultados;
	}

	public void setResultados(Vector<InfoClientePush> resultados) {
		this.resultados = resultados;
	}
}
