package es.sadesi.webmovil.action;

import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AdministracionFacturacion extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionFacturacion.class);
	
	private String factMonth = "";
	private String factYear = "";
	
	public String execute() throws Exception {
		logger.info("Ejecutando acción AdministracionFacturacion");
		
		setSeccion("administracion");
		
		Calendar cal = Calendar.getInstance ();
		
		// Si factMonth = "" calculamos el mes actual
		if (factMonth == null || "".equals(factMonth)) {
			int mesActual = 1 + cal.get(Calendar.MONTH);
			factMonth = mesActual < 10 ? "0" + mesActual : "" + mesActual;
		}
		
		// Idem con factYear
		if (factYear == null || "".equals(factYear)) {
			factYear = "" + cal.get(Calendar.YEAR);
		}
		
		// Hecho.

		return SUCCESS;
	}

	public String getFactMonth() {
		return factMonth;
	}

	public void setFactMonth(String factMonth) {
		this.factMonth = factMonth;
	}

	public String getFactYear() {
		return factYear;
	}

	public void setFactYear(String factYear) {
		this.factYear = factYear;
	}
}
