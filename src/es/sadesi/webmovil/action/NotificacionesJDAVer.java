package es.sadesi.webmovil.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.opensymphony.xwork2.ActionContext;

import es.sadesi.sms.P3SProperties;
import es.sadesi.sms.ws.cliente.SmsWS;
import es.sadesi.sms.ws.cliente.SmsWSServiceLocator;
import es.sadesi.webmovil.model.Attachment;
import es.sadesi.webmovil.model.DefItem;
import es.sadesi.webmovil.model.OptionItem;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilWebmovil;

public class NotificacionesJDAVer extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog (NotificacionesJDAVer.class);
	private static String claveEmisor = P3SProperties.getInstance().getProperty(P3SProperties.CLAVE_EMISOR);
	
	private int id;
	private int page;
	private String tlfn;
	
	private String accion;
	private List<Attachment> adjuntos;
	private int numAdjuntos;
	private String texto;
	private String tlfnoDestino;
	private List<DefItem> listaTipos;
	private List<OptionItem> opciones;
	private int numOpciones;
	private int flags;
	private boolean respuestaFlagFeedback;
	private boolean opcionesOn;
	private boolean ackOn;
	private String comesFrom = "ver";
	
	private String someError;
	
	private String [] binaryTypes = {
		"image/jpg", "image/png", "image/gif", "application/pdf", "application/msword", 
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.oasis.opendocument.text",
		"audio/mpeg"
	};
	
	private String deFixDate (String fixedDate) {
		String date = "";
		if (!fixedDate.matches("\\d{2}-\\d{2}-\\d{4} \\d{2}:\\d{2}:\\d{2}")) {
			date = fixedDate.substring (8, 10) + "-" +
					fixedDate.substring (5, 7) + "-" +
					fixedDate.substring (0, 4) + " " +
					fixedDate.substring (11);
		} else date = fixedDate;
		return date;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String execute() throws Exception {
		logger.info ("NotificacionesJDA - Ver");
		Usuario usuario = getUsuarioRemoto();
		
		listaTipos = new ArrayList<DefItem> ();
		listaTipos.add (new DefItem ("p3s/url", "Url"));
		listaTipos.add (new DefItem ("p3s/event", "Cita"));
		listaTipos.add (new DefItem ("p3s/option", "Respuesta"));
		listaTipos.add (new DefItem ("p3s/loc", "Localización"));
		listaTipos.add (new DefItem ("p3s/video", "Enlace a video"));
		listaTipos.add (new DefItem ("image/jpg", "Imagen JPG"));
		listaTipos.add (new DefItem ("image/png", "Imagen PNG"));
		listaTipos.add (new DefItem ("image/gif", "Imagen GIF"));
		listaTipos.add (new DefItem ("application/pdf", "Documento PDF"));
		listaTipos.add (new DefItem ("application/msword", "Documento DOC"));
		listaTipos.add (new DefItem ("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "Documento DOCX"));
		listaTipos.add (new DefItem ("application/vnd.oasis.opendocument.text", "Documento ODT"));
		listaTipos.add (new DefItem ("audio/mpeg", "Audio MP3"));
		
		// Instanciar cliente de Webservices
		SmsWSServiceLocator locator;
		SmsWS smsWS = null;
		try {
			locator = new SmsWSServiceLocator ();
			smsWS = locator.getP3SWebmovilWS ();
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción al crear instancia del cliente de WS!");
			setSomeError ("Error fatal. Contacte con su administrador: " + e.getMessage ());
		}
		
		// Get message data
		try {
			HashMap<String,Object> notificacionServer = smsWS.p3SPushGetMensajeIdUser(
					usuario.getEmisor().getEmisor(),
					usuario.getUsuario(),
					claveEmisor,
					tlfn,
					id);
			
			texto = (String) notificacionServer.get ("texto");
			if(notificacionServer.get ("tlf_destino") == null) {
				tlfnoDestino = tlfn;
			} else {
				tlfnoDestino = (String) notificacionServer.get ("tlf_destino");
			}
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción al obtener un mensaje del WS (id = " + id);
			setSomeError ("Error obteniendo mensaje, id_push = " + id + ". Contacte con su administrador: " + e.getMessage ());
		}
		
		// Get attachment list
		try {
			Vector<HashMap<String,String>> adjuntosServer = smsWS.p3SPushGetAdjuntosMensajeUser(
					usuario.getEmisor().getEmisor(),
					usuario.getUsuario(),
					claveEmisor,
					id);
			
			// Get / parse / create attachments
			adjuntos = new ArrayList<Attachment> ();
			numAdjuntos = 0;
			if(adjuntosServer != null && adjuntosServer.size() > 0) {
				for (HashMap<String, String> adjuntoServer : adjuntosServer) {
					int idAdjunto = Integer.parseInt (adjuntoServer.get("id"));
					String tipoAdjunto = adjuntoServer.get ("tipo");
					
					Attachment adjunto = new Attachment (numAdjuntos ++);
					adjunto.setIdOnServer(idAdjunto);
					adjunto.setUriToGet(P3SProperties.getInstance().getProperty(P3SProperties.URI_P3S) + "GetAdjunto?id=" + idAdjunto + "&tlfno=" + tlfn);
					
					adjunto.setType(tipoAdjunto);
					for (DefItem item : listaTipos) {
						if (item.getKey ().equals (tipoAdjunto)) {
							adjunto.setTypeName (item.getValue ());
							break;
						}
					}
					String partIconName = tipoAdjunto.substring (1 + tipoAdjunto.lastIndexOf("/"));
					if("msword".equals(partIconName)) {
						partIconName = "doc";
					} else if("vnd.openxmlformats-officedocument.wordprocessingml.document".equals(partIconName)) {
						partIconName = "docx";
					} else if("vnd.oasis.opendocument.text".equals(partIconName)) {
						partIconName = "odt";
					}
					adjunto.setIconName ("ft-" + partIconName + ".png");
					
					// Binary or text?
					Vector<String> binaryTypesV = UtilWebmovil.makeVectorFromStringArray(binaryTypes);
					boolean addAsAttachment = true;
					if (binaryTypesV.contains (tipoAdjunto)) {
						adjunto.setAlreadyOnServer(true);
						adjunto.setAlreadyUploaded(true);	
					} else {
						String json = smsWS.p3SPushGetAdjuntoComoTexto (
								usuario.getEmisor().getEmisor(),
								usuario.getUsuario(),
								claveEmisor,
								idAdjunto);
						
				        logger.debug ("Reading text from " + adjunto.getUriToGet ());
				        logger.debug ("Text read: " + json);
				        
				        // Parse json - Sketchy. I reverse-engineered gson using teh debugger.
				        Gson gson = new Gson();
				        
				        if ("p3s/url".equals (tipoAdjunto)) {			     
							HashMap<String,String> parsed = new HashMap<String,String> ();
							parsed = gson.fromJson(json, parsed.getClass ());
				        	adjunto.setUrl(parsed.get("url"));
				        	adjunto.setDesc(parsed.get("desc"));
				        } else if ("p3s/video".equals (tipoAdjunto)) {
				        	HashMap<String,String> parsed = new HashMap<String,String> ();
							parsed = gson.fromJson(json, parsed.getClass ());
				        	adjunto.setTit(parsed.get("tit"));
				        	adjunto.setUrl(parsed.get("url"));
				        } else if ("p3s/loc".equals(tipoAdjunto)) {
				        	HashMap<String,String> parsed = new HashMap<String,String> ();
							parsed = gson.fromJson(json, parsed.getClass ());
				        	adjunto.setDesc(parsed.get("desc"));
				        	adjunto.setLon(parsed.get("lon"));
				        	adjunto.setLat(parsed.get("lat"));
				        } else if ("p3s/event".equals(tipoAdjunto)) {
				        	HashMap<String,Object> parsed = new HashMap<String,Object> ();
				        	parsed = gson.fromJson(json, parsed.getClass ());
				        	adjunto.setTitle((String) parsed.get("title"));
				        	adjunto.setAllDay(false);
				        	adjunto.setStart(deFixDate ((String) parsed.get("start")));
				        	adjunto.setEnd(deFixDate ((String) parsed.get("end")));
				        	adjunto.setUrl((String) parsed.get("url"));
				        	Map<String,String> loc = (Map<String,String>) parsed.get("loc");
				        	adjunto.setDesc(loc.get("desc"));
				        	logger.debug ("DESC = " + adjunto.getDesc ());
				        	adjunto.setLon(loc.get("lon"));
				        	adjunto.setLat(loc.get("lat"));
				        } else if ("p3s/option".equals(tipoAdjunto)) {
				        	JsonObject jsonObject = gson.fromJson (json, JsonObject.class);
				        	JsonArray jsonArray = jsonObject.get("opciones").getAsJsonObject ().get("opcion").getAsJsonArray ();
				        	opcionesOn = true;
				        	numOpciones = jsonArray.size ();
				        	opciones = new ArrayList<OptionItem> ();
				        	for (int i = 0; i < jsonArray.size (); i ++) {
				        		JsonObject option = jsonArray.get (i).getAsJsonObject();
				        		OptionItem item = new OptionItem(option.get("texto").getAsString(), option.get("url").getAsString());
				        		opciones.add(item);
				        	}
				        	flags = Integer.parseInt(jsonObject.get("flags").getAsString ());
				        	if ((flags & 1) == 1) respuestaFlagFeedback = true; else respuestaFlagFeedback = false; 
				        	addAsAttachment = false;
				        } else if ("p3s/ack".equals(tipoAdjunto)) {
				        	// Por ahora, nos vale con esto...
				        	ackOn = true;
				        	addAsAttachment = false;
				        }
					}
					if (addAsAttachment) {
			        	adjuntos.add (adjunto);
			        }
				}
			}
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción al obtener los adjuntos del mensaje del WS (id = " + id + ")", e);
			setSomeError ("Error obteniendo adjuntos, id_push = " + id + ". Contacte con su administrador: " + e.getMessage ());
		}
		
		// Store
		logger.debug ("Guardando tiestos en la sesión");
		remapAdjuntos ();
		ActionContext.getContext ().getSession ().put ("adjuntos", adjuntos);
		numAdjuntos = adjuntos.size ();
		
		logger.debug ("All done");
		
		// Ya está todo listo. Sólo tengo que mostrarlo en la vista.
		
		// Profit!
		
		return SUCCESS;
	}

	private void remapAdjuntos () {
		for (int i = 0; i < adjuntos.size (); i ++) {
			adjuntos.get (i).setId (i);
		}
	}
	
	public static String getClaveEmisor() {
		return claveEmisor;
	}

	public static void setClaveEmisor(String claveEmisor) {
		NotificacionesJDAVer.claveEmisor = claveEmisor;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<Attachment> getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(List<Attachment> adjuntos) {
		this.adjuntos = adjuntos;
	}

	public int getNumAdjuntos() {
		return numAdjuntos;
	}

	public void setNumAdjuntos(int numAdjuntos) {
		this.numAdjuntos = numAdjuntos;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTlfnoDestino() {
		return tlfnoDestino;
	}

	public void setTlfnoDestino(String tlfnoDestino) {
		this.tlfnoDestino = tlfnoDestino;
	}

	public List<DefItem> getListaTipos() {
		return listaTipos;
	}

	public void setListaTipos(List<DefItem> listaTipos) {
		this.listaTipos = listaTipos;
	}

	public List<OptionItem> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<OptionItem> opciones) {
		this.opciones = opciones;
	}

	public int getNumOpciones() {
		return numOpciones;
	}

	public void setNumOpciones(int numOpciones) {
		this.numOpciones = numOpciones;
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

	public boolean isRespuestaFlagFeedback() {
		return respuestaFlagFeedback;
	}

	public void setRespuestaFlagFeedback(boolean respuestaFlagFeedback) {
		this.respuestaFlagFeedback = respuestaFlagFeedback;
	}

	public boolean isOpcionesOn() {
		return opcionesOn;
	}

	public void setOpcionesOn(boolean opcionesOn) {
		this.opcionesOn = opcionesOn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getTlfn() {
		return tlfn;
	}

	public void setTlfn(String tlfn) {
		this.tlfn = tlfn;
	}

	public String getSomeError() {
		return someError;
	}

	public void setSomeError(String someError) {
		this.someError = someError;
	}

	public String getComesFrom() {
		return comesFrom;
	}

	public void setComesFrom(String comesFrom) {
		this.comesFrom = comesFrom;
	}

	public boolean isAckOn() {
		return ackOn;
	}

	public void setAckOn(boolean ackOn) {
		this.ackOn = ackOn;
	}
}
