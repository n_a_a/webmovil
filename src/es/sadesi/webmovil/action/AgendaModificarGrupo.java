package es.sadesi.webmovil.action;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;

public class AgendaModificarGrupo extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory
			.getLog(AgendaModificarGrupo.class);

	private Contacto grupo;
	private Contacto grupoActual;
	private String accion;
	
	private String idGrupoSel;

	public void validate() {
		setSeccion("agenda");
		if ("Aceptar".equals(accion)) {
			Usuario usuario = getUsuarioRemoto();
			Emisor emisor = usuario.getEmisor();
			try {
				Map<String, Object> datosGrupo = getP3sAgenManager()
						.agendaObtieneGrupo(usuario.getId(),
								emisor.getEmisor(), grupo.getIdGrupo());
				grupoActual = new Contacto();
				grupoActual.setIdGrupo(grupo.getIdGrupo());
				grupoActual.setNombre((String) datosGrupo
						.get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE));

				String nombreGrupo = grupo.getNombre().trim();
				if (nombreGrupo == null || "".equals(nombreGrupo)) {
					addFieldError("grupo.nombre",
							"Introduzca el nuevo nombre del grupo.");
					grupo.setNombre(grupoActual.getNombre());
				}
			} catch (Exception e) {
				logger.error("Error al obtener el grupo con id '"
						+ grupo.getIdGrupo() + "' para el usuario '"
						+ usuario.getUsuario() + "'.", e);
				addActionError("Ocurrió un error al modificar el grupo.");
			}
		}
	}

	public String execute() throws Exception {
		if (logger.isDebugEnabled())
			logger.debug("Ejecutando la acción AgendaNuevoGrupo.");

		setSeccion("agenda");

		Usuario usuario = getUsuarioRemoto();
		Emisor emisor = usuario.getEmisor();

		if ("Volver".equals(accion)) {
			return "volver";
		} else if ("Aceptar".equals(accion)) {
			try {
				modificaGrupo(usuario);
			} catch (Exception e) {
				logger.error("Error al modificar el grupo con id '"
						+ grupo.getIdGrupo() + "' para el usuario '"
						+ usuario.getUsuario() + "'.", e);
				addActionError("Ocurrió un error al modificar el grupo.");
				return INPUT;
			}
			addActionMessage("La modificación del grupo se ha realizado con éxito.");
			return SUCCESS;
		}

		try {
			if (grupo.getNombre() == null) {
				Map<String, Object> datosGrupo = getP3sAgenManager()
						.agendaObtieneGrupo(usuario.getId(),
								emisor.getEmisor(), grupo.getIdGrupo());
				grupo.setNombre((String) datosGrupo
						.get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE));
			}
		} catch (Exception e) {
			logger.error(
					"Error al intentar obtener el grupo a modificar (id = "
							+ grupo.getIdGrupo() + ") para el usuario '"
							+ usuario.getUsuario() + "'.", e);
			addActionError("Ocurrió un error al intentar obtener el grupo a modificar.");
			return INPUT;
		}

		return SUCCESS;
	}

	public Contacto getGrupo() {
		return grupo;
	}

	public void setGrupo(Contacto grupo) {
		this.grupo = grupo;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	private void modificaGrupo(Usuario usuario) throws Exception {

		Emisor emisor = usuario.getEmisor();
		String nombreGrupo = grupo.getNombre().trim();
		if (nombreGrupo != null && !"".equals(nombreGrupo)
				&& !nombreGrupo.equals(grupoActual.getNombre())) {
			getP3sAgenManager().agendaModificaGrupo(usuario.getId(),
					emisor.getEmisor(), grupo.getIdGrupo(),
					P3SManagerImpl.KEY_CONTACTOS_NOMBRE, nombreGrupo);
		}
	}

	public void setIdGrupoSel(String idGrupoSel) {
		this.idGrupoSel = idGrupoSel;
	}

	public String getIdGrupoSel() {
		return idGrupoSel;
	}
}
