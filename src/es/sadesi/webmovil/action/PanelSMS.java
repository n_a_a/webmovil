package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PanelSMS extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(PanelSMS.class);
	
	public String execute() throws Exception {
		logger.info("Ejecutando acción PanelSMS");
		
		setSeccion("SMS");

		return SUCCESS;
	}
}
