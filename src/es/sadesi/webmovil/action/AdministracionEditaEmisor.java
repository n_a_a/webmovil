package es.sadesi.webmovil.action;

import java.io.File;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.sms.P3SProperties;
import es.sadesi.webmovil.model.Usuario;

public class AdministracionEditaEmisor extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory
			.getLog(AdministracionEditaEmisor.class);

	private String whatToDo = "NUEVO";
	private String idEmisor;
	private String formIp;
	private String formDescripcion;
	private String formEmail;
	private String formClave;
	private String formLimite;
	private String formLimiteBlando;
	private String formFirma;
	private String formTlfn;
	private String formTon = "1";
	private String formNpi = "1";
	private String formNavision = "";
	private String activity;
	private File formIcon;
	private String formIconFileName;
	private String uriIcon;
	private boolean formAltTextEnable = false;
	private String formAltText = "";

	private String accion;
	private int numPaginaEmisor;

	private String mensajeEstado = "";

	public void validate() {
		setSeccion("administracion");

		if (null == formTlfn)
			formTlfn = "34600123456";

		if ("Enviar".equals(accion)) {
			// Comprobaciones inmediatas:
			// idEmisor, clave, descripcion, limite, tlfn, ton y npi no vacíos.
			if (idEmisor == null || "".equals(idEmisor)
					|| formDescripcion == null || "".equals(formDescripcion) ||
					/* formEmail == null || "".equals (formEmail) || */
					formClave == null || "".equals(formClave)
					|| formLimite == null || "".equals(formLimite)
					|| formTlfn == null || "".equals(formTlfn)
					|| formTon == null || "".equals(formTon) || formNpi == null
					|| "".equals(formNpi)) {
				addFieldError("editaEmisor.general",
						"Debe rellenar todos los campos obligatorios.");
			}

			// Limitaciones de rango
			try {
				Integer.parseInt(formLimite);
			} catch (Exception e) {
				addFieldError(
						"editaEmisor.general",
						"Valor incorrecto para el límite. Debe introducir un valor menor que 2147483648");
			}

		}
	}

	public String execute() throws Exception {
		setSeccion("administracion");

		logger.info("Ejecutando acción AdministracionEditaEmisor");

		// Acciones
		if ("Enviar".equals(accion)) {
			// Vemos si es edición o creación
			Usuario user = getUsuarioRemoto();
			String emisor = user.getEmisor().getEmisor();
			if ("NUEVO".equals(whatToDo)) {
				// Crear
				try {
					getP3sManager().creaEmisor(emisor, idEmisor, formIp,
							formDescripcion, formClave, formLimite, formFirma,
							formTlfn, formTon, formNpi);
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"navision", formNavision.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"limite_blando", formLimiteBlando.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"email", formEmail.trim());
					// processIcon ();
					getP3sManager().setAltText(idEmisor, formAltText.trim());
					getP3sManager().setAltTextEnable(idEmisor,
							formAltTextEnable);
				} catch (Exception e) {
					String mensajeDeError = e.toString();
					mensajeEstado = "Ocurrió un error al crear el emisor "
							+ idEmisor
							+ " en la base de datos."
							+ mensajeDeError.substring(mensajeDeError
									.indexOf("Exception") + 10);
					logger.error(mensajeEstado);
					return INPUT;
				}
				return "emisorCreado";
			} else {
				// Editar
				try {
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"ip", formIp.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"descripcion", formDescripcion.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"email", formEmail.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"clave", formClave.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"limite", formLimite.trim());
					if (formLimiteBlando == null || "".equals(formLimiteBlando)) {
						int limiteInt = Integer.parseInt(formLimite.trim());
						formLimiteBlando = "" + (0.9 * limiteInt);
					}
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"limite_blando", formLimiteBlando.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"firma", formFirma.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"tlfn", formTlfn.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"ton", formTon.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"npi", formNpi.trim());
					getP3sManager().modificaAtributoEmisor(emisor, idEmisor,
							"navision", formNavision.trim());
					// processIcon ();
					getP3sManager().setAltText(idEmisor, formAltText.trim());
					getP3sManager().setAltTextEnable(idEmisor,
							formAltTextEnable);
				} catch (Exception e) {
					mensajeEstado = "Ocurrió un error. Revise sus datos: "
							+ e.toString();
					return INPUT;
				}
				mensajeEstado = "Los atributos del emisor " + idEmisor
						+ " fueron editados correctamente";
				uriIcon = P3SProperties.getInstance().getProperty(
						P3SProperties.URI_P3S)
						+ "IconoEmisor/" + idEmisor;
				return INPUT;
			}
		}

		// Mostrar: Si idEmisor != "NUEVO", tenemos que leer los
		// datos del WS y poblar los controles del formulario.
		if (!("NUEVO".equals(whatToDo))) {
			Map<String, Object> datosEmisor = getP3sManager().infoEmisor(
					idEmisor);
			formIp = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_IP));
			if (formIp != null)
				formIp = formIp.trim();
			else
				formIp = "";
			formDescripcion = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_DESCRIPCION));
			if (formDescripcion != null)
				formDescripcion = formDescripcion.trim();
			else
				formDescripcion = "";
			formEmail = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_EMAIL));
			if (formEmail != null)
				formEmail = formEmail.trim();
			else
				formEmail = "";
			formClave = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_CLAVE));
			if (formClave != null)
				formClave = formClave.trim();
			else
				formClave = "";
			formLimite = ((String) "" + datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_LIMITE));
			if (formLimite != null)
				formLimite = formLimite.trim();
			else
				formLimite = "0";
			formLimiteBlando = ((String) "" + datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_LIMITE_BLANDO));
			if (formLimiteBlando != null)
				formLimiteBlando = formLimiteBlando.trim();
			else
				formLimiteBlando = "0";
			formFirma = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_FIRMA));
			if (formFirma != null)
				formFirma = formFirma.trim();
			else
				formFirma = "";
			formTlfn = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_TELEFONO));
			if (formTlfn != null)
				formTlfn = formTlfn.trim();
			else
				formTlfn = "";
			formTon = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_TON));
			if (formTon != null)
				formTon = formTon.trim();
			else
				formTon = "";
			formNpi = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_NPI));
			if (formNpi != null)
				formNpi = formNpi.trim();
			else
				formNpi = "";
			formNavision = ((String) datosEmisor
					.get(P3SManagerImpl.KEY_INFO_EMISOR_NAVISION));
			if (formNavision != null)
				formNavision = formNavision.trim();
			else
				formNavision = "";
			uriIcon = P3SProperties.getInstance().getProperty(
					P3SProperties.URI_P3S)
					+ "IconoEmisor/" + idEmisor;
			formAltText = getP3sManager().getAltText(idEmisor);
			if (formAltText != null)
				formAltText = formAltText.trim();
			else
				formAltText = "";
			formAltTextEnable = getP3sManager().getAltTextEnable(idEmisor);
		} else {
			uriIcon = "#";
		}

		return SUCCESS;
	}

	/*
	 * private void processIcon () throws Exception { // Aquí: Si hay algo en el
	 * campo formIconFile, subirlo a la BD para el emisor idEmisor. // Esto se
	 * hace a pelo a falta de mejores formas. Es guarrisucio, pero bueno. if
	 * (formIconFileName != null && !"".equals(formIconFileName) ) { LOG.info
	 * (formIconFileName);
	 * 
	 * // Comprobar que es de 256x256 int width, height; try { BufferedImage
	 * bimg = ImageIO.read(formIcon); width = bimg.getWidth (); height =
	 * bimg.getHeight (); } catch (Exception e) { logger.error (e); throw new
	 * Exception ("La imagen subida no es válida."); } if (width != 256 ||
	 * height != 256) throw new Exception
	 * ("La imagen debe ser de 256x256 píxels.");
	 * 
	 * Connection con = null; PreparedStatement ps = null; ResultSet rs = null;
	 * UtilBD utilBD = null; try { utilBD = new UtilBD (
	 * P3SProperties.getInstance().getProperty(P3SProperties.BD_DRIVER),
	 * P3SProperties.getInstance().getProperty(P3SProperties.BD_URL),
	 * P3SProperties.getInstance().getProperty(P3SProperties.BD_USER),
	 * P3SProperties.getInstance().getProperty(P3SProperties.BD_PASS),
	 * Integer.parseInt
	 * (P3SProperties.getInstance().getProperty(P3SProperties.BD_MINLIMIT)),
	 * Integer
	 * .parseInt(P3SProperties.getInstance().getProperty(P3SProperties.BD_MAXLIMIT
	 * )), Long.parseLong(P3SProperties.getInstance().getProperty(P3SProperties.
	 * BD_TIME_BETWEEN_EVICTION_RUNS_MILLIS)),
	 * Integer.parseInt(P3SProperties.getInstance
	 * ().getProperty(P3SProperties.BD_NUM_TESTS_PER_EVICTION)),
	 * Long.parseLong(P3SProperties
	 * .getInstance().getProperty(P3SProperties.BD_MIN_EVICTABLE_IDLE_TIME_MILLIS
	 * )), Long.parseLong(P3SProperties.getInstance().getProperty(P3SProperties.
	 * BD_MAX_WAIT)) );
	 * 
	 * con = utilBD.getConnection (); con.setAutoCommit(false); ps =
	 * con.prepareStatement
	 * ("UPDATE p3s_emisores SET icon = ? WHERE id_emisor = ?"); ps.setString
	 * (2, idEmisor); InputStream input = new BufferedInputStream(new
	 * FileInputStream(formIcon)); ps.setBinaryStream (1, input, (int)
	 * formIcon.length ()); ps.executeUpdate(); con.commit(); } catch (Exception
	 * e) { throw e; } finally { if (utilBD != null) {
	 * utilBD.closeAll("processIcon", rs, ps, con); } } } }
	 */
	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getFormIp() {
		return formIp;
	}

	public void setFormIp(String formIp) {
		this.formIp = formIp;
	}

	public String getFormDescripcion() {
		return formDescripcion;
	}

	public void setFormDescripcion(String formDescripcion) {
		this.formDescripcion = formDescripcion;
	}

	public String getFormClave() {
		return formClave;
	}

	public void setFormClave(String formClave) {
		this.formClave = formClave;
	}

	public String getFormLimite() {
		return formLimite;
	}

	public void setFormLimite(String formLimite) {
		this.formLimite = formLimite;
	}

	public String getFormFirma() {
		return formFirma;
	}

	public void setFormFirma(String formFirma) {
		this.formFirma = formFirma;
	}

	public String getFormTlfn() {
		return formTlfn;
	}

	public void setFormTlfn(String formTlfn) {
		this.formTlfn = formTlfn;
	}

	public String getFormTon() {
		return formTon;
	}

	public void setFormTon(String formTon) {
		this.formTon = formTon;
	}

	public String getFormNpi() {
		return formNpi;
	}

	public void setFormNpi(String formNpi) {
		this.formNpi = formNpi;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setNumPaginaEmisor(int numPaginaEmisor) {
		this.numPaginaEmisor = numPaginaEmisor;
	}

	public int getNumPaginaEmisor() {
		return numPaginaEmisor;
	}

	public void setMensajeEstado(String mensajeEstado) {
		this.mensajeEstado = mensajeEstado;
	}

	public String getMensajeEstado() {
		return mensajeEstado;
	}

	public String getWhatToDo() {
		return whatToDo;
	}

	public void setWhatToDo(String whatToDo) {
		this.whatToDo = whatToDo;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getActivity() {
		return activity;
	}

	public void setFormNavision(String formNavision) {
		this.formNavision = formNavision;
	}

	public String getFormNavision() {
		return formNavision;
	}

	public String getFormEmail() {
		return formEmail;
	}

	public void setFormEmail(String formEmail) {
		this.formEmail = formEmail;
	}

	public String getFormLimiteBlando() {
		return formLimiteBlando;
	}

	public void setFormLimiteBlando(String formLimiteBlando) {
		this.formLimiteBlando = formLimiteBlando;
	}

	public File getFormIcon() {
		return formIcon;
	}

	public void setFormIcon(File formIconFile) {
		this.formIcon = formIconFile;
	}

	public String getFormIconFileName() {
		return formIconFileName;
	}

	public void setFormIconFileName(String formIconFileName) {
		this.formIconFileName = formIconFileName;
	}

	public String getUriIcon() {
		return uriIcon;
	}

	public void setUriIcon(String uriIcon) {
		this.uriIcon = uriIcon;
	}

	public boolean isFormAltTextEnable() {
		return formAltTextEnable;
	}

	public void setFormAltTextEnable(boolean formAltTextEnable) {
		this.formAltTextEnable = formAltTextEnable;
	}

	public String getFormAltText() {
		return formAltText;
	}

	public void setFormAltText(String formAltText) {
		this.formAltText = formAltText;
	}

}
