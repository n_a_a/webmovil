package es.sadesi.webmovil.action;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SProperties;
import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilWebmovil;

public class EnvioMasivo extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(EnvioMasivo.class);

	private static String rutaEnvioMasivo = P3SProperties.getInstance()
			.getProperty(P3SProperties.RUTA_ENVIO_MASIVO);

	// Formulario
	private String formFirmaEmisor;
	private File formDestinatarios;
	private String formDestinatariosFileName;
	private String formMes;
	private String formMensaje;
	private String formFirma;
	private String formTxtFirma;
	private String formPerFirma;
	private String formProgDia;
	private String formProgMes;
	private String formProgAnno;
	private String formProgHoras;
	private String formProgMinutos;
	private String formTitulo;
	private String formAccion = "";
	private String formIdGrupo = "";
	private String formNombreGrupo = "";
	private String formAcuseRecibo = "";
	private String formAlfanumerico = "";
	private String formTipoMensaje = "";

	// Cosas calculadas
	private String[] listaFicherosAnteriores = { "Seleccione fichero" };
	private int contCaracteres;
	private int contMensajes;
	private String firmaCorporativa;
	private String alfa = null;
	private boolean canSetAlfa = false;

	public String execute() throws Exception {
		logger.info("Ejecutando acción EnvioMasivo");

		setSeccion("SMS");

		// Rellenamos campos automáticos

		Usuario usuario = getUsuarioRemoto();

		// Primero, por robustez, comprobamos que existen todos los directorios
		// en juego: queue, users, users/<employeenumber>/dest y
		// users/<employeenumber>/log
		// Creamos los que no existan.

		UtilWebmovil.compruebaSiExisteYSiNoCrea(rutaEnvioMasivo + "queue");
		UtilWebmovil.compruebaSiExisteYSiNoCrea(rutaEnvioMasivo + "users");
		UtilWebmovil.compruebaSiExisteYSiNoCrea(rutaEnvioMasivo + "users/"
				+ usuario.getId());
		UtilWebmovil.compruebaSiExisteYSiNoCrea(rutaEnvioMasivo + "users/"
				+ usuario.getId() + "/dest");
		UtilWebmovil.compruebaSiExisteYSiNoCrea(rutaEnvioMasivo + "users/"
				+ usuario.getId() + "/log");

		// Lista de archivos .dest ya existentes
		File dir = new File(rutaEnvioMasivo + "users/" + usuario.getId()
				+ "/dest");
		String[] children = dir.list(); if (children == null) children = new String [0];
		String[] temp = new String[listaFicherosAnteriores.length
				+ children.length];
		temp[0] = listaFicherosAnteriores[0];
		for (int i = 0; i < children.length; i++) {
			temp[1 + i] = children[i];
		}
		listaFicherosAnteriores = temp;

		if (formPerFirma == null)
			formPerFirma = "true";

		if (formAccion == null || "".equals(formAccion)) {

			// Firma del usuario
			if (formTxtFirma == null || "".equals(formTxtFirma))
				formTxtFirma = usuario.getFirmaPersonal();

			// Firma corporativa
			firmaCorporativa = usuario.getEmisor().getFirma();
			
			// Permiso de alfanuméricos
			alfa = null;
			canSetAlfa = false;
			try {
				String operacion = "obtieneAlfanumericoNew";
				HashMap<String,Object> args = new HashMap<String,Object>();
				args.put("idEmisor", usuario.getEmisor().getEmisor());
				HashMap<String,Object> lecturaBD = null;
				lecturaBD = getP3sManager().generalReadBD(operacion, args);
				if (lecturaBD != null) {
					alfa = (String)lecturaBD.get("resultado");
					canSetAlfa = (alfa != null && !"".equals(alfa));
				}
			} catch (Exception e) {
				logger.warn("Ocurrió una excepción al tratar de obtener el alfanumérico asociado al emisor del usuario", e);
			}

			// Hora del envío: siguiente cuarto de hora
			Calendar cal = Calendar.getInstance();
			int minutoActual = cal.get(Calendar.MINUTE);
			int cuartoDeHoraActual = 15 * (minutoActual / 15); // para minuto =
																// 17 da minuto
																// = 15, etc.

			// Establecemos la hora al cuarto de hora actual:
			cal.set(Calendar.MINUTE, cuartoDeHoraActual);

			// Sumamos 15 minutos:
			cal.add(Calendar.MINUTE, 15);

			// Obtenemos todo:
			int mesActual = 1 + cal.get(Calendar.MONTH);
			int annoActual = cal.get(Calendar.YEAR);
			int diaActual = cal.get(Calendar.DAY_OF_MONTH);
			int horaActual = cal.get(Calendar.HOUR_OF_DAY);
			minutoActual = cal.get(Calendar.MINUTE);

			logger.debug("" + diaActual + "/" + mesActual + "/" + annoActual
					+ ", " + horaActual + ":" + minutoActual);

			// Damos valores a los controles del formulario:
			formProgDia = "" + diaActual;
			formProgMes = mesActual < 10 ? "0" + mesActual : "" + mesActual;
			formProgAnno = "" + annoActual;
			formProgHoras = horaActual < 10 ? "0" + horaActual : ""
					+ horaActual;
			formProgMinutos = minutoActual < 10 ? "0" + minutoActual : ""
					+ minutoActual;

			return INPUT;
		} else {
			// Se pulsó el botón
			// Procesamos formDestinatarios

			if (formMensaje == null || "".equals(formMensaje)) {
				addFieldError("envioMasivo",
						"ERROR : Debe especificar un texto para el mensaje.");
				return INPUT;
			} else {
				// Una vez llegados a este punto, copiamos el archivo .dest
				// enviado
				// en users/<employeenumber>/dest (hay que cambiar la extensión
				// por 'dest'

				String nombreArchivoDest = "";
				String filePath = "";

				// Comprobamos si subimos uno nuevo o elegimos uno existente, o
				// usamos un
				// grupo de la agenda:

				if (formDestinatariosFileName != null
						&& !"".equals(formDestinatariosFileName)) {
					// Subir archivo

					formDestinatariosFileName = sanitizeString(formDestinatariosFileName);

					if (formDestinatariosFileName.lastIndexOf(".") != -1) {
						nombreArchivoDest = formDestinatariosFileName
								.substring(0, formDestinatariosFileName
										.lastIndexOf("."))
								+ ".dest";
					} else {
						nombreArchivoDest = formDestinatariosFileName + ".dest";
					}

					try {
						filePath = rutaEnvioMasivo + "users/" + usuario.getId()
								+ "/dest";
						logger.debug("Copiando " + nombreArchivoDest + " a "
								+ filePath);
						File fileToCreate = new File(filePath,
								nombreArchivoDest);
						FileUtils.copyFile(formDestinatarios, fileToCreate);
					} catch (Exception e) {
						logger.error("Ocurrió un error al copiar "
								+ nombreArchivoDest + " en " + filePath + ": "
								+ e.toString());
						addFieldError(
								"envioMasivo",
								"ERROR : Ocurrió un error copiando el archivo de destinatarios. El administrador ha sido notificado. Disculpe las molestias.");
						return INPUT;
					}
				} else if (formMes != null && !"".equals(formMes)
						&& !"Seleccione fichero".equals(formMes)) {
					// Emplear archivo existente

					nombreArchivoDest = formMes;
					filePath = rutaEnvioMasivo + "users/" + usuario.getId()
							+ "/dest";
				} else if (formIdGrupo != null && !"".equals(formIdGrupo)) {
					// Construir el archivo de destinatarios con el grupo
					// formIdGrupo de la agenda
					// Obtenemos todos los teléfonos del grupo seleccionado
					// (recursivamente)

					List<String[]> destinatarios = UtilWebmovil
							.validaDestinatario(formNombreGrupo, null,
									formIdGrupo, usuario);

					// Abrimos un archivo .dest para escritura

					filePath = rutaEnvioMasivo + "users/" + usuario.getId()
							+ "/dest";
					String nombreArchivoDestGenerado = sanitizeString(
							formNombreGrupo.replace(".", "_"))
							.replace(" ", "_") + "_" + formIdGrupo + ".dest";

					File fileToCreate = new File(filePath,
							nombreArchivoDestGenerado);
					Writer out = new BufferedWriter(
							new FileWriter(fileToCreate));

					// Escribimos los números

					Iterator<String[]> iter = destinatarios.iterator();
					try {
						while (iter.hasNext()) {
							String destino = iter.next()[0];
							out.write(destino + "\n");
						}
					} catch (Exception e) {
						logger.error("Ocurrió un error al generar el archivo dest "
								+ nombreArchivoDestGenerado
								+ " en "
								+ filePath
								+ ": " + e.toString());
						addFieldError(
								"envioMasivo",
								"ERROR : Ocurrió un error programando el envío. El administrador ha sido notificado. Disculpe las molestias.");
						return INPUT;
					} finally {
						out.close();
					}

					nombreArchivoDest = nombreArchivoDestGenerado;
				} else {
					// Si no tenemos a quien mandar...

					addFieldError("envioMasivo",
							"ERROR : Debe especificar un archivo o grupo de destinatarios.");
					return INPUT;
				}

				// Vamos a comprobar el límite si es el mes actual
				Calendar cal = Calendar.getInstance();
				int mesActual = 1 + cal.get(Calendar.MONTH);
				String mesActualS = mesActual < 10 ? "0" + mesActual : ""
						+ mesActual;
				if (mesActualS.equals(formProgMes)) {
					logger.info("Programando para el mes actual.");

					int enviosNacionalesEnLista = 0;
					int enviosInternacionalesEnLista = 0;
					int limiteNacional = usuario.getMaxNacional();
					int limiteInternacional = usuario.getMaxInternacional();
					int enviadosNacional = getP3sManager().estadisticasUsuario(
							usuario.getUsuario(),
							usuario.getEmisor().getEmisor(),
							"enviadosNacionalesContador", "", "");
					int enviadosInternacional = getP3sManager()
							.estadisticasUsuario(usuario.getUsuario(),
									usuario.getEmisor().getEmisor(),
									"enviadosInternacionalesContador", "", "");

					// Recorremos todos los destinatarios y acumulamos
					try {
						File archivoLog = new File(rutaEnvioMasivo + "users/"
								+ usuario.getId() + "/dest", nombreArchivoDest);
						BufferedReader input = new BufferedReader(
								new FileReader(archivoLog));
						try {
							String linea = "";
							while ((linea = input.readLine()) != null) {
								// Vemos el tipo de este teléfono
								String[] telefono = { "", "" };
								try {
									telefono = UtilWebmovil
											.validaNumeroTelefono(linea.trim());
								} catch (Exception e) {
									//
								}
								if (ConstantesWebmovil.TIPO_NUMERO_INTERNACIONAL
										.equals(telefono[0])) {
									enviosInternacionalesEnLista++;
									logger.debug(linea + " es internacional.");
								} else {
									enviosNacionalesEnLista++;
									logger.debug(linea + " es nacional.");
								}
							}
						} catch (Exception e) {
							logger.error("Ocurrió un error leyendo el archivo del envío en "
									+ rutaEnvioMasivo
									+ "users/"
									+ usuario.getId()
									+ "/dest/"
									+ nombreArchivoDest + ":" + e.toString());
						} finally {
							input.close ();
						}
					} catch (Exception e) {
						logger.error("Ocurrió un error leyendo el archivo del envío en "
								+ rutaEnvioMasivo
								+ "users/"
								+ usuario.getId()
								+ "/dest/"
								+ nombreArchivoDest
								+ ":"
								+ e.toString());
					}

					// Comprobamos
					boolean limitePocho = false;

					if (enviadosNacional + enviosNacionalesEnLista > limiteNacional) {
						addFieldError(
								"envioMasivo",
								"ERROR : Este envío hace que se exceda su límite de mensajes a destinos nacionales.");
						limitePocho = true;
					}

					if (enviadosInternacional + enviosInternacionalesEnLista > limiteInternacional) {
						addFieldError(
								"envioMasivo",
								"ERROR : Este envío hace que se exceda su límite de mensajes a destinos internacionales.");
						limitePocho = true;
					}

					if (limitePocho) {
						return INPUT;
					}
				} else {
					logger.info("Programando para un mes fuera del período de facturación.");
				}

				// Y generamos el archivo de cola en users/queue/

				// El archivo de cola en users/queue tiene el siguiente nombre
				// de archivo:
				// <employeenumber>#<identificador>#<fechayhoratojunto>.msg

				if (formTitulo == null || "".equals(formTitulo)) {
					formTitulo = "ENVIO_PROGRAMADO";
				}

				// Fix del formTitulo: sustituir espacios o puntos por _.

				formTitulo = sanitizeString(formTitulo);
				formTitulo = formTitulo.replace(" ", "_");
				formTitulo = formTitulo.replace(".", "_");

				// Construimos el nombre del archivo queue:

				if (formProgMes.length() == 1)
					formProgMes = "0" + formProgMes;

				if (formProgDia.length() == 1)
					formProgDia = "0" + formProgDia;

				String nombreArchivoQueue = usuario.getId() + "#" + formTitulo
						+ "#" + formProgAnno + formProgMes + formProgDia
						+ formProgHoras + formProgMinutos + ".msg";
				filePath = rutaEnvioMasivo + "queue";

				// Contiene seis lineas de texto:
				// usuario
				// emisor
				// archivo.dest
				// texto común del mensaje
				// acuse de recibo
				// push (0: sms, 1: push, 2: auto)

				logger.info("creado " + nombreArchivoQueue + " en " + filePath);
				File fileToCreate = new File(filePath, nombreArchivoQueue);
				Writer out = new BufferedWriter(new FileWriter(fileToCreate));

				// Eliminamos saltos de linea y tabulaciones del mensaje de texto a enviar
				if(formMensaje.contains("\r\n")) {
					logger.info("Eliminamos salto de linea \\r\\n del mensaje de texto a enviar");
					formMensaje = formMensaje.replace("\r\n", " ");
				}
				if(formMensaje.contains("\n")) {
					logger.info("Eliminamos salto de linea \\n del mensaje de texto a enviar");
					formMensaje = formMensaje.replace("\n", " ");
				}
				if(formMensaje.contains("\r")) {
					logger.info("Eliminamos salto de linea \\r del mensaje de texto a enviar");
					formMensaje = formMensaje.replace("\r", " ");
				}
				if(formMensaje.contains("\t")) {
					logger.info("Eliminamos tabulaciones \\t del mensaje de texto a enviar");
					formMensaje = formMensaje.replace("\t", " ");
				}
				
				String mensajeCompleto = "";
				mensajeCompleto = mensajeCompleto + formMensaje;
				if ("true".equals(formFirma)) {
					mensajeCompleto += formTxtFirma;
				}
				if ("true".equals(formPerFirma)) {
					mensajeCompleto += firmaCorporativa;
				}

				try {
					out.write(usuario.getUsuario() + "\n");
					out.write(usuario.getEmisor().getEmisor() + "\n");
					out.write(nombreArchivoDest + "\n");
					out.write(mensajeCompleto + "\n");
					if ("true".equals(formAcuseRecibo))
						out.write("1\n");
					else
						out.write("0\n");
					int tipoInt = 2;
					try {
						tipoInt = Integer.parseInt(formTipoMensaje);
					} catch (Exception e) {
					}
					out.write("" + tipoInt + "\n");
					if ("true".equals(formAlfanumerico))
						out.write ("1\n");
					else
						out.write ("0\n");
				} catch (Exception e) {
					logger.error("Ocurrió un error al general el archivo de queue "
							+ nombreArchivoQueue
							+ " en "
							+ filePath
							+ ": "
							+ e.toString());
					addFieldError(
							"envioMasivo",
							"ERROR : Ocurrió un error programando el envío. El administrador ha sido notificado. Disculpe las molestias.");
					return INPUT;
				} finally {
					out.close();
				}
			}
			return SUCCESS;
		}
	}

	public String getFormFirmaEmisor() {
		return formFirmaEmisor;
	}

	public void setFormFirmaEmisor(String formFirmaEmisor) {
		this.formFirmaEmisor = formFirmaEmisor;
	}

	public File getFormDestinatarios() {
		return formDestinatarios;
	}

	public void setFormDestinatarios(File formDestinatarios) {
		this.formDestinatarios = formDestinatarios;
	}

	public String getFormMes() {
		return formMes;
	}

	public void setFormMes(String formMes) {
		this.formMes = formMes;
	}

	public String getFormMensaje() {
		return formMensaje;
	}

	public void setFormMensaje(String formMensaje) {
		this.formMensaje = formMensaje;
	}

	public String getFormFirma() {
		return formFirma;
	}

	public void setFormFirma(String formFirma) {
		this.formFirma = formFirma;
	}

	public String getFormTxtFirma() {
		return formTxtFirma;
	}

	public void setFormTxtFirma(String formTxtFirma) {
		this.formTxtFirma = formTxtFirma;
	}

	public String getFormPerFirma() {
		return formPerFirma;
	}

	public void setFormPerFirma(String formPerFirma) {
		this.formPerFirma = formPerFirma;
	}

	public String getFormProgDia() {
		return formProgDia;
	}

	public void setFormProgDia(String formProgDia) {
		this.formProgDia = formProgDia;
	}

	public String getFormProgMes() {
		return formProgMes;
	}

	public void setFormProgMes(String formProgMes) {
		this.formProgMes = formProgMes;
	}

	public String getFormProgAnno() {
		return formProgAnno;
	}

	public void setFormProgAnno(String formProgAnno) {
		this.formProgAnno = formProgAnno;
	}

	public String getFormProgHoras() {
		return formProgHoras;
	}

	public void setFormProgHoras(String formProgHoras) {
		this.formProgHoras = formProgHoras;
	}

	public String getFormProgMinutos() {
		return formProgMinutos;
	}

	public void setFormProgMinutos(String formProgMinutos) {
		this.formProgMinutos = formProgMinutos;
	}

	public String getFormTitulo() {
		return formTitulo;
	}

	public void setFormTitulo(String formTitulo) {
		this.formTitulo = formTitulo;
	}

	public void setListaFicherosAnteriores(String[] listaFicherosAnteriores) {
		this.listaFicherosAnteriores = listaFicherosAnteriores;
	}

	public String[] getListaFicherosAnteriores() {
		return listaFicherosAnteriores;
	}

	public void setContCaracteres(int contCaracteres) {
		this.contCaracteres = contCaracteres;
	}

	public int getContCaracteres() {
		return contCaracteres;
	}

	public void setContMensajes(int contMensajes) {
		this.contMensajes = contMensajes;
	}

	public int getContMensajes() {
		return contMensajes;
	}

	public void setFirmaCorporativa(String firmaCorporativa) {
		this.firmaCorporativa = firmaCorporativa;
	}

	public String getFirmaCorporativa() {
		return firmaCorporativa;
	}

	public void setFormAccion(String formAccion) {
		this.formAccion = formAccion;
	}

	public String getFormAccion() {
		return formAccion;
	}

	public static void setRutaEnvioMasivo(String rutaEnvioMasivo) {
		EnvioMasivo.rutaEnvioMasivo = rutaEnvioMasivo;
	}

	public static String getRutaEnvioMasivo() {
		return rutaEnvioMasivo;
	}

	public void setFormDestinatariosFileName(String formDestinatariosFileName) {
		this.formDestinatariosFileName = formDestinatariosFileName;
	}

	public String getFormDestinatariosFileName() {
		return formDestinatariosFileName;
	}

	public String getFormIdGrupo() {
		return formIdGrupo;
	}

	public void setFormIdGrupo(String formIdGrupo) {
		this.formIdGrupo = formIdGrupo;
	}

	public String getFormNombreGrupo() {
		return formNombreGrupo;
	}

	public void setFormNombreGrupo(String formNombreGrupo) {
		this.formNombreGrupo = formNombreGrupo;
	}

	private String sanitizeString(String cad) {
		char[] stringArray;
		stringArray = cad.toCharArray();

		for (int i = 0; i < stringArray.length; i++) {
			if (!((stringArray[i] >= '0' && stringArray[i] <= '9')
					|| (stringArray[i] >= 'A' && stringArray[i] <= 'Z')
					|| (stringArray[i] >= 'a' && stringArray[i] <= 'z') || (stringArray[i] == '.'))) {
				stringArray[i] = '_';
			}
		}

		String result = new String(stringArray);
		return result;
	}

	public String getFormAcuseRecibo() {
		return formAcuseRecibo;
	}

	public void setFormAcuseRecibo(String formAcuseRecibo) {
		this.formAcuseRecibo = formAcuseRecibo;
	}

	public String getFormTipoMensaje() {
		return formTipoMensaje;
	}

	public void setFormTipoMensaje(String formTipoMensaje) {
		this.formTipoMensaje = formTipoMensaje;
	}

	public String getFormAlfanumerico() {
		return formAlfanumerico;
	}

	public void setFormAlfanumerico(String formAlfanumerico) {
		this.formAlfanumerico = formAlfanumerico;
	}

	public String getAlfa() {
		return alfa;
	}

	public void setAlfa(String alfa) {
		this.alfa = alfa;
	}

	public boolean isCanSetAlfa() {
		return canSetAlfa;
	}

	public void setCanSetAlfa(boolean canSetAlfa) {
		this.canSetAlfa = canSetAlfa;
	}

}
