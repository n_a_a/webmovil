package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NotificacionesJDA extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog (NotificacionesJDA.class);
	
	public String execute() throws Exception {
		logger.info ("NotificacionesJDA");
		setSeccion("notificacionesjda");
		return SUCCESS;
	}
}
