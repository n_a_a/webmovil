package es.sadesi.webmovil.action;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;

public class AgendaNuevoGrupo extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AgendaNuevoGrupo.class);

	private String nombreGrupo;
	private List<Contacto> grupos;
	private String idGrupoSel;
	private String accion;
	
	public void validate() {
		setSeccion("agenda");
		if ("Aceptar".equals(accion)) {
			if (nombreGrupo == null || "".equals(nombreGrupo)) {
				addFieldError("nombreGrupo", "Introduzca el nombre del grupo.");
				grupos = getAgendaManager().getGrupos(getUsuarioRemoto());
			}
		}
	}

	public String execute() throws Exception {
		if (logger.isDebugEnabled())
			logger.debug("Ejecutando la acción AgendaNuevoGrupo.");
		
		setSeccion("agenda");

		Usuario usuario = getUsuarioRemoto();

		if ("Volver".equals(accion)) {
			return "volver";
		} if ("Aceptar".equals(accion)) {
			try {
				creaGrupo(usuario);
			} catch (Exception e) {
				logger.error("Error al crear el grupo '" + nombreGrupo
						+ "' en el grupo con id '" + idGrupoSel
						+ "' para el usuario '" + usuario.getUsuario() + "'.",
						e);
				addActionError("Ocurrió un error al crear el grupo '"
						+ nombreGrupo + "'.");
				return INPUT;
			}
			return "grupoCreado";
		}

		grupos = getAgendaManager().getGrupos(getUsuarioRemoto());

		return SUCCESS;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public List<Contacto> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Contacto> grupos) {
		this.grupos = grupos;
	}

	public String getIdGrupoSel() {
		return idGrupoSel;
	}

	public void setIdGrupoSel(String idGrupoSel) {
		this.idGrupoSel = idGrupoSel;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	private void creaGrupo(Usuario usuario) throws Exception {

		Emisor emisor = usuario.getEmisor();
		getP3sAgenManager().agendaCreaGrupo(usuario.getId(),
				emisor.getEmisor(), idGrupoSel, nombreGrupo);
	}
}
