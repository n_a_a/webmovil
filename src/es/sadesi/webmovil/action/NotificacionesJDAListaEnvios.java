package es.sadesi.webmovil.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.sms.P3SProperties;
import es.sadesi.sms.ws.cliente.SmsWS;
import es.sadesi.sms.ws.cliente.SmsWSServiceLocator;
import es.sadesi.webmovil.model.Attachment;
import es.sadesi.webmovil.model.Notificacion;
import es.sadesi.webmovil.model.Usuario;

public class NotificacionesJDAListaEnvios extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog (NotificacionesJDAListaEnvios.class);
	private static String claveEmisor = P3SProperties.getInstance().getProperty(P3SProperties.CLAVE_EMISOR);
	
	private String accion;
	
	private int numNotificaciones;
	private int page;
	private Vector<Notificacion> notificaciones;
	
	private int numItems;
	private String someError;
	
	private int maxPage;
	private int itemsPerPage;
	
	private int regFin;
	private int regIni;
		
	public String execute() throws Exception {
		@SuppressWarnings("unchecked")
		Map<String, Object> session = (Map<String, Object>) ActionContext.getContext ().get ("session");
		
		Usuario usuario = getUsuarioRemoto();
		itemsPerPage = usuario.getItemPorPagina();
		
		logger.info ("NotificacionesJDAListaEnvios");
		setSeccion("notificacionesjda");
				
		// Instanciar cliente de Webservices
		SmsWSServiceLocator locator;
		SmsWS smsWS = null;
		try {
			locator = new SmsWSServiceLocator ();
			smsWS = locator.getP3SWebmovilWS ();
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción al crear instancia del cliente de WS!");
			setSomeError("Error fatal. Contacte con su administrador: " + e.getMessage ());
		}
		
		// Calculate maxPage / do page stuff - cached in session!
		if (null == session.get ("NotificacionesJDAListaEnviosNumItems")) {
			try {
				setNumItems(smsWS.p3SPushGetNumNotificationesUser(
						usuario.getEmisor ().getEmisor (),
						usuario.getUsuario (),
						claveEmisor));
			} catch (Exception e) {
				logger.error ("Ocurrió una excepción al intentar obtener el número de notificaciones enviadas por el usuario" + e.getMessage ());
			}
			session.put ("NotificacionesJDAListaEnviosNumItems", new Integer (getNumItems ()));
		} else {
			setNumItems (((Integer) session.get("NotificacionesJDAListaEnviosNumItems")).intValue ());
		}
		if (numItems == 0) maxPage = 0; else maxPage = (numItems - 1) / itemsPerPage;
		
		// Page actions
		if ("pgHo".equals (accion)) {
			page = 0;
		} else if ("pgUp".equals (accion)) {
			page --;
		} else if ("pgDw".equals (accion)) {
			page ++;
		} else if ("pgEn".equals (accion)) {
			page = maxPage;
		}
		
		// Same ol' shit
		if (page > maxPage) page = maxPage;
		regIni = 1 + page * itemsPerPage;
		regFin = 1 + (page + 1) * itemsPerPage - 1;
		if (regFin >= numItems) regFin = numItems - 1; 
		
		logger.debug ("Se leerá desde " + regIni + " hasta " + regFin);
		
		// Get messages from this page
		try {
			@SuppressWarnings("unchecked")
			Vector<HashMap<String,String>> notificacionesServer = smsWS.p3SPushGetNotificacionesUser(
					usuario.getEmisor ().getEmisor (),
					usuario.getUsuario (),
					claveEmisor,
					regIni, regFin);
			
			notificaciones = new Vector<Notificacion> ();
			for (HashMap<String,String> notificacionServer : notificacionesServer) {
				Notificacion notificacion = new Notificacion ();
				try {
					// Fill in notification data
					notificacion.setId (Integer.parseInt (notificacionServer.get ("id")));
					String texte = notificacionServer.get ("texto");
					notificacion.setTexto(texte);
					int scalerson = 50; if (texte.length() > scalerson) texte = texte.substring (0, scalerson - 3) + "...";
					notificacion.setTextoCut (texte);
					logger.debug ("Leyendo notificación " + notificacion.getId () + " -> " + texte);
					notificacion.setTlfn (notificacionServer.get ("tlfn"));
					String dstring = notificacionServer.get ("ts");
					DateFormat inputFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
					Date d1 = inputFormat.parse(dstring);
					notificacion.setTs(d1.getTime());
					
					// Ahora a obtener el estado:
					
					try {
						@SuppressWarnings("unchecked")
						HashMap<String,String> mensajePush = smsWS.p3SObtieneMensaje (notificacion.getId(), notificacion.getTlfn(), 999999, "P3SKEY");
						notificacion.setEstado ((String) mensajePush.get ("estado"));
					} catch (Exception e) {
						logger.info ("Error obteniendo estado o estado no disponible: " + e.getMessage ());
						notificacion.setEstado ("X");
					}
				} catch (Exception e) {
					logger.error ("Ocurrió un error estableciendo los valores para la notificación #" + notificacion.getId ());
				}
				
				// Get attachments
				try {
					@SuppressWarnings("unchecked")
					Vector<HashMap<String,String>> adjuntosServer = smsWS.p3SPushGetAdjuntosMensajeUser(
							usuario.getEmisor ().getEmisor (),
							usuario.getUsuario (),
							claveEmisor,
							notificacion.getId ());
					Vector<Attachment> adjuntosTemp = new Vector<Attachment> ();
					int it = 0;
					for (HashMap<String,String> adjuntoServer : adjuntosServer) {
						Attachment adjuntoTemp = new Attachment (it ++);
						adjuntoTemp.setId (Integer.parseInt (adjuntoServer.get ("id")));
						adjuntoTemp.setType (adjuntoServer.get ("tipo"));
						String partIconName = adjuntoServer.get ("tipo").substring (1 + adjuntoServer.get ("tipo").lastIndexOf ("/"));
						if("msword".equals(partIconName)) {
							partIconName = "doc";
						} else if("vnd.openxmlformats-officedocument.wordprocessingml.document".equals(partIconName)) {
							partIconName = "docx";
						} else if("vnd.oasis.opendocument.text".equals(partIconName)) {
							partIconName = "odt";
						}
						adjuntoTemp.setIconName ("icon-" + partIconName + ".png");
						adjuntoTemp.setTypeName (adjuntoTemp.getType ());
						adjuntosTemp.add (adjuntoTemp);
					}
					notificacion.setAdjuntos(adjuntosTemp);
				} catch (Exception e) {
					logger.error ("Ocurrió un error obteniendo los adjuntos para la notificación #" + notificacion.getId ());
				}
				
				notificaciones.add (notificacion);
			}
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción al intentar obtener las notificaciones enviadas por el usuario de la " + regIni + " a la " + regFin + ": " + e.getMessage ());
		}
		
		return SUCCESS;
	}

	public int getNumNotificaciones() {
		return numNotificaciones;
	}

	public void setNumNotificaciones(int numNotificaciones) {
		this.numNotificaciones = numNotificaciones;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Vector<Notificacion> getNotificaciones() {
		return notificaciones;
	}

	public void setNotificaciones(Vector<Notificacion> notificaciones) {
		this.notificaciones = notificaciones;
	}

	public int getNumItems() {
		return numItems;
	}

	public void setNumItems(int numItems) {
		this.numItems = numItems;
	}

	public String getSomeError() {
		return someError;
	}

	public void setSomeError(String someError) {
		this.someError = someError;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public int getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public int getRegIni() {
		return regIni;
	}

	public void setRegIni(int regIni) {
		this.regIni = regIni;
	}

	public int getRegFin() {
		return regFin;
	}

	public void setRegFin(int regFin) {
		this.regFin = regFin;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}
