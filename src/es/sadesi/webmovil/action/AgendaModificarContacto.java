package es.sadesi.webmovil.action;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;
import es.sadesi.webmovil.util.UtilWebmovil;

public class AgendaModificarContacto extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory
			.getLog(AgendaModificarContacto.class);

	private Contacto contacto;
	private Contacto contactoActual;
	private String[] tiposTelefono = { "Nacional", "Internacional" };
	private String tipoTlfMovil = "Nacional";
	private String tipoTlfFijo = "Nacional";
	private String accion;
	
	private String idGrupoSel;

	public void validate() {
		setSeccion("agenda");
		if ("Aceptar".equals(accion)) {
			Usuario usuario = getUsuarioRemoto();
			Emisor emisor = usuario.getEmisor();
			try {
				Map<String, Object> datosContacto = getP3sAgenManager()
						.agendaObtieneContacto(usuario.getId(),
								emisor.getEmisor(), contacto.getIdContacto());
				contactoActual = new Contacto();
				contactoActual.setIdContacto(contacto.getIdContacto());
				contactoActual.setNombre((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE));
				contactoActual.setIdGrupo((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ID_GRUPO));
				contactoActual.setIdContacto((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ID_CONTACTO));
				contactoActual.setIdPadre((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ID_PADRE));
				contactoActual.setEmail((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_EMAIL));
				contactoActual.setTlfMovil((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_TLF_MOVIL));
				contactoActual.setTlfFijo((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_TLF_FIJO));
				contactoActual.setEntidad((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ENTIDAD));
				contactoActual.setNotas((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_NOTAS));

				String nombreContacto = contacto.getNombre().trim();
				if (nombreContacto == null || "".equals(nombreContacto)) {
					addFieldError("contacto.nombre",
							"Introduzca el nuevo nombre del contacto.");
					contacto.setNombre(contactoActual.getNombre());
				}

				String tlfMovilContacto = contacto.getTlfMovil().trim();
				if (tlfMovilContacto == null || "".equals(tlfMovilContacto)) {
					addFieldError("contacto.tlfMovil",
							"Introduzca un número de teléfono móvil.");
					contacto.setTlfMovil(contactoActual.getTlfMovil());
				} else {
					if ("Nacional".equals(tipoTlfMovil)) {
						try {
							UtilWebmovil.validaDestinatario(tlfMovilContacto, contacto.getIdContacto(), contacto.getIdGrupo(), usuario);
						} catch (NumberFormatException nfe) {
							addFieldError("contacto.tlfMovil", nfe.getMessage());
						}
					} else if ("Internacional".equals(tipoTlfMovil)) {
						try {
							UtilWebmovil.validaDestinatario(tlfMovilContacto, contacto.getIdContacto(), contacto.getIdGrupo(), usuario);
						} catch (NumberFormatException nfe) {
							addFieldError("contacto.tlfMovil", nfe.getMessage());
						}
					} else
						addFieldError("contacto.tlfMovil",
								"Tipo de teléfono incorrecto.");
				}

				String tlfFijoContacto = contacto.getTlfFijo().trim();
				if (tlfFijoContacto != null && !("".equals(tlfFijoContacto))
						&& !tlfFijoContacto.equals(contactoActual.getTlfFijo())) {
					if ("Nacional".equals(tipoTlfFijo)) {
						try {
							UtilWebmovil.validaDestinatario(tlfMovilContacto, contacto.getIdContacto(), contacto.getIdGrupo(), usuario);
						} catch (NumberFormatException nfe) {
							addFieldError("contacto.tlfFijo", nfe.getMessage());
						}
					} else if ("Internacional".equals(tipoTlfFijo)) {
						try {
							UtilWebmovil.validaDestinatario(tlfMovilContacto, contacto.getIdContacto(), contacto.getIdGrupo(), usuario);
						} catch (NumberFormatException nfe) {
							addFieldError("contacto.tlfFijo", nfe.getMessage());
						}
					} else
						addFieldError("contacto.tlfFijo",
								"Tipo de teléfono incorrecto.");
				}
			} catch (Exception e) {
				logger.error("Error al obtener el contacto con id '"
						+ contacto.getIdContacto() + "' para el usuario '"
						+ usuario.getUsuario() + "'.", e);
				addActionError("Ocurrió un error al modificar el contacto.");
			}
		}
	}

	public String execute() throws Exception {
		if (logger.isDebugEnabled())
			logger.debug("Ejecutando la acción AgendaNuevoGrupo.");

		setSeccion("agenda");

		Usuario usuario = getUsuarioRemoto();
		Emisor emisor = usuario.getEmisor();
		
		if ("Volver".equals(accion)) {
			return "volver";
		} else if ("Aceptar".equals(accion)) {
			try {
				String tlfMovilContacto = contacto.getTlfMovil().trim();
				String tlfFijoContacto = contacto.getTlfFijo().trim();
				
				tlfFijoContacto = tlfFijoContacto.replaceAll("[^\\d]", "");
				tlfMovilContacto = tlfMovilContacto.replaceAll("[^\\d]", "");
				
				contacto.setTlfFijo(tlfFijoContacto);
				contacto.setTlfMovil(tlfMovilContacto);
			} catch (Exception e) {
				
			}
			try {
				modificaContacto(usuario);
			} catch (Exception e) {
				logger.error("Error al modificar el contacto con id '"
						+ contacto.getIdContacto() + "' para el usuario '"
						+ usuario.getUsuario() + "'.", e);
				addActionError("Ocurrió un error al modificar el contacto.");
				return INPUT;
			}
			addActionMessage("La modificación del contacto se ha realizado con éxito.");
			return SUCCESS;
		}

		try {
			if (contacto.getNombre() == null) {
				Map<String, Object> datosContacto = getP3sAgenManager()
						.agendaObtieneContacto(usuario.getId(),
								emisor.getEmisor(), contacto.getIdContacto());
				contacto.setNombre((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE));
				contacto.setIdGrupo((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ID_GRUPO));
				contacto.setIdContacto((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ID_CONTACTO));
				contacto.setIdPadre((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ID_PADRE));
				contacto.setEmail((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_EMAIL));
				contacto.setTlfMovil((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_TLF_MOVIL));
				contacto.setTlfFijo((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_TLF_FIJO));
				contacto.setEntidad((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_ENTIDAD));
				contacto.setNotas((String) datosContacto
						.get(P3SManagerImpl.KEY_CONTACTOS_NOTAS));
			}
		} catch (Exception e) {
			logger.error(
					"Error al intentar obtener el contacto a modificar (id = "
							+ contacto.getIdContacto() + ") para el usuario '"
							+ usuario.getUsuario() + "'.", e);
			addActionError("Ocurrió un error al intentar obtener el contacto a modificar.");
			return INPUT;
		}

		return SUCCESS;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String[] getTiposTelefono() {
		return tiposTelefono;
	}

	public void setTiposTelefono(String[] tiposTelefono) {
		this.tiposTelefono = tiposTelefono;
	}

	public String getTipoTlfMovil() {
		return tipoTlfMovil;
	}

	public void setTipoTlfMovil(String tipoTlfMovil) {
		this.tipoTlfMovil = tipoTlfMovil;
	}

	public String getTipoTlfFijo() {
		return tipoTlfFijo;
	}

	public void setTipoTlfFijo(String tipoTlfFijo) {
		this.tipoTlfFijo = tipoTlfFijo;
	}

	public Contacto getContacto() {
		return contacto;
	}

	public void setContacto(Contacto contacto) {
		this.contacto = contacto;
	}

	private void modificaContacto(Usuario usuario) throws Exception {

		Emisor emisor = usuario.getEmisor();
		String nombreContacto = contacto.getNombre().trim();
		if (nombreContacto != null && !"".equals(nombreContacto)
				&& !nombreContacto.equals(contactoActual.getNombre())) {
			getP3sAgenManager().agendaModificaContacto(usuario.getId(),
					emisor.getEmisor(), contacto.getIdContacto(),
					P3SManagerImpl.KEY_CONTACTOS_NOMBRE, nombreContacto);
		}
		String tlfMovilContacto = contacto.getTlfMovil().trim();
		if (tlfMovilContacto != null && !"".equals(tlfMovilContacto)
				&& !tlfMovilContacto.equals(contactoActual.getTlfMovil())) {
			getP3sAgenManager().agendaModificaContacto(usuario.getId(),
					emisor.getEmisor(), contacto.getIdContacto(),
					P3SManagerImpl.KEY_CONTACTOS_TLF_MOVIL, tlfMovilContacto);
		}
		String tlfFijoContacto = contacto.getTlfFijo().trim();
		if (tlfFijoContacto != null && !"".equals(tlfFijoContacto)
				&& !tlfFijoContacto.equals(contactoActual.getTlfFijo())) {
			getP3sAgenManager().agendaModificaContacto(usuario.getId(),
					emisor.getEmisor(), contacto.getIdContacto(),
					P3SManagerImpl.KEY_CONTACTOS_TLF_FIJO, tlfFijoContacto);
		}
		String emailContacto = contacto.getEmail().trim();
		if (emailContacto != null
				&& !emailContacto.equals(contactoActual.getEmail())) {
			getP3sAgenManager().agendaModificaContacto(usuario.getId(),
					emisor.getEmisor(), contacto.getIdContacto(),
					P3SManagerImpl.KEY_CONTACTOS_EMAIL, emailContacto);
		}
		String entidadContacto = contacto.getEntidad().trim();
		if (entidadContacto != null && !"".equals(entidadContacto)
				&& !entidadContacto.equals(contactoActual.getEntidad())) {
			getP3sAgenManager().agendaModificaContacto(usuario.getId(),
					emisor.getEmisor(), contacto.getIdContacto(),
					P3SManagerImpl.KEY_CONTACTOS_ENTIDAD, entidadContacto);
		}
		String notasContacto = contacto.getNotas().trim();
		if (notasContacto != null && !"".equals(notasContacto)
				&& !notasContacto.equals(contactoActual.getNotas())) {
			getP3sAgenManager().agendaModificaContacto(usuario.getId(),
					emisor.getEmisor(), contacto.getIdContacto(),
					P3SManagerImpl.KEY_CONTACTOS_NOTAS, notasContacto);
		}
	}

	public void setIdGrupoSel(String idGrupoSel) {
		this.idGrupoSel = idGrupoSel;
	}

	public String getIdGrupoSel() {
		return idGrupoSel;
	}
}
