package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.Usuario;

public class PreferenciasUsuario extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(PreferenciasUsuario.class);
	
	private String formFirma;
	private String formMsgsPagina;
	private boolean formRecibirEmail;
	private String accion;

	public String execute() throws Exception {
		
		logger.info("Ejecutando acción PreferenciasUsuario");
		
		setSeccion("preferencias");
		
		// Obtenemos los datos necesarios del usuario de la sesión:
		
		Usuario user = getUsuarioRemoto();
		String usuario = user.getUsuario();
		String emisor = user.getEmisor().getEmisor();
		
		// Vemos si tenemos que actualizar los datos del usuario
		
		if ("Aceptar".equals (accion)) {
			// Escribimos los datos del usuario
			
			if (!"".equals (formMsgsPagina)) {
				// Llamamos al WS para actualizar el usuario
				getP3sManager().modificaAtributoUsuario(usuario, emisor, "firma", formFirma);
				getP3sManager().modificaAtributoUsuario(usuario, emisor, "msgs_pagina", formMsgsPagina);
				final String recibirEmail = formRecibirEmail ? "1" : "0";
				getP3sManager().modificaAtributoUsuario(usuario, emisor, "recibir_email", recibirEmail);
				
				// Modificamos el usuario en la sesión con los nuevos valores.
				user.setFirmaPersonal(formFirma);
				user.setItemPorPagina(Integer.parseInt(formMsgsPagina));
				user.setRecibirEmail(formRecibirEmail);
				
				// Logar como niños weno
				logger.info ("Se actualizaron las preferencias del usuario " + usuario + ".");
			}
		}
		
		// Rellenamos los formularios con la información
		// del usuario.
		
		formFirma = user.getFirmaPersonal();
		formMsgsPagina = "" + user.getItemPorPagina();
		formRecibirEmail = user.isRecibirEmail();
		
		return SUCCESS;
	}


	public void setFormFirma(String formFirma) {
		this.formFirma = formFirma;
	}


	public String getFormFirma() {
		return formFirma;
	}


	public void setFormMsgsPagina(String formMsgsPagina) {
		this.formMsgsPagina = formMsgsPagina;
	}


	public String getFormMsgsPagina() {
		return formMsgsPagina;
	}

	public boolean isFormRecibirEmail() {
		return formRecibirEmail;
	}


	public void setFormRecibirEmail(boolean formRecibirEmail) {
		this.formRecibirEmail = formRecibirEmail;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}


	public String getAccion() {
		return accion;
	}
}
