package es.sadesi.webmovil.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;

import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.model.FacturacionEmisor;

public class AdministracionFacturacionExcel extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionFacturacionExcel.class);
	
	private InputStream inputStream;

	private String showPeriod;
	private String factMonth;
	private String factYear;
	private Vector<FacturacionEmisor> facturacionStore = null;
	private String trimestreMes1 = "";
	private String trimestreMes2 = "";
	private String trimestreMes3 = "";
	
	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		logger.info("Ejecutando acción AdministracionFacturacionExcel");
		
		// Aquí iremos construyendo la tabla para exportar a excel
		String text = "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>\n<body><table>";
				
		// Construimos el texto showPeriod para mostrar en la vista.
		
		String [] meses = {	"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
				"Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
		int mes1 = 0, mes2 = 0, mes3 = 0, anyo1 = 0, anyo2 = 0, anyo3 = 0;
		
		if ("0".equals(factMonth) || "0".equals(factYear)) {
			showPeriod = "Datos del trimestre actual";
			
			// Calculamos los tres meses para mostrar.
			
			Calendar cal = Calendar.getInstance ();
			mes3 = 1 + cal.get(Calendar.MONTH);
			anyo3 = cal.get(Calendar.YEAR);
			mes2 = mes3 - 1;
			anyo2 = anyo3;
			if (mes2 == 0) {
				mes2 = 12;
				anyo2 --;
			}
			mes1 = mes2 - 1;
			anyo1 = anyo2;
			if (mes1 == 0) {
				mes1 = 12;
				anyo1 --;
			}
			trimestreMes1 = meses[mes1 - 1];
			trimestreMes2 = meses[mes2 - 1];
			trimestreMes3 = meses[mes3 - 1];
			
			// Escribimos la cabecera de la tabla:
			
			text = text + "<tr><th cospan=\"13\"><strong>" + showPeriod + "</strong></th></tr>";
			text = text + "<tr>";
			text = text + "<th rowspan = \"2\">Emisor</th>";
			text = text + "<th rowspan = \"2\">Cod. Nav.</th>";
			text = text + "<th rowspan = \"2\"></th>";
			text = text + "<th colspan = \"3\">" + trimestreMes1 + "</th>";
			text = text + "<th rowspan = \"2\"></th>";
			text = text + "<th colspan = \"3\">" + trimestreMes2 + "</th>";
			text = text + "<th rowspan = \"2\"></th>";
			text = text + "<th colspan = \"3\">" + trimestreMes3 + "</th>";
			text = text + "</tr>";
			text = text + "<tr>";
			text = text + "<th>C</th><th>NC</th><th>I</th>";
			text = text + "<th>C</th><th>NC</th><th>I</th>";
			text = text + "<th>C</th><th>NC</th><th>I</th>";
			text = text + "</tr>";
		} else {
			showPeriod = "Datos de " + meses[Integer.parseInt(factMonth) - 1] + " de " + factYear + "(" + anyo1 + ")";
			
			// Escribimos la cabecera de la tabla:
			
			text = text + "<tr><th cospan=\"4\"><strong>" + showPeriod + "</strong></th></tr>";
			text = text + "<tr>";
			text = text + "<th width = \"67%\">Emisor</th>";
			text = text + "<th width = \"7%\">Cod. Nav.</th>";
			text = text + "<th width = \"10%\">Corp.</th>";
			text = text + "<th width = \"10%\">No Corp.</th>";
			text = text + "<th width = \"10%\">Intern.</th>";
			text = text + "</tr>";
		}
		
		// Llegados a este action, sabemos que la facturación está en la sesión, por
		// lo que la pillamos de ahí de forma instantánea (mejora sobre Webmail actual)
		
		Map<String,Object> session = (Map<String,Object>) ActionContext.getContext().get("session");
		facturacionStore = (Vector<FacturacionEmisor>) session.get (ConstantesWebmovil.ADMINISTRACION_FACTURACION_RESULTADOS_ALMACENADOS);
		
		// Ahora recorremos facturacionStore para construir nuestra tabla excel
		
		for (int i = 0; i < facturacionStore.size(); i ++) {
			FacturacionEmisor datosEmisor = facturacionStore.get (i);
			if ("0".equals(factMonth) || "0".equals(factYear)) {
				text = text + "<tr>";
				text = text + "<td>" + datosEmisor.getIdEmisor() + "</td>";
				String navision = datosEmisor.getNavision();
				if (navision == null) navision = "";
				text = text + "<td>" + navision + "</td>";
				text = text + "<td></td>";
				text = text + "<td>" + datosEmisor.getEnviadosCorporativos1() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosNoCorporativos1() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosInternacionales1() + "</td>";
				text = text + "<td></td>";
				text = text + "<td>" + datosEmisor.getEnviadosCorporativos2() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosNoCorporativos2() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosInternacionales2() + "</td>";
				text = text + "<td></td>";
				text = text + "<td>" + datosEmisor.getEnviadosCorporativos3() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosNoCorporativos3() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosInternacionales3() + "</td>";
				text = text + "</tr>";
			} else {
				text = text + "<tr>";
				text = text + "<td>" + datosEmisor.getIdEmisor() + "</td>";
				String navision = datosEmisor.getNavision();
				if (navision == null) navision = "";
				text = text + "<td>" + navision + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosCorporativos1() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosNoCorporativos1() + "</td>";
				text = text + "<td>" + datosEmisor.getEnviadosInternacionales1() + "</td>";
				text = text + "</tr>";
			}
		}
		
		// Fin de la tabla
		
		text = text + "</table></body>";
		
		// Generar excel para descarga:
			
		try {
			inputStream = new ByteArrayInputStream(text.getBytes("UTF-8"));
		} catch (Exception e) {
			logger.error ("Ocurrió una excepción " + e);
		}
		
		return SUCCESS;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public String getShowPeriod() {
		return showPeriod;
	}

	public void setShowPeriod(String showPeriod) {
		this.showPeriod = showPeriod;
	}

	public String getFactMonth() {
		return factMonth;
	}

	public void setFactMonth(String factMonth) {
		this.factMonth = factMonth;
	}

	public String getFactYear() {
		return factYear;
	}

	public void setFactYear(String factYear) {
		this.factYear = factYear;
	}

	public Vector<FacturacionEmisor> getFacturacionStore() {
		return facturacionStore;
	}

	public void setFacturacionStore(Vector<FacturacionEmisor> facturacionStore) {
		this.facturacionStore = facturacionStore;
	}	
}
