package es.sadesi.webmovil.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.Alfanumerico;

public class AdministracionListaAlfanumericos extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionListaAlfanumericos.class);
	
	private Vector<Alfanumerico> listaAlfanumericosStore = null;
	private String idEmisor;
	private String accion;
	
	@SuppressWarnings("unchecked")
	public String execute() throws Exception {
		setSeccion("administracion");
		
		// Primero acciones
		if ("eliminar".equals (accion)) {
			try {
				logger.info("Eliminando alfanumerico " + this.idEmisor);
				String idEmisor = this.idEmisor;
				String operacion = "escribeAlfanumericoNew";
				HashMap<String,Object> args = new HashMap<String,Object> ();
				args.put ("id_emisor", idEmisor);
				args.put ("alfa", null);
				getP3sManager().generalWriteBD(operacion, args);
			} catch (Exception e) {
				logger.error ("Ocurrió un error al intentar eliminar alfanumérico [" + idEmisor + "]: " + e);
				addFieldError ("listaAlfanumericos.general", "Ocurrió un error al intentar eliminar alfanumérico. Contacte con el ardministrador");
				return INPUT;
			}
		}
		
		logger.info ("Obteniendo lista de alfanuméricos");
		
		String operacion = "listaAlfanumericosNew";
		HashMap<String,Object> args = null;
		
		HashMap<String,Object> lecturaBD = null;
		
		try {
			lecturaBD = getP3sManager().generalReadBD(operacion, args);
		} catch (Exception e) {
			logger.error ("Ocurrió un error al intentar obtener la lista de alfanuméricos." + e);
			addFieldError ("listaAlfanumericos.general", "Ocurrió un error al intentar obtener la lista de alfanuméricos. Contacte con el ardministrador");
			return INPUT;
		}
		
		Vector<HashMap<String,Object>> resultados = (Vector<HashMap<String,Object>>) lecturaBD.get("resultado");
		Iterator<HashMap<String,Object>> iter = resultados.iterator ();
		listaAlfanumericosStore = new Vector<Alfanumerico> ();
		while (iter.hasNext()) {
			HashMap<String,Object> resultado = (HashMap<String,Object>) iter.next();
			Alfanumerico alfanumerico = new Alfanumerico ();
			alfanumerico.setIdEmisor ((String) resultado.get("id_emisor"));
			alfanumerico.setDescripcion ((String) resultado.get ("descripcion"));
			alfanumerico.setAlfa ((String) resultado.get ("alfa"));
			listaAlfanumericosStore.add (alfanumerico);
		}
		
		return SUCCESS;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public Vector<Alfanumerico> getListaAlfanumericosStore() {
		return listaAlfanumericosStore;
	}

	public void setListaAlfanumericosStore(
			Vector<Alfanumerico> listaAlfanumericosStore) {
		this.listaAlfanumericosStore = listaAlfanumericosStore;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}
