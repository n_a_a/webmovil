package es.sadesi.webmovil.action;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Vector;

import es.sadesi.sms.P3SProperties;
import es.sadesi.webmovil.model.Usuario;

public class OfflineExcels extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	
	private static String uriExcels = P3SProperties.getInstance().getProperty(P3SProperties.URI_EXCELS);
	
	private Vector<HashMap<String,String>> listaArchivos = null;
	
	public String execute() throws Exception {
		// Crear listaArchivos
		
		Usuario user = getUsuarioRemoto();
		String usuario = user.getUsuario();
		
		listaArchivos = new Vector<HashMap<String,String>> ();
		
		Vector<String> listOfFiles = new Vector<String> ();
		URL url = new URL (uriExcels + usuario + "/files-list.txt");
		InputStream is = url.openStream();
		BufferedReader br = new BufferedReader (new InputStreamReader (is));
		String line = null;
		while((line = br.readLine()) != null) {
			listOfFiles.add(line.trim ());
		}
		
		// Sort (quick'n'dirty, will suffice!)
		for (int i = 0; i < listOfFiles.size(); i ++) {
			for (int j = i + 1; j < listOfFiles.size(); j ++) {
				if (listOfFiles.get(i).compareTo(listOfFiles.get(j)) < 0) {
					String tempFile = listOfFiles.get(i);
					listOfFiles.set(i, listOfFiles.get(j));
					listOfFiles.set(j, tempFile);
				}
			}
		}
		
		for (int i = 0; i < listOfFiles.size(); i ++) {
			String fileName = listOfFiles.get (i);
			if (fileName.trim ().endsWith (".zip")) {
				HashMap<String,String> entry = new HashMap<String,String> ();
				entry.put("uri", uriExcels + usuario + "/" + fileName.trim ());
				entry.put("year", fileName.trim ().substring (0, 4));
				entry.put("month", fileName.trim ().substring (5, 7));
				listaArchivos.add (entry);
			}
		}
		
		return SUCCESS;
	}

	public Vector<HashMap<String,String>> getListaArchivos() {
		return listaArchivos;
	}

	public void setListaArchivos(Vector<HashMap<String,String>> listaArchivos) {
		this.listaArchivos = listaArchivos;
	}
}
