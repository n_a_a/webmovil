package es.sadesi.webmovil.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SProperties;
import es.sadesi.webmovil.model.Usuario;

public class EnvioMasivoHistoricoResultado extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(EnvioMasivoHistoricoResultado.class);

	private static String rutaEnvioMasivo = P3SProperties.getInstance().getProperty(P3SProperties.RUTA_ENVIO_MASIVO);
	
	private String nombreArchivo;
	private String todoElTexto;
	private String descripcion;
	private int numPagina;
	
	public String execute() throws Exception {
		logger.info("Ejecutando acción EnvioMasivoHistoricoResultado");
		
		setSeccion("SMS");
		
		// Rellenamos campos automáticos
		Usuario usuario = getUsuarioRemoto();
		
		// Calculamos la ruta del log del usuario
		String rutaLog = rutaEnvioMasivo + "users/" + usuario.getId() + "/log";
	
		// Abrimos el archivo y escribimos todo el texto
		try {
			File archivoLog = new File(rutaLog, nombreArchivo);
			BufferedReader input = new BufferedReader (new FileReader(archivoLog));
			todoElTexto = "";
			try {
				String linea = null;
				while (( linea = input.readLine()) != null){
					todoElTexto += linea + "\n";
				}
			} catch (IOException e) {
				logger.error ("Ocurrió un error leyendo el resultado del envío en " + nombreArchivo + ":" + e.toString ());
			} finally {
				input.close ();
			}
		} catch (Exception e) {
			logger.error ("Ocurrió un error leyendo el archivo del envío en " + nombreArchivo + ":" + e.toString ());
		}
		
		return SUCCESS;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getTodoElTexto() {
		return todoElTexto;
	}

	public void setTodoElTexto(String todoElTexto) {
		this.todoElTexto = todoElTexto;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public int getNumPagina() {
		return numPagina;
	}
}
