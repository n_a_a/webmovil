package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PanelAdministracion extends WebmovilAction {
	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(PanelAdministracion.class);
	
	public String execute() throws Exception {
		logger.info("Ejecutando acción PanelAdministracion");
		
		setSeccion("administracion");
		
		// Hecho.

		return SUCCESS;
	}
}
