package es.sadesi.webmovil.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.webmovil.model.Usuario;

public class AdministracionCambioFirmaEmisor extends WebmovilAction {

	private static final long serialVersionUID = 1L;
	private static final Log logger = LogFactory.getLog(AdministracionCambioFirmaEmisor.class);
	
	private String formFirma;
	private String accion;
	
	public String execute() throws Exception {
		
		setSeccion("administracion");
		
		logger.info("Ejecutando acción PreferenciasUsuario");
		
		// Obtenemos los datos necesarios del usuario de la sesión:
		
		Usuario user = getUsuarioRemoto();
		String emisor = user.getEmisor().getEmisor();
		
		// Vemos si tenemos que actualizar los datos del usuario
		
		if ("Aceptar".equals (accion)) {
			// Escribimos los datos del usuario
				
			// Llamamos al WS para actualizar el emisor
			getP3sManager().modificaAtributoEmisor(emisor, emisor, "firma", formFirma);
			
			// Modificamos el emisor del usuario en la sesión con los nuevos valores.
			user.getEmisor().setFirma(formFirma);
			
			// Logar como niños weno
			logger.info ("Se actualizó la firma del emisor " + emisor + ".");
		}
		
		// Rellenamos los formularios con la información
		// del usuario.
		
		formFirma = user.getEmisor().getFirma();
		
		return SUCCESS;
	}

	public void setFormFirma(String formFirma) {
		this.formFirma = formFirma;
	}

	public String getFormFirma() {
		return formFirma;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}
}
