package es.sadesi.webmovil.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Salir extends ActionSupport {

	/**
	 * Auto-generado.
	 */
	private static final long serialVersionUID = -6813787475834742752L;

	@Override
	public String execute() throws Exception {

		ActionContext context = ActionContext.getContext();
		context.getSession().clear();
		//context.getSession().remove(ConstantesWebmovil.USER_HANDLE);

		return SUCCESS;
	}
}
