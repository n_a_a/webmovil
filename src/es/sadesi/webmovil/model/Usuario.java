package es.sadesi.webmovil.model;

import java.util.Hashtable;

public class Usuario {

	private String usuario;
	private String id;
	private Emisor emisor;
	private int maxNacional;
	private int maxInternacional;
	private String nivel;
	private String firmaPersonal;
	private String telefonoMovil;
	private String telefonoMovilDest;
	private int itemPorPagina;
	private boolean recibirEmail;
	private Hashtable<String, Boolean> permisos;

	public int getMaxNacional() {
		return maxNacional;
	}

	public void setMaxNacional(int maxNacional) {
		this.maxNacional = maxNacional;
	}

	public int getMaxInternacional() {
		return maxInternacional;
	}

	public void setMaxInternacional(int maxInternacional) {
		this.maxInternacional = maxInternacional;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFirmaPersonal() {
		return firmaPersonal;
	}

	public void setFirmaPersonal(String firmaPersonal) {
		this.firmaPersonal = firmaPersonal;
	}

	public Emisor getEmisor() {
		return emisor;
	}

	public void setEmisor(Emisor emisor) {
		this.emisor = emisor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getTelefonoMovilDest() {
		return telefonoMovilDest;
	}

	public void setTelefonoMovilDest(String telefonoMovilDest) {
		this.telefonoMovilDest = telefonoMovilDest;
	}

	public int getItemPorPagina() {
		return itemPorPagina;
	}

	public void setItemPorPagina(int itemPorPagina) {
		this.itemPorPagina = itemPorPagina;
	}

	public boolean isRecibirEmail() {
		return recibirEmail;
	}

	public void setRecibirEmail(boolean recibirEmail) {
		this.recibirEmail = recibirEmail;
	}

	public Hashtable<String, Boolean> getPermisos() {
		return permisos;
	}

	public void setPermisos(Hashtable<String, Boolean> permisos) {
		this.permisos = permisos;
	}
	
	public Boolean getPermiso(String permiso) {
		if (permisos != null)
			return permisos.get(permiso);
		else
			return null;
	}
	
	public void setPermiso(String permiso, Boolean valor) {
		if (permisos == null)
			permisos = new Hashtable<String, Boolean>();
		permisos.put(permiso, valor);
	}
	
	public boolean esSuperAdministrador() {
		if ("S".equals(nivel))
			return true;
		return false;
	}
	
	public boolean esAdministrador() {
		if ("A".equals(nivel))
			return true;
		return false;
	}
	
	public boolean esFacturacion() {
		if ("F".equals(nivel))
			return true;
		return false;
	}
}
