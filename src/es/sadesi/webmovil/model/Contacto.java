package es.sadesi.webmovil.model;

public class Contacto {
	
	String nombre;
	String idPadre;
	String idGrupo;
	String email;
	String idContacto;
	String tlfMovil;
	String tlfFijo;
	String entidad;
	String notas;
	private boolean clientePush;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getIdGrupo() {
		return idGrupo;
	}
	
	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getIdContacto() {
		return idContacto;
	}
	
	public void setIdContacto(String idContacto) {
		this.idContacto = idContacto;
	}

	public String getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}

	public String getTlfMovil() {
		return tlfMovil;
	}

	public void setTlfMovil(String tlfMovil) {
		this.tlfMovil = tlfMovil;
	}

	public String getTlfFijo() {
		return tlfFijo;
	}

	public void setTlfFijo(String tlfFijo) {
		this.tlfFijo = tlfFijo;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public boolean isClientePush() {
		return clientePush;
	}

	public void setClientePush(boolean clientePush) {
		this.clientePush = clientePush;
	}
}
