package es.sadesi.webmovil.model;

public class UsuarioEstadistico {
	private String usuario;
	private String numEnviados;
	private String numEnviadosCorporativos;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getNumEnviados() {
		return numEnviados;
	}
	public void setNumEnviados(String numEnviados) {
		this.numEnviados = numEnviados;
	}
	public void setNumEnviadosCorporativos(String numEnviadosCorporativos) {
		this.numEnviadosCorporativos = numEnviadosCorporativos;
	}
	public String getNumEnviadosCorporativos() {
		return numEnviadosCorporativos;
	}
}
