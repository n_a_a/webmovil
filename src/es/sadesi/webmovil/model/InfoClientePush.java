package es.sadesi.webmovil.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InfoClientePush {
	private String tlfno;
	private boolean push;
	private double lat;
	private double lon;
	private long ts;
	private String dateTimeString;

	public InfoClientePush() {
	}

	public InfoClientePush(String tlfno) {
		this.tlfno = tlfno;
		this.setPush(false);
	}
	
	public InfoClientePush(String tlfno, boolean push, int lat, int lon,
			long ts) {
		this.tlfno = tlfno;
		this.setPush(push);
		this.setLat(lat);
		this.setLon(lon);
		this.setTs (ts);
	}

	public String getTlfno() {
		return tlfno;
	}

	public void setTlfno(String tlfno) {
		this.tlfno = tlfno;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
		
		// Calculate dateTimeString
		Date date = new Date (ts);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss");
		dateTimeString = simpleDateFormat.format (date);
	}

	public boolean isPush() {
		return push;
	}

	public void setPush(boolean push) {
		this.push = push;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	@Override
	public String toString() {
		return "InfoClientePush [tlfno=" + tlfno + ", push=" + push + ", lat="
				+ lat + ", lon=" + lon + ", ts=" + ts + "]";
	}

	public String getDateTimeString() {
		return dateTimeString;
	}

	public void setDateTimeString(String dateTimeString) {
		this.dateTimeString = dateTimeString;
	}
}
