package es.sadesi.webmovil.model;

import java.io.File;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/*
 * Esta clase describe un adjunto de NotificacionesJunta
 * Cada elemento puede representar cualquier tipo de adjunto.
 */

public class Attachment {
	private int id;
	private String type;
	private String typeName;
	private String iconName;
	
	private String url;
	private String desc;
	
	private String title;
	private boolean allDay;
	private String start;
	private String end;
	
	private String lon;
	private String lat;
	
	private String tit;

	private String filename;
	private String justFilename;
	private String fullPathToFile;
	private File file;
	private boolean alreadyUploaded; 
	private boolean alreadyOnServer;
	private int idOnServer;
	
	private String lastError;
	private Vector<String> syntaxErrors;
	
	private String uriToGet;
	
	private static final Log logger = LogFactory.getLog (Attachment.class);
	
	public void fixStart () {
		this.start = fixDate (this.start);
	}
	
	public void fixEnd () {
		this.end = fixDate (this.end);
	}
	
	private String fixDate (String date) {
		String result = "";
		if (!date.matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}")) {
			result = date.substring (6, 10) + "-" +
					date.substring (3, 5) + "-" +
					date.substring (0, 2) + " " +
					date.substring (11);
			logger.debug ("Date en formato mal. Era: " + date + ", es: " + result);
		} else result = date;
		return result;
	}
	
	public void addSyntaxError (String syntaxError) {
		if (this.syntaxErrors == null) this.syntaxErrors = new Vector<String> ();
		syntaxErrors.add (syntaxError);
	}
	
	public Attachment (int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isAllDay() {
		return allDay;
	}

	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getTit() {
		return tit;
	}

	public void setTit(String tit) {
		this.tit = tit;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String orgFilename) {
		this.filename = orgFilename;
	}

	public boolean isAlreadyUploaded() {
		return alreadyUploaded;
	}

	public void setAlreadyUploaded(boolean alreadyUploaded) {
		this.alreadyUploaded = alreadyUploaded;
	}

	public String getJustFilename() {
		return justFilename;
	}

	public void setJustFilename(String justFilename) {
		this.justFilename = justFilename;
	}

	public String getFullPathToFile() {
		return fullPathToFile;
	}

	public void setFullPathToFile(String fullPathToFile) {
		this.fullPathToFile = fullPathToFile;
	}

	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Vector<String> getSyntaxErrors() {
		return syntaxErrors;
	}

	public void setSyntaxErrors(Vector<String> syntaxErrors) {
		this.syntaxErrors = syntaxErrors;
	}

	public boolean isAlreadyOnServer() {
		return alreadyOnServer;
	}

	public void setAlreadyOnServer(boolean alreadyOnServer) {
		this.alreadyOnServer = alreadyOnServer;
	}

	public int getIdOnServer() {
		return idOnServer;
	}

	public void setIdOnServer(int idOnServer) {
		this.idOnServer = idOnServer;
	}

	public String getUriToGet() {
		return uriToGet;
	}

	public void setUriToGet(String uriToGet) {
		this.uriToGet = uriToGet;
	}
}
