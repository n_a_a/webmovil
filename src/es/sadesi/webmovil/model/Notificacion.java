package es.sadesi.webmovil.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Notificacion {
	private Vector<Attachment> adjuntos;
	private int id;
	private String texto;
	private String textoCut;
	private String tlfn;
	private long ts;
	private Date date;
	private String dateStr;
	private String estado;

	private static final Log logger = LogFactory.getLog (Notificacion.class);
	
	public Notificacion () {
		date = new Date ();
		this.adjuntos = new Vector<Attachment> ();
	}
	
	public void syncDate () {
		logger.debug ("Synching dates");
		logger.debug ("ts = " + ts);
		date.setTime(ts);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		dateStr = df.format(date);
		logger.debug ("sDate = " + dateStr);
	}
	
	public Vector<Attachment> getAdjuntos() {
		return adjuntos;
	}
	public void setAdjuntos(Vector<Attachment> adjuntos) {
		this.adjuntos = adjuntos;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getTlfn() {
		return tlfn;
	}
	public void setTlfn(String tlfn) {
		this.tlfn = tlfn;
	}
	public long getTs() {
		return ts;
	}
	public void setTs(long ts) {
		this.ts = ts;
		syncDate ();
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}


	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public String getTextoCut() {
		return textoCut;
	}

	public void setTextoCut(String textoCut) {
		this.textoCut = textoCut;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}
