package es.sadesi.webmovil.model;

import java.util.List;

public class Envio {

	private String tipoDestinatario = "Nacional";
	private String destinatario = "";
	private String idContacto; // Si se ha seleccionado un contacto de la agenda
	private String idGrupo; // Si se ha seleccionado un grupo de la agenda
	private List<String[]> destinatarios;
	private String texto;
	private boolean conFirmaPersonal = false;
	private String firmaPersonal;
	private boolean conFirmaCorporativa = true;
	private String firmaCorporativa;
	private boolean acuseRecibo = false;
	private boolean prioridadAlta = false;
	private String vigencia = "1 día";
	private boolean remitenteAlfanumerico = false;
	private String mensaje;
	private String tipo = "Estándar";

	public String getMensaje() {
		mensaje = getTexto();
		if (isConFirmaPersonal())
			mensaje = mensaje + getFirmaPersonal();
		if (isConFirmaCorporativa())
			mensaje = mensaje + getFirmaCorporativa();

		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getFirmaPersonal() {
		return firmaPersonal;
	}

	public void setFirmaPersonal(String firmaPersonal) {
		this.firmaPersonal = firmaPersonal;
	}

	public String getFirmaCorporativa() {
		return firmaCorporativa;
	}

	public void setFirmaCorporativa(String firmaCorporativa) {
		this.firmaCorporativa = firmaCorporativa;
	}

	public boolean isAcuseRecibo() {
		return acuseRecibo;
	}

	public void setAcuseRecibo(boolean acuseRecibo) {
		this.acuseRecibo = acuseRecibo;
	}

	public boolean isPrioridadAlta() {
		return prioridadAlta;
	}

	public void setPrioridadAlta(boolean prioridadAlta) {
		this.prioridadAlta = prioridadAlta;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getTipoDestinatario() {
		return tipoDestinatario;
	}

	public void setTipoDestinatario(String tipoDestinatario) {
		this.tipoDestinatario = tipoDestinatario;
	}

	public boolean isConFirmaPersonal() {
		return conFirmaPersonal;
	}

	public void setConFirmaPersonal(boolean conFirmaPersonal) {
		this.conFirmaPersonal = conFirmaPersonal;
	}

	public boolean isConFirmaCorporativa() {
		return conFirmaCorporativa;
	}

	public void setConFirmaCorporativa(boolean conFirmaCorporativa) {
		this.conFirmaCorporativa = conFirmaCorporativa;
	}

	public boolean isRemitenteAlfanumerico() {
		return remitenteAlfanumerico;
	}

	public void setRemitenteAlfanumerico(boolean remitenteAlfanumerico) {
		this.remitenteAlfanumerico = remitenteAlfanumerico;
	}

	public List<String []> getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(List<String[]> destinatarios) {
		this.destinatarios = destinatarios;
	}

	public String getIdContacto() {
		return idContacto;
	}

	public void setIdContacto(String idContacto) {
		this.idContacto = idContacto;
	}

	public String getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(String idGrupo) {
		this.idGrupo = idGrupo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
