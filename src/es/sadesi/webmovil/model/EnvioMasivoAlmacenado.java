package es.sadesi.webmovil.model;

public class EnvioMasivoAlmacenado {
	private String fecha;
	private String hora;
	private String descripcion;
	private String texto;
	private String nombreArchivo;
	private String archivoDest;
	private String tipo;

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setArchivoDest(String archivoDest) {
		this.archivoDest = archivoDest;
	}

	public String getArchivoDest() {
		return archivoDest;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
