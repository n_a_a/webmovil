package es.sadesi.webmovil.model;

public class FacturacionEmisor {
	private String idEmisor;
	private String navision;
	private int enviadosCorporativos1;
	private int enviadosNoCorporativos1;
	private int enviadosInternacionales1;
	private int enviadosCorporativos2;
	private int enviadosNoCorporativos2;
	private int enviadosInternacionales2;
	private int enviadosCorporativos3;
	private int enviadosNoCorporativos3;
	private int enviadosInternacionales3;
	public String getIdEmisor() {
		return idEmisor;
	}
	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}
	public int getEnviadosCorporativos1() {
		return enviadosCorporativos1;
	}
	public void setEnviadosCorporativos1(int enviadosCorporativos1) {
		this.enviadosCorporativos1 = enviadosCorporativos1;
	}
	public int getEnviadosNoCorporativos1() {
		return enviadosNoCorporativos1;
	}
	public void setEnviadosNoCorporativos1(int enviadosNoCorporativos1) {
		this.enviadosNoCorporativos1 = enviadosNoCorporativos1;
	}
	public int getEnviadosInternacionales1() {
		return enviadosInternacionales1;
	}
	public void setEnviadosInternacionales1(int enviadosInternacionales1) {
		this.enviadosInternacionales1 = enviadosInternacionales1;
	}
	public int getEnviadosCorporativos2() {
		return enviadosCorporativos2;
	}
	public void setEnviadosCorporativos2(int enviadosCorporativos2) {
		this.enviadosCorporativos2 = enviadosCorporativos2;
	}
	public int getEnviadosNoCorporativos2() {
		return enviadosNoCorporativos2;
	}
	public void setEnviadosNoCorporativos2(int enviadosNoCorporativos2) {
		this.enviadosNoCorporativos2 = enviadosNoCorporativos2;
	}
	public int getEnviadosInternacionales2() {
		return enviadosInternacionales2;
	}
	public void setEnviadosInternacionales2(int enviadosInternacionales2) {
		this.enviadosInternacionales2 = enviadosInternacionales2;
	}
	public int getEnviadosCorporativos3() {
		return enviadosCorporativos3;
	}
	public void setEnviadosCorporativos3(int enviadosCorporativos3) {
		this.enviadosCorporativos3 = enviadosCorporativos3;
	}
	public int getEnviadosNoCorporativos3() {
		return enviadosNoCorporativos3;
	}
	public void setEnviadosNoCorporativos3(int enviadosNoCorporativos3) {
		this.enviadosNoCorporativos3 = enviadosNoCorporativos3;
	}
	public int getEnviadosInternacionales3() {
		return enviadosInternacionales3;
	}
	public void setEnviadosInternacionales3(int enviadosInternacionales3) {
		this.enviadosInternacionales3 = enviadosInternacionales3;
	}
	public void setNavision(String navision) {
		this.navision = navision;
	}
	public String getNavision() {
		return navision;
	}
}
