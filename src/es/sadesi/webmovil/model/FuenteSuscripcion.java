package es.sadesi.webmovil.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FuenteSuscripcion {
	private int id;
	private String tipo;
	private String idEmisor;
	private String desc;
	private String urlAlta;
	private String urlBaja;
	private String method;
	
	private static final Log logger = LogFactory
			.getLog(FuenteSuscripcion.class);

	public FuenteSuscripcion() {
	}

	public FuenteSuscripcion(int id, String tipo, String idEmisor, String desc,
			String urlAlta, String urlBaja, String method) {
		this.id = id;
		this.tipo = tipo;
		this.idEmisor = idEmisor;
		this.desc = desc;
		this.urlAlta = urlAlta;
		this.urlBaja = urlBaja;
		this.method = method;
		
		logger.info (this.toString ());
	}

	@Override
	public String toString() {
		return "FuenteSuscripcion [id=" + id + ", tipo=" + tipo + ", idEmisor="
				+ idEmisor + ", desc=" + desc + ", urlAlta=" + urlAlta
				+ ", urlBaja=" + urlBaja + ", method=" + method + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getUrlAlta() {
		return urlAlta;
	}

	public void setUrlAlta(String urlAlta) {
		this.urlAlta = urlAlta;
	}

	public String getUrlBaja() {
		return urlBaja;
	}

	public void setUrlBaja(String urlBaja) {
		this.urlBaja = urlBaja;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
}
