package es.sadesi.webmovil.model;

public class Carpeta {
	/* 
	 * Esta clase modela una carpeta de mensajes de usuario
	 */
	
	private String idCarpeta;
	private String nombreCarpeta;
	private int numMensajes;

	public Carpeta (){
	}
	
	public Carpeta (String idCarpeta, String nombreCarpeta, int numMensajes) {
		this.idCarpeta = idCarpeta;
		this.nombreCarpeta = nombreCarpeta;
		this.numMensajes = numMensajes;
	}
	
	public String getIdCarpeta() {
		return idCarpeta;
	}
	
	public void setIdCarpeta(String idCarpeta) {
		this.idCarpeta = idCarpeta;
	}
	
	public String getNombreCarpeta() {
		return nombreCarpeta;
	}
	
	public void setNombreCarpeta(String nombreCarpeta) {
		this.nombreCarpeta = nombreCarpeta;
	}
	
	public int getNumMensajes() {
		return numMensajes;
	}
	
	public void setNumMensajes(int numMensajes) {
		this.numMensajes = numMensajes;
	}
	
	public String toString () {
		return nombreCarpeta;
	}
}
