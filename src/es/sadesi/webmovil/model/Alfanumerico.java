package es.sadesi.webmovil.model;

public class Alfanumerico {
	String idEmisor;
	String descripcion;
	String alfa;
	
	public Alfanumerico () {
	}
	
	public Alfanumerico (String idEmisor, String descripcion, String alfa) {
		this.idEmisor = idEmisor;
		this.descripcion = descripcion;
		this.alfa = alfa;
	}

	public String getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAlfa() {
		return alfa;
	}

	public void setAlfa(String alfa) {
		this.alfa = alfa;
	}

	@Override
	public String toString() {
		return "Alfanumerico [id_emisor=" + idEmisor + ", descripcion=" + descripcion + ", " + ", alfa="
				+ alfa + "]";
	}
}
