package es.sadesi.webmovil.model;

import java.util.Vector;

public class Emisor {
	
	private String emisor;
	private String descripcion;
	private int limite;
	private String telefono;
	private String firma;
	private Vector<String> ips;
	private String alfa;
	
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getLimite() {
		return limite;
	}
	public void setLimite(int limite) {
		this.limite = limite;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getFirma() {
		return firma;
	}
	public void setFirma(String firma) {
		this.firma = firma;
	}
	public Vector<String> getIps() {
		return ips;
	}
	public void setIps(Vector<String> ips) {
		this.ips = ips;
	}
	public String getAlfa() {
		return alfa;
	}
	public void setAlfa(String alfa) {
		this.alfa = alfa;
	}
}
