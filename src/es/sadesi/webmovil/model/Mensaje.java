package es.sadesi.webmovil.model;

public class Mensaje {
	private String idEsme;
	private String telefono;
	private String texto;
	private String fecha;
	private String estado;
	private String estadoImg;
	private String estadoAlt;
	private String idCarpeta;
	private String nombreCarpeta;
	private int tipoMensaje;
	private boolean push;
	
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
		if (estado.equals ("A")) {
			estadoImg = "pic_aceptado.gif";
			estadoAlt = "Aceptado para enviar";
		} else if (estado.equals ("S")) {
			estadoImg = "pic_enviado.gif";
			estadoAlt = "Enviado correctamente";
		} else if (estado.equals ("X") || estado.equals ("Y") || estado.equals ("R")) {
			estadoImg = "pic_recibido.gif";
			estadoAlt = "Recibido";
		} else if (estado.equals ("E")) {
			estadoImg = "pic_error.gif";
			estadoAlt = "Error al enviar";
		} else {
			estadoImg = "pic_enviado.gif";
			estadoAlt = "Estado desconocido";
		}
	}
	public void setIdEsme(String idEsme) {
		this.idEsme = idEsme;
	}
	public String getIdEsme() {
		return idEsme;
	}
	public void setIdCarpeta(String idCarpeta) {
		this.idCarpeta = idCarpeta;
	}
	public String getIdCarpeta() {
		return idCarpeta;
	}
	public void setNombreCarpeta(String nombreCarpeta) {
		this.nombreCarpeta = nombreCarpeta;
	}
	public String getNombreCarpeta() {
		return nombreCarpeta;
	}
	public void setEstadoImg(String estadoImg) {
		this.estadoImg = estadoImg;
	}
	public String getEstadoImg() {
		return estadoImg;
	}
	public void setEstadoAlt(String estadoAlt) {
		this.estadoAlt = estadoAlt;
	}
	public String getEstadoAlt() {
		return estadoAlt;
	}
	public int getTipoMensaje() {
		return tipoMensaje;
	}
	public void setTipoMensaje(int tipoMensaje) {
		this.tipoMensaje = tipoMensaje;
	}
	public boolean isPush() {
		return push;
	}
	public void setPush(boolean push) {
		this.push = push;
	}
}
