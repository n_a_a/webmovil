package es.sadesi.webmovil.model;

public class OptionItem {
	private String texto;
	private String url;
	
	public OptionItem () {
	}
	
	public OptionItem (String texto, String url) {
		this.setTexto(texto);
		this.setUrl(url);
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
