package es.sadesi.webmovil.manager;

import es.sadesi.webmovil.model.Emisor;

public interface EmisorManager {

	Emisor getEmisor(String emisor) throws Exception;
	
	Emisor getEmisor(String emisor, String desde, String hasta) throws Exception;
}
