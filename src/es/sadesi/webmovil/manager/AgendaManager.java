package es.sadesi.webmovil.manager;

import java.util.List;

import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Usuario;


public interface AgendaManager {
	
	Contacto getContacto(String idContacto, Usuario usuario) throws Exception;
	
	//Contacto getContactoPorNombre(String nombre, Usuario usuario) throws Exception;
	
	Contacto getGrupo(String idGrupo, Usuario usuario) throws Exception;
	
	List<Contacto> getGrupos(Usuario usuario);

}
