package es.sadesi.webmovil.manager;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SAgenManager;
import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Usuario;


public class AgendaManagerImpl implements AgendaManager {
	
	private static final Log logger = LogFactory.getLog(AgendaManagerImpl.class);
	
	P3SManager p3sManager;
	P3SAgenManager p3sAgenManager;
	
	public AgendaManagerImpl(P3SManager manager, P3SAgenManager agenManager) {
		super();
		p3sManager = manager;
		p3sAgenManager = agenManager;
	}

	@Override
	public Contacto getContacto(String idContacto, Usuario usuario)
			throws Exception {
		Map<String, Object> mapContacto = p3sAgenManager.agendaObtieneContacto(usuario.getId(), usuario.getEmisor().getEmisor(), idContacto);
		
		Contacto contacto = new Contacto();
		contacto.setNombre((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE));
		contacto.setIdGrupo((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_ID_GRUPO));
		contacto.setIdContacto((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_ID_CONTACTO));
		contacto.setIdPadre((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_ID_PADRE));
		contacto.setEmail((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_EMAIL));
		contacto.setTlfMovil((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_TLF_MOVIL));
		contacto.setTlfFijo((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_TLF_FIJO));
		contacto.setEmail((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_EMAIL));
		contacto.setEntidad((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_ENTIDAD));
		contacto.setNotas((String) mapContacto.get(P3SManagerImpl.KEY_CONTACTOS_NOTAS));

		return contacto;
	}

	/*@Override
	public Contacto getContactoPorNombre(String nombre, Usuario usuario)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public Contacto getGrupo(String idGrupo, Usuario usuario) throws Exception {
		Map<String, Object> mapGrupo = p3sAgenManager.agendaObtieneGrupo(usuario.getId(), usuario.getEmisor().getEmisor(), idGrupo);
		
		Contacto grupo = new Contacto();
		grupo.setIdGrupo(idGrupo);
		grupo.setIdContacto((String) mapGrupo.get(P3SManagerImpl.KEY_CONTACTOS_ID_CONTACTO));
		grupo.setNombre((String) mapGrupo.get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE));
		grupo.setIdPadre((String) mapGrupo.get(P3SManagerImpl.KEY_CONTACTOS_ID_PADRE));
		
		return grupo;
	}

	@Override
	public List<Contacto> getGrupos(Usuario usuario) {
		Vector<Contacto> grupos = new Vector<Contacto>();
		Contacto contacto = new Contacto();
		contacto.setNombre("General");
		contacto.setIdGrupo("0");
		grupos.add(contacto);
		try {
			Vector<Map<String, Object>> listaGrupos = p3sAgenManager.listaTodosLosGrupos(usuario.getId(), usuario.getEmisor().getEmisor());
			
			if (listaGrupos != null) {
				for (int i = 0; i < listaGrupos.size(); i++) {
					contacto = new Contacto();
					String nombre = (String) listaGrupos.get(i).get("nombre");
					// Sustituimos los puntos por espacios.
					String nombreSangrado = nombre.replaceAll("\\.", "&nbsp;&nbsp;&nbsp;");
					contacto.setNombre(nombreSangrado);
					contacto.setIdGrupo((String) listaGrupos.get(i).get("id_grupo"));
					grupos.add(contacto);
				}
			}
		} catch (Exception e) {
			logger.error("Error al obtener la lista de los grupos de la agenda del usuario " + usuario.getId(), e);
		}
		return grupos;
	}
}
