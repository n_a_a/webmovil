package es.sadesi.webmovil.manager;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.model.Emisor;

public class EmisorManagerImpl implements EmisorManager {
	
	private P3SManager p3sManager;

	public EmisorManagerImpl(P3SManager manager) {
		super();
		p3sManager = manager;
	}

	@Override
	public Emisor getEmisor(String emisor) throws Exception {
		
		return getEmisor(emisor, "29990101000000", "29990101000001");
	}

	@Override
	public Emisor getEmisor(String emisor, String desde, String hasta)
			throws Exception {

		Map<String, Object> infoEmisor = p3sManager.infoEmisor(emisor);
		
		Emisor objEmisor = new Emisor();
		objEmisor.setEmisor(emisor);

		objEmisor.setDescripcion((String) infoEmisor
				.get(P3SManagerImpl.KEY_INFO_EMISOR_DESCRIPCION));
		objEmisor.setFirma((String) infoEmisor
				.get(P3SManagerImpl.KEY_INFO_EMISOR_FIRMA));
		objEmisor.setLimite(((Integer) infoEmisor
				.get(P3SManagerImpl.KEY_INFO_EMISOR_LIMITE))
				.intValue());
		objEmisor.setTelefono((String) infoEmisor
				.get(P3SManagerImpl.KEY_INFO_EMISOR_TELEFONO));
		String ips = (String) infoEmisor
				.get(P3SManagerImpl.KEY_INFO_EMISOR_IP);
		
		Vector<String> vIps = new Vector<String>();
		
		StringTokenizer st = new StringTokenizer(ips, "-");
		while (st.hasMoreTokens()) {
			vIps.add(st.nextToken());
		}
		objEmisor.setIps(vIps);
		
		return objEmisor;
	}

}
