package es.sadesi.webmovil.manager;

import java.util.Map;

import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;

public class UsuarioManagerImpl implements UsuarioManager {

	private P3SManager p3sManager;
	private EmisorManager emisorManager;

	public UsuarioManagerImpl(P3SManager manager, EmisorManager emisorManager) {
		super();
		p3sManager = manager;
		this.emisorManager = emisorManager;
	}

	@Override
	public Usuario getUsuario(String usuario, String emisor) throws Exception {
		return getUsuario(usuario, emisor, "29990101000000", "29990101000001");
	}

	@Override
	public Usuario getUsuario(String usuario, Emisor emisor) throws Exception {
		return getUsuario(usuario, emisor.getEmisor(), "29990101000000", "29990101000001");
	}

	@Override
	public Usuario getUsuario(String usuario, String emisor, String desde,
			String hasta) throws Exception {

		Map<String, Object> infoUsuario = p3sManager.infoUsuarioEstaticos(usuario,
				emisor);
		
		Emisor objEmisor = emisorManager.getEmisor(emisor);

		Usuario objUsuario = new Usuario();
		objUsuario.setUsuario(usuario);
		objUsuario.setEmisor(objEmisor);

		objUsuario.setId((String) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_ID));
		objUsuario.setNivel((String) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_NIVEL));
		objUsuario.setFirmaPersonal((String) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_FIRMA));
		objUsuario.setMaxInternacional(((Integer) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_MAXIMO_INTERNACIONALES))
				.intValue());
		objUsuario.setMaxNacional(((Integer) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_MAXIMO_NACIONALES))
				.intValue());
		objUsuario.setTelefonoMovil((String) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_TLF_MOVIL));
		objUsuario.setTelefonoMovilDest((String) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_TLF_MOVIL_DEST));
		objUsuario.setItemPorPagina(((Integer) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_MSGS_PAGINA))
				.intValue());
		final int recibirEmail = ((Integer) infoUsuario
				.get(P3SManagerImpl.KEY_INFO_USUARIO_RECIBIR_EMAIL))
				.intValue();
		objUsuario.setRecibirEmail(recibirEmail == 1 ? Boolean.TRUE : Boolean.FALSE);
		
		return objUsuario;
	}

	@Override
	public Usuario getUsuario(String usuario, Emisor emisor, String desde,
			String hasta) throws Exception {
		return getUsuario(usuario, emisor.getEmisor(), desde, hasta);
	}

	@Override
	public boolean tienePermiso(String permiso, Usuario usuario) {
		try {
			Boolean b = usuario.getPermiso(permiso);
			if (b == null)
			{
				
				Map<String, Object> infoUsuario = p3sManager.infoUsuarioEstaticos
					(usuario.getUsuario(), usuario.getEmisor().getEmisor());
				
				String sPermiso = (String)infoUsuario.get(permiso);
				if ("S".equals(sPermiso)) {
					usuario.setPermiso(permiso, new Boolean(true));
					return true;
				} else {
					usuario.setPermiso(permiso, new Boolean(false));
					return false;
				}
			} else {
				return b.booleanValue();
			}
		} catch (Exception e) {
			return false;
		}
	}
}
