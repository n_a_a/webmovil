package es.sadesi.webmovil.manager;

import es.sadesi.webmovil.model.Emisor;
import es.sadesi.webmovil.model.Usuario;

public interface UsuarioManager {
	
	Usuario getUsuario(String usuario, String emisor) throws Exception;
	
	Usuario getUsuario(String usuario, Emisor emisor) throws Exception;
	
	Usuario getUsuario(String usuario, String emisor, String desde, String hasta) throws Exception;
	
	Usuario getUsuario(String usuario, Emisor emisor, String desde, String hasta) throws Exception;
	
	boolean tienePermiso(String permiso, Usuario usuario);
}
