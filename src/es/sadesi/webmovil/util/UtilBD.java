package es.sadesi.webmovil.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

public class UtilBD {
	// Esta clase contiene funciones para obtener una conexión
	
	private static final Log logger = LogFactory.getLog(UtilBD.class);
	private String driver;
	private String url;
	private String user;
	private String pass;
	private int minLimit;
	private int maxLimit;
	private long timeBetweenEvictionRunsMillis;
	private int numTestsPerEviction;
	private long minEvictableIdleTimeMillis;
	private long maxWait;
	
	private static boolean initialized = false;
	
	public UtilBD (String driver, String url, String user, String pass, 
			int minLimit, int maxLimit, long timeBetweenEvictionRunsMillis,
			int numTestsPerEviction, long minEvictableIdleTimeMillis,
			long maxWait) throws Exception {
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.pass = pass;
		this.minLimit = minLimit;
		this.maxLimit = maxLimit;
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
		this.numTestsPerEviction = numTestsPerEviction;
		this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
		this.maxWait = maxWait;
		inicializaPool ();
	}
	
	private void inicializaPool () throws Exception {
		if (initialized) return;

		try {
			Class.forName (this.driver);
		} catch (ClassNotFoundException e) {
			logger.error ("No se ha podido cargar la clase " + this.driver + " del driver JDBC");
			throw e; 
		}
		
		try {
			logger.info ("Creando conexion con " + url + "; " + user + ":" + pass + "(" + minLimit + "-" + maxLimit + ")");
			
			// Prueba guarrera de conexiones:
			// maxLimit = 2;
			// Fin de prueba guarrera.
			
			GenericObjectPool connectionPool = new GenericObjectPool(null);
			connectionPool.setMaxActive (maxLimit);
			connectionPool.setTestOnBorrow (true);
			connectionPool.setTestOnReturn (true);
			
			// Fixtures: esto activa el "Evictor Thread", que se encarga de examinar
			// las conexiones "idle" y "desahuciarlas" si llevan en ese estado más de 
			// "minEvictableIdleTimeMillis" milisegundos. El "Evictor Thread" se ejecutará
			// cada "timeBetweenEvictionRunsMillis" milisegundos y comprobará un máximo de
			// "numTestsPerEviction" conexiones "idle".
			connectionPool.setMaxIdle(maxLimit / 2);
			connectionPool.setTestWhileIdle (true);
			connectionPool.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis); // 60000
			connectionPool.setNumTestsPerEvictionRun(numTestsPerEviction);
			connectionPool.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis); 		// 300000
			connectionPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);	// Nunca debería pasar
			connectionPool.setMaxWait(maxWait);												// 60000
			//
			
			ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(url, user, pass);
			
			String validationQuery = "SELECT 1 FROM DUAL";
			new PoolableConnectionFactory (connectionFactory, connectionPool, null, validationQuery ,false,true);
			PoolingDriver driver = new PoolingDriver();
			driver.registerPool("webmovilPool", connectionPool);
					
			initialized = true;
		} catch (Exception e) {
			logger.error ("No se ha podido inicializar el pool", e);
			throw e;
		}
		
		logger.info ("Conexión OK");
	}
	
	public Connection getConnection () throws SQLException {
		Connection con = DriverManager.getConnection("jdbc:apache:commons:dbcp:webmovilPool");
		logger.debug ("Creada conexión " + con.hashCode());
		return con;
	}
	
	public void closeAll (String operacion, ResultSet rs, Statement st, Connection con) {
		logger.debug("Cerrando conexión " + con.hashCode ());
		if (rs != null) { try { rs.close(); } catch (Exception e) { logger.warn("No se ha podido cerrar 'ResultSet' en "+operacion+".", e); } }
        if (st != null) { try { st.close(); } catch (Exception e) { logger.warn("No se ha podido cerrar 'Statement' en "+operacion+".", e); } }
        if (con != null) { try { con.close(); } catch (Exception e) { logger.warn("No se ha podido cerrar 'Connection' en "+operacion+".", e); } }
	}
}
