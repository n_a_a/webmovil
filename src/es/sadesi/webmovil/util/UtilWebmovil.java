package es.sadesi.webmovil.util;

import java.io.File;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.sms.P3SAgenManager;
import es.sadesi.sms.P3SAgenManagerImpl;
import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;
import es.sadesi.webmovil.ConstantesWebmovil;
import es.sadesi.webmovil.manager.AgendaManager;
import es.sadesi.webmovil.manager.AgendaManagerImpl;
import es.sadesi.webmovil.model.Contacto;
import es.sadesi.webmovil.model.Usuario;

public class UtilWebmovil {

	private static final Log logger = LogFactory.getLog(UtilWebmovil.class);
	
	static FormatosProperties formatosProperties = FormatosProperties
			.getInstance();
	private static P3SManager p3sManager;
	private static P3SAgenManager p3sAgenManager;
	
	public static P3SManager getP3sManager() {
		if (p3sManager == null) {
			p3sManager = new P3SManagerImpl();
		}
		return p3sManager;
	}
	
	public static P3SAgenManager getP3sAgenManager() {
		if (p3sAgenManager == null) {
			p3sAgenManager = new P3SAgenManagerImpl();
		}
		return p3sAgenManager;
	}

	@SuppressWarnings("unused")
	public static List<String[]> validaDestinatario(String destinatario, String idContacto, String idGrupo, 
			Usuario usuario) throws Exception {
		
		Vector<String[]> result = new Vector<String[]>();

		int longTlfNacional = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_NACIONAL_LONG));
		int longTlfCorporativoCorto = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_LONG));
		
		String prefijoNacional = formatosProperties
				.getProperty(FormatosProperties.TLF_NACIONAL_PREFIJO);

		if (esCadenaNumerica(destinatario) || (destinatario.startsWith("+") && esCadenaNumerica(destinatario.substring(1)))) {
			String[] elemento = new String[2];
			try {
				elemento = validaNumeroTelefono(destinatario);
			} catch (NumberFormatException nfe) {
				elemento[0] = destinatario;
				elemento[1] = nfe.getMessage();
			}
			logger.debug ("Añadiendo elemento numérico: " + elemento[0] + ", " + elemento [1]);
			result.add(elemento);
		} else { // Comprobamos si es un contacto o un grupo de contactos de la agenda.
			
			String[] elemento = new String[2];
			if ((idContacto == null || "".equals(idContacto)) && (idGrupo == null || "".equals(idGrupo))) {
				elemento[0] = destinatario;
				elemento[1] = "Tiene que seleccionar el contacto o grupo de la agenda a través del enlace directo.";
			} else if (idContacto != null && !"".equals(idContacto)) { // Ha seleccionado un contacto
				
				AgendaManager agendaManager = new AgendaManagerImpl(new P3SManagerImpl(), new P3SAgenManagerImpl());
				try {
					Contacto contacto = agendaManager.getContacto(idContacto, usuario);
					elemento = validaNumeroTelefono(contacto.getTlfMovil());
				} catch (Exception e) {
					logger.error("Ocurrió un error al obtener el contacto " + idContacto + " de la agenda. Contacte a su administrador. Seguimos con el siguiente.");
					//throw new Exception("Ocurrió un error al obtener el contacto " + idContacto + " de la agenda. Contacte a su administrador");
				}
				logger.debug ("Añadiendo elemento alfanumérico: " + elemento[0] + ", " + elemento [1]);
				result.add(elemento);
			} else { // Ha seleccionado un grupo
				logger.debug ("Procesando recursivamente el grupo " + idGrupo);
				AgendaManager agendaManager = new AgendaManagerImpl(new P3SManagerImpl(), new P3SAgenManagerImpl());
				
				// Primero añado todos los contactos				
				try {
					logger.debug ("Buscando subcontactos");
					Vector<HashMap<String,Object>> subContactos = getP3sAgenManager().listaContactosGrupo(usuario.getId(), usuario.getEmisor().getEmisor(), idGrupo, 0, 1000000);
					for (int i = 0; i < subContactos.size (); i ++) {
						Contacto contacto = agendaManager.getContacto ((String) subContactos.get(i).get(P3SManagerImpl.KEY_CONTACTOS_ID_CONTACTO), usuario);
						try {
							elemento = validaNumeroTelefono(contacto.getTlfMovil());
							logger.debug ("Añadiendo elemento alfanumérico: " + elemento[0] + ", " + elemento [1]);
							result.add(elemento);
						} catch (NumberFormatException e) {
							logger.info("Ocurrio un numberFormatException en el numero de telefono de un contacto del grupo " + idGrupo);
						} catch (Exception e) {
							logger.info("Ocurrio una Excepción!", e);
						}
					}
				} catch (Exception e) {
					logger.error ("Hubo un error obteniendo los contactos de idGrupo = " + idGrupo);
					//throw new Exception("Ocurrió un error obteniendo los contactos de idGrupo = " + idGrupo + " por exceso de carga. Contacte con su administrador o pruebe de nuevo más tarde." + e);
				}
				
				// Luego añado el contenido todos los grupos
				try {
					logger.debug ("Buscando subgrupos");
					Vector<HashMap<String,Object>> subGrupos = getP3sAgenManager().listaSubgruposGrupo(usuario.getId (), usuario.getEmisor().getEmisor(), idGrupo, 0, 1000000);
					for (int i = 0; i < subGrupos.size (); i ++) {
						// Llamada recursiva
						logger.debug ("Llamando recursivamente con " +(String) subGrupos.get(i).get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE)+ "[" +(String) subGrupos.get(i).get(P3SManagerImpl.KEY_CONTACTOS_ID_GRUPO)+ "]");
						List<String[]> subResult = validaDestinatario ((String) subGrupos.get(i).get(P3SManagerImpl.KEY_CONTACTOS_NOMBRE), "", (String) subGrupos.get(i).get(P3SManagerImpl.KEY_CONTACTOS_ID_GRUPO), usuario);
						// Combinamos:
						result.addAll(subResult);
					}
				} catch (Exception e) {
					logger.error ("Hubo un error obteniendo los subgrupos de idGrupo = " + idGrupo);
					//throw new Exception("Ocurrió un error obteniendo los subgrupos de idGrupo = " + idGrupo + " por exceso de carga. Contacte con su administrador o pruebe de nuevo más tarde.");
				}
			}
		}

		return result;
	}
	
	public static String[] validaNumeroTelefono(String telefono) throws NumberFormatException {
		
		telefono = telefono.trim ();
		
		String[] result = new String[2];

		int longTlfNacional = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_NACIONAL_LONG));
		int longTlfCorporativoCorto = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_LONG));
		
		String prefijoNacional = formatosProperties
				.getProperty(FormatosProperties.TLF_NACIONAL_PREFIJO);
		
		if (telefono.startsWith("+") && esCadenaNumerica(telefono.substring(1)))
		{
			result[1] = ConstantesWebmovil.TIPO_NUMERO_INTERNACIONAL;
			result[0] = telefono;
		}
		else if (esCadenaNumerica(telefono))
		{
			int nDestinatario = telefono.length();
			int nPrefijoNacional = prefijoNacional.length();
			
			if (nDestinatario == longTlfNacional && comienzaBien(telefono)) {
				result[0] = prefijoNacional + telefono;
				if (esNumeroCorporativo(telefono))
					result[1] = ConstantesWebmovil.TIPO_NUMERO_CORPORATIVO;
				else
					result[1] = ConstantesWebmovil.TIPO_NUMERO_NACIONAL;
			}
			else if (nDestinatario == longTlfCorporativoCorto) {
				String destino = prefijoNacional + conversionANumLargo(telefono);
				Vector<String> destinatarios = new Vector<String>();
				destinatarios.add(destino);
				result[0] = destino;
				result[1] = ConstantesWebmovil.TIPO_NUMERO_CORPORATIVO;
			}
			else if (nDestinatario == (longTlfNacional + nPrefijoNacional) && telefono.startsWith(prefijoNacional))
			{
				result = validaNumeroTelefono(telefono.substring(nPrefijoNacional));
			}
			else
				throw new NumberFormatException("Número de teléfono incorrecto.");
		}
		
		return result;
	}

	public static boolean esCadenaNumerica(String cadena) {

		try {
			Long.parseLong(cadena);
		} catch (NumberFormatException nfe) {

			return false;
		}

		return true;
	}

	public static boolean comienzaBien(String numero) {
		
		int numIniciosTlfNacional = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_NACIONAL_INICIOS));
		
		boolean enc = false;
		for (int i=0; i!=numIniciosTlfNacional && !enc; i++){
			if (numero.startsWith(formatosProperties.getProperty(FormatosProperties.TLF_NACIONAL_PREFIJO_INICIO + i)))
				enc = true;
		}
	
		return enc;
	}

	public static String conversionANumLargo(String corporativoCorto)
			throws NumberFormatException {

		String result = corporativoCorto;

		int longTlfCorporativoCorto = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_LONG));

		if (corporativoCorto.length() == longTlfCorporativoCorto) {
			
			int numPrefijosCorporativoCorto = Integer.parseInt(formatosProperties
					.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_PREFIJOS));
			
			boolean enc = false;
			String prefijoCorto = null;
			for (int i=0; i!=numPrefijosCorporativoCorto && !enc; i++){
				prefijoCorto = formatosProperties.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_PREFIJO + i);
				if (corporativoCorto.startsWith(prefijoCorto))
					enc = true;
			}
			
			if (enc) {
				String prefijoLargo = formatosProperties.getProperty(FormatosProperties.TLF_CORPORATIVO_LARGO_PREFIJO + prefijoCorto);
				if (prefijoLargo != null)
					result = corporativoCorto.replaceFirst(prefijoCorto, prefijoLargo);
				else
					throw new NumberFormatException("No existe una correspondencia para el prefijo corto: '" + prefijoCorto + "'.");
			}
			else {
				throw new NumberFormatException("El número no es un número corporativo válido.");
			}
			
		} else
			throw new NumberFormatException(
					"El número no es un número corporativo corto.");

		return result;
	}
	
	public static boolean esNumeroCorporativo (String numero) {
		
		boolean result = false;
		
		int longTlfNacional = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_NACIONAL_LONG));
		int longTlfCorporativoCorto = Integer.parseInt(formatosProperties
				.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_LONG));
		
		if (numero.length() == longTlfNacional)
		{
			int numPrefijosCorporativoCorto = Integer.parseInt(formatosProperties
					.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_PREFIJOS));
			
			boolean enc = false;
			for (int i=0; i!=numPrefijosCorporativoCorto && !enc ; i++){
				String prefijoCorto = formatosProperties.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_PREFIJO + i);
				String prefijoLargo = formatosProperties.getProperty(FormatosProperties.TLF_CORPORATIVO_LARGO_PREFIJO + prefijoCorto);
				if (numero.startsWith(prefijoLargo))
					enc = true;
			}
			result = enc;
		}
		else if (numero.length() == longTlfCorporativoCorto)
		{
			int numPrefijosCorporativoCorto = Integer.parseInt(formatosProperties
					.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_PREFIJOS));
			
			boolean enc = false;
			for (int i=0; i!=numPrefijosCorporativoCorto && !enc ; i++){
				String prefijoCorto = formatosProperties.getProperty(FormatosProperties.TLF_CORPORATIVO_CORTO_PREFIJO + i);
				if (numero.startsWith(prefijoCorto))
					enc = true;
			}
			result = enc;
		}
		
		return result;
	}
	
	public static String getPrimerDiaMesActual() {

		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		int dia = gc.get(GregorianCalendar.DAY_OF_MONTH);
		gc.add(GregorianCalendar.DAY_OF_MONTH, (0 - dia + 1));
		String desde = P3SManager.P3S_DATE_FORMAT.format(gc.getTime());
		return desde;
	}

	public static String getUltimoDiaMesActual() {
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.HOUR_OF_DAY, 23);
		gc.set(GregorianCalendar.MINUTE, 59);
		gc.set(GregorianCalendar.SECOND, 59);
		gc.add(GregorianCalendar.MONTH, 1);
		int dias = gc.get(GregorianCalendar.DAY_OF_MONTH);
		gc.add(GregorianCalendar.DAY_OF_MONTH, (0 - dias));

		String hasta = P3SManager.P3S_DATE_FORMAT.format(gc.getTime());
		return hasta;
	}
	
	public static void compruebaSiExisteYSiNoCrea (String pathName) { 
		File file = new File (pathName);
		if (!file.exists ()) {
			logger.info ("Creando el directorio " + pathName);
			if (!file.mkdir ()) {
				logger.error ("Error creando el directorio " + pathName);
			}
		}
	}
	
	public static void vaciaDirectorio (String pathName) {
		try {
			FileUtils.cleanDirectory (new File (pathName));
		} catch (Exception e) {
			logger.warn ("Ocurrió una excepción limpiando el directorio " + pathName + ": " + e.getMessage());
		}
	}
	
	public static Vector<String> makeVectorFromStringArray (String [] array) {
		Vector<String> result = new Vector<String> ();
		for (int i = 0; i < array.length; i ++) {
			result.add (array [i]);
		}
		return result;
	}
	
	public static String fixURL (String url) {
		String res;
		if (url.length() < 9) return "http://" + url;
				
		if (!"http://".equals (url.substring(0, 7)) && !"https://".equals (url.substring(0, 8))) {
			res = "http://" + url;
		} else res = url;
		return res;
	}

	public static String cleanUpTlfno(String tlfnoToken) {
		String result = tlfnoToken;
		if (tlfnoToken.length () > 9) {
			if ("34".equals (tlfnoToken.substring (0, 2))) {
				logger.info ("Quitando 34 al teléfono " + tlfnoToken);
				result = tlfnoToken.substring (2);
			} else result = tlfnoToken;
		}
		return result;
	}
}
