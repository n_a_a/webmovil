package es.sadesi.webmovil.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.util.ClassLoaderUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

public final class FormatosProperties implements Serializable {

	/**
	 * Auto-generado
	 */
	private static final long serialVersionUID = 7170871823629067071L;

	private static final Log logger = LogFactory.getLog(FormatosProperties.class);
	
	public static final String TLF_NACIONAL_LONG = "telefono.nacional.longitud";
	public static final String TLF_NACIONAL_INICIOS = "telefono.nacional.inicios";
	public static final String TLF_NACIONAL_PREFIJO_INICIO = "telefono.nacional.inicio.";
	public static final String TLF_NACIONAL_PREFIJO = "telefono.nacional.prefijo";
	
	public static final String TLF_CORPORATIVO_CORTO_LONG = "telefono.corporativo.corto.longitud";
	public static final String TLF_CORPORATIVO_CORTO_PREFIJOS = "telefono.corporativo.corto.prefijos";
	public static final String TLF_CORPORATIVO_CORTO_PREFIJO = "telefono.corporativo.corto.prefijo.";
	public static final String TLF_CORPORATIVO_LARGO_PREFIJO = "telefono.corporativo.largo.prefijo.";
		

	/**
	 * Singleton instance
	 */
	private static FormatosProperties instance;

	private Properties properties;

	// ~ Constructors ///////////////////////////////////////////////////////////

	/**
	 * Don't use this constructor most of the time. To use LdapProperties as a
	 * singleton, use {@link #getInstance()} .
	 */
	public FormatosProperties() {
		this("formatos.properties");
	}

	public FormatosProperties(String filename) {
		String loc = "/" + filename;
		InputStream in = ClassLoaderUtil.getResourceAsStream(loc, this
				.getClass());

		if (in == null) {
			loc = filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/formatos-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/formatos-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loading using config : " + loc);
		}

		if (in == null) {
			throw new ExceptionInInitializerError("The configuration file "
					+ filename + " could not be found.");
		}

		try {
			properties = new Properties();
			properties.load(in);
		} catch (IOException e) {
			logger.error("Error al cargar el fichero de propiedades", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loaded using config : " + loc);
		}
	}

	// ~ Methods ////////////////////////////////////////////////////////////////

	public static void setInstance(FormatosProperties formatosProperties) {
		instance = formatosProperties;
	}

	/**
	 * Entry-point to Singleton instance
	 */
	public static FormatosProperties getInstance() {
		try {
			if (instance == null) {
				instance = new FormatosProperties();
			}
		} catch (ExceptionInInitializerError e) {
			logger.error("Unable to load configuration", e);
		} catch (RuntimeException e) {
			logger.error("unexpected runtime exception during initialization",
					e);
		}

		return instance;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String nombre) {
		return properties.getProperty(nombre);
	}

}
