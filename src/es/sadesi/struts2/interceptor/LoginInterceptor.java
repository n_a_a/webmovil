package es.sadesi.struts2.interceptor;

import java.util.Map;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.interceptor.Interceptor;

import es.sadesi.security.AuthenticationException;
import es.sadesi.security.SecurityManager;

public class LoginInterceptor implements Interceptor {

	/** Auto-generated serialization id */
	private static final long serialVersionUID = -8919532455846070296L;

	private static final String LOGIN_ATTEMPT = "loginAttempt";
	private static final String USERNAME = "usuario";
	private static final String PASSWORD = "clave";

	private static final Log logger = LogFactory.getLog(LoginInterceptor.class);
	
	private static SecurityManager securityManager = SecurityManager.getSecurityManager();

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {

		// Get the action context from the invocation so we can access the
		// HttpServletRequest and HttpSession objects.
		final ActionContext context = invocation.getInvocationContext();
		
		/*if ("Test".equals(invocation.getAction().toString()))
			return invocation.invoke();*/
		if ("es.sadesi.webmovil.action.Test".equals(invocation.getAction().getClass().getName())) {
			logger.debug ("Test - bypassing login");
			return invocation.invoke();
		}
		
		Map<String, Object> parametros = context.getParameters();
		Map<String, Object> atributos = context.getSession();
		
		// Is the user attempting to log in right now?
		String loginAttempt = null;
		Object objLoginAttempt = parametros.get(LOGIN_ATTEMPT);
		if (objLoginAttempt != null)
			loginAttempt = ((String[]) objLoginAttempt)[0];
		if (!StringUtils.isBlank(loginAttempt)) { // The user is attempting to log in.
			atributos.clear(); // Limpiamos la sesión.
		}

		// Is there a "user" object stored in the user's HttpSession?
		Object user = atributos.get(SecurityManager.USER_HANDLE);
		if (user == null) {
			// The user has not logged in yet.

			// Is the user attempting to log in right now?
			if (!StringUtils.isBlank(loginAttempt)) { // The user is attempting to log in.

				try {
					// Process the user's login attempt.
					if (processLoginAttempt(parametros, atributos)) {
						// The login succeeded send them the login-success page.
						return "login-success";
					} else {
						// The login failed. Set an error if we can on the
						// action.
						Object action = invocation.getAction();
						if (action instanceof ValidationAware) {
							((ValidationAware) action)
									.addActionError("Ha ocurrido un error.");
						}
					}
				} catch (AuthenticationException ae) {
					// The login failed. Set an error if we can on the action.
					String mensaje = ae.getMessage();
					if (mensaje == null || "".equals(mensaje))
						mensaje = "Usuario o clave incorrectos.";
					Object action = invocation.getAction();
					if (action instanceof ValidationAware) {
						((ValidationAware) action)
								.addActionError(mensaje);
					}
				}
			}

			// Either the login attempt failed or the user hasn't tried to login
			// yet,
			// and we need to send the login form.
			return "login";
		} else {
			return invocation.invoke();
		}
	}

	/**
	 * Attempt to process the user's login attempt delegating the work to the
	 * SecurityManager.
	 */
	public boolean processLoginAttempt(Map<String, Object> parametros,
			Map<String, Object> atributos) throws AuthenticationException {
		// Get the username and password submitted by the user from the
		// HttpRequest.
		String username = ((String[]) parametros.get(USERNAME))[0];
		String password = ((String[]) parametros.get(PASSWORD))[0];

		if (password == null || "".equals(password)) 
			return false;
		
		// Use the security manager to validate the user's username and
		// password.
		Object user = securityManager.login(username, password);

		if (user != null) {
			// The user has successfully logged in. Store their user object in
			// their HttpSession. Then return true.
			atributos.put(SecurityManager.USER_HANDLE, user);
			return true;
		} else {
			// The user did not successfully log in. Return false.
			return false;
		}
	}

}
