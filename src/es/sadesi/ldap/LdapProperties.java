package es.sadesi.ldap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.util.ClassLoaderUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

public final class LdapProperties implements Serializable {

	/**
	 * Auto-generado
	 */
	private static final long serialVersionUID = 94165823543675288L;

	private static final Log logger = LogFactory.getLog(LdapProperties.class);

	/**
	 * Singleton instance
	 */
	private static LdapProperties instance;

	private Properties properties;

	// ~ Constructors ///////////////////////////////////////////////////////////

	/**
	 * Don't use this constructor most of the time. To use LdapProperties as a
	 * singleton, use {@link #getInstance()} .
	 */
	public LdapProperties() {
		this("ldap.properties");
	}

	public LdapProperties(String filename) {
		String loc = "/" + filename;
		InputStream in = ClassLoaderUtil.getResourceAsStream(loc, this
				.getClass());

		if (in == null) {
			loc = filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/ldap-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/ldap-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loading using config : " + loc);
		}

		if (in == null) {
			throw new ExceptionInInitializerError("The configuration file "
					+ filename + " could not be found.");
		}

		try {
			properties = new Properties();
			properties.load(in);
		} catch (IOException e) {
			logger.error("Error al cargar el fichero de propiedades", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loaded using config : " + loc);
		}
	}

	// ~ Methods ////////////////////////////////////////////////////////////////

	public static void setInstance(LdapProperties ldapProperties) {
		instance = ldapProperties;
	}

	/**
	 * Entry-point to Singleton instance
	 */
	public static LdapProperties getInstance() {
		try {
			if (instance == null) {
				instance = new LdapProperties();
			}
		} catch (ExceptionInInitializerError e) {
			logger.error("Unable to load configuration", e);
		} catch (RuntimeException e) {
			logger.error("unexpected runtime exception during initialization",
					e);
		}

		return instance;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String nombre) {
		return properties.getProperty(nombre);
	}

}
