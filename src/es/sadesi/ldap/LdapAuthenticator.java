package es.sadesi.ldap;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LdapAuthenticator {

	private static final Log logger = LogFactory
			.getLog(LdapAuthenticator.class);

	private static final String LDAP_INITIAL_CONTEXT_FACTORY = "ldap.initial.context.factory";
	private static final String LDAP_URL = "ldap.url";
	private static final String LDAP_SEARCH_BASE = "ldap.searchBase";
	private static final String LDAP_UID_SEARCH_NAME = "ldap.uidSearchName";

	public static void autenticaUsuario(String usuario, String clave)
			throws AuthenticationException {

		try {

			LdapProperties ldapProperties = LdapProperties.getInstance();

			// Anonymous Authentication

			// Set up the environment for creating the initial context
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, ldapProperties
					.getProperty(LDAP_INITIAL_CONTEXT_FACTORY));
			env.put(Context.PROVIDER_URL, ldapProperties.getProperty(LDAP_URL));
			env.put(Context.SECURITY_AUTHENTICATION, "none");

			// Create the initial context
			DirContext ctx = new InitialDirContext(env);

			SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

			NamingEnumeration<SearchResult> ne = ctx.search(ldapProperties
					.getProperty(LDAP_SEARCH_BASE), ldapProperties
					.getProperty(LDAP_UID_SEARCH_NAME)
					+ "=" + usuario, sc);

			String dnUsuario = null;

			if (ne.hasMore()) {
				SearchResult sr = ne.next();
				dnUsuario = sr.getName();
			}

			// LDAP Authentication

			// Set up the environment for creating the initial context
			env.put(Context.INITIAL_CONTEXT_FACTORY, ldapProperties
					.getProperty(LDAP_INITIAL_CONTEXT_FACTORY));
			env.put(Context.PROVIDER_URL, ldapProperties.getProperty(LDAP_URL));

			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, dnUsuario + ","
					+ ldapProperties.getProperty(LDAP_SEARCH_BASE));
			env.put(Context.SECURITY_CREDENTIALS, clave);

			ctx = new InitialDirContext(env);

		} catch (NamingException ne) {
			logger.error(ne);
			throw new AuthenticationException();
		}
	}
}
