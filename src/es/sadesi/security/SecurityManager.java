package es.sadesi.security;

import java.util.Properties;

public abstract class SecurityManager {
	
	public static final String USER_HANDLE = "objetoUsuario";
	
	static private SecurityManager SECURITY_MANAGER = null;
	private static final String KEY_NOMBRE_SECURITY_MANAGER = "security.manager";
	
	static {
		try {
			Properties props = new Properties();
			props.load(SecurityManager.class.getResourceAsStream("SecurityManager.properties"));
			String nombreClase = props.getProperty(KEY_NOMBRE_SECURITY_MANAGER);
			Class<?> clase = Class.forName(nombreClase);
			SECURITY_MANAGER = (SecurityManager)clase.newInstance();
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}
	
	public static final SecurityManager getSecurityManager () {
		return SECURITY_MANAGER;
	}
	
	public abstract Object login (String username, String password) throws AuthenticationException;

}
