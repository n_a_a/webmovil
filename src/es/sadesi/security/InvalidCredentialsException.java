package es.sadesi.security;

public class InvalidCredentialsException extends Exception {

	/** Auto-generado */
	private static final long serialVersionUID = 84032137835040157L;

	public InvalidCredentialsException() {
		super();
	}

	public InvalidCredentialsException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidCredentialsException(String message) {
		super(message);
	}

	public InvalidCredentialsException(Throwable cause) {
		super(cause);
	}

}
