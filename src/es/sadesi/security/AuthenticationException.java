package es.sadesi.security;

public class AuthenticationException extends Exception {

	/** Auto-generado */
	private static final long serialVersionUID = 5364627674972622156L;	

	public AuthenticationException() {
		super();
	}

	public AuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}

	public AuthenticationException(String message) {
		super(message);
	}

	public AuthenticationException(Throwable cause) {
		super(cause);
	}

}
