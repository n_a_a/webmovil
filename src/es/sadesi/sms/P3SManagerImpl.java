package es.sadesi.sms;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.axis.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import es.sadesi.security.InvalidCredentialsException;
import es.sadesi.sms.ws.cliente.SmsWS;
import es.sadesi.sms.ws.cliente.SmsWSServiceLocator;

public class P3SManagerImpl implements P3SManager {

	private static final Log logger = LogFactory.getLog(P3SManagerImpl.class);

	private static String claveEmisor = P3SProperties.getInstance()
			.getProperty(P3SProperties.CLAVE_EMISOR);

	public static final String KEY_INFO_USUARIO_ENVIADOS_INTERNACIONALES = "enviados_internacionales";
	public static final String KEY_INFO_USUARIO_MAXIMO_INTERNACIONALES = "maximo_internacionales";
	public static final String KEY_INFO_USUARIO_MAXIMO_NACIONALES = "maximo_nacionales";
	public static final String KEY_INFO_USUARIO_MAXIMO_INTERNACIONALES_BLANDO = "maximo_internacionales_blando";
	public static final String KEY_INFO_USUARIO_MAXIMO_NACIONALES_BLANDO = "maximo_nacionales_blando";
	public static final String KEY_INFO_USUARIO_RECIBIDOS = "recibidos";
	public static final String KEY_INFO_USUARIO_ENVIADOS_NACIONALES = "enviados_nacionales";
	public static final String KEY_INFO_USUARIO_ENVIADOS_TOTALES = "enviados_totales";
	public static final String KEY_INFO_USUARIO_ENVIADOS_CORPORATIVOS = "enviados_corporativos";
	public static final String KEY_INFO_USUARIO_ENVIADOS_NO_CORPORATIVOS = "enviados_no_corporativos";
	public static final String KEY_INFO_USUARIO_UID = "uid";
	public static final String KEY_INFO_USUARIO_ID = "id";
	public static final String KEY_INFO_USUARIO_NIVEL = "nivel_usuario";
	public static final String KEY_INFO_USUARIO_FIRMA = "firma_personal";
	public static final String KEY_INFO_USUARIO_TLF_MOVIL = "tlfno_movil";
	public static final String KEY_INFO_USUARIO_TLF_MOVIL_DEST = "tlfno_movil_dest";
	public static final String KEY_INFO_USUARIO_MSGS_PAGINA = "msgs_pagina";
	public static final String KEY_INFO_USUARIO_RECIBIR_EMAIL = "recibir_email";

	public static final String KEY_INFO_USUARIO_PERMISO_FIRMA = "per_firma";
	public static final String KEY_INFO_USUARIO_PERMISO_SOLO_CORPORATIVO = "per_solocorp";
	public static final String KEY_INFO_USUARIO_PERMISO_MASIVO = "per_masivo";
	public static final String KEY_INFO_USUARIO_PERMISO_PRIORIDAD = "per_prioridad";
	public static final String KEY_INFO_USUARIO_PERMISO_RECEPCION = "per_recepcion";
	public static final String KEY_INFO_USUARIO_PERMISO_RECIBO = "per_recibo";

	public static final String KEY_SMS_STATUS_STRING = "statusStrin";
	public static final String KEY_SMS_STATUS_CODE = "statusCode";
	public static final String KEY_SMS_CODIGOS = "codigos";

	public static final String KEY_INFO_EMISOR_ID = "id_emisor";
	public static final String KEY_INFO_EMISOR_NAVISION = "navision";
	public static final String KEY_INFO_EMISOR_DESCRIPCION = "descripcion";
	public static final String KEY_INFO_EMISOR_LIMITE = "limite";
	public static final String KEY_INFO_EMISOR_LIMITE_BLANDO = "limite_blando";
	public static final String KEY_INFO_EMISOR_EMAIL = "email";
	public static final String KEY_INFO_EMISOR_TELEFONO = "tlfn";
	public static final String KEY_INFO_EMISOR_FIRMA = "firma";
	public static final String KEY_INFO_EMISOR_IP = "ip";
	public static final String KEY_INFO_EMISOR_ALFA = "alfa";
	public static final String KEY_INFO_EMISOR_TON = "ton";
	public static final String KEY_INFO_EMISOR_NPI = "npi";
	public static final String KEY_INFO_EMISOR_CLAVE = "clave";
	public static final String KEY_INFO_EMISOR_ENVIADOS = "enviados";
	public static final String KEY_INFO_EMISOR_ENVIADOS_INTERNACIONALES = "enviados_internacionales";
	public static final String KEY_INFO_EMISOR_RECIBIDOS = "recibidos";
	public static final String KEY_INFO_EMISOR_ENVIADOS_CORPORATIVOS = "enviados_corporativos";
	public static final String KEY_INFO_EMISOR_PENDIENTES = "pendientes";
	public static final String KEY_INFO_EMISOR_ERROR = "error";
	public static final String KEY_INFO_EMISOR_ACUSE = "acuse";
	public static final String KEY_INFO_EMISOR_PRIORIDAD_ALTA = "prioridad_alta";
	public static final String KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS = "usuarios_mas_activos";
	public static final String KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_USUARIO = "usuario";
	public static final String KEY_INFO_EMISOR_USUARIOS_MAS_ACTIVOS_NUM_MENSAJES = "numero_mensajes";
	public static final String KEY_INFO_EMISOR_ESTADISTICAS_USUARIOS_NUM_MENSAJES = "n_mensajes_enviados";

	public static final String KEY_CONTACTOS_NOMBRE = "nombre";
	public static final String KEY_CONTACTOS_ID_PADRE = "id_padre";
	public static final String KEY_CONTACTOS_ID_GRUPO = "id_grupo";
	public static final String KEY_CONTACTOS_EMAIL = "email";
	public static final String KEY_CONTACTOS_ID_CONTACTO = "id_contacto";
	public static final String KEY_CONTACTOS_TLF_MOVIL = "tlfno_movil";
	public static final String KEY_CONTACTOS_TLF_FIJO = "tlfno_fijo";
	public static final String KEY_CONTACTOS_ENTIDAD = "entidad";
	public static final String KEY_CONTACTOS_NOTAS = "notas";

	@Override
	public String identificaUsuario(String usuario)
			throws InvalidCredentialsException, Exception {

		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.identifiticaUsuario(usuario);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (AxisFault af) {
			tratarAxisFault(af);
			logger.error("Error al intentar la identificación del usuario "
					+ usuario, af);
			throw af;
		} catch (Exception e) {
			logger.error("Error al intentar la identificación del usuario "
					+ usuario, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> infoUsuarioEstaticos(String usuario,
			String emisor) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.infoUsuarioEstaticos(usuario, emisor, claveEmisor);
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar la información del usuario "
							+ usuario, e);
			throw e;
		}
	}

	public int estadisticasUsuario(String usuario, String emisor,
			String idEstadistica, String desde, String hasta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.estadisticasUsuario(usuario, emisor, claveEmisor,
					idEstadistica, desde, hasta);
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar la información del usuario "
							+ usuario, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<HashMap<String, Object>> listaCarpetasUsuario(String usuario,
			String emisor) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.listaCarpetasUsuario(usuario, emisor, claveEmisor);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar la lista de carpetas del usuario "
							+ usuario, e);
			throw new Exception(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> enviaSMS(String usuario, String emisor,
			String destinatario, String texto, int prioridad, int acuseRecibo,
			int alfanumerico, String fechaCaducidad, String idMensaje)
			throws InvalidCredentialsException, LimitExceededException,
			Exception {

		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();

			return smsWS.SMS(usuario, emisor, claveEmisor, destinatario, texto,
					prioridad, acuseRecibo, alfanumerico, fechaCaducidad,
					idMensaje);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (es.sadesi.sms.ws.cliente.LimitExceededException e) {
			throw new LimitExceededException();
		} catch (AxisFault e) {
			String fs = e.getFaultString();
			if (fs.indexOf("Limit") > 0) {
				logger.error("Excedido el límite del emisor");
				throw new LimitExceededException();
			} else {
				logger.error("Error al intentar la identificación del usuario "
						+ usuario, e);
				throw new Exception(e);
			}
		} catch (Exception e) {
			logger.error("Excepción", e);
			throw e;
		}
	}

	private void tratarAxisFault(AxisFault af) throws Exception {
		String nombreExcepcion = null;
		org.w3c.dom.Element exceptionName = af
				.lookupFaultDetail(org.apache.axis.Constants.QNAME_FAULTDETAIL_EXCEPTIONNAME);
		if (exceptionName != null) {
			nombreExcepcion = exceptionName.getTextContent();
		} else {
			String faultString = af.getFaultString();
			if (faultString != null) {
				int pos = faultString.indexOf(":");
				if (pos != -1)
					nombreExcepcion = faultString.substring(0, pos);
			}
		}

		if ("sadesi.sms.ws.InvalidCredentialsException".equals(nombreExcepcion)) {
			throw new InvalidCredentialsException();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Vector<HashMap<String, Object>> obtieneMensajesCarpetaUsuario(
			String usuario, String emisor, String carpeta, String desde,
			String hasta, int regIni, int regFin) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.obtieneMensajesCarpetaUsuario(usuario, emisor,
					claveEmisor, carpeta, desde, hasta, regIni, regFin);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar la lista de carpetas del usuario "
							+ usuario, e);
			throw new Exception(e);
		}
	}

	public int obtieneNumMensajesCarpetaUsuario(String usuario, String emisor,
			String carpeta, String desde, String hasta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.obtieneNumMensajesCarpetaUsuario(usuario, emisor,
					claveEmisor, carpeta, desde, hasta);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar la lista de carpetas del usuario "
							+ usuario, e);
			throw new Exception(e);
		}
	}

	@Override
	public void mueveMensajeCarpeta(String usuario, String emisor,
			String idEsme, String idCarpeta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.mueveMensajeCarpeta(usuario, emisor, claveEmisor, idEsme,
					idCarpeta);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error("Error al intentar mover el mensaje " + idEsme
					+ " a la carpeta " + idCarpeta, e);
			throw new Exception(e);
		}
	}

	public void creaCarpetaUsuario(String usuario, String emisor,
			String nombreCarpeta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.creaCarpetaUsuario(usuario, emisor, claveEmisor,
					nombreCarpeta);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error("Error al intentar crear la carpeta " + nombreCarpeta
					+ " para el usuario " + usuario, e);
			throw new Exception(e);
		}
	}

	public void eliminaCarpetaUsuario(String usuario, String emisor,
			String idCarpeta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.eliminaCarpetaUsuario(usuario, emisor, claveEmisor, idCarpeta);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error("Error al intentar eliminar la carpeta con id "
					+ idCarpeta + " para el usuario " + usuario, e);
			throw new Exception(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> infoEmisor(String emisor) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.infoEmisor(emisor, claveEmisor);
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar la información del emisor "
							+ emisor, e);
			throw e;
		}
	}

	public int estadisticasEmisor(String emisor, String idEstadistica,
			String desde, String hasta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.estadisticasEmisor(emisor, claveEmisor, idEstadistica,
					desde, hasta);
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar la información estadística del emisor "
							+ emisor, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<HashMap<String, Object>> usuariosMasActivosEmisor(
			String emisor, String desde, String hasta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.usuariosMasActivosEmisor(emisor, claveEmisor, desde,
					hasta);
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar los usuarios más activos del emisor "
							+ emisor, e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Vector<Map<String, Object>> usuariosMasActivosEmisorPorIntervalo(final String idEmisor,
			final Calendar fechaDesde, final Calendar fechaHasta) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.usuariosMasActivosEmisorPorIntervalo(idEmisor, claveEmisor, fechaDesde,
					fechaHasta);
		} catch (Exception e) {
			logger.error(
					"Error al intentar recuperar los usuarios más activos por intervalo del emisor "
							+ idEmisor, e);
			throw e;
		}
	}

	public void modificaAtributoUsuario(String usuario, String emisor,
			String nombreAtributo, String nuevoValor) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.modificaAtributoUsuario(usuario, emisor, claveEmisor,
					nombreAtributo, nuevoValor);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error("Error al intentar establecer " + nombreAtributo
					+ " = '" + nuevoValor + "' para " + usuario, e);
			throw new Exception(e);
		}
	}

	public void modificaAtributoEmisor(String emisor, String emisorModificado,
			String nombreAtributo, String nuevoValor) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.modificaAtributoEmisor(emisor, claveEmisor, emisorModificado,
					nombreAtributo, nuevoValor);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error("Error al intentar establecer " + nombreAtributo
					+ " = '" + nuevoValor + "' para " + emisor, e);
			throw new Exception(e);
		}
	}

	public void creaUsuario(String usuario, String emisor, String limite,
			String limite_blando, String limite_internacional,
			String limite_internacional_blando, String tlfno_movil,
			String nivel, String per_prioridad, String per_recibo,
			String per_solocorp, String per_firma, String per_recepcion,
			String per_masivo) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.creaUsuario(emisor, claveEmisor, usuario, limite,
					limite_blando, limite_internacional,
					limite_internacional_blando, tlfno_movil, nivel,
					per_prioridad, per_recibo, per_solocorp, per_firma,
					per_recepcion, per_masivo);
		} catch (Exception e) {
			logger.error("Error al intentar crear el usuario " + usuario);
			throw e;
		}
	}

	public void eliminaUsuario(String emisor, String usuario) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.eliminaUsuario(emisor, claveEmisor, usuario);
		} catch (Exception e) {
			logger.error("Error al intentar borrar el usuario " + usuario);
			throw e;
		}
	}

	public void eliminaEmisor(String emisor, String id_emisor_borrar)
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.eliminaEmisor(emisor, claveEmisor, id_emisor_borrar);
		} catch (Exception e) {
			logger.error("Error al intentar borrar el emisor " + emisor);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<HashMap<String, String>> listaEmisores(String idEmisor)
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.listaEmisores(idEmisor, claveEmisor);
		} catch (Exception e) {
			logger.error("Error al intentar obtener una lista de emisores", e);
			throw e;
		}
	}

	public void creaEmisor(String emisor, String id_nuevo_emisor, String ip,
			String descripcion, String clave, String limite, String firma,
			String tlfn, String ton, String npi) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.creaEmisor(emisor, claveEmisor, id_nuevo_emisor, ip,
					descripcion, clave, limite, firma, tlfn, ton, npi);
		} catch (Exception e) {
			logger.error("Error al intentar crear el emisor " + id_nuevo_emisor);
			throw e;
		}
	}

	public String emisorUsuario(String idUsuario) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.emisorUsuario(idUsuario);
		} catch (Exception e) {
			logger.error("Error al intentar encontrar el emisor de "
					+ idUsuario);
			throw e;
		}
	}

	public String version() throws Exception {
		SmsWSServiceLocator locator = new SmsWSServiceLocator();
		SmsWS smsWS = locator.getP3SWebmovilWS();
		return smsWS.version();
	}

	@SuppressWarnings("unchecked")
	public Vector<HashMap<String, String>> listaUsuariosEmisor(String emisor)
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.listaUsuariosEmisor(emisor, claveEmisor);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (Exception e) {
			logger.error("Error al intentar obtener la lista de usuarios de "
					+ emisor, e);
			throw new Exception(e);
		}
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, Object> generalReadBD(String operacion,
			HashMap<String, Object> args) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.generalReadBD(operacion, args);
		} catch (Exception e) {
			logger.error("Error al realizar la lectura genérica '" + operacion
					+ "' en la BD.", e);
			throw new Exception(e);
		}
	}

	public void generalWriteBD(String operacion, HashMap<String, Object> args)
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.generalWriteBD(operacion, args);
		} catch (Exception e) {
			logger.error("Error al realizar la escritura genérica '"
					+ operacion + "' en la BD.", e);
			throw new Exception(e);
		}
	}

	public void setAltText(String idEmisor, String altText) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.p3SPushSetAltText(idEmisor, altText);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
	}

	public void setAltTextEnable(String idEmisor, boolean enable)
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.p3SPushSetAltTextEnable(idEmisor, enable);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
	}

	public String getAltText(String idEmisor) throws Exception {
		String result = null;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = smsWS.p3SPushGetAltText(idEmisor);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
		return result;
	}

	public boolean getAltTextEnable(String idEmisor) throws Exception {
		boolean result = false;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = smsWS.p3SPushGetAltTextEnable(idEmisor);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, String> getInfoLocClientePush(String tlfno)
			throws Exception {
		HashMap<String, String> result = null;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = (HashMap<String, String>) smsWS
					.p3SPushGetInfoLocClientePush(tlfno);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
		return result;
	}

	public boolean esClientePush(String tlfno) throws Exception {
		boolean result = false;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = smsWS.p3SEsClientePush(tlfno);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, Object> SMSEx(String usuario, String emisor,
			String destinatario, String texto, int prioridad, int acuseRecibo,
			int alfanumerico, String fechaCaducidad, String idMensaje, int push)
			throws InvalidCredentialsException, LimitExceededException,
			SyntaxErrorException, Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();

			return smsWS.SMSEx(usuario, emisor, claveEmisor, destinatario,
					texto, prioridad, acuseRecibo, alfanumerico,
					fechaCaducidad, idMensaje, 0, push);
		} catch (es.sadesi.sms.ws.cliente.InvalidCredentialsException e) {
			throw new InvalidCredentialsException();
		} catch (es.sadesi.sms.ws.cliente.LimitExceededException e) {
			throw new LimitExceededException();
		} catch (AxisFault e) {
			String fs = e.getFaultString();
			if (fs.indexOf("Limit") > 0) {
				logger.error("Excedido el límite del emisor");
				throw new LimitExceededException();
			} else if (fs.indexOf("dado de alta") > 0) {
				logger.error("No está dado de alta como cliente push");
				throw new SyntaxErrorException();
			} else {
				logger.error("Error al intentar la identificación del usuario "
						+ usuario, e);
				throw new Exception(e);
			}
		} catch (Exception e) {
			logger.error("Excepción", e);
			throw e;
		}
	}

	public int nuevaFuenteSuscripcion(String tipo, String idEmisor,
			String desc, String urlAlta, String urlBaja, String method)
			throws Exception {
		int result = -1;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = smsWS.p3SPushNuevaFuenteSuscripcion(tipo, idEmisor, desc, urlAlta, urlBaja, method);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
		return result;
	}

	public void eliminaFuenteSuscripcion(int id) throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.p3SPushEliminaFuenteSuscripcion(id);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}		
	}

	@SuppressWarnings("unchecked")
	public Vector<HashMap<String, String>> getFuentesEmisor(
			String idEmisor) throws Exception {
		Vector<HashMap<String, String>> result = null;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = smsWS.p3SPushGetFuentesEmisor(idEmisor);
		} catch (Exception e) {
			logger.error("Error al ", e);
			throw new Exception(e);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String,String> obtieneMensajePush (int id, String tlfno) throws Exception {
		HashMap<String,String> result = null;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = smsWS.p3SObtieneMensaje (id, tlfno, 999999, "P3SKEY");
		} catch (Exception e) {
			logger.error ("Error trayéndome un mensaje push ", e);
			throw new Exception(e);
		}
		return result;
	}

	@Override
	public boolean getDisableInvitationSMS(final String idEmisor) throws Exception {
		boolean result = false;
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			result = smsWS.p3SPushGetDisableInvitationSMS(idEmisor);
		} catch (Exception e) {
			logger.error("Error al obtener el campo que indica si se ha desactivado el envio de notificaciones SMS", e);
			throw new Exception(e);
		}
		return result;
	}
	
	public void setDisableInvitationSMS(final String idEmisor, final boolean disable)
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.p3SPushSetDisableInvitationSMS(idEmisor, disable);
		} catch (Exception e) {
			logger.error("Error al establecer el campo desactivar el envio de notificaciones SMS", e);
			throw new Exception(e);
		}
	}
	
	@Override
	public void procesarEstadisticas(final String desde, final String hasta)
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			smsWS.p3SProcessStatistics(desde, hasta);
		} catch (Exception e) {
			logger.error("Error al procesar las estadisticas", e);
			throw new Exception(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> obtenerEstadisticas(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.p3SGetStatistics(idEmisor, claveEmisor, fechaDesde, fechaHasta);
		} catch (Exception e) {
			logger.error("Error al obtener las estadisticas", e);
			throw new Exception(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> obtenerEstadisticasAgrupadasPorEmisor(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.p3SGetStatisticsGroupByEmisor(idEmisor, claveEmisor, fechaDesde, fechaHasta);
		} catch (Exception e) {
			logger.error("Error al obtener las estadisticas", e);
			throw new Exception(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Vector<Map<String, Object>> obtenerEstadisticasUsuarios(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.p3SGetUsersStatistics(idEmisor, claveEmisor, fechaDesde, fechaHasta);
		} catch (Exception e) {
			logger.error("Error al obtener las estadisticas de usuarios", e);
			throw new Exception(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Vector<Map<String, Object>> obtenerEstadisticasUsuariosAgrupadasPorUsuario(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception {
		try {
			SmsWSServiceLocator locator = new SmsWSServiceLocator();
			SmsWS smsWS = locator.getP3SWebmovilWS();
			return smsWS.p3SGetUsersStatisticsGroupByUser(idEmisor, claveEmisor, fechaDesde, fechaHasta);
		} catch (Exception e) {
			logger.error("Error al obtener las estadisticas de usuarios", e);
			throw new Exception(e);
		}
	}
}
