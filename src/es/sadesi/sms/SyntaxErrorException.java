package es.sadesi.sms;

public class SyntaxErrorException extends Exception {

	/** Auto-generado */
	private static final long serialVersionUID = 7524658364074751976L;

	public SyntaxErrorException() {
		super();
	}

	public SyntaxErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public SyntaxErrorException(String message) {
		super(message);
	}

	public SyntaxErrorException(Throwable cause) {
		super(cause);
	}

}
