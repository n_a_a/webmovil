package es.sadesi.sms;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.util.ClassLoaderUtil;

public class P3SAgenManagerProperties implements Serializable {
	private static final long serialVersionUID = -263910090249128578L;
	private static final Log logger = LogFactory.getLog(P3SAgenManagerProperties.class);
	private static P3SAgenManagerProperties instance;
	private Properties properties;
	
	public static final String BD_DRIVER = "bd.driver";
	public static final String BD_URL = "bd.url";
	public static final String BD_USER = "bd.user";
	public static final String BD_PASS = "bd.pass";
	public static final String BD_MINLIMIT = "bd.minLimit";
	public static final String BD_MAXLIMIT = "bd.maxLimit";
	public static final String BD_TIME_BETWEEN_EVICTION_RUNS_MILLIS = "bd.timeBetweenEvictionRunsMillis";
	public static final String BD_NUM_TESTS_PER_EVICTION = "bd.numTestsPerEviction";
	public static final String BD_MIN_EVICTABLE_IDLE_TIME_MILLIS = "bd.minEvictableIdleTimeMillis";
	public static final String BD_MAX_WAIT = "bd.maxWait";
	
	public P3SAgenManagerProperties() {
		this("p3sAgen.properties");
	}
	
	public P3SAgenManagerProperties(String filename) {
		String loc = "/" + filename;
		InputStream in = ClassLoaderUtil.getResourceAsStream(loc, this
				.getClass());

		if (in == null) {
			loc = filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/p3s-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/p3s-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loading using config : " + loc);
		}

		if (in == null) {
			throw new ExceptionInInitializerError("The configuration file "
					+ filename + " could not be found.");
		}

		try {
			properties = new Properties();
			properties.load(in);
		} catch (IOException e) {
			logger.error("Error al cargar el fichero de propiedades", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loaded using config : " + loc);
		}
	}
	
	public static void setInstance(P3SAgenManagerProperties p3sAgenManagerProperties) {
		instance = p3sAgenManagerProperties;
	}
	
	public static P3SAgenManagerProperties getInstance() {
		try {
			if (instance == null) {
				instance = new P3SAgenManagerProperties();
			}
		} catch (ExceptionInInitializerError e) {
			logger.error("Unable to load configuration", e);
		} catch (RuntimeException e) {
			logger.error("unexpected runtime exception during initialization",
					e);
		}

		return instance;
	}
	
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String nombre) {
		return properties.getProperty(nombre);
	}
}
