package es.sadesi.sms;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


// import es.sadesi.sms.ws.cliente.SmsWS;
// import es.sadesi.sms.ws.cliente.SmsWSServiceLocator;
import es.sadesi.webmovil.util.UtilBD;

public class P3SAgenManagerImpl implements P3SAgenManager {
	private static final Log logger = LogFactory.getLog(P3SAgenManagerImpl.class);
	// private static String claveEmisor = P3SProperties.getInstance().getProperty(P3SProperties.CLAVE_EMISOR);

	private static UtilBD utilBD;
	
	static {
		try {
			logger.info ("Inicializando UtilBD...");
			utilBD = new UtilBD (
					P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_DRIVER),
					P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_URL),
					P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_USER),
					P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_PASS),
					Integer.parseInt(P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_MINLIMIT)),
					Integer.parseInt(P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_MAXLIMIT)),
					Long.parseLong(P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_TIME_BETWEEN_EVICTION_RUNS_MILLIS)),
					Integer.parseInt(P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_NUM_TESTS_PER_EVICTION)),
					Long.parseLong(P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_MIN_EVICTABLE_IDLE_TIME_MILLIS)),
					Long.parseLong(P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_MAX_WAIT))
			);
			
			logger.info("UtilBD inicializada con estos parámetros: ");
			logger.info("maxLimit = " + P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_MAXLIMIT) +
					", timeBetweenEvictionRunsMillis = " + P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_TIME_BETWEEN_EVICTION_RUNS_MILLIS) + 
					", numTestsPerEviction = " + P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_NUM_TESTS_PER_EVICTION) +
					", minEvictableIdleTimeMillis = " + P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_MIN_EVICTABLE_IDLE_TIME_MILLIS) +
					", maxWait = " + P3SAgenManagerProperties.getInstance().getProperty(P3SAgenManagerProperties.BD_MAX_WAIT) + ".");
		} catch (NumberFormatException e) {
			logger.error("Error al inicializar UtilBD. Comprueba minLimit y maxLimit en p3sAgen.properties");
		} catch (Exception e) {
			logger.error("Error al inicializar UtilBD.", e);
		}
	}
	
	@Override
	public Vector<HashMap<String, Object>> listaContactosGrupo(String idUsuario,
			String emisor, String idGrupo, int regIni, int regFin)
			throws Exception {
		
		logger.debug("> listaContactosGrupo");
		
		Vector<HashMap<String, Object>> resultado = new Vector<HashMap<String, Object>> ();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = utilBD.getConnection ();
			StringBuffer select;
			if ("0".equals(idGrupo)) {
				select = new StringBuffer ("select * from ( select /*+ FIRST_ROWS(n) */ a.*, ROWNUM rnum from (" +
						"SELECT id_contacto, nombre, tlfno_movil, email, tlfno_fijo, entidad, notas FROM p3s_agenda WHERE " +
						"id_usuario = '" + idUsuario + "' AND id_grupo = '0' ORDER BY nombre) a " +
						"where ROWNUM <= " + regFin + " ) where rnum  >= " + regIni);
			} else {
				select = new StringBuffer ("select * from ( select /*+ FIRST_ROWS(n) */ a.*, ROWNUM rnum from (" +
						"SELECT p3s_agenda.id_contacto, p3s_agenda.nombre, p3s_agenda.tlfno_movil, p3s_agenda.email, p3s_agenda.tlfno_fijo, " +
						"p3s_agenda.entidad, p3s_agenda.notas FROM p3s_agenda, p3s_grupo_contacto WHERE " +
						"p3s_agenda.id_usuario = '" + idUsuario + "' AND p3s_agenda.id_grupo <= 0 AND " +
						"p3s_grupo_contacto.id_contacto = p3s_agenda.id_contacto AND p3s_grupo_contacto.id_grupo = '" + idGrupo + "' " +
						"ORDER BY nombre) a where ROWNUM <= " + regFin + " ) where rnum  >= " + regIni);
			}
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			while (rs.next()) {
				HashMap<String, Object> entry = new HashMap<String, Object> ();
				entry.put ("id_grupo", "");
				entry.put ("id_contacto", rs.getString (1));
				entry.put ("id_padre", idGrupo);
				entry.put ("nombre", rs.getString (2));
				entry.put ("tlfno_movil", rs.getString (3));
				entry.put ("email", rs.getString (4));
				entry.put ("tlfno_fijo", rs.getString (5));
				entry.put ("entidad", rs.getString (6));
				entry.put ("notas", rs.getString (7));
				resultado.add (entry);
			}
			return resultado;
		} catch (SQLException e) {
			logger.error("Error al obtener los contactos del grupo " + idGrupo + ".", e);
			throw new Exception(e);
		} finally {
			utilBD.closeAll("listaContactosGrupo", rs, ps, con);
			logger.debug("< listaContactosGrupo");
		}
	}
	
	@Override
	public Vector<HashMap<String, Object>> listaSubgruposGrupo(String idUsuario,
			String emisor, String idGrupo, int regIni, int regFin)
			throws Exception {
		logger.debug("> listaSubgruposGrupo");
		Vector<HashMap<String, Object>> resultado = new Vector<HashMap<String, Object>> ();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = utilBD.getConnection();
			StringBuffer select = new StringBuffer ("select * from ( select /*+ FIRST_ROWS(n) */ a.*, ROWNUM rnum from (" +
					"SELECT p3s_grupos.id_grupo, p3s_agenda.nombre, p3s_agenda.id_contacto FROM p3s_grupos, p3s_agenda WHERE " +
					"p3s_grupos.id_usuario = '" + idUsuario + "' AND p3s_grupos.id_padre = '" + idGrupo + "' AND p3s_grupos.id_grupo = p3s_agenda.id_grupo ORDER BY nombre) a " +
					"where ROWNUM <= " + regFin + " ) where rnum  >= " + regIni);
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			while (rs.next ()) {
				HashMap<String, Object> entry = new HashMap<String, Object> ();
				entry.put ("id_grupo", rs.getString (1));
				entry.put ("id_contacto", rs.getString (3));
				entry.put ("id_padre", idGrupo);
				entry.put ("nombre", rs.getString (2));
				entry.put ("tlfno_movil", "");
				entry.put ("email", "");
				entry.put ("tlfno_fijo", "");
				entry.put ("entidad", "");
				entry.put ("notas", "");
				resultado.add (entry);
			}
			return resultado;
		} catch (SQLException e) {
			logger.error ("Error al obtener los subgrupos del grupo " + idGrupo + ".", e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("listaSubgruposGrupo", rs, ps, con);
			logger.debug("< listaSubgruposGrupo");
		}
	}
	
	@Override
	public int cuentaContactosGrupo(String idUsuario, String emisor,
			String idGrupo) throws Exception {
		logger.debug("> cuentaContactosGrupo");
		int resultado = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = utilBD.getConnection();
			StringBuffer select;
			if ("0".equals(idGrupo)) {
				select = new StringBuffer ("SELECT COUNT(*) FROM p3s_agenda WHERE id_usuario = '" + idUsuario + "' AND id_grupo = '0'");
			} else {
				select = new StringBuffer ("SELECT COUNT(*) FROM p3s_grupo_contacto WHERE id_grupo = '" + idGrupo + "'");
			}
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			rs.next();
			resultado = rs.getInt(1);
			return resultado;
		} catch (SQLException e) {
			logger.error(
					"Error al intentar obtener el número de contactos en el grupo '"
							+ idGrupo + "' de la agenda del usuario '"
							+ idUsuario + "'.", e);
			throw new Exception(e);
		} finally {
			utilBD.closeAll("cuentaContactosGrupo", rs, ps, con);
			logger.debug("< cuentaContactosGrupo");
		}
	}
	
	@Override
	public int cuentaSubgruposGrupo(String idUsuario, String emisor,
			String idGrupo) throws Exception {
		logger.debug("> cuentaSubgruposGrupo");
		int resultado = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = utilBD.getConnection();
			StringBuffer select = new StringBuffer (
					"SELECT COUNT (p3s_grupos.id_grupo) FROM p3s_grupos, p3s_agenda WHERE p3s_grupos.id_usuario = '" + idUsuario + 
					"' AND p3s_grupos.id_padre = '" + idGrupo + "' AND p3s_grupos.id_grupo = p3s_agenda.id_grupo");
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			rs.next ();
			resultado = rs.getInt (1);
			return resultado;
		} catch (SQLException e) {
			logger.error ("Error al intentar contar los subgrupos del grupo " + idGrupo + ".", e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("cuentaSubgruposGrupo", rs, ps, con);
			logger.debug("< cuentaSubgruposGrupo");
		}
	}
	
	@Override
	public Map<String, Object> agendaObtieneGrupo(String idUsuario,
			String emisor, String idGrupo) throws Exception {
		logger.debug("> agendaObtieneGrupo");
		HashMap<String, Object> resultado = new HashMap<String, Object> ();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = utilBD.getConnection();
			StringBuffer select = new StringBuffer ("SELECT p3s_agenda.id_contacto, p3s_agenda.nombre, p3s_agenda.id_grupo, p3s_grupos.id_padre FROM " +
					"p3s_agenda, p3s_grupos WHERE p3s_agenda.id_usuario = '" + idUsuario + "' AND " +
					"p3s_agenda.id_grupo = p3s_grupos.id_grupo AND p3s_grupos.id_grupo = '" + idGrupo + "'");
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			rs.next ();
			
			resultado.put ("id_grupo", rs.getString (3));
			resultado.put ("id_contacto", rs.getString (1));
			resultado.put ("id_padre", rs.getString (4));
			resultado.put ("nombre", rs.getString (2));
			resultado.put ("tlfno_movil", "");
			resultado.put ("email", "");
			resultado.put ("tlfno_fijo", "");
			resultado.put ("entidad", "");
			resultado.put ("notas", "");
			
			return resultado;
		} catch (SQLException e){
			logger.error ("Error al obtener el grupo " + idGrupo + " de la agenda.", e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaObtieneGrupo", rs, ps, con);
			logger.debug("< agendaObtieneGrupo");
		}
	}
	
	@Override
	public void agendaEliminaContacto(String idUsuario, String emisor,
			String idContacto) throws Exception {
		logger.debug("> agendaEliminaContacto");
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = utilBD.getConnection ();
			
			// Primero consultamos si es usuario o grupo
			StringBuffer select = new StringBuffer ("SELECT id_grupo FROM p3s_agenda WHERE id_contacto = '" + idContacto + "'");
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			rs.next ();
			int idGrupoTemp = Integer.parseInt (rs.getString (1));
			
			// Borramos el contacto en sí
			StringBuffer update = new StringBuffer ("DELETE FROM p3s_agenda WHERE id_contacto = '" + idContacto + "' AND id_usuario = '" + idUsuario + "'");
			ps = con.prepareStatement (update.toString ());
			ps.executeUpdate ();
			
			// Si idGrupoTemp != 0, entonces tenemos que borrar en p3s_grupo_contacto (si es -1) 
			// o p3s_grupos (si es > 0)
			if (idGrupoTemp == -1) {
				update = new StringBuffer ("DELETE FROM p3s_grupo_contacto WHERE id_contacto = '" + idContacto + "'");
				ps = con.prepareStatement (update.toString ());
				ps.executeUpdate ();
			} else if (idGrupoTemp == 0) {
				update = new StringBuffer ("DELETE FROM p3s_grupos WHERE id_grupo = '" + idGrupoTemp + "'");
				ps = con.prepareStatement (update.toString ());
				ps.executeUpdate ();
			}
		} catch (SQLException e) {
			logger.error ("Error al eliminar el contacto " + idContacto, e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaEliminaContacto", rs, ps, con);
			logger.debug("< agendaEliminaContacto");
		}
	}
	
	@Override
	public void agendaCreaContacto(String idUsuario, String emisor,
			String idPadre, String nombre, String tlfMovil, String email,
			String tlfFijo, String entidad, String notas) throws Exception {
		logger.debug("> agendaCreaContacto");
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = utilBD.getConnection();
			StringBuffer update = null;
			
			if ("0".equals (idPadre)) {
				update = new StringBuffer (
						"INSERT INTO p3s_agenda (id_grupo, id_usuario, nombre, tlfno_movil, email, tlfno_fijo, entidad, notas) " + 
						"VALUES ('0', '" + idUsuario + "', '" + nombre + "', '" + tlfMovil + "', '" + email + "', '" + 
						tlfFijo + "', '" + entidad + "', '" + notas + "')" );
				ps = con.prepareStatement (update.toString ());
				ps.executeUpdate ();
			} else {
				// Queremos que el update nos devuelva el valor del id_contacto autogenerado				
				String keyColumn [] = {"id_contacto"};

				// Preparamos nuestro update (un INSERT)
				update = new StringBuffer (
						"INSERT INTO p3s_agenda (id_grupo, id_usuario, nombre, tlfno_movil, email, tlfno_fijo, entidad, notas) " + 
						"VALUES ('-1', '" + idUsuario + "', '" + nombre + "', '" + tlfMovil + "', '" + email + "', '" +
						tlfFijo + "', '" + entidad + "', '" + notas + "')" );
				ps = con.prepareStatement (update.toString (), keyColumn);
				ps.executeUpdate ();
				
				// Obtenemos en un resultset el id_contacto autogenerado y lo guardamos en un String
				rs = ps.getGeneratedKeys ();
				rs.next ();
				String idContacto = rs.getString (1);
				
				// Actualizamos la table p3s_grupo_contacto con el par idGrupo, idContacto
				update = new StringBuffer (
						"INSERT INTO p3s_grupo_contacto (id_grupo, id_contacto) VALUES ('" + idPadre + "', '" + idContacto + "')");
				ps = con.prepareStatement (update.toString ());
				ps.executeUpdate ();
			}
		} catch (SQLException e) {
			logger.error ("Error al crear el contacto " + nombre + " en el grupo " + idPadre, e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaCreaContacto", rs, ps, con);
			logger.debug("< agendaCreaContacto");
		}
	}
	
	@Override
	public void agendaCreaGrupo(String idUsuario, String emisor, String idPadre,
			String nombre) throws Exception {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		logger.debug("> agendaCreaGrupo");
		try {
			con = utilBD.getConnection();
			String dummy = agendaCreaGrupoImpl (idUsuario, emisor, idPadre, nombre, con, ps, rs);
			logger.debug ("Creado " + dummy);
		} catch (Exception e){
			logger.error ("Error al crear el grupo " + nombre + " en el grupo " + idPadre, e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaCreaGrupo", rs, ps, con);
		}
		logger.debug("< agendaCreaGrupo");
	}
		
	private String agendaCreaGrupoImpl(String idUsuario, String emisor, String idPadre,
			String nombre, Connection con, PreparedStatement ps, ResultSet rs) throws Exception {
		logger.debug("> agendaCreaGrupoImpl [" + idPadre + "]");
		
		
		try {
			// Queremos que el update nos devuelva el valor del id_grupo autogenerado
			String keyColumn [] = {"id_grupo"};
			
			// Primero metemos el grupo en p3s_grupos
			StringBuffer update = new StringBuffer (
					"INSERT INTO p3s_grupos (id_usuario, nombre, id_padre) " +
					"VALUES ('" + idUsuario + "', '" + nombre + "', '" + idPadre + "')" );
			ps = con.prepareStatement (update.toString (), keyColumn);
			ps.executeUpdate ();
			
			// Obtenemos un resultSet con el id_grupo autogenerado y lo guardamos en un String
			rs = ps.getGeneratedKeys ();
			rs.next ();
			String idGrupo = rs.getString (1);
			
			// Ahora insertamos en p3s_agenda
			update = new StringBuffer (
					"INSERT INTO p3s_agenda (id_grupo, id_usuario, nombre) " +
					"VALUES ('" + idGrupo + "', '" + idUsuario + "', '" + nombre + "')" );
			ps = con.prepareStatement (update.toString ());
			ps.executeUpdate ();
			
			return idGrupo;
		} catch (SQLException e) {
			logger.error ("Error al crear el grupo " + nombre + " en el grupo " + idPadre, e);
			throw new Exception (e);
		} finally {
			logger.debug("< agendaCreaGrupoImpl [" + idPadre + "]");
		}
	}
	
	@Override
	
	public void agendaCopiaContacto (String idUsuario, String emisor,
			String idContacto, String idGrupo) throws Exception {
		logger.debug ("> agendaCopiaContacto");
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = utilBD.getConnection ();
			agendaCopiaContactoImpl (idUsuario, emisor, idContacto, idGrupo, con, ps, rs);
		} catch (Exception e) {
			logger.error("Error al copiar el contacto " + idContacto + " al grupo " + idGrupo, e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaCopiaContacto", rs, ps, con);
			logger.debug ("< agendaCopiaContacto");
		}
	}
	
	private void agendaCopiaContactoImpl (String idUsuario, String emisor,
			String idContacto, String idGrupo, Connection con, PreparedStatement ps, ResultSet rs) throws Exception {
		logger.debug("> agendaCopiaContactoImpl [" + idContacto + "]");
		
		
		try {
			logger.debug ("idContacto = " + idContacto + ". Veamos qué tipo de entidad queremos copiar...");
			
			// Primero consultamos si es usuario o grupo. 
			StringBuffer select = new StringBuffer (
					"SELECT id_grupo FROM p3s_agenda WHERE p3s_agenda.id_contacto = " + idContacto);
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			rs.next ();
			int idGrupoTemp = Integer.parseInt (rs.getString (1));
			
			if (idGrupoTemp <= 0) {
				logger.debug("La entidad que quiere copiarse es un contacto.");
				// Es un contacto (0 en raíz, -1 en subgrupo). 
				
				if ("0".equals (idGrupo)) {
					// Si el contacto quiere copiarse a la raíz, símplemente le pondremos un 0
					// en id_grupo, ya que la copia original tendría un -1 y, por tanto, una entrada
					// en p3s_grupo_contacto que seguirá activa.
					StringBuffer update = new StringBuffer (
							"UPDATE p3s_agenda SET id_grupo = 0 WHERE id_contacto = " + idContacto);
					ps = con.prepareStatement (update.toString ());
					ps.executeUpdate ();
					logger.debug ("Contacto copiado a la raíz");
				} else {
					// Si quiere copiarse a cualquier otro sitio, sólo tenemos que crear una
					// nueva entrada en la tabla grupo_contacto.
					
					// ¡Pero solo si no existe ya!
					select = new StringBuffer (
							"SELECT id_contacto FROM p3s_grupo_contacto WHERE id_contacto = " + idContacto +
							"AND id_grupo = " + idGrupo);
					ps = con.prepareStatement (select.toString ());
					rs = ps.executeQuery ();
					if (!rs.next ()) {
						StringBuffer update = new StringBuffer (
								"INSERT INTO p3s_grupo_contacto (id_contacto, id_grupo) VALUES (" +
								idContacto + ", " + idGrupo + ")");
						ps = con.prepareStatement (update.toString ());
						ps.executeUpdate ();
						logger.debug ("Contacto copiado al grupo " + idGrupo);
					} else
						logger.debug ("No se hace nada: el contacto ya existía en el grupo " + idGrupo);
				}
			} else {
				logger.debug("La entidad que quiere copiarse es un grupo.");
				
				// Veamos qué nombre tiene el grupo
				select = new StringBuffer ("SELECT nombre FROM p3s_grupos WHERE id_grupo = " + idGrupoTemp);
				ps = con.prepareStatement (select.toString ());
				rs = ps.executeQuery ();
				rs.next ();
				String nombreGrupoTemp = rs.getString (1);
				
				// Primero creamos un nuevo grupo con el mismo nombre en idGrupo:
				String idNuevoGrupo = agendaCreaGrupoImpl (idUsuario, emisor, idGrupo, nombreGrupoTemp, con, ps, rs);
				logger.debug("Grupo contenedor copiado con éxito.");
				
				// Ahora recurrimos para todo lo que tiene:
				
				// Primero creamos todos sus contactos:
				select = new StringBuffer (
						"SELECT id_contacto FROM p3s_grupo_contacto WHERE id_grupo = " + idGrupoTemp);
				ps = con.prepareStatement (select.toString ());
				rs = ps.executeQuery ();
				while (rs.next ()) {
					String idNuevoContacto = rs.getString(1);
					StringBuffer update = new StringBuffer (
							"INSERT INTO p3s_grupo_contacto (id_contacto, id_grupo) VALUES (" + idNuevoContacto + ", " + idNuevoGrupo + ")");
					ps = con.prepareStatement (update.toString ());
					ps.executeUpdate ();
					logger.debug ("Contacto " + idNuevoContacto + " contenido en " + idGrupoTemp + " copiado con éxito a " + idNuevoGrupo + ".");
				}
				
				// Luego creamos todos los grupos:
				select = new StringBuffer (
						"SELECT id_grupo FROM p3s_grupos WHERE id_padre = " + idGrupoTemp);
				ps = con.prepareStatement (select.toString ());
				rs = ps.executeQuery ();
				while (rs.next ()) {
					String idGrupoParaCopiar = rs.getString (1);
					// Encontramos el id_contacto de este grupo
					select = new StringBuffer (
							"SELECT id_contacto FROM p3s_agenda WHERE id_grupo = " + idGrupoParaCopiar);
					PreparedStatement ps2 = con.prepareStatement (select.toString ());
					ResultSet rs2 = ps2.executeQuery ();
					if (rs2.next ()) {
						String idNuevoContacto = rs2.getString(1);
						logger.debug ("Procesando el subgrupo " + idGrupoParaCopiar + "(" + idNuevoContacto + ") del grupo " + idGrupoTemp + ".");
						agendaCopiaContactoImpl (idUsuario, emisor, idNuevoContacto, idNuevoGrupo, con, ps, rs);
						logger.debug ("Subgrupo " + idGrupoParaCopiar + "(" + idNuevoContacto + ") del grupo " + idGrupoTemp + " copiado con éxito a " + idNuevoGrupo + ".");
					}
				}
			}
		} catch (SQLException e) {
			logger.error("Error al copiar el contacto " + idContacto + " al grupo " + idGrupo, e);
			throw new Exception (e);
		} finally {
			logger.debug("< agendaCopiaContactoImpl [" + idContacto + "]");
		}
	}
	
	@Override
	public void agendaMueveContacto(String idUsuario, String emisor,
			String idContacto, String idGrupo, String idGrupoOriginal) throws Exception {
		logger.debug("> agendaMueveContacto");
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		logger.debug("Moviendo contacto " + idContacto + " al grupo " + idGrupo + ".");
		
		try {
			con = utilBD.getConnection ();
			
			// Primero consultamos si es usuario o grupo
			StringBuffer select = new StringBuffer ("SELECT id_grupo FROM p3s_agenda WHERE id_contacto = '" + idContacto + "'");
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			rs.next ();
			int idGrupoTemp = Integer.parseInt (rs.getString (1));
			
			// Movemos el contacto.
			
			// Si idGrupoTemp == -1 e idGrupo != 0, entonces hay que modificar la entrada correspondiente en p3s_grupo_contacto
			// Si idGrupoTemp == -1 e idGrupo = 0, entonces hay que poner un 0 en id_grupo en p3s_agenda y eliminar su entrada de p3s_grupo_contacto
			// Si idGrupoTemp == 0, entonces hay que poner un -1 como id_grupo en p3s_agenda y
			//	crear una entrada en p3s_grupo_contacto,
			// Si idGrupoTemp > 0, entonces hay que modificar id_padre en p3s_grupos

			StringBuffer update = null;
			if (idGrupoTemp == -1) {
				if ("0".equals (idGrupo)) {
					update = new StringBuffer ("UPDATE p3s_agenda SET id_grupo = '0' WHERE id_contacto = '" + idContacto + "'");
					ps = con.prepareStatement (update.toString ());
					ps.executeUpdate ();
					update = new StringBuffer ("DELETE FROM p3s_grupo_contacto WHERE id_contacto = '" + idContacto + "'");
					ps = con.prepareStatement (update.toString ());
					ps.executeUpdate ();
				} else {
					try {
						update = new StringBuffer ("UPDATE p3s_grupo_contacto SET id_grupo = '" + idGrupo + "' WHERE id_contacto = '" + idContacto + "'");
						ps = con.prepareStatement (update.toString ());
						ps.executeUpdate ();
					} catch (Exception e) {
						logger.info ("El contacto " + idContacto + " ya existía en el grupo " + idGrupo + ".");
						update = new StringBuffer ("DELETE FROM p3s_grupo_contacto WHERE id_contacto = '" + idContacto + "' AND id_grupo = '" + idGrupoOriginal + "'");
						ps = con.prepareStatement (update.toString ());
						ps.executeUpdate ();
					}
				}
			} else if (idGrupoTemp == 0) {
				if ("0".equals (idGrupo)) {
					// Do nothing
				} else 	{
					update = new StringBuffer ("UPDATE p3s_agenda SET id_grupo = '-1' WHERE id_contacto = '" + idContacto + "'");
					ps = con.prepareStatement (update.toString ());
					ps.executeUpdate ();
					update = new StringBuffer ("INSERT INTO p3s_grupo_contacto (id_grupo, id_contacto) VALUES ('" + idGrupo + "', '" + idContacto + "')");
					ps = con.prepareStatement (update.toString ());
					ps.executeUpdate ();
				}
			} else {
				update = new StringBuffer ("UPDATE p3s_grupos SET id_padre = '" + idGrupo + "' WHERE id_grupo = '" + idGrupoTemp + "'");
				ps = con.prepareStatement (update.toString ());
				ps.executeUpdate ();
			}
		} catch (SQLException e) {
			logger.error("No se pudo mover el contacto " + idContacto + " al grupo " + idGrupo + ": " + e.toString ());
			//throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaMueveContacto", rs, ps, con);
			logger.debug("< agendaMueveContacto");
		}
	}

	@Override
	public Vector<Map<String, Object>> listaTodosLosGrupos(String idUsuario,
			String emisor) throws Exception {
		Vector<Map<String,Object>> resultado = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		logger.debug("> listaTodosLosGrupos");
		try {
			con = utilBD.getConnection();
			resultado = listaTodosLosGruposRec ("0", idUsuario, 0, con, ps, rs);
		} catch (SQLException e) {
			logger.error("Error al listar los grupos de " + idUsuario, e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("listaTodosLosGruposRec", rs, ps, con);
		}
		
		logger.debug("< listaTodosLosGrupos");
		return resultado;
		
	}
	
	private Vector<Map<String, Object>> listaTodosLosGruposRec (String idGrupoActual, 
			String idUsuario, int profundidad, Connection con, PreparedStatement ps, 
			ResultSet rs) throws Exception {
		logger.debug ("> listaTodosLosGruposRec [" + idGrupoActual + "]");
		Vector<Map<String, Object>> resultado = new Vector<Map<String, Object>> ();
		
		String indentado = "";
		for (int i = 0; i <= profundidad - 1; i ++) 
			indentado = indentado + ".";
		
		try {
			StringBuffer select;
			
			if ("0".equals (idGrupoActual)) {
				select = new StringBuffer ("SELECT p3s_grupos.id_grupo, p3s_agenda.nombre FROM " +
						"p3s_agenda, p3s_grupos WHERE p3s_grupos.id_padre = '0' AND " +
						"p3s_agenda.id_usuario = '" + idUsuario + "' AND p3s_agenda.id_grupo = p3s_grupos.id_grupo ORDER BY p3s_agenda.nombre");
			} else {
				select = new StringBuffer ("SELECT id_grupo, nombre FROM p3s_grupos WHERE id_padre = '" + idGrupoActual + "'");
			}
			
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			
			while (rs.next ()) {
				HashMap<String, Object> grupoDescriptor = new HashMap<String, Object> ();
				grupoDescriptor.put ("id_grupo", rs.getString (1));
				grupoDescriptor.put ("nombre", indentado + rs.getString (2));
				resultado.add (grupoDescriptor);
				
				Vector<Map<String, Object>> resultadoLlamadaRecursiva = listaTodosLosGruposRec (rs.getString (1), idUsuario, profundidad + 1, con, ps, rs);
				resultado.addAll (resultadoLlamadaRecursiva);
			}
			
			return resultado;
		} catch (SQLException e) {
			logger.error("Error al listar los grupos de " + idUsuario, e);
			throw new Exception (e);
		} finally {
			logger.debug ("< listaTodosLosGruposRec [" + idGrupoActual + "]");
		}
	}
	
	@Override
	public void agendaModificaContacto(String idUsuario, String emisor,
			String idContacto, String nombreAtributo, String nuevoValor)
			throws Exception {
		logger.debug ("> agendaModificaContacto");
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = utilBD.getConnection();
			StringBuffer update = new StringBuffer ("UPDATE p3s_agenda SET " + nombreAtributo + " = '" + nuevoValor + "' WHERE id_contacto = '" + idContacto + "' AND id_usuario = '" + idUsuario + "'");
			ps = con.prepareStatement (update.toString ());
			ps.executeUpdate ();
		} catch (SQLException e) {
			logger.error ("Error al modificar el contacto " + idContacto + " para el usuario " + idUsuario, e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaModificaContacto", rs, ps, con);
			logger.debug ("< agendaModificaContacto");
		}
	}

	@Override
	public void agendaModificaGrupo(String idUsuario, String emisor,
			String idGrupo, String nombreAtributo, String nuevoValor)
			throws Exception {
		logger.debug ("> agendaModificaGrupo");
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = utilBD.getConnection();
			StringBuffer update = new StringBuffer ("UPDATE p3s_agenda SET " + nombreAtributo + " = '" + nuevoValor + "' WHERE id_grupo = '" + idGrupo + "' AND id_usuario = '" + idUsuario + "'");
			ps = con.prepareStatement (update.toString ());
			if (ps.executeUpdate () > 0) {
				// Si se modificó algo es que existe el grupo para 
				// el usuario, por lo tanto podemos seguir modificando 
				// p3s_grupos:
				if ("nombre".equals(nombreAtributo)) {
					update = new StringBuffer ("UPDATE p3s_grupos SET nombre = '" + nuevoValor + "' WHERE id_grupo = '" + idGrupo + "'");
					ps = con.prepareStatement (update.toString ());
					ps.executeUpdate ();
				}
			}
		} catch (SQLException e) {
			logger.error ("Error al modificar el grupo " + idGrupo + " para el usuario " + idUsuario, e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaModificaGrupo", rs, ps, con);
			logger.debug ("< agendaModificaGrupo");
		}
	}

	@Override
	public Map<String, Object> agendaObtieneContacto(String idUsuario,
			String emisor, String idContacto) throws Exception {
		logger.debug ("> agendaObtieneContacto");
		HashMap<String, Object> resultado = new HashMap<String, Object> ();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = utilBD.getConnection();
			StringBuffer select = new StringBuffer ( "SELECT id_contacto, nombre, tlfno_movil, email, id_grupo, tlfno_fijo, entidad, notas FROM p3s_agenda WHERE id_contacto = '" + idContacto +
					"'"); //" AND id_usuario = '" + idUsuario + "'");
			ps = con.prepareStatement (select.toString ());
			rs = ps.executeQuery ();
			rs.next ();
			
			int idGrupoTemp = Integer.parseInt(rs.getString(5));
			if (idGrupoTemp > 0) {
				resultado.put ("id_grupo", rs.getString (5));
			} else {
				resultado.put ("id_grupo", "");
			}
			
			resultado.put ("id_contacto", rs.getString (1));
			resultado.put ("nombre", rs.getString (2));
			resultado.put ("tlfno_movil", rs.getString (3));
			resultado.put ("email", rs.getString (4));
			resultado.put ("tlfno_fijo", rs.getString (6));
			resultado.put ("entidad", rs.getString (7));
			resultado.put ("notas", rs.getString (8));
				
			// Tenemos que buscar ahora el contenedor del contacto. Si es 0 delvolvemos directamente,
			// Si es -1 miramos en p3s_grupo_contacto, si es > 0 miramos en p3s_grupos
			
			if (idGrupoTemp == 0) {
				resultado.put ("id_padre", "0");
			} else if (idGrupoTemp == -1) {
				select = new StringBuffer ("SELECT id_grupo FROM p3s_grupo_contacto WHERE id_contacto = '" + idContacto + "'");
				ps = con.prepareStatement (select.toString ());
				rs = ps.executeQuery ();
				rs.next ();
				resultado.put ("id_padre", rs.getString (1));
			} else {
				select = new StringBuffer ("SELECT id_padre FROM p3s_grupos WHERE id_grupo = '" + idGrupoTemp + "'");
				ps = con.prepareStatement (select.toString ());
				rs = ps.executeQuery ();
				rs.next ();
				resultado.put ("id_padre", rs.getString (1));
			}
			
			return resultado;
		} catch (SQLException e) {
			logger.error ("Error al obtener el contacto " + idContacto + " de la agenda.", e);
			throw new Exception (e);
		} finally {
			utilBD.closeAll("agendaObtieneContacto", rs, ps, con);
			logger.debug ("< agendaObtieneContacto");
		}
	}
}
