package es.sadesi.sms;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opensymphony.xwork2.util.ClassLoaderUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

public final class P3SProperties implements Serializable {

	/**
	 * Auto-generado
	 */
	private static final long serialVersionUID = -273910090249128578L;

	private static final Log logger = LogFactory.getLog(P3SProperties.class);
	
	public static final String CLAVE_EMISOR = "clave.emisor";
	public static final String RUTA_ENVIO_MASIVO = "ruta.envio.masivo";
	public static final String RUTA_ARCHIVOS_TEMPORALES = "ruta.archivos.temporales";
	public static final String RUTA_EXCELS = "ruta.excels";
	public static final String URI_EXCELS = "uri.excels";
	
	public static final String BD_DRIVER = "bd.driver";
	public static final String BD_URL = "bd.url";
	public static final String BD_USER = "bd.user";
	public static final String BD_PASS = "bd.pass";
	public static final String BD_MINLIMIT = "bd.minLimit";
	public static final String BD_MAXLIMIT = "bd.maxLimit";
	public static final String BD_TIME_BETWEEN_EVICTION_RUNS_MILLIS = "bd.timeBetweenEvictionRunsMillis";
	public static final String BD_NUM_TESTS_PER_EVICTION = "bd.numTestsPerEviction";
	public static final String BD_MIN_EVICTABLE_IDLE_TIME_MILLIS = "bd.minEvictableIdleTimeMillis";
	public static final String BD_MAX_WAIT = "bd.maxWait";
	
	public static final String URI_P3S = "uri.p3s";
	
	public static final String QUARTZ_CRON_EXPRESSION = "quartz.cron.expression";

	/**
	 * Singleton instance
	 */
	private static P3SProperties instance;

	private Properties properties;

	// ~ Constructors ///////////////////////////////////////////////////////////

	/**
	 * Don't use this constructor most of the time. To use LdapProperties as a
	 * singleton, use {@link #getInstance()} .
	 */
	public P3SProperties() {
		this("p3s.properties");
	}

	public P3SProperties(String filename) {
		String loc = "/" + filename;
		InputStream in = ClassLoaderUtil.getResourceAsStream(loc, this
				.getClass());

		if (in == null) {
			loc = filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/" + filename;
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "/META-INF/p3s-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (in == null) {
			loc = "META-INF/p3s-default.xml";
			in = ClassLoaderUtil.getResourceAsStream(loc, this.getClass());
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loading using config : " + loc);
		}

		if (in == null) {
			throw new ExceptionInInitializerError("The configuration file "
					+ filename + " could not be found.");
		}

		try {
			properties = new Properties();
			properties.load(in);
		} catch (IOException e) {
			logger.error("Error al cargar el fichero de propiedades", e);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("loaded using config : " + loc);
		}
	}

	// ~ Methods ////////////////////////////////////////////////////////////////

	public static void setInstance(P3SProperties p3sProperties) {
		instance = p3sProperties;
	}

	/**
	 * Entry-point to Singleton instance
	 */
	public static P3SProperties getInstance() {
		try {
			if (instance == null) {
				instance = new P3SProperties();
			}
		} catch (ExceptionInInitializerError e) {
			logger.error("Unable to load configuration", e);
		} catch (RuntimeException e) {
			logger.error("unexpected runtime exception during initialization",
					e);
		}

		return instance;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}

	public String getProperty(String nombre) {
		return properties.getProperty(nombre);
	}

}
