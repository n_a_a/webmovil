package es.sadesi.sms;

public class LimitExceededException extends Exception {

	/** Auto-generado */
	private static final long serialVersionUID = 7524658366074751976L;

	public LimitExceededException() {
		super();
	}

	public LimitExceededException(String message, Throwable cause) {
		super(message, cause);
	}

	public LimitExceededException(String message) {
		super(message);
	}

	public LimitExceededException(Throwable cause) {
		super(cause);
	}

}
