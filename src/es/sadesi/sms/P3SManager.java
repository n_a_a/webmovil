package es.sadesi.sms;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import es.sadesi.security.InvalidCredentialsException;

public interface P3SManager {

	public static final SimpleDateFormat P3S_DATE_FORMAT = new SimpleDateFormat(
			"yyyyMMddHHmmss");

	String identificaUsuario(String usuario)
			throws InvalidCredentialsException, Exception;

	Vector<HashMap<String, Object>> listaCarpetasUsuario(String usuario,
			String emisor) throws Exception;

	Map<String, Object> enviaSMS(String usuario, String emisor,
			String destinatario, String texto, int prioridad, int acuseRecibo,
			int alfanumerico, String fechaCaducidad, String idMensaje)
			throws InvalidCredentialsException, LimitExceededException,
			Exception;

	Vector<HashMap<String, Object>> obtieneMensajesCarpetaUsuario(
			String usuario, String emisor, String carpeta, String desde,
			String hasta, int regIni, int regFin) throws Exception;

	int obtieneNumMensajesCarpetaUsuario(String usuario, String emisor,
			String carpeta, String desde, String hasta) throws Exception;

	void mueveMensajeCarpeta(String usuario, String emisor, String idEsme,
			String idCarpeta) throws Exception;

	void creaCarpetaUsuario(String usuario, String emisor, String nombreCarpeta)
			throws Exception;

	void eliminaCarpetaUsuario(String usuario, String emisor, String idCarpeta)
			throws Exception;

	Map<String, Object> infoEmisor(String emisor) throws Exception;

	void modificaAtributoUsuario(String usuario, String emisor,
			String nombreAtributo, String nuevoValor) throws Exception;

	void modificaAtributoEmisor(String emisor, String emisorModificado,
			String nombreAtributo, String nuevoValor) throws Exception;

	Vector<HashMap<String, String>> listaUsuariosEmisor(String emisor)
			throws Exception;

	void creaUsuario(String usuario, String emisor, String limite,
			String limite_blando, String limite_internacional,
			String limite_internacional_blando, String tlfno_movil,
			String nivel, String per_prioridad, String per_recibo,
			String per_solocorp, String per_firma, String per_recepcion,
			String per_masivo) throws Exception;

	void eliminaUsuario(String emisor, String usuario) throws Exception;

	void eliminaEmisor(String emisor, String id_emisor_borrar) throws Exception;

	Vector<HashMap<String, String>> listaEmisores(String idEmisor)
			throws Exception;

	void creaEmisor(String emisor, String id_nuevo_emisor, String ip,
			String descripcion, String clave, String limite, String firma,
			String tlfn, String ton, String npi) throws Exception;

	String emisorUsuario(String idUsuario) throws Exception;

	String version() throws Exception;

	Map<String, Object> infoUsuarioEstaticos(String usuario, String emisor)
			throws Exception;

	int estadisticasUsuario(String usuario, String emisor,
			String idEstadistica, String desde, String hasta) throws Exception;

	int estadisticasEmisor(String emisor, String idEstadistica, String desde,
			String hasta) throws Exception;

	Vector<HashMap<String, Object>> usuariosMasActivosEmisor(String emisor,
			String desde, String hasta) throws Exception;

	Vector<Map<String, Object>> usuariosMasActivosEmisorPorIntervalo(final String idEmisor,
			final Calendar fechaDesde, final Calendar fechaHasta) throws Exception;

	public HashMap<String, Object> generalReadBD(String operacion,
			HashMap<String, Object> args) throws Exception;

	public void generalWriteBD(String operacion, HashMap<String, Object> args)
			throws Exception;

	public void setAltText(String idEmisor, String altText) throws Exception;

	public void setAltTextEnable(String idEmisor, boolean enable)
			throws Exception;

	public String getAltText(String idEmisor) throws Exception;

	public boolean getAltTextEnable(String idEmisor) throws Exception;

	public HashMap<String, String> getInfoLocClientePush(String tlfno)
			throws Exception;

	public boolean esClientePush(String tlfno) throws Exception;

	public HashMap<String, Object> SMSEx(String usuario, String emisor,
			String destinatario, String texto, int prioridad, int acuseRecibo,
			int alfanumerico, String fechaCaducidad, String idMensaje, int push)
			throws InvalidCredentialsException, LimitExceededException,
			SyntaxErrorException, Exception;

	public int nuevaFuenteSuscripcion(String tipo, String idEmisor,
			String desc, String urlAlta, String urlBaja, String method)
			throws Exception;

	public void eliminaFuenteSuscripcion(int id) throws Exception;

	public Vector<HashMap<String, String>> getFuentesEmisor(
			String idEmisor) throws Exception;
	
	public HashMap<String,String> obtieneMensajePush (int id, String tlfno) throws Exception;
	
	public boolean getDisableInvitationSMS(final String idEmisor) throws Exception;
	
	public void setDisableInvitationSMS(final String idEmisor, final boolean disable)
			throws Exception;
	
	public void procesarEstadisticas(final String desde, final String hasta)
			throws Exception;
	
	Map<String, Object> obtenerEstadisticas(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception;
	
	Map<String, Object> obtenerEstadisticasAgrupadasPorEmisor(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception;
	
	Vector<Map<String, Object>> obtenerEstadisticasUsuarios(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception;
	
	Vector<Map<String, Object>> obtenerEstadisticasUsuariosAgrupadasPorUsuario(final String idEmisor, final String fechaDesde, final String fechaHasta) 
			throws Exception;

}
