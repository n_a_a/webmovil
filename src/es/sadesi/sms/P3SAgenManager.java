package es.sadesi.sms;

import java.util.HashMap;
import java.util.Vector;
import java.util.Map;

public interface P3SAgenManager {
	
	Vector<HashMap<String, Object>> listaContactosGrupo(String usuario,
			String emisor, String idGrupo, int regIni, int regFin)
			throws Exception;

	Vector<HashMap<String, Object>> listaSubgruposGrupo(String usuario,
			String emisor, String idGrupo, int regIni, int regFin)
			throws Exception;
	
	Map<String, Object> agendaObtieneGrupo(String usuario, String emisor,
			String idGrupo) throws Exception;
	
	void agendaEliminaContacto(String usuario, String emisor, String idContacto)
			throws Exception;

	void agendaCreaContacto(String usuario, String emisor, String idPadre,
			String nombre, String tlfMovil, String email, String tlfFijo,
			String entidad, String notas) throws Exception;

	void agendaCreaGrupo(String usuario, String emisor, String idPadre,
			String nombre) throws Exception;

	void agendaMueveContacto(String usuario, String emisor, String idContacto,
			String idGrupo, String idGrupoOriginal) throws Exception;
	
	void agendaCopiaContacto(String idUsuario, String emisor,
			String idContacto, String idGrupo) throws Exception;
	
	Vector<Map<String, Object>> listaTodosLosGrupos(String usuario,
			String emisor) throws Exception;

	void agendaModificaGrupo(String usuario, String emisor, String idGrupo,
			String nombreAtributo, String nuevoValor) throws Exception;

	void agendaModificaContacto(String usuario, String emisor,
			String idContacto, String nombreAtributo, String nuevoValor)
			throws Exception;

	int cuentaContactosGrupo(String usuario, String emisor, String idGrupo)
			throws Exception;

	int cuentaSubgruposGrupo(String usuario, String emisor, String idGrupo)
			throws Exception;
	
	Map<String, Object> agendaObtieneContacto(String usuario, String emisor,
			String idContacto) throws Exception;
}
