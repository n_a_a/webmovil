package es.sadesi.quartz.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import es.sadesi.quartz.job.EstadisticasJob;
import es.sadesi.sms.P3SProperties;

public class QuartzListener implements ServletContextListener {

	private static final Log LOG = LogFactory.getLog(QuartzListener.class);
	
	private static final String CRON_EXPRESSION = P3SProperties.getInstance().getProperty(P3SProperties.QUARTZ_CRON_EXPRESSION);
	
	private static final String ESTADISTICA_JOB_KEY = "estadisticaJob";
	
	private static final String ESTADISTICA_JOB_GROUP = "estadisticaGroup";
	
	private static final String ESTADISTICA_CRON_TRIGGER_KEY = "estadisticaCronTrigger";
	
	private static final String ESTADISTICA_CRON_TRIGGER_GROUP = "estadisticaCronGroup";
	
	private Scheduler sch;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
	    try {
			final SchedulerFactory schfa = new StdSchedulerFactory();
			sch = schfa.getScheduler();
			// Comprobamos si ya existe el job, para no volver a crearlo (En el caso que este configurado en cluster [utiliza BBDD])
			final JobKey estadisticaJobKey = new JobKey(ESTADISTICA_JOB_KEY, ESTADISTICA_JOB_GROUP);
			if(!sch.checkExists(estadisticaJobKey)) {
				final JobDetail jobdetail = JobBuilder.newJob(EstadisticasJob.class).withIdentity(estadisticaJobKey).build();
				// Expresion cron cada 2 minutos: 0 0/2 * 1/1 * ? *
				// Expresion cron cada dia 1 de cada mes: 0 0 0 1 1/1 ? *
				final CronTrigger crontrigger = TriggerBuilder.newTrigger().withIdentity(ESTADISTICA_CRON_TRIGGER_KEY, ESTADISTICA_CRON_TRIGGER_GROUP)
					    .withSchedule(CronScheduleBuilder.cronSchedule(CRON_EXPRESSION)).build();
				sch.scheduleJob(jobdetail, crontrigger);
			} 
			
			sch.start();
		} catch (SchedulerException e) {
			LOG.error("Error al iniciar el scheduler de quartz.", e);
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		LOG.info("Inside Context Destroyed method.");
		try {
			sch.shutdown();
		} catch (final SchedulerException e) {
			LOG.error("Error al realizar shutdown al scheduler de quartz.");
		}
	}
	
}
