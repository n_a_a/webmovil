package es.sadesi.quartz.job;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

import es.sadesi.sms.P3SManager;
import es.sadesi.sms.P3SManagerImpl;

public class EstadisticasJob implements Job {

	private static final Log LOG = LogFactory.getLog(EstadisticasJob.class);
	
	private P3SManager p3sManager;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.info("-- INICIO EstadisticasJob --");
		
		final JobDetail jobDetail = context.getJobDetail();
		final JobKey key = jobDetail.getKey();
		LOG.info("Job key: "+ key +", Date: "+ new Date());
		
		// Fechas desde y hasta del mes anterior (En Calendar, los meses van de 0 a 11)
		final Calendar cal = Calendar.getInstance ();
		cal.add (Calendar.MONTH, -1);
		final int mesAnterior = cal.get(Calendar.MONTH) +  1;
		final int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		final String ultimoDiaDelMesS = ultimoDiaDelMes < 10 ? "0" + ultimoDiaDelMes : "" + ultimoDiaDelMes;
		final String mesDesdeS = mesAnterior < 10 ? "0" + mesAnterior : "" + mesAnterior;
		final String mesHastaS = mesDesdeS;
		final String anyoDesdeS = "" + cal.get(Calendar.YEAR);
		final String anyoHastaS = anyoDesdeS;
		
		final String desde = anyoDesdeS + mesDesdeS + "01000000";
		final String hasta = anyoHastaS + mesHastaS + ultimoDiaDelMesS + "235959";
		
		try {
			LOG.info("Procesar estadisticas desde: " + desde + ", hasta: " + hasta);
			// Procesar estadisticas
			getP3sManager().procesarEstadisticas(desde, hasta);
			LOG.info("Estadisticas procesadas");
		} catch(final Exception e) {
			LOG.error("Error al procesar las estadisticas", e);
		}
		
		LOG.info("-- FIN EstadisticasJob --");
	}

	public P3SManager getP3sManager() {	
		if (p3sManager == null) {
			p3sManager = new P3SManagerImpl();
		}
		return p3sManager;
	}
}
