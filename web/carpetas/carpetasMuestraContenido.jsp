<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Lista de mensajes</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/carpetas.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<h2 id="titulo">Lista de mensajes, carpeta &quot;<s:property value="nombreCarpeta" />&quot;</h2>
		
		<!-- vemos si hay que mostrar la carpeta donde está el mensaje -->
		<s:if
			test="nombreCarpeta.equals('Enviados') || nombreCarpeta.equals('Recibidos')">
			<s:set name="esCarpetaEspecial" value="1" />
		</s:if>
		<s:else>
			<s:set name="esCarpetaEspecial" value="0" />
		</s:else>
		
		<s:form action="actionCarpetasObtieneContenido" method="post">			
			<s:if test="#esCarpetaEspecial == 1">
				<table class="generalBox">
					<tr><td>
						Seleccionar intervalo: Dia <s:select name="formDia" list="formDiaList" headerKey="0" theme="simple" /> Mes <s:select name="formMes" list="#{'00':'TODOS', '01':'Enero', '02':'Febrero', '03':'Marzo', '04':'Abril', '05':'Mayo', '06':'Junio', '07':'Julio', '08':'Agosto', '09':'Septiembre', '10':'Octubre', '11':'Noviembre', '12':'Diciembre'}" theme="simple" /> de <s:textfield name="formAnyo" maxlength="4" size="4" theme="simple" /> &nbsp; <s:submit type="button" name="accion" value="Actualizar intervalo" theme="simple" />
					</td></tr>
				</table>
			</s:if>
			
			<!-- vemos si hay mensajes que mostrar -->
			<s:if test="mensajesTotales == 0">
				<!-- Mensaje de que no hay mensajes -->
				<table class="generalBox">
					<tr><td>
					La carpeta <strong>&quot;<s:property
					value="nombreCarpeta" />&quot;</strong> no contiene ning&uacute;n mensaje.
					</td></tr>
				</table>
			</s:if>
			<s:else>
				<!-- presentamos una lista de mensajes -->
				<table class="generalBox">
					<tr><td>
					Total de mensajes en la carpeta <strong><s:property
					value="nombreCarpeta" /></strong>
					<s:if test="#esCarpetaEspecial == 1"> en el intervalo seleccionado</s:if>: <s:property value="mensajesTotales" />
					</td></tr>
				</table>
				<table id="tablaListaMensajes">
					<tr>
						<th></th>
						<th>Id</th>
						<th>Teléfono</th>
						<th>Texto</th>
						<th>Fecha</th>
						<th>Estado</th>
						<s:if test="#esCarpetaEspecial == 1">
							<th>Carpeta</th>
						</s:if>
					</tr>
		
					<s:iterator value="mensajesStore" status="offset" var="mensaje">
						<!-- Color de las lineas de la tabla -->
						<s:if test="#offset.odd == true">
							<s:set name="trclass">impar</s:set>
						</s:if> 
						<s:else>
							<s:set name="trclass">par</s:set>
						</s:else>
						
						<tr class="${trclass}">
							<td class="numerico" width="5%"><s:checkbox name="mensajesSeleccionados"
								fieldValue="%{idEsme}" theme="simple"></s:checkbox></td>
							<td class="numerico" width="10%"><s:property value="idEsme" /></td>
							<td class="numerico" width="10%"><s:property value="telefono" /></td>
							<s:if test="push">
								<td class="texto_push" width="50%"><s:property value="texto" /></td>
							</s:if>
							<s:else>
								<td class="texto" width="50%"><s:property value="texto" /></td>
							</s:else>
							<td class="numerico" width="10%"><s:property value="fecha" /></td>
							<td class="numerico" width="5%">
								<s:if test="push">
									<img src="images/push.png" alt="Mensaje enviado mediante notificación push" />
								</s:if>							
								<s:else>
									<img src="images/<s:property value="estadoImg" />" alt="<s:property value="estadoAlt" />" />
								</s:else>
							</td>
							<s:if test="#esCarpetaEspecial == 1">
								<td class="numerico" width="10%"><s:property value="nombreCarpeta" /></td>
							</s:if>
						</tr>
					</s:iterator>
				</table>
		
				<!-- Paginador -->
				<table class="generalBox">
					<tr>
						<td width="32">
							<s:if test="numPagina > 0">
								<s:url action="actionCarpetasObtieneContenido"
									var="primeraPaginaLink">
									<s:param name="numPagina">0</s:param>
									<s:param name="accion"></s:param>
									<s:param name="idCarpeta">
										<s:property value="idCarpeta" />
									</s:param>
									<s:param name="nombreCarpeta">
										<s:property value="nombreCarpeta" />
									</s:param>
									<s:param name="formAnyo">
										<s:property value="formAnyo" />
									</s:param>
									<s:param name="formMes">
										<s:property value="formMes" />
									</s:param>
									<s:param name="formDia">
										<s:property value="formDia" />
									</s:param>
								</s:url>
								<a href="${primeraPaginaLink}"><img src="images/icono-flecha-0.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-0-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina > 0">
								<s:url action="actionCarpetasObtieneContenido"
									var="paginaAnteriorLink">
									<s:param name="numPagina">
										<s:property value='numPagina' />
									</s:param>
									<s:param name="accion">paginaAnterior</s:param>
									<s:param name="idCarpeta">
										<s:property value="idCarpeta" />
									</s:param>
									<s:param name="nombreCarpeta">
										<s:property value="nombreCarpeta" />
									</s:param>
									<s:param name="formAnyo">
										<s:property value="formAnyo" />
									</s:param>
									<s:param name="formMes">
										<s:property value="formMes" />
									</s:param>
									<s:param name="formDia">
										<s:property value="formDia" />
									</s:param>
								</s:url>
								<a href="${paginaAnteriorLink}"><img src="images/icono-flecha-1.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-1-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina < maxPagina">
								<s:url action="actionCarpetasObtieneContenido"
									var="paginaSiguienteLink">
									<s:param name="numPagina">
										<s:property value='numPagina' />
									</s:param>
									<s:param name="accion">paginaSiguiente</s:param>
									<s:param name="idCarpeta">
										<s:property value="idCarpeta" />
									</s:param>
									<s:param name="nombreCarpeta">
										<s:property value="nombreCarpeta" />
									</s:param>
									<s:param name="formAnyo">
										<s:property value="formAnyo" />
									</s:param>
									<s:param name="formMes">
										<s:property value="formMes" />
									</s:param>
									<s:param name="formDia">
										<s:property value="formDia" />
									</s:param>
								</s:url>
								<a href="${paginaSiguienteLink}"><img src="images/icono-flecha-2.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-2-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina < maxPagina">
								<s:url action="actionCarpetasObtieneContenido"
									var="ultimaPaginaLink">
									<s:param name="numPagina">
										<s:property value="maxPagina" />
									</s:param>
									<s:param name="accion"></s:param>
									<s:param name="idCarpeta">
										<s:property value="idCarpeta" />
									</s:param>
									<s:param name="nombreCarpeta">
										<s:property value="nombreCarpeta" />
									</s:param>
									<s:param name="formAnyo">
										<s:property value="formAnyo" />
									</s:param>
									<s:param name="formMes">
										<s:property value="formMes" />
									</s:param>
									<s:param name="formDia">
										<s:property value="formDia" />
									</s:param>
								</s:url>
								<a href="${ultimaPaginaLink}"><img src="images/icono-flecha-3.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-3-g.png" />
							</s:else>
						</td>
						<!-- Página actual -->
						<td>
							P&aacute;gina <s:property value="numPagina + 1" /> de <s:property value="maxPagina + 1" />
						</td>
						<!-- botones de mover/borrar mensajes -->
						<td align="right">
							<s:submit type="button" name="accion" value="Borrar seleccionados" theme="simple" />
							<s:submit type="button"	name="accion" value="Mover seleccionados a" theme="simple" /> 
							<s:select list="miListaCarpetas" name="carpetaDestinoOperacion" theme="simple" />
						</td>
					</tr>
				</table>
					
				
			</s:else>
			<!-- Propagamos los valores actuales de la vista -->
			<s:hidden name="idCarpeta" value="%{idCarpeta}" />
			<s:hidden name="nombreCarpeta" value="%{nombreCarpeta}" />
			<s:hidden name="numPagina" value="%{numPagina}" />			
		</s:form>
		
		<s:url action="actionCarpetasObtieneContenidoExcel" var="excelLink">
			<s:param name="formDia"><s:property value="formDia" /></s:param>
			<s:param name="formMes"><s:property value="formMes" /></s:param>
			<s:param name="formAnyo"><s:property value="formAnyo" /></s:param>
			<s:param name="nombreCarpeta"><s:property value="nombreCarpeta" /></s:param>
			<s:param name="idCarpeta"><s:property value="idCarpeta" /></s:param>
			<s:param name="accion">Exportar Excel</s:param>
		</s:url>
		<br />
		<table class="generalBox">
			<tr>
				<td>
				<s:if test="excelAvailable">
					<a href="${excelLink}">Obtener resultados en hoja Excel (si hay muchos mensajes en el intervalo, puede tardar bastante)</a>
				</s:if>
				<s:else>
					El número de mensajes en el intervalo es demasiado grande para poder generar una hoja Excel. Seleccione un intervalo menor.
				</s:else>
				</td>
			</tr>
		</table>
		
		<!--  Enlace para volver -->
		<br />
		<table class="generalBox">
			<tr>
				<td>
					<a href="<s:url action='actionCarpetasLista'/>">Volver a la lista de carpetas</a>
				</td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>