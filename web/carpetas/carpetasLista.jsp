<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Lista de Carpetas</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/carpetas.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>	

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<!-- presentamos una lista de carpetas -->
		<h2 id="titulo">Lista de carpetas</h2>
		<table id="tablaListaCarpetas">
			<tr>
				<th>Nombre</th>
				<th>Mensajes</th>
				<th>Acción</th>
			</tr>
			
			<s:iterator value="carpetasListaStore" status="offset">
				<!-- Color de las lineas de la tabla -->
				<s:if test="#offset.odd == true">
					<s:set name="trclass">impar</s:set>
				</s:if> 
				<s:else>
					<s:set name="trclass">par</s:set>
				</s:else>
			
				<!-- Decidimos la acción que hay que realizar -->
				<s:if test="idCarpeta == -1">
					<s:if test="usuarioEspecial == true">
						<s:url action="actionOfflineExcels" var="folderLink">
						</s:url>
					</s:if>
					<s:else>
						<s:url action="actionCarpetasObtieneContenido" var="folderLink">
							<s:param name="idCarpeta">enviados</s:param>
							<s:param name="nombreCarpeta">
								<s:property value="nombreCarpeta" />
							</s:param>
						</s:url>
					</s:else>
				
				</s:if>
				<s:elseif test="idCarpeta == -2">
					<s:url action="actionCarpetasObtieneContenido" var="folderLink">
						<s:param name="idCarpeta">recibidos</s:param>
						<s:param name="nombreCarpeta">
							<s:property value="nombreCarpeta" />
						</s:param>
					</s:url>
				</s:elseif>
				<s:else>
					<s:url action="actionCarpetasObtieneContenido" var="folderLink">
						<s:param name="idCarpeta">
							<s:property value="idCarpeta" />
						</s:param>
						<s:param name="nombreCarpeta">
							<s:property value="nombreCarpeta" />
						</s:param>
					</s:url>
				</s:else>
				<tr class="${trclass}">
					<td class="texto" width="60%">
						<span class="hidden"><s:property value="idCarpeta" /></span>
						<img src="images/icono-carpeta.png" />
						<a href="${folderLink}"><s:property value="nombreCarpeta" /></a>
						<s:if test="idCarpeta == -1 && usuarioEspecial == true"> (descarga de archivos Excel)</s:if>
					</td>
					<td class="numerico" width="20%">
						<s:if test="idCarpeta == -1 && usuarioEspecial == true">N/A</s:if>
						<s:else><s:property value="numMensajes" /></s:else>
					</td>
					<td class="botones" width="20%">
						<s:if test="idCarpeta > 0">
							<s:url action="actionCarpetasLista" var="eliminarLink">
								<s:param name="accionIdCarpeta">
									<s:property value="idCarpeta" />
								</s:param>
								<s:param name="accionNombreCarpeta">
									<s:property value="nombreCarpeta" />
								</s:param>
								<s:param name="accion">eliminar</s:param>
							</s:url>
							<a href="${eliminarLink}"><img src="images/icono-borrar.png" alt="eliminar ${nombreCarpeta}" /></a>
						</s:if>
					</td>
				</tr>
			</s:iterator>
			
			<s:form action="actionCarpetasLista" method="post">
				<div id="botoncontainer">
					<s:textfield name="accionNombreCarpeta" maxlength="20" theme="simple" />
					<s:submit type="button" name="accion" value="Crear carpeta" theme="simple" />
				</div>
			</s:form>
			
		</table>
		
	</div>
</div>

</body>
</html>