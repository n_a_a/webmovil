<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - descarga de Excel</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/carpetas.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<h2 id="titulo">Descarga de Excel de mensajes enviados</h2>
		
		<table class="generalBox">
			<tr>
				<td>
					<p>Su volumen de envíos es demasiado grande para ofrecerle una navegación interactiva por su lista
					de mensajes enviados. En su lugar, cada mes se extraerá la información en formato Excel y podrá
					descargarla desde esta misma página</p>
				</td>
			</tr>
		</table>
		<table id="tablaListaCarpetas">
			<s:iterator value="listaArchivos" status="offset">
				<!-- Color de las lineas de la tabla -->
				<s:if test="#offset.odd == true">
					<s:set name="trclass">impar</s:set>
				</s:if> 
				<s:else>
					<s:set name="trclass">par</s:set>
				</s:else>
				<tr class="${trclass}">
					<td class="texto">
						<center><a href="${uri}">Archivo para el <s:property value="month" /> de <s:property value="year" /></a></center>
					</td>
				</tr>
			</s:iterator>
		</table>
	</div>
</div>
</body>
</html>