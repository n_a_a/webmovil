<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Avisos Junta</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/sms.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>	

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />
	
	<table class="smsTable">
		<tr>
			<th>SMS</th>
		</tr>
		<tr>
			<td><a href="<s:url action='enviarSMSInput'/>">Env&iacute;o</a></td>
		</tr>
		<s:if test="%{tienePermiso('per_masivo')}">
			<tr>
				<td><a href="<s:url action='actionEnvioMasivo'/>">Env&iacute;o programado</a></td>
			</tr>
		</s:if>
		<tr>
			<td><a href="<s:url action='actionCarpetasLista'/>">Carpetas</a></td>
		</tr>
	</table>
</div>
</body>
</html>