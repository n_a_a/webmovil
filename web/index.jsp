<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WebMovil - Bienvenido</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css" />
<link rel="stylesheet" href="styles/index.css?v=<s:property value="version" />" TYPE="text/css" />
</head>
<body>	

	<div id="mainContainer">
	
		<div id="logosContainer">
			<div id="logoJunta">
				<span class="hidden">Junta de Andaluc&iacute;a</span>
			</div>
			<div id="logoWebMovil">
				<span class="hidden">Webmovil</span>
			</div>
			<p class="clear">&nbsp;</p>
		</div>
	
		<div id="bodyContainer">
		<br />
			<tableborder="0" cellpadding="0" cellspacing="0" width="35%"
				align="center" id="login_form">
				<tr>
					<td align="center" style="padding-bottom: 10px;"><s:actionerror theme="simple"/></td>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0" width="35%"
				align="center" id="login_form"
				class="table_login">				
				<tr>
					<td colspan="2" height="25" width="100%" align="center"
						bgcolor="#007f40"
						style="border-left: 1px solid #000000; border-top: 1px solid #000000; border-right: 1px solid #000000; max-width: 450px;"><font
						class="v10Nblanco">Identificaci&oacute;n</font></td>
				</tr>
				<tr>
					<td bgcolor="#e7efc6" align="center"
						style="border-left: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000;">
					<s:form action="index">
						<s:hidden name="loginAttempt" value="%{'1'}" />
						<s:textfield name="usuario" label="Usuario" size="30" cssClass="v10negro" />
						<s:password name="clave" label="Clave" size="30" cssClass="v10negro" />
						<s:submit type="button" value="Aceptar" title="Identificación" align="center" />
					</s:form>
					<p class="version">v3.3.0.0</p>
					</td>
				</tr>
			</table>
		<br /><br />
		</div>
	</div>
</body>
</html>
