<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Histórico de envíos masivos programados</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/envios.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />

	<div id="bodyContainer">
		<h2>Resultado del envío masivo "<s:property value="descripcion" />"</h2>
		<table class="generalBox">
			<tr>
				<td class="texto"><pre><s:property value="todoElTexto" /></pre></td>
			</tr>
		</table>
		<s:url action="actionEnvioMasivoHistorico" var="returnLink">
			<s:param name="numPagina"><s:property value="numPagina" /></s:param>
		</s:url>
		<a href="${returnLink}">Volver al hist&oacute;rico</a>
	</div>
</div>
</body>
