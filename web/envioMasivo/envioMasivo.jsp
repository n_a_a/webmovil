<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Envío masivo programado</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/envios.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/jquery-ui.css?v=<s:property value="version" />" TYPE="text/css">
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery-ui.min.js"></script>
<script language="JavaScript" src="scripts/envios.js"
	type="text/javascript"></script>
<script>
	$(function() {
		$(document).tooltip();
	});
</script>
<script language="JavaScript" src="scripts/enviosMasivos.js"
	type="text/javascript"></script>
</head>
<body onload="javascript:cuenta_caracteres(document.forms[0]);">

	<div id="mainContainer">

		<s:include value="/includes/cabecera.jsp" />

		<div id="bodyContainer">
			<s:form action="actionEnvioMasivo" theme="simple" method="post"
				enctype="multipart/form-data">
				<table id="enviosTable">
					<s:if test="%{hasFieldErrors()}">
						<s:set name="errores" value="%{fieldErrors.get('envioMasivo')}" />
						<s:iterator value="%{fieldErrors.get('envioMasivo')}">
							<tr class="enviosTableTr">
								<td class="enviosTableTdCenter" colspan="3"><strong><span
										class="errorMessage"><s:property /></span></strong></td>
							</tr>
						</s:iterator>
					</s:if>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight" width="20%">Fichero de datos (<a
							href="#" onclick="openFormatoWindow();">Formato</a>):
						</td>
						<td class="enviosTableTd" colspan="2"><s:file
								name="formDestinatarios" size="20" label="Examinar" /></td>
					</tr>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight">Utilizar fichero anterior:</td>
						<td class="enviosTableTd" colspan="2"><s:select
								name="formMes" list="listaFicherosAnteriores" /></td>
					</tr>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight">Utilizar grupo de la agenda:</td>
						<td class="enviosTableTd"><s:textfield
								name="formNombreGrupoShow" disabled="true" /> <s:hidden
								name="formIdGrupo" /> <s:hidden name="formNombreGrupo" /></td>
						<td class="enviosTableTd"><s:a
								href="actionAgendaContenidoDirectoMasivo.action"
								onclick="openAgendaWindow(); return false;">Acceso a la agenda</s:a></td>
					</tr>
					<tr class="enviosTableTr">
						<td class="enviosTableTd">&nbsp;</td>
						<td class="enviosTableTd">Restantes: <s:textfield
								name="contCaracteres" disabled="true" cssClass="enviosInputInfo" />
						</td>
						<td class="enviosTableTdRight">Mensajes: <s:textfield
								name="contMensajes" disabled="true" cssClass="enviosInputInfo" />
						</td>
					</tr>
					<tr class="enviosTableTr" valign="top">
						<td class="enviosTableTdRight">Texto:</td>
						<!-- TODO: Añadir cuenta de carácteres -->
						<td class="enviosTableTd" colspan="2"><s:textarea
								name="formMensaje" cols="70" rows="7"
								onkeyup="javascript:cuenta_caracteres(document.forms[0]);" /></td>
					</tr>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight"><s:checkbox name="formFirma"
								onclick="javascript:cuenta_caracteres(document.forms[0]);" /></td>
						<td class="enviosTableTd" width="20%">Firma personal:</td>
						<td class="enviosTableTd" colspan="2"><s:textfield
								name="formTxtFirma"
								onkeyup="javascript:cuenta_caracteres(document.forms[0]);" /></td>
					</tr>
					<tr class="enviosTableTr">
						<s:if test="%{tienePermiso('per_firma')}">
							<td class="enviosTableTdRight"><s:checkbox
									name="formPerFirma"
									onclick="javascript:cuenta_caracteres(document.forms[0]);" /></td>
						</s:if>
						<s:else>
							<td class="enviosTableTdRight"><s:checkbox
									name="formPerFirma" disabled="true"
									onclick="javascript:cuenta_caracteres(document.forms[0]);" /></td>
						</s:else>
						<td class="enviosTableTd">Firma corporativa:</td>
						<td class="enviosTableTd"><s:textfield
								name="firmaCorporativa" readonly="true"
								cssStyle="border:1px solid #ffffff" /></td>
					</tr>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight"><s:checkbox
								name="formAcuseRecibo" /></td>
						<td class="enviosTableTd" colspan="2">Enviar con acuse de
							recibo.</td>
					</tr>
					<s:if test="%{canSetAlfa}">
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight"><s:checkbox name="formAlfanumerico" /></td>
						<td class="enviosTableTd" colspan="2">Enviar con remitente alfanumérico. (<s:property value="alfa" />)</td>
					</tr>
					</s:if>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight">Programar el:</td>
						<td class="enviosTableTd" colspan="2"><s:textfield
								name="formProgDia" maxlength="2" size="2" theme="simple" /> de <s:select
								name="formProgMes"
								list="#{'01':'Enero', '02':'Febrero', '03':'Marzo', '04':'Abril', '05':'Mayo', '06':'Junio', '07':'Julio', '08':'Agosto', '09':'Septiembre', '10':'Octubre', '11':'Noviembre', '12':'Diciembre'}"
								theme="simple" /> de <s:textfield name="formProgAnno"
								maxlength="4" size="4" theme="simple" /> a las <s:select
								name="formProgHoras"
								list="#{'00':'00', '01':'01', '02':'02', '03':'03', '04':'04', '05':'05', '06':'06', '07':'07', '08':'08', '09':'09', '10':'10', '11':'11', '12':'12', '13':'13', '14':'14', '15':'15', '16':'16', '17':'17', '18':'18', '19':'19', '20':'20', '21':'21', '22':'22', '23':'23'}"
								theme="simple" /> :<s:select name="formProgMinutos"
								list="#{'00':'00', '15':'15', '30':'30', '45':'45'}"
								theme="simple" /></td>
					</tr>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight">Nombre para almacenar el
							env&iacute;o en el sistema:</td>
						<td class="enviosTableTd" colspan="2"><s:textfield
								name="formTitulo" maxlength="20" size="20" theme="simple" /></td>
					</tr>
					<tr class="enviosTableTr">
						<td class="enviosTableTdRight">Tipo de envío <a href="#"
							title="<strong>Estándar</strong> enviará el mensaje como notificación push a los destinatarios con la aplicación 'Avisos Junta' instalada (sin coste) o como SMS al resto. <br /><strong>Forzar SMS</strong> enviará SMS en cualquier caso.<br /> <strong>Sólo Avisos Junta</strong> enviará una notificación a la aplicación Avisos Junta (si el destinatario no tiene la aplicación instalada, no recibirá el mensaje y el proceso devolverá un error)"><img
								src="images/help.png" /></a>:
						</td>
						<td class="enviosTableTd" colspan="2"><s:select
								name="formTipoMensaje"
								list="#{'02':'Estándar', '00':'Forzar SMS', '01':'Sólo Avisos Junta', '03':'Avisos Junta Con Entrega Segura'}" />
						</td>
					</tr>
					<tr class="enviosTableTr">
						<td colspan="3"><s:submit type="button" name="formAccion"
								value="Programar este envío" /></td>
					</tr>
				</table>
				<a href="<s:url action='actionEnvioMasivoProgramados' />">Env&iacute;os
					programados</a> | <a
					href="<s:url action='actionEnvioMasivoHistorico' />">Hist&oacute;rico
					de env&iacute;os</a>
			</s:form>
		</div>
	</div>
</body>
</html>