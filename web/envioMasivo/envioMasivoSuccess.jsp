<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Envío masivo programado</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/envios.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />

	<div id="bodyContainer">
			<table id="enviosTable">
				<tr class="enviosTableTr">
					<td class="enviosTableTdCenter" width="20%"><strong>Envío programado satisfactoriamente.</strong></td>
				</tr>
			</table>
			<a href="<s:url action='actionEnvioMasivoProgramados' />">Env&iacute;os programados</a> | <a href="<s:url action='actionEnvioMasivoHistorico' />">Hist&oacute;rico de env&iacute;os</a>
	</div>
</div>
</body>
</html>