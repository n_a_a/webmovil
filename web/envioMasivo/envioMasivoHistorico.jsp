<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Histórico de envíos masivos programados</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/envios.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />

	<div id="bodyContainer">
		<h2>Hist&oacute;rico de env&iacute;os masivos programados</h2>
		<s:if test="numAlmacenados == 0">
			<!-- Mensaje de que no almacenados -->
			<table class="generalBox"><tr><td>No existen env&iacute;os programados.</td></tr></table>
		</s:if>
		<s:else>
			<!-- Muestra la página actual de la lista de almacenados -->
			<table class="generalBox">
				<tr><td>Envíos masivos programados: <s:property value="numAlmacenados" /></td></tr>
			</table>
			<table id="tablaListaAlmacenados">
				<tr>
					<th width="10%">Fecha</th>
					<th width="10%">Hora</th>
					<th width="20%">Descripci&oacute;n</th>
					<th width="50%">Texto</th>
					<th width="5%">Res</th>
					<th width="5%"></th>
				</tr>
				<s:iterator value="envioMasivoStore" status="offset">
					<!-- Color de las lineas de la tabla -->
					<s:if test="#offset.odd == true">
						<s:set name="trclass">impar</s:set>
					</s:if> 
					<s:else>
						<s:set name="trclass">par</s:set>
					</s:else>
					<tr class="${trclass}">
						<s:url action="actionEnvioMasivoHistoricoResultado" var="resLink">
							<s:param name="nombreArchivo"><s:property value="nombreArchivo" /></s:param>
							<s:param name="descripcion"><s:property value="descripcion" /></s:param>
							<s:param name="numPagina"><s:property value="numPagina" /></s:param>
						</s:url>
						<s:url action="actionEnvioMasivoHistorico" var="delLink">
							<s:param name="nombreArchivo"><s:property value="nombreArchivo" /></s:param>
							<s:param name="accion">delete</s:param>
							<s:param name="numPagina"><s:property value="numPagina" /></s:param>
						</s:url>
						<td class="numerico"><s:property value="fecha" /></td>
						<td class="numerico"><s:property value="hora" /></td>
						<td class="numerico"><s:property value="descripcion" /></td>
						<td class="texto"><s:property value="texto" /></td>
						<td class="numerico"><a href="${resLink}"><img src="images/icono-estadisticas.png" /></a></td>
						<td class="numerico"><a href="${delLink}"><img src="images/icono-borrar.png" /></a></td>
					</tr>
				</s:iterator>
			</table>
			<!-- Paginador -->
			<table class="generalBox">
				<tr>
					<td width="32">
						<s:if test="numPagina > 0">
							<s:url action="actionEnvioMasivoHistorico" var="primeraPaginaLink">
								<s:param name="accion">paginaPrimera</s:param>
								<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
								<s:param name="numPagina"><s:property value="numPagina"/></s:param>
							</s:url>
							<a href="${primeraPaginaLink}"><img src="images/icono-flecha-0.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-0-g.png" />
						</s:else>
					</td>
					<td width="32">
						<s:if test="numPagina > 0">
							<s:url action="actionEnvioMasivoHistorico" var="paginaAnteriorLink">
								<s:param name="accion">paginaAnterior</s:param>
								<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
								<s:param name="numPagina"><s:property value="numPagina"/></s:param>
							</s:url>
							<a href="${paginaAnteriorLink}"><img src="images/icono-flecha-1.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-1-g.png" />
						</s:else>
					</td>
					<td width="32">
						<s:if test="numPagina < maxPagina">
							<s:url action="actionEnvioMasivoHistorico" var="paginaSiguienteLink">
								<s:param name="accion">paginaSiguiente</s:param>
								<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
								<s:param name="numPagina"><s:property value="numPagina"/></s:param>
							</s:url>
							<a href="${paginaSiguienteLink}"><img src="images/icono-flecha-2.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-2-g.png" />
						</s:else>
					</td>
					<td width="32">
						<s:if test="numPagina < maxPagina">
							<s:url action="actionEnvioMasivoHistorico" var="ultimaPaginaLink">
								<s:param name="accion">paginaUltima</s:param>
								<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
								<s:param name="numPagina"><s:property value="numPagina"/></s:param>
							</s:url>
							<a href="${ultimaPaginaLink}"><img src="images/icono-flecha-3.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-3-g.png" />
						</s:else>
					</td>
					<!-- Página actual -->
					<td>
						P&aacute;gina <s:property value="numPagina + 1" /> de <s:property value="maxPagina + 1" />
					</td>
				</tr>
			</table>
		</s:else>
	</div>
</div>
</body>