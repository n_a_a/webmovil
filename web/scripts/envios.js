function muestra_texto() {
	var opcion = document.getElementById('enviarSMS_envio_tipoDestinatario')[document
			.getElementById('enviarSMS_envio_tipoDestinatario').selectedIndex].value;

	if (opcion == 'Nacional') {
		document.getElementById('texto_nacional').style.display = 'block';
		document.getElementById('texto_internacional').style.display = 'none';
	} else {
		document.getElementById('texto_internacional').style.display = 'block';
		document.getElementById('texto_nacional').style.display = 'none';
	}
}

function limpia_form(s, f) {
	if (s.value == 'Nacional')
		f.value = "";
	else
		f.value = "+";
}

function filtra_caracteres(f, sel) {
	var t = f.value;
	var s = "";
	var nacionalidad = sel.value;
	var i;

	for (i = 0; i < t.length; i++) {
		var c = t.charAt(i);
		if (nacionalidad == 'Nacional') {
			if (!isNaN(parseInt(c)) && i < 9)
				s += c;
		} else {
			if ((!isNaN(parseInt(c)) && s != "") || (c == '+' && s == ""))
				s += c;
		}

	}
	f.value = s;
}

function filtra_caracteres_corp(f) {
	var t = f.value;
	var s = "";
	var i;

	for (i = 0; i < t.length; i++) {
		var c = t.charAt(i);
		if (!isNaN(parseInt(c)) && i < 6)
			s += c;
	}
	f.value = s;
}

function cuenta_caracteres(f) {

	var caracteres = 0;

	if (f.enviarSMS_envio_conFirmaPersonal.checked == true) {
		caracteres += parseInt(f.enviarSMS_envio_firmaPersonal.value.length, 10);
	}

	if (f.enviarSMS_envio_conFirmaCorporativa)
		if (f.enviarSMS_envio_conFirmaCorporativa.checked == true) {
			caracteres += parseInt(
					f.enviarSMS_envio_firmaCorporativa.value.length, 10);
		}

	caracteres += f.enviarSMS_envio_texto.value.length;

	var mensajes = 1 + parseInt(caracteres / 160);
	var restoCaracteres = 160 - caracteres % 160;

	var restoMensajes = f.restoSmsNacionales.value;
	// var restoMensajes = 1;
	var indice = f.enviarSMS_envio_tipoDestinatario.selectedIndex;
	var opcion = f.enviarSMS_envio_tipoDestinatario.options[indice].value;

	if (opcion == 'Internacional')
		restoMensajes = f.restoSmsInternacionales.value;
	// restoMensajes = 2;

	var total = f.totalNacionales.value;
	
	if (opcion == 'Internacional')
		total = f.totalInternacionales.value;
	
	if (mensajes > restoMensajes && total != -1) {
		mensajes = restoMensajes;
		restoCaracteres = 0;

		var carSobrantes = caracteres - (mensajes * 160);
		var finTexto = f.enviarSMS_envio_texto.value.length - carSobrantes;
		var texto = f.enviarSMS_envio_texto.value.substring(0, finTexto);

		f.enviarSMS_envio_texto.value = texto;
	}

	f.contCaracteres.value = restoCaracteres;
	f.contMensajes.value = mensajes;
}

function openAgendaWindow() {
	// var vWinUsers = window.open('actionAgendaContenidoDirecto.action',
	// 'Contactos',
	// 'status=yes,resizable=yes,top=100,left=200,width=580,height=750,scrollbars=yes');
	window.open('actionAgendaContenidoDirecto.action', 'Contactos', 'status=yes,resizable=yes,top=100,left=200,width=680,height=750,scrollbars=yes');
	// vWinUsers.opener = self;
	// vWinUsers.focus();
	return false;
}
