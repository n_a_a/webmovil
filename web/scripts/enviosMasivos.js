function cuenta_caracteres(f) {

	var caracteres = 0;

	if (f.actionEnvioMasivo_formFirma.checked == true) {
		caracteres += parseInt(f.actionEnvioMasivo_formTxtFirma.value.length, 10);
	}

	if (f.actionEnvioMasivo_formPerFirma.checked == true) {
		caracteres += parseInt(
				f.actionEnvioMasivo_firmaCorporativa.value.length, 10);
	}
	
	caracteres += f.actionEnvioMasivo_formMensaje.value.length;

	var mensajes = 1 + parseInt(caracteres / 160);
	var restoCaracteres = 160 - caracteres % 160;
/*
	var restoMensajes = f.restoSmsNacionales.value;
	// var restoMensajes = 1;
	var indice = f.actionEnvioMasivo_envio_tipoDestinatario.selectedIndex;
	var opcion = f.actionEnvioMasivo_envio_tipoDestinatario.options[indice].value;

	if (opcion == 'Internacional')
		restoMensajes = f.restoSmsInternacionales.value;
	// restoMensajes = 2;

	if (mensajes > restoMensajes) {
		mensajes = restoMensajes;
		restoCaracteres = 0;

		var carSobrantes = caracteres - (mensajes * 160);
		var finTexto = f.actionEnvioMasivo_formMensaje.value.length - carSobrantes;
		var texto = f.actionEnvioMasivo_formMensaje.value.substring(0, finTexto);

		f.actionEnvioMasivo_formMensaje.value = texto;
	}
*/
	f.contCaracteres.value = restoCaracteres;
	f.contMensajes.value = mensajes;
}

function openAgendaWindow() {
	window.open('actionAgendaContenidoDirectoMasivo.action', 'Contactos', 'status=yes,resizable=yes,top=100,left=200,width=680,height=750,scrollbars=yes');
}

function openFormatoWindow() {
	window.open('envioMasivo/formatoMasivo.html', 'Formato', 'status=yes,resizable=yes,top=100,left=200,width=680,height=650,scrollbars=yes');
}

