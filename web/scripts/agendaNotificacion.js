
function seleccionaContacto(valor, id, url) {
	var urlCargarDestinatarios = url + "?idContacto=" + id + "&tlfnoDestino=" + opener.document.forms[0].tlfnoDestino.value;
	cargarDestinatarios(urlCargarDestinatarios);
}

function seleccionaGrupo(valor, id, url) {
	var urlCargarDestinatarios = url + "?idGrupo=" + id + "&tlfnoDestino=" + opener.document.forms[0].tlfnoDestino.value;
	cargarDestinatarios(urlCargarDestinatarios);
}

function cargarDestinatarios(url) {	
	$.ajax({
		url : url,
		type : 'POST',
		success : function(data) {
			opener.document.forms[0].tlfnoDestino.value = $(data).val();
			window.close();
		}
	});
}
