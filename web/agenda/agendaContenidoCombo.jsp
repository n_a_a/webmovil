<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Agenda</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/agenda.css?v=<s:property value="version" />" TYPE="text/css">
<s:head />
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
	
		<s:form action="actionAgendaContenidoCombo" theme="simple">
			<s:hidden name="idGrupoSel" value="%{grupoSel.idGrupo}"/>
			<table class="botonesArriba">
				<tr>
					<td align="right">
							<s:submit type="button" name="accion" value="Nuevo contacto" action="actionAgendaNuevoContacto" />&nbsp;
							<s:submit type="button" name="accion" value="Nuevo grupo" action="actionAgendaNuevoGrupo" />
					</td>
				</tr>
			</table>
			<s:actionerror theme="simple"/>
			<s:if test="grupoSel.nombre != null && grupoSel.nombre != ''">
				<table class="rutaBox">
					<tr>
						<td width="17"><img src="images/pic_grupo.gif" /></td>
						<s:url action="actionAgendaModificarGrupo" var="modificarGrupo">
							<s:param name="grupo.idGrupo"><s:property value="grupoSel.idGrupo" /></s:param>
							<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
						</s:url>
						<td align="left"><a href="${modificarGrupo}"><s:property value="grupoSel.nombre" /></a></td>
						<td align="right" width="20">
							<s:url action="actionAgendaContenidoCombo" var="verGrupoSuperior">
								<s:param name="idGrupoSel"><s:property value="grupoSel.idPadre" /></s:param>
							</s:url>
							<a href="${verGrupoSuperior}"><img src="images/boton_subirNivel.gif" /></a>
						</td>
					</tr>
				</table>
			</s:if>
			<s:else>
				<table class="rutaBox">
					<tr>
						<td width="17"><img src="images/pic_grupo.gif" /></td>
						<td align="left"><a href="#">Grupo raíz</a></td>
					</tr>
				</table>
			</s:else>
			
			<table id="tablaCombo">
				<tr valign="top">
					<td width="30%">
						
						<table id="tablaListaGrupos">
							<tr><th colspan="2">Subgrupos</th></tr>
							<tr>
								<th></th>
								<th>Nombre</th>
							</tr>
							<s:if test="numGrupos == 0">
								<tr><td class="mensaje" colspan="4">Sin subgrupos.</td></tr>
							</s:if>			
							<s:else>
								<!-- presentamos una lista de subgrupos -->
					
								<s:iterator value="listaGrupos" status="offset">
									<!-- Color de las lineas de la tabla -->
									<s:if test="#offset.odd == true">
										<s:set name="trclass">impar</s:set>
									</s:if> 
									<s:else>
										<s:set name="trclass">par</s:set>
									</s:else>
									
									<tr class="${trclass}">
										<td class="numerico" width="5%"><s:checkbox name="gruposSeleccionados"
											fieldValue="%{idContacto}" theme="simple"></s:checkbox></td>
										<td class="texto" width="95%" align="left">
											<s:url action="actionAgendaContenidoCombo" var="verContenidoGrupo">
												<s:param name="idGrupoSel"><s:property value="idGrupo" /></s:param>
											</s:url>
											<a href="${verContenidoGrupo}" class="enlaceGrupo"><s:property value="nombre" /></a>
										</td>
									</tr>
								</s:iterator>	
							</s:else>
						</table>
					</td>
					<td width="70%">
						<table id="tablaListaContactos">
							<tr><th colspan="4">Contactos</th></tr>
							<tr>
								<th></th>
								<th>Nombre</th>
								<th>Tel&eacute;fono</th>
								<th>Correo electr&oacute;nico</th>
							</tr>
							<s:if test="numContactos == 0">
								<tr><td class="mensaje" colspan="4">Este grupo no tiene contactos.</td></tr>
							</s:if>			
							<s:else>
								<!-- presentamos una lista de contactos -->
					
								<s:iterator value="listaContactos" status="offset">
									<!-- Color de las lineas de la tabla -->
									<s:if test="#offset.odd == true">
										<s:set name="trclass">impar</s:set>
									</s:if> 
									<s:else>
										<s:set name="trclass">par</s:set>
									</s:else>
									
									<tr class="${trclass}"  height="34">
										<td class="numerico" width="5%"><s:checkbox name="contactosSeleccionados"
											fieldValue="%{idContacto}" theme="simple"></s:checkbox></td>
										<td class="texto" width="45%">
											<s:url action="actionAgendaModificarContacto" var="modificarContacto">
												<s:param name="contacto.idContacto"><s:property value="idContacto" /></s:param>
												<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
											</s:url>
											<a href="${modificarContacto}" class="enlaceContacto"><s:property value="nombre" /></a>
										</td>
										<td class="texto" width="20%" valign="middle">
											<s:if test="clientePush == true">
												<em><s:property value="tlfMovil" /></em>&nbsp;
												<img src="images/avisosjunta.png" alt="Este contacto tiene instalada la aplicación Avisos Junta" title="Este contacto tiene instalada la aplicación Avisos Junta" style="vertical-align:middle;" />
											</s:if>
											<s:else>
												<s:property value="tlfMovil" />
											</s:else>
										</td>
										<td class="texto" width="30%"><s:property value="email" /></td>
									</tr>
								</s:iterator>
							</s:else>
						</table>
							
						<!-- Paginador -->
						<table class="botonesAbajoPaginacion">
							<tr>
								<!-- Página actual -->
								<td align="right">
									<b><s:property value="regIni" /></b>&nbsp;-&nbsp;<b><s:property value="regFin" /></b>&nbsp;de&nbsp;<b><s:property value="numContactos" /></b>&nbsp;
								</td>
								<td width="32">
									<s:if test="numPagina > 0">
										<s:url action="actionAgendaContenidoCombo" var="primeraPaginaLink">
											<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
											<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
										</s:url>
										<a href="${primeraPaginaLink}"><img src="images/icono-flecha-0.png" /></a>
									</s:if>
									<s:else>
										<img src="images/icono-flecha-0-g.png" />
									</s:else>
								</td>
								<td width="32">
									<s:if test="numPagina > 0">
										<s:url action="actionAgendaContenidoCombo" var="paginaAnteriorLink">
											<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
											<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
											<s:param name="numPagina"><s:property value="numPagina - 1" /></s:param>
										</s:url>
										<a href="${paginaAnteriorLink}"><img src="images/icono-flecha-1.png" /></a>
									</s:if>
									<s:else>
										<img src="images/icono-flecha-1-g.png" />
									</s:else>
								</td>
								<td width="32">
									<s:if test="numPagina < maxPagina">
										<s:url action="actionAgendaContenidoCombo" var="paginaSiguienteLink">
											<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
											<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
											<s:param name="numPagina"><s:property value="numPagina + 1" /></s:param>
										</s:url>
										<a href="${paginaSiguienteLink}"><img src="images/icono-flecha-2.png" /></a>
									</s:if>
									<s:else>
										<img src="images/icono-flecha-2-g.png" />
									</s:else>
								</td>
								<td width="32">
									<s:if test="numPagina < maxPagina">
										<s:url action="actionAgendaContenidoCombo" var="ultimaPaginaLink">
											<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
											<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
											<s:param name="numPagina"><s:property value="maxPagina" /></s:param>
										</s:url>
										<a href="${ultimaPaginaLink}"><img src="images/icono-flecha-3.png" /></a>
									</s:if>
									<s:else>
										<img src="images/icono-flecha-3-g.png" />
									</s:else>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			
			<!-- Acciones -->
			<table class="botonesAbajo">
				<tr>
					<td align="center">
						<s:submit type="button" name="accion" value="Eliminar"/>
						&nbsp;&nbsp;<s:submit type="button" name="accion" value="Mover a" />
						<s:select name="idGrupoMover" list="grupos" listKey="idGrupo" listValue="nombre" />
						&nbsp;&nbsp;<s:submit type="button" name="accion" value="Copiar a" />
						<s:select name="idGrupoCopiar" list="grupos" listKey="idGrupo" listValue="nombre" />
					</td>
				</tr>
			</table>
			
			<!-- Propagamos los valores actuales de la vista -->
			<!--<s:hidden name="idGrupoSel" />-->
			<s:hidden name="numPagina" />
			<br />
			<!-- Importación de nuevos contactos -->
			<table id="tablaImportar">
				<tr>
					<th>Importar contactos</th>
				</tr>
				<s:if test="resultadoImportar != null">
					<tr>
						<td>
							<p><s:iterator value="resultadoImportar" ><s:property /><br /></s:iterator></p>
						</td>
					</tr>
				</s:if>
				<tr>
					<td>
						<p>Introduzca cada contacto en una linea. Separe nombre y tel&eacute;fono m&oacute;vil con 
						un punto y coma. Los contactos se importar&aacute;n en el grupo actual. Ejemplo:</p>
						<p><tt>Antonio P&eacute;rez;600999999<br />Jos&eacute; Rodr&iacute;guez;600112233</tt></p>
					</td>
				</tr>
				<tr>
					<td>
						<s:textarea name="textoImportar" cols="70" rows="7" />
					</td>
				</tr>
				<tr>
					<td>
						<s:submit type="button" name="accion" value="Importar" />
					</td>
				</tr>
			</table>
		</s:form>
		
	</div>
</div>
</body>
</html>