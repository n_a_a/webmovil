<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Agenda - Nuevo Grupo</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/agenda.css?v=<s:property value="version" />" TYPE="text/css">
<s:head />
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
	
		<s:form action="actionAgendaNuevoGrupo" method="post" theme="simple">
			<!--<s:hidden name="idGrupoSel" value="%{idGrupoSel}"/>-->
			<table id="grupoTable">
				<tr>
					<td class="mensajeError" colspan="2"><s:actionerror theme="simple"/></td>
				</tr>
				<s:if test="%{hasFieldErrors()}">
					<s:iterator value="%{fieldErrors.get('nombreGrupo')}">
						<tr class="grupoTableTr">
							<td class="grupoTableTd"  align="center" colspan="2"><span class="errorMessage"><s:property /></span></td>
						</tr>
					</s:iterator>
				</s:if>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Nombre del grupo:</td>
					<td class="grupoTableTd"><s:textfield name="nombreGrupo" size="30" /></td>
				</tr>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Grupo en el que se incluir&aacute; el grupo:</td>
					<td class="grupoTableTd"><s:select name="idGrupoSel" list="grupos" listKey="idGrupo" listValue="nombre" /></td>
				</tr>
				<tr class="blanco">
						<td colspan="2"></td>
				</tr>
				<tr class="grupoTableTr" height="35" valign="middle">
					<td class="grupoTableTd" colspan="2" align="center"> 
						<s:submit type="button" name="accion" value="Aceptar" />
						<s:submit type="button" name="accion" value="Volver" />
					</td>
				</tr>
			</table>
		</s:form>
		
	</div>
</div>
</body>
</html>
