<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Agenda - Nuevo Grupo</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/agenda.css?v=<s:property value="version" />" TYPE="text/css">
<s:head />
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
	
		<s:form action="actionAgendaNuevoContacto" method="post" theme="simple">
			<!--<s:hidden name="idGrupoSel" value="%{idGrupoSel}"/>-->
			<table id="grupoTable">
				<tr>
					<td class="mensajeError" colspan="3"><s:actionerror theme="simple"/></td>
				</tr>
				<s:if test="%{hasFieldErrors()}">
					<s:iterator value="%{fieldErrors.get('nombreContacto')}">
						<tr class="grupoTableTr">
							<td class="grupoTableTd"  align="center" colspan="3"><span class="errorMessage"><s:property /></span></td>
						</tr>
					</s:iterator>
				</s:if>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Nombre del contacto:</td>
					<td class="grupoTableTd" colspan="2"><s:textfield name="nombreContacto" size="50" /></td>
				</tr>
				<s:if test="%{hasFieldErrors()}">
					<s:iterator value="%{fieldErrors.get('tlfMovil')}">
						<tr class="grupoTableTr">
							<td class="grupoTableTd"  align="center" colspan="3"><span class="errorMessage"><s:property /></span></td>
						</tr>
					</s:iterator>
				</s:if>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Tel&eacute;fono m&oacute;vil:</td>
					<td class="grupoTableTd" width="100"><s:select name="tipoTelefono" list="tiposTelefono" /></td>
					<td class="grupoTableTd"><s:textfield name="tlfMovil" size="34" /></td>
				</tr>
				<s:if test="%{hasFieldErrors()}">
					<s:iterator value="%{fieldErrors.get('tlfFijo')}">
						<tr class="grupoTableTr">
							<td class="grupoTableTd"  align="center" colspan="3"><span class="errorMessage"><s:property /></span></td>
						</tr>
					</s:iterator>
				</s:if>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Tel&eacute;fono fijo:</td>
					<td class="grupoTableTd" width="100"><s:select name="tipoTelefono" list="tiposTelefono" /></td>
					<td class="grupoTableTd"><s:textfield name="tlfFijo" size="34" /></td>
				</tr>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Correo electr&oacute;nico:</td>
					<td class="grupoTableTd" colspan="2"><s:textfield name="email" size="50" /></td>
				</tr>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Entidad:</td>
					<td class="grupoTableTd" colspan="2"><s:textfield name="entidad" size="50" /></td>
				</tr>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Grupo en el que se incluir&aacute; el contacto:</td>
					<td class="grupoTableTd" colspan="2"><s:select name="idGrupoSel" list="grupos" listKey="idGrupo" listValue="nombre" value="%{idGrupoSel}"/></td>
				</tr>
				<tr class="grupoTableTr">
					<td class="grupoTableTd" valign="top">Notas:</td>
					<td class="grupoTableTd" colspan="2"><s:textarea name="notas" cols="50" rows="10" /></td>
				</tr>
				<tr class="blanco">
						<td colspan="2"></td>
				</tr>
				<tr class="grupoTableTr" height="35" valign="middle">
					<td class="grupoTableTd" colspan="3" align="center"> 
						<s:submit type="button" name="accion" value="Aceptar" />
						<s:submit type="button" name="accion" value="Volver" />
					</td>
				</tr>
			</table>
		</s:form>
		
	</div>
</div>
</body>
</html>
