<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Agenda</title>
<script language="JavaScript" src="scripts/agenda.js" type="text/javascript"></script>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/agenda.css?v=<s:property value="version" />" TYPE="text/css">
<s:head />
</head>
<body>

<div id="mainContainer">

	<div id="logosContainer">
		<div id="logoJunta">
			<span class="hidden">Junta de Andaluc&iacute;a</span>
		</div>
		<div id="logoWebMovil">
			<span class="hidden">Webmovil</span>
		</div>
		<p class="clear">&nbsp;</p>
	</div>

	<div id="infoBar">
		<div id="infoBarUsuario">
			<s:property value="#session.objetoUsuario.usuario" />
		</div>
		<div id="infoBarEmisor">
			<s:property value="#session.objetoUsuario.emisor.emisor" />
		</div>
		<p class="clear">&nbsp;</p>
	</div>

	<div id="menuBar">
		<div class="menuItemSelected">CONTACTOS</div>
		<p class="clear">&nbsp;</p>
	</div>
		
	<div id="bodyContainer">
	
		<s:form action="actionAgendaContenidoDirecto" theme="simple">
			<table class="botonesArriba">
				<tr>
					<td align="left">
						<s:select list="tiposContactos" name="tipoContactos" />
						<s:submit type="button" name="accion" value="Ver" />
					</td>
				</tr>
			</table>
			<s:actionerror theme="simple"/>
			<s:if test="grupoSel.nombre != null && grupoSel.nombre != ''">
				<table class="rutaBox">
					<tr>
						<td width="17"><img src="images/pic_grupo.gif" /></td>
						<td align="left"><s:property value="grupoSel.nombre" /></td>
						<td align="right" width="20">
							<s:url action="actionAgendaContenidoDirecto" var="verGrupoSuperior">
								<s:param name="idGrupoSel"><s:property value="grupoSel.idPadre" /></s:param>
							</s:url>
							<a href="${verGrupoSuperior}"><img src="images/boton_subirNivel.gif" /></a>
						</td>
					</tr>
				</table>
			</s:if>
			<!-- vemos si hay contactos que mostrar -->
			<s:if test="numContactos == 0">
				<s:if test="idGrupoSel == '0' && tipoContactos == 'Contactos'">
					<table id="tablaListaContactos">
						<tr>
							<th></th>
							<th>Nombre</th>
							<th>Tel&eacute;fono</th>
						</tr>
						<!-- Mensaje de que no hay contactos -->
						<tr><td class="mensaje" colspan="3">No hay contactos.</td></tr>
					</table>
				</s:if>
				<s:elseif test="idGrupoSel == '0' && tipoContactos == 'Grupos'">
					<table id="tablaListaContactos">
						<tr>
							<th></th>
							<th>Nombre</th>
						</tr>
						<!-- Mensaje de que no hay grupos -->
						<tr><td class="mensaje" colspan="2">No hay grupos de contactos.</td></tr>
					</table>
				</s:elseif>
				<s:elseif test="tipoContactos == 'Contactos'">
					<table id="tablaListaContactos">
						<tr>
							<th></th>
							<th>Nombre</th>
							<th>Tel&eacute;fono</th>
						</tr>
						<!-- Mensaje de que no hay contactos en el grupo-->
						<tr>
							<td class="mensaje" colspan="3">
								El grupo <b><s:property value="grupoSel.nombre" /></b> no contiene contactos.
							</td>
						</tr>
					</table>
				</s:elseif>
				<s:else>
					<table id="tablaListaContactos">
						<tr>
							<th></th>
							<th>Nombre</th>
						</tr>
						<!-- Mensaje de que no hay grupos en el grupo -->
						<tr>
							<td class="mensaje" colspan="2">
								El grupo <b><s:property value="grupoSel.nombre" /></b> no contiene grupos de contactos.
							</td>
						</tr>
					</table>
				</s:else>
			</s:if>
			<s:else>
				<!-- presentamos una lista de contactos -->
				<s:if test="tipoContactos == 'Contactos'">
					<table id="tablaListaContactos">
						<tr>
							<th></th>
							<th>Nombre</th>
							<th>Tel&eacute;fono</th>
						</tr>
			
						<s:iterator value="listaContactos" status="offset">
							<!-- Color de las lineas de la tabla -->
							<s:if test="#offset.odd == true">
								<s:set name="trclass">impar</s:set>
							</s:if> 
							<s:else>
								<s:set name="trclass">par</s:set>
							</s:else>
							
							<tr class="${trclass}"  height="34">
								<td class="numerico" width="5%"><!--<s:checkbox name="contactosSeleccionados"
									fieldValue="%{idContacto}" theme="simple"></s:checkbox>--></td>
								<td class="texto" width="70%">
									<s:a href="actionAgendaContenidoDirecto.action" onclick="javascript:seleccionaContacto('%{nombre}', '%{idContacto}'); return false;"><s:property value="nombre" /></s:a>
								</td>
								<td class="texto" width="25%">
									<s:if test="clientePush == true">
										<em><s:property value="tlfMovil" /></em>&nbsp;
										<img src="images/avisosjunta.png" alt="Este contacto tiene instalada la aplicación Avisos Junta" title="Este contacto tiene instalada la aplicación Avisos Junta" style="vertical-align:middle;" />
									</s:if>
									<s:else>
										<s:property value="tlfMovil" />
									</s:else>
								</td>
							</tr>
						</s:iterator>
					</table>
				</s:if>
				<s:else>
					<table id="tablaListaContactos">
						<tr>
							<th></th>
							<th>Nombre</th>
							<th></th>
						</tr>
			
						<s:iterator value="listaContactos" status="offset">
							<!-- Color de las lineas de la tabla -->
							<s:if test="#offset.odd == true">
								<s:set name="trclass">impar</s:set>
							</s:if> 
							<s:else>
								<s:set name="trclass">par</s:set>
							</s:else>
							
							<tr class="${trclass}">
								<td class="numerico" width="5%"><!--<s:checkbox name="contactosSeleccionados"
									fieldValue="%{idContacto}" theme="simple"></s:checkbox>--></td>
								<td class="texto" width="90%" align="left">
									<s:url action="actionAgendaContenidoDirecto" var="verContenidoGrupo">
										<s:param name="idGrupoSel"><s:property value="idGrupo" /></s:param>
									</s:url>
									<a href="${verContenidoGrupo}" class="enlaceGrupo"><s:property value="nombre" /></a>
								</td>
								<td class="numerico" width="5%">
									<s:a href="actionAgendaContenidoDirecto.action" onclick="javascript:seleccionaGrupo('%{nombre}', '%{idGrupo}'); return false;"><img src="images/icono-enviar.png" /></s:a>
								</td>
							</tr>
						</s:iterator>
					</table>
				</s:else>
				
				<!-- Paginador -->
				<table class="botonesAbajo">
					<tr>
						<td width="32">
							<s:if test="numPagina > 0">
								<s:url action="actionAgendaContenidoDirecto" var="primeraPaginaLink">
									<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
									<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
								</s:url>
								<a href="${primeraPaginaLink}"><img src="images/icono-flecha-0.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-0-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina > 0">
								<s:url action="actionAgendaContenidoDirecto" var="paginaAnteriorLink">
									<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
									<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
									<s:param name="numPagina"><s:property value="numPagina - 1" /></s:param>
								</s:url>
								<a href="${paginaAnteriorLink}"><img src="images/icono-flecha-1.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-1-g.png" />
							</s:else>
						</td>
						<!-- Página actual -->
						<td>
							<b><s:property value="regIni" /></b>&nbsp;-&nbsp;<b><s:property value="regFin" /></b>&nbsp;de&nbsp;<b><s:property value="numContactos" /></b>&nbsp;
						</td>
						<td width="32">
							<s:if test="numPagina < maxPagina">
								<s:url action="actionAgendaContenidoDirecto" var="paginaSiguienteLink">
									<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
									<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
									<s:param name="numPagina"><s:property value="numPagina + 1" /></s:param>
								</s:url>
								<a href="${paginaSiguienteLink}"><img src="images/icono-flecha-2.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-2-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina < maxPagina">
								<s:url action="actionAgendaContenidoDirecto" var="ultimaPaginaLink">
									<s:param name="idGrupoSel"><s:property value="idGrupoSel" /></s:param>
									<s:param name="tipoContactos"><s:property value="tipoContactos" /></s:param>
									<s:param name="numPagina"><s:property value="maxPagina" /></s:param>
								</s:url>
								<a href="${ultimaPaginaLink}"><img src="images/icono-flecha-3.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-3-g.png" />
							</s:else>
						</td>
					</tr>
				</table>
			</s:else>
			<!-- Propagamos los valores actuales de la vista -->
			<s:hidden name="idGrupoSel" />
			<s:hidden name="numPagina" />
		</s:form>
		
	</div>
</div>
</body>
</html>