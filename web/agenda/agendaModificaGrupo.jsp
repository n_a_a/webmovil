<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Agenda - Modificar Grupo</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/agenda.css?v=<s:property value="version" />" TYPE="text/css">
<s:head />
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
	
		<s:form action="actionAgendaModificarGrupo" method="post" theme="simple">
			<s:hidden name="grupo.idGrupo" />
			<s:hidden name="idGrupoSel" value="%{idGrupoSel}"/>
			<table id="grupoTable">
				<s:if test="%{hasActionErrors()}">
					<tr>
						<td class="mensajeError" colspan="3"><s:actionerror theme="simple"/></td>
					</tr>
				</s:if>
				<s:elseif test="%{hasActionMessages()}">
					<tr>
						<td class="actionMessage" colspan="3"><s:actionmessage theme="simple"/></td>
					</tr>
				</s:elseif>
				<s:if test="%{hasFieldErrors()}">
					<s:iterator value="%{fieldErrors.get('grupo.nombre')}">
						<tr class="grupoTableTr">
							<td class="grupoTableTd"  align="center" colspan="2"><span class="errorMessage"><s:property /></span></td>
						</tr>
					</s:iterator>
				</s:if>
				<tr class="grupoTableTr">
					<td class="grupoTableTd">Nombre del grupo:</td>
					<td class="grupoTableTd"><s:textfield name="grupo.nombre" size="30" /></td>
				</tr>
				<tr class="blanco">
						<td colspan="2"></td>
				</tr>
				<tr class="grupoTableTr" height="35" valign="middle">
					<td class="grupoTableTd" colspan="2" align="center"> 
						<s:submit type="button" name="accion" value="Aceptar" />
						<s:submit type="button" name="accion" value="Volver" />
					</td>
				</tr>
			</table>
		</s:form>
		
	</div>
</div>
</body>
</html>
