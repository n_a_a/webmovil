<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<title>WebMovil - Panel de Administraci&oacute;n - Lista de usuarios</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<h2>Lista de usuarios del emisor <s:property value="idEmisor" /></h2>
		<s:if test="mensajeEstado != null" >
			<table>
				<s:iterator value="mensajeEstado"> 
					<tr class="titulin">
						<td colspan = "3">
							<span class="errorMessage">
								<s:property />
							</span>
						</td>
					</tr>
				</s:iterator>
			</table>
		</s:if>
		<s:form action="actionAdministracionListaUsuarios" method="post" theme="simple">
			<!-- Vemos si hay usuarios en este emisor que mostrar -->
			<s:if test="numUsuariosEmisor == 0">
				<!-- Mensaje de que no hay usuarios -->
				<table class="generalBox"><tr><td>Este emisor no tienen ning&uacute;n usuario.</td></tr></table>
			</s:if>
			<s:else>
				<!-- Muestra la página actual de la lista de usuarios -->
				<table class="generalBox">
					<tr><td>El emisor <s:property value="idEmisor" /> tiene <s:property value="numUsuariosEmisor" /> usuarios.</td></tr>
				</table>
				<table id="tablaListaUsuarios">
					<tr>
						<th width="5%"></th>
						<th width="15%">Id</th>
						<th width="75%">Nombre usuario (uid)</th>
						<th width="5%"></th>
					</tr>
					<s:iterator value="listaUsuariosStore" status="offset">
						<!-- Color de las lineas de la tabla -->
						<s:if test="#offset.odd == true">
							<s:set name="trclass">impar</s:set>
						</s:if> 
						<s:else>
							<s:set name="trclass">par</s:set>
						</s:else>
						<tr class="${trclass}">
							<s:url action="actionAdministracionEditaUsuario" var="userLink">
								<s:param name="formUsuario"><s:property value="usuario" /></s:param>
								<s:param name="idUsuario"><s:property value="id" /></s:param>
								<s:param name="idEmisor"><s:property value="idEmisor" /></s:param>
								<s:param name="numPagina"><s:property value="numPagina" /></s:param>
							</s:url>
							<td class="numerico" width="5%"><s:checkbox name="usuariosSeleccionados" fieldValue="%{usuario}" theme="simple"></s:checkbox></td>
							<td class="numerico" width="15%">
								<a href="${userLink}"><s:property value="id" /></a>
							</td>
							<td class="texto" width="75%">
								<a href="${userLink}"><s:property value="usuario" /></a>
							</td>
							<td class="numerico" width="5%">
								<a href="${userLink}"><img src="images/icono-editar.png" /></a>
							</td>
						</tr>
					</s:iterator>
				</table>
			</s:else>
				<!-- Paginador -->
				<table class="generalBox">
					<tr>
						<td width="32">
							<s:if test="numPagina > 0">
								<s:url action="actionAdministracionListaUsuarios" var="primeraPaginaLink">
									<s:param name="accion">paginaPrimera</s:param>
									<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
									<s:param name="numPagina"><s:property value="numPagina"/></s:param>
								</s:url>
								<a href="${primeraPaginaLink}"><img src="images/icono-flecha-0.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-0-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina > 0">
								<s:url action="actionAdministracionListaUsuarios" var="paginaAnteriorLink">
									<s:param name="accion">paginaAnterior</s:param>
									<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
									<s:param name="numPagina"><s:property value="numPagina"/></s:param>
								</s:url>
								<a href="${paginaAnteriorLink}"><img src="images/icono-flecha-1.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-1-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina < maxPagina">
								<s:url action="actionAdministracionListaUsuarios" var="paginaSiguienteLink">
									<s:param name="accion">paginaSiguiente</s:param>
									<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
									<s:param name="numPagina"><s:property value="numPagina"/></s:param>
								</s:url>
								<a href="${paginaSiguienteLink}"><img src="images/icono-flecha-2.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-2-g.png" />
							</s:else>
						</td>
						<td width="32">
							<s:if test="numPagina < maxPagina">
								<s:url action="actionAdministracionListaUsuarios" var="ultimaPaginaLink">
									<s:param name="accion">paginaUltima</s:param>
									<s:param name="idEmisor"><s:property value="idEmisor"/></s:param>
									<s:param name="numPagina"><s:property value="numPagina"/></s:param>
								</s:url>
								<a href="${ultimaPaginaLink}"><img src="images/icono-flecha-3.png" /></a>
							</s:if>
							<s:else>
								<img src="images/icono-flecha-3-g.png" />
							</s:else>
						</td>
						<!-- Página actual -->
						<td>
							P&aacute;gina <s:property value="numPagina + 1" /> de <s:property value="maxPagina + 1" />
						</td>
						<!-- botones de borrar/crear usuarios -->
						<td align="right">
							<s:hidden name="idAlfanumerico" value="NUEVO" />
							<s:submit type="button" name="accion" value="Nuevo Usuario" theme="simple" action="actionAdministracionEditaUsuario" />
							<s:submit type="button" name="accion" value="Borrar seleccionados" theme="simple" />
						</td>
					</tr>
				</table>
				<!-- Propagamos los valores actuales de la vista -->
				<s:hidden name="idEmisor" value="%{idEmisor}" />
				<s:hidden name="numPagina" value="%{numPagina}" />
				<s:hidden name="activity" value="%{activity}" />
			
			
			<s:if test="isVieneDeListaEmisores()" >
				<br />
				<table class="generalBox">
					<s:url action="actionAdministracionListaEmisores" var="listaEmisoresLink">
						<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
						<s:param name="activity">GestionEmisores</s:param>
					</s:url>
					<tr><td><a href="${listaEmisoresLink}">Volver a la lista de emisores</a></td></tr>
				</table>
			</s:if>
		</s:form>
	</div>
</div>
</body>
</html>