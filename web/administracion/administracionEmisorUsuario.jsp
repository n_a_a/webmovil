<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Encontrar el emisor de un usuario</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<s:form action="actionAdministracionEmisorUsuario" method="post" theme="simple">
			<table class="generalBox">
				<tr>
					<td>
						Introduzca un nombre de usuario:&nbsp;
						<s:textfield name="formUsuario" size="40" theme="simple" />&nbsp;
						<s:submit type="button" name="accion" value="Encontrar" theme="simple" />
					</td>
				</tr>
				<s:if test="textoError != null && !textoError.equals('')">
					<tr>
						<td>
							<font color="red"><strong><s:property value="textoError" /></strong></font>
						</td>
					</tr>
				</s:if>
				<s:if test="textoResultado != null && !textoResultado.equals('')">
					<tr>
						<td>
							Emisor: <strong><s:property value="textoResultado" /></strong>
						</td>
					</tr>
				</s:if>
			</table>
		</s:form>
	</div>
</div>
</body>
</html>