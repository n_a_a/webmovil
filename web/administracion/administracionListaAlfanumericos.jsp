<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<title>WebMovil - Panel de Administraci&oacute;n - Lista de alfanuméricos</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	<div id="bodyContainer">
		<h2>Listado de alfanuméricos</h2>
		
		<s:form action="actionAdministracionListaAlfanumericos" method="post" theme="simple">
			<s:if test="%{hasFieldErrors ()}" >
				<tr class="titulin">
					<td colspan = "6">
						<s:iterator value="%{fieldErrors.get('listaAlfanumericos.general')}" >
							<span class="errorMessage">
								<s:property />
							</span>
						</s:iterator>
					</td>
				</tr>
			</s:if>
			<table id="tablaListaUsuarios">
				<tr>
					<th width="20%">Id Emisor</th>
					<th width="50%">Descripción</th>
					<th width="20%">Alfanumérico</th>
					<th width="10%"></th>					
				</tr>
				<s:iterator value="listaAlfanumericosStore" status="offset">
					<!-- Color de las lineas de la tabla -->
					<s:if test="#offset.odd == true">
						<s:set name="trclass">impar</s:set>
					</s:if> 
					<s:else>
						<s:set name="trclass">par</s:set>
					</s:else>
					<tr class="${trclass}">
						
						<td><s:property value="idEmisor" /></td>
						<td><s:property value="descripcion" /></td>
						<td><s:property value="alfa" /></td>						
						<td>
							<s:url action="actionAdministracionEditaAlfanumerico" var="alfanumericoLink">
								<s:param name="idEmisor"><s:property value="idEmisor" /></s:param>
							</s:url>
							<s:url action="actionAdministracionListaAlfanumericos" var="eliminaAlfanumericoLink">
								<s:param name="idEmisor"><s:property value="idEmisor" /></s:param>
								<s:param name="accion">eliminar</s:param>
							</s:url>
							<s:if test="%{#session.objetoUsuario.esSuperAdministrador()}" >
								<a href="${alfanumericoLink}">Editar</a>&nbsp;|&nbsp;<a href="${eliminaAlfanumericoLink}">Eliminar</a>
							</s:if>
						</td>
					</tr>
				</s:iterator>
			</table>
			<table class="generalBox">
				<tr>
					<td align="right">
						<s:hidden name="idEmisor" value="NUEVO" />
						<s:submit type="button" name="accion" value="Crear nuevo" theme="simple" action="actionAdministracionEditaAlfanumerico" />
					</td>
				</tr>
			</table>
		</s:form>
	</div>
</div>
</body>
</html>