<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Resumen de facturaci&oacute;n</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<table class="generalBox">
			<tr><td><h3>Datos globales</h3></td></tr>
			<tr class="impar">
				<s:form action="actionAdministracionFacturacionResultados" method="post" theme="simple">
				<td class="numerico">
						<strong>Datos mensuales:&nbsp;</strong><s:select name="factMonth" list="#{'01':'Enero', '02':'Febrero', '03':'Marzo', '04':'Abril', '05':'Mayo', '06':'Junio', '07':'Julio', '08':'Agosto', '09':'Septiembre', '10':'Octubre', '11':'Noviembre', '12':'Diciembre'}" theme="simple" /> de <s:textfield name="factYear" maxlength="4" size="4" theme="simple" /> &nbsp; <s:submit type="button" name="accion" value="Enviar" theme="simple" />
				</td>
				</s:form>
				<td>
					<s:url action="actionAdministracionFacturacionResultados" var="ultimoTrimestreLink">
						<s:param name="factMonth">0</s:param>
						<s:param name="factYear">0</s:param>
					</s:url>
					<a href="${ultimoTrimestreLink}">Datos del último trimestre</a>
				</td>
			</tr>
		</table>
		<br></br>
		<table class="generalBox">
			<tr><td><h3>Datos SADESI</h3></td></tr>
			<tr class="impar">
				<td>
					<s:url action="actionAdministracionFacturacionDatosSadesi" var="datosSadesiLink">
					</s:url>
					<a href="${datosSadesiLink}">Obtener estadísticas sobre los usuarios de SADESI</a>
				</td>
			</tr>
		</table>
		<br />
		<table class="generalBox">
			<tr>
				<td>
					<em>El cálculo de los datos puede tardar un momento. Por favor, haga clic una sóla vez sobre su elección y espere a que se muestren por pantalla.</em>
				</td>
			</tr>
		</table>
		<br />
	</div>
</div>
</body>
</html>