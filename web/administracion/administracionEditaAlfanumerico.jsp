<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<title>WebMovil - Panel de Administraci&oacute;n - Editar/A&ntilde;adir alfanum&eacute;rico</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		
		<!-- Información / Titulín -->
		
		<table class="generalBox">
			<tr>
				<td>
					<s:if test="idEmisor.equals ('NUEVO')">
					Nuevo alfanum&eacute;rico.
					</s:if>
					<s:else>
					Editando alfanum&eacute;rico para <strong><s:property value="alfanumerico.idEmisor" /></strong>
					</s:else>
				</td>
			</tr>
		</table>
		<br />
		
		<!-- El formulario -->
		<table id="tablaFormulario">
			<s:form action="actionAdministracionEditaAlfanumerico" method="post" theme="simple">
				<s:if test="%{hasFieldErrors ()}" >
					<tr class="titulin">
						<td colspan = "4">
							<s:iterator value="%{fieldErrors.get('editaAlfanumerico.general')}" >
								<span class="errorMessage">
									<s:property />
								</span>
							</s:iterator>
						</td>
					</tr>
				</s:if>
							
				<tr class="normal">
					<td class="izquierda" width="10%">idEmisor:</td>
					<td class="izquierda" width="40%">
						<s:textfield name="alfanumerico.idEmisor" maxlength="20" size="20" theme="simple" />
					</td>
					<td class="izquierda" width="10%">Alfanumérico:</td>
					<td class="izquierda" width="40%">
						<s:textfield name="alfanumerico.alfa" maxlength="50" size="50" theme="simple" />
					</td>					
				</tr>
				<tr class="normal">
					<td colspan="4" class="derecha">
						<s:submit type="button" name="accion" value="Enviar" theme="simple" />
					</td>
				</tr>
			</s:form>
		</table>
		<table class="generalBox">
			<tr>
				<td>
					<s:url action="actionAdministracionListaAlfanumericos" var="backLink">
					</s:url>
					<a href="${backLink}">Volver sin hacer nada.</a>
				</td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>