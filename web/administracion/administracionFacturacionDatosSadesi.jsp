<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Resumen de facturaci&oacute;n - Datos SADESI</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<!-- Selección de fecha -->
		<s:form action="actionAdministracionFacturacionDatosSadesi" method="post" theme="simple">
		<table class="generalBox">
			<tr>
				<td>
					Desde:&nbsp;
					<s:textfield name="diaIni" maxlength="2" size="2" theme="simple" />
					<s:select name="mesIni" list="#{'01':'Enero', '02':'Febrero', '03':'Marzo', '04':'Abril', '05':'Mayo', '06':'Junio', '07':'Julio', '08':'Agosto', '09':'Septiembre', '10':'Octubre', '11':'Noviembre', '12':'Diciembre'}" theme="simple" />
					<s:textfield name="anyoIni" maxlength="4" size="4" theme="simple" />
				</td>
				<td>
					Hasta:&nbsp;
					<s:textfield name="diaFin" maxlength="2" size="2" theme="simple" />
					<s:select name="mesFin" list="#{'01':'Enero', '02':'Febrero', '03':'Marzo', '04':'Abril', '05':'Mayo', '06':'Junio', '07':'Julio', '08':'Agosto', '09':'Septiembre', '10':'Octubre', '11':'Noviembre', '12':'Diciembre'}" theme="simple" />
					<s:textfield name="anyoFin" maxlength="4" size="4" theme="simple" />
				</td>
				<td>
					<s:submit type="button" name="accion" value="Mostrar" theme="simple" />
				</td>
			</tr>
			<!-- Errores de formulario -->
			<tr class="titulin">
				<td colspan = "3">
					<s:if test="%{hasFieldErrors ()}" >
						<s:iterator value="%{fieldErrors.get('datosSadesi.date')}" >
							<span class="errorMessage">
								<s:property /><br />
							</span>
						</s:iterator>
					</s:if>
					&nbsp;
				</td>
			</tr>
		</table>
		</s:form>
		<!-- Resultados -->
		<table id="tablaFacturacion">
			<tr>
				<th>Nombre de usuario</th>
				<th>Corporativos</th>
				<th>No corporativos</th>
			</tr>
			<s:iterator value="usuariosStore" status="offset">
				<!-- Color de las lineas de la tabla -->
				<s:if test="#offset.odd == true">
					<s:set name="trclass">impar</s:set>
				</s:if> 
				<s:else>
					<s:set name="trclass">par</s:set>
				</s:else>
				<!--  Lista -->
				<tr class="${trclass}">
					<td class="texto"><s:property value="usuario" /></td>
					<td class="numerico"><s:property value="numEnviadosCorporativos" /></td>
					<td class="numerico"><s:property value="numEnviados" /></td>
				</tr>
			</s:iterator>
		</table>
	</div>
</div>
</body>
</html>