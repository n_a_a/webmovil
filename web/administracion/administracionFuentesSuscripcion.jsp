<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Avisos Junta - Fuentes de Suscripción</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/notificaciones.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/jquery-ui.css?v=<s:property value="version" />" TYPE="text/css">
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery-ui.min.js"></script>
<script>
	function updateFormFields () {
		var tipo = $('#formTipo').find(":selected").text();
		if (tipo == 'Completa') {
			$('#mainFormUrl2Row').css('visibility', 'visible');
			$('#mainFormUrl1Title').html('Url Alta');
		} else {
			$('#mainFormUrl2Row').css('visibility', 'hidden');
			$('#mainFormUrl1Title').html('Url');
		}
	}
	
	function cleanFormFields () {
		$('#formId').val ('');
		$('#formDesc').val ($(this).data(''));
		$('#formUrlAlta').val ($(this).data(''));
		$('#formUrlBaja').val ($(this).data(''));
		$('#formTipo').val ($(this).data(''));
		$('#formAction').val ('Crear');
	}

	$(document).ready(function() {
		//cleanFormFields ();
				
		$('#nuevaFuente').click (function () {
			cleanFormFields ();
			$('#mainFormTitle').html('Nueva');
			updateFormFields ();
			$('#mainForm').css('visibility', 'visible');
		});
		
		$('#formTipo').change (function () {
			updateFormFields ();
		});
		
		$('.editIcon').click (function () {
			var id = $(this).attr('id').replace('edit_', '');
			$('#formId').val (id);
			$('#formDesc').val ($(this).data('desc'));
			$('#formUrlAlta').val ($(this).data('url1'));
			$('#formUrlBaja').val ($(this).data('url2'));
			$('#formTipo').val ($(this).data('tipo'));
			$('#mainFormTitle').html('Editar');			
			updateFormFields ();
			$('#formAction').val ('Editar');
			$('#mainForm').css('visibility', 'visible');
		});
	});

</script>
</head>
<body>
	<div id="mainContainer">
		<s:include value="/includes/cabecera.jsp" />
		<div id="bodyContainer">
			<table class="generalBox">
				<tr>
					<td><h2>Fuentes de Suscripción para <s:property value="idEmisor" /></h2></td>
				</tr>
			</table>
			<br />

			<!-- Show last error (if present) -->
			<s:if test="lastError != null && !''.equals(lastError)">
				<table class="tablaFormulario">
					<tr class="titulin">
						<td><span style="color: red;"> <s:property
									value="lastError" />
						</span></td>
					</tr>
				</table>
			</s:if>

			<!-- Primero presentamos una lista de las fuentes disponibles para este idEmisor -->
			<table class="tablaFormulario">
				<s:if test="numFuentes > 0">
					<tr>
						<th>Descripción</th>
						<th>Tipo</th>
						<th></th>
					</tr>
					<s:iterator value="listaFuentes" status="offset">
						<s:if test="#offset.odd == true">
							<s:set name="trclass">impar</s:set>
							<s:set name="trclass2">par</s:set>
						</s:if>
						<s:else>
							<s:set name="trclass">par</s:set>
							<s:set name="trclass2">impar</s:set>
						</s:else>
						<tr class="${trclass}">
							<td><s:property value="desc" /></td>
							<td><s:property value="tipo" /></td>
							<td><img src="images/icono-editar.png" class="editIcon"
								id="edit_<s:property value="id" />"
								data-desc="<s:property value="desc" />"
								data-tipo="<s:property value="tipo" />"
								data-url1="<s:property value="urlAlta" />"
								data-url2="<s:property value="urlBaja" />" />
								<s:url action="actionAdministracionFuentesSuscripcion" var="delLink">
									<s:param name="formAction">Eliminar</s:param>
									<s:param name="formId"><s:property value="id" /></s:param>
								</s:url> 
								<a href="${delLink}"><img
									src="images/icono-borrar.png" class="deleteIcon"
									id="delete_<s:property value="id" />" /></a></td>
						</tr>
					</s:iterator>
				</s:if>
				<s:else>
					<tr>
						<td>Su emisor no tiene definida ninguna fuente de
							suscripción.</td>
					</tr>
				</s:else>
				<tr>
					<td colspan="3" class="derecha"><input type="submit"
						value="Nueva..." id="nuevaFuente" /></td>
				</tr>
			</table>

			<div id="mainForm" style="visibility: hidden;">
				<s:form action="actionAdministracionFuentesSuscripcion"
					method="post" theme="simple">
					<input type="hidden" name="formId" value="" id="formId" />

					<table class="tablaFormulario">
						<tr class="titulin">
							<td colspan="2"><h3><span id="mainFormTitle">Nueva</span> fuente
								de suscripción</h3></td>
						</tr>
						<tr>
							<td width="20%" class="derecha">Descripción:</td>
							<td width="80%" class="izquierda"><input type="text"
								size="100" maxlength="1000" name="formDesc" id="formDesc" /></td>
						</tr>
						<tr>
							<td class="derecha">Tipo:</td>
							<td class="izquierda"><s:select name="formTipo"
									list="#{'simple':'Simple', 'completa':'Completa'}"
									id="formTipo" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<span style="font-size:75%">Asegúrese de que las URL están completas (no olvide especificar http o https) e incluyen todos
								los parámetros necesarios para su funcionamiento.</span>
							</td>
						</tr>
						<tr>
							<td class="derecha"><span id="mainFormUrl1Title">Url</span>:
							</td>
							<td class="izquierda"><input type="text" size="100"
								maxlength="1000" name="formUrlAlta" id="formUrlAlta" /></td>
						</tr>
						<tr style="visibility: hidden;" id="mainFormUrl2Row">
							<td class="derecha">Url baja:</td>
							<td class="izquierda"><input type="text" size="100"
								maxlength="1000" name="formUrlBaja" id="formUrlBaja" /></td>
						</tr>
						<tr>
							<td colspan="2" class="derecha"><input type="submit"
								name="formAction" value="Crear" id="formAction"></td>
						</tr>
					</table>
				</s:form>
			</div>
		</div>
	</div>
</body>