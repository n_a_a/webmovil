<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Editar usuario</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/preferencias.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		
		<!-- Información / Titulín -->
		
		<table class="generalBox">
			<tr>
				<td>
					<s:if test="idUsuario.equals ('NUEVO') || ''.equals(idUsuario)">
					Nuevo usuario del emisor <strong><s:property value="idEmisor" /></strong>
					</s:if>
					<s:else>
					Editando usuario <strong><s:property value="formUsuario" /></strong> [<s:property value="idUsuario" />] del emisor <strong><s:property value="idEmisor" /></strong>
					</s:else>
				</td>
			</tr>
		</table>
		<br />
		
		<!-- El formulario -->
		
		<s:form action="actionAdministracionEditaUsuario" method="post" theme="simple">
			<table id="tablaFormulario">
				<s:if test="!''.equals(mensajeEstado)" >
					<tr class="titulin">
						<td colspan = "3">
							<span class="errorMessage">
								<s:property value="mensajeEstado" />
							</span>
						</td>
					</tr>
				</s:if>
				<tr class="titulin">
					<td colspan="3" class="izquierda"><strong>General:</strong></td>
				</tr>
				<tr class="normal">
					<td class="izquierda">Usuario LDAP:</td>
					<td colspan="2" class="izquierda">
						<s:textfield name="formUsuario" maxlength="100" size="60" theme="simple" />
					</td>
				</tr>
				<tr class="normal">
					<td class="izquierda">Tel&eacute;fono:</td>
					<td colspan="2" class="izquierda">
						<s:textfield name="formTlfnoMovil" maxlength="20" size="20" theme="simple" />
					</td>
				</tr>
				<tr class="normal">
					<td class="izquierda">Nivel:</td>
					<td colspan="2" class="izquierda">
						<s:select name="formNivel" list="#{'N':'Normal', 'A':'Administrador', 'F':'Facturación'}" theme="simple" />
					</td>
				</tr>
				<tr class="titulin">
					<td colspan = "3">
						<s:if test="%{hasFieldErrors ()}" >
							<s:iterator value="%{fieldErrors.get('editaUsuario.general')}" >
								<span class="errorMessage">
									<s:property />
								</span>
							</s:iterator>
						</s:if>
						&nbsp;
					</td>
				</tr>
				<tr class="titulin">
					<td colspan="3" class="izquierda"><strong>L&iacute;mites:</strong></td>
				</tr>
				<tr class="normal">
					<td class="izquierda">Nacional</td>
					<td class="centro">Duro: <s:textfield name="formLimite" maxlength="7" size="6" theme="simple" /></td>
					<td class="centro">Blando: <s:textfield name="formLimiteBlando" maxlength="7" size="6" theme="simple" /></td>
				</tr>
				<tr class="normal">
					<td class="izquierda">Internacional</td>
					<td class="centro">Duro: <s:textfield name="formLimiteInternacional" maxlength="7" size="6" theme="simple" /></td>
					<td class="centro">Blando: <s:textfield name="formLimiteInternacionalBlando" maxlength="7" size="6" theme="simple" /></td>
				</tr>
				<tr class="titulin">
					<td colspan = "3">
						<s:if test="%{hasFieldErrors ()}" >
							<s:iterator value="%{fieldErrors.get('editaUsuario.limites')}" >
								<span class="errorMessage">
									<s:property />
								</span>
							</s:iterator>
						</s:if>
						&nbsp;
					</td>
				</tr>
				<tr class="titulin">
					<td colspan="3" class="izquierda"><strong>Permisos:</strong></td>
				</tr>
				<tr class="normal">
					<td colspan="3" class="izquierda">
						<s:checkbox name="formPerPrioridad" theme="simple" /> Puede asignar prioridad alta a sus mensajes.
					</td>
				</tr>
				<tr class="normal">
					<td colspan="3" class="izquierda">
						<s:checkbox name="formPerRecibo" theme="simple" /> Puede solicitar acuse de recibo.
					</td>
				</tr>
				<tr class="normal">
					<td colspan="3" class="izquierda">
						<s:checkbox name="formPerSolocorp" theme="simple" /> Sólo puede enviar a móviles corporativos.
					</td>
				</tr>
				<tr class="normal">
					<td colspan="3" class="izquierda">
						<s:checkbox name="formPerFirma" theme="simple" /> Puede eliminar la firma del emisor.
					</td>
				</tr>
				<tr class="normal">
					<td colspan="3" class="izquierda">
						<s:checkbox name="formPerRecepcion" theme="simple" /> Puede recibir mensajes en su buzón.
					</td>
				</tr>
				<tr class="normal">
					<td colspan="3" class="izquierda">
						<s:checkbox name="formPerMasivo" theme="simple" /> Puede programar envíos masivos.
					</td>
				</tr>
				<tr class="titulin"><td>&nbsp;</td></tr>
				<tr class="normal">
					<td colspan="3" class="izquierda">
						<s:submit type="button" name="accion" value="Enviar" theme="simple" />
						<s:submit type="button" name="accion" value="Resetear" theme="simple" />
					</td>
				</tr>
			</table>
			
			<!-- Refrescamos las paranoias que no están en el formulario -->
			<s:hidden name="idUsuario" value="%{idUsuario}" />
			<s:hidden name="idEmisor" value="%{idEmisor}" />
			<s:hidden name="numPagina" value="%{numPagina}" />
		</s:form>
		
		<!-- Navegación -->
		
		<table class="generalBox">
			<tr>
				<td>
					<s:url action="actionAdministracionListaUsuarios" var="backLink">
						<s:param name="numPagina"><s:property value="numPagina" /></s:param>
					</s:url>
					<a href="${backLink}">Volver a la lista de usuarios</a>
				</td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>