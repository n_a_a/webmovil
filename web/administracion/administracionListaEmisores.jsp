<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Panel de Administraci&oacute;n - Lista de emisores</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<h2>Lista de Emisores</h2>
		<s:if test="mensajeEstado != null" >
			<table>
				<s:iterator value="mensajeEstado"> 
					<tr class="titulin">
						<td colspan = "3">
							<span class="errorMessage">
								<s:property />
							</span>
						</td>
					</tr>
				</s:iterator>
			</table>
		</s:if>
		<s:form action="actionAdministracionListaEmisores" method="post" theme="simple">
			<table class="generalBox">
				<tr><td><s:property value="numEmisores" /> emisores en total.</td></tr>
			</table>
			<table id="tablaListaUsuarios">
				<tr>
					<th width="5%"></th>
					<th width="10%">Id</th>
					<th width="75%">Descripción</th>
					<th width="10%"></th>
				</tr>
				<s:iterator value="listaEmisoresStore" status="offset">
					<!-- Color de las lineas de la tabla -->
					<s:if test="#offset.odd == true">
						<s:set name="trclass">impar</s:set>
					</s:if> 
					<s:else>
						<s:set name="trclass">par</s:set>
					</s:else>
					<tr class="${trclass}">
						<s:if test="activity == 'GestionEmisores'" >
							<s:url action="actionAdministracionEditaEmisor" var="editLink">
								<s:param name="idEmisor"><s:property value="emisor" /></s:param>
								<s:param name="whatToDo">EDIT</s:param>
								<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
							</s:url>
							<s:url action="actionAdministracionListaUsuarios" var="usersLink">
								<s:param name="idEmisor"><s:property value="emisor" /></s:param>
								<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
								<s:param name="numPagina">0</s:param>
								<s:param name="vieneDeListaEmisores">true</s:param>
							</s:url>
							<td class="numerico" width="5%"><s:checkbox name="emisoresSeleccionados" fieldValue="%{emisor}" theme="simple"></s:checkbox></td>
							<td class="numerico" width="10%">
								<a href="${editLink}"><s:property value="emisor" /></a>
							</td>
							<td class="texto" width="75%">
								<a href="${usersLink}"><s:property value="descripcion" /></a>
							</td>
							<td class="numerico" width="10%">
								<a href="${editLink}"><img src="images/icono-editar.png" /></a>
								<a href="${usersLink}"><img src="images/icono-usuarios.png" /></a>
							</td>
						</s:if>
						<s:elseif test="activity == 'Estadisticas'" >
							<s:url action="actionAdministracionEstadisticasEmisor" var="estadisticasLink">
								<s:param name="idEmisor"><s:property value="emisor" /></s:param>
								<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
							</s:url>
							
							<td class="numerico" width="5%">&nbsp;</td>
							<td class="numerico" width="10%">
								<a href="${estadisticasLink}"><s:property value="emisor" /></a>
							</td>
							<td class="texto" width="75%">
								<a href="${estadisticasLink}"><s:property value="descripcion" /></a>
							</td>
							<td class="numerico" width="10%">
								<a href="${estadisticasLink}"><img src="images/icono-estadisticas.png" /></a>
							</td>
						</s:elseif>
						<s:else>
							
						</s:else>
					</tr>
				</s:iterator>
			</table>
				
			<!-- Paginador -->
			<table class="generalBox">
				<tr>
					<td width="32">
						<s:if test="numPaginaEmisores > 0">
							<s:url action="actionAdministracionListaEmisores" var="primeraPaginaLink">
								<s:param name="accion">paginaPrimera</s:param>
								<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
								<s:param name="activity"><s:property value="activity" /></s:param>
							</s:url>
							<a href="${primeraPaginaLink}"><img src="images/icono-flecha-0.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-0-g.png" />
						</s:else>
					</td>
					<td width="32">
						<s:if test="numPaginaEmisores > 0">
							<s:url action="actionAdministracionListaEmisores" var="paginaAnteriorLink">
								<s:param name="accion">paginaAnterior</s:param>
								<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
								<s:param name="activity"><s:property value="activity" /></s:param>
							</s:url>
							<a href="${paginaAnteriorLink}"><img src="images/icono-flecha-1.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-1-g.png" />
						</s:else>
					</td>
					<td width="32">
						<s:if test="numPaginaEmisores < maxPagina">
							<s:url action="actionAdministracionListaEmisores" var="paginaSiguienteLink">
								<s:param name="accion">paginaSiguiente</s:param>
								<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
								<s:param name="activity"><s:property value="activity" /></s:param>
							</s:url>
							<a href="${paginaSiguienteLink}"><img src="images/icono-flecha-2.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-2-g.png" />
						</s:else>
					</td>
					<td width="32">
						<s:if test="numPaginaEmisores < maxPagina">
							<s:url action="actionAdministracionListaEmisores" var="ultimaPaginaLink">
								<s:param name="accion">paginaUltima</s:param>
								<s:param name="numPaginaEmisores"><s:property value="numPaginaEmisores" /></s:param>
								<s:param name="activity"><s:property value="activity" /></s:param>
							</s:url>
							<a href="${ultimaPaginaLink}"><img src="images/icono-flecha-3.png" /></a>
						</s:if>
						<s:else>
							<img src="images/icono-flecha-3-g.png" />
						</s:else>
					</td>
					<!-- Página actual -->
					<td>
						P&aacute;gina <s:property value="numPaginaEmisores + 1" /> de <s:property value="maxPagina + 1" />
					</td>
					<!-- botones de borrar/crear emisores -->
					<td align="right">
						<s:if test="activity == 'GestionEmisores'" >
							<s:hidden name="whatToDo" value="NUEVO" />
							<s:submit type="button" name="accion" value="Nuevo Emisor" theme="simple" action="actionAdministracionEditaEmisor" />
							<s:submit type="button" name="accion" value="Borrar seleccionados" theme="simple" />
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
					</td>
				</tr>
			</table>
			<!-- Propagamos los valores actuales de la vista -->
			<s:hidden name="idEmisor" value="%{idEmisor}" />
			<s:hidden name="numPagina" value="%{numPagina}" />
			<s:hidden name="activity" value="%{activity}" />
		</s:form>
	</div>
</div>
</body>
</html>