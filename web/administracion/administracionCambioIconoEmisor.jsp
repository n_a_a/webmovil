<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Cambiar firma del Emisor <s:property value="#session.objetoUsuario.emisor.emisor" /></title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/preferencias.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<s:form action="actionAdministracionCambioIconoEmisor" method="post" theme="simple" enctype="multipart/form-data">
			<table id="preferenciasTable">
				<s:if test="!''.equals(mensajeEstado)" >
					<tr class="titulin">
						<td colspan = "3">
							<span class="errorMessage">
								<s:property value="mensajeEstado" />
							</span>
						</td>
					</tr>
				</s:if>
				<tr class="preferenciasTableTr">
					<td class = "preferenciasTableTd" width="20%">Icono personalizado (256x256 pixels, png):</td>
					<td class = "preferenciasTableTd"><s:file name="formIcon" size="20" label="Examinar" /></td>
					<td width="20%"><img src="<s:property value="uriIcon" />" width="64" height="64" /></td>
				</tr>
				<tr class="preferenciasTableTr">
					<td class = "preferenciasTableTd" colspan = "3" align = "center"><s:submit type="button" name="accion" value="Aceptar" theme="simple" /></td>
				</tr>
			</table>
		</s:form>
		<table class="generalBox">
			<tr>
				<td>
					<a href="<s:url action='actionPanelAdministracion'/>">Volver al panel de administraci&oacute;n</a>
				</td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>