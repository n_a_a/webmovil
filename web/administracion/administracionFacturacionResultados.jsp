<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Resumen de facturaci&oacute;n - Resultados</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<h2><s:property value="showPeriod" /></h2>
		<table id="tablaFacturacion">
			<s:if test="tipoVista == 'trimestral'">
				<tr>
					<th width = "45%" rowspan = "2">Emisor</th>
					<th width = "7%" rowspan = "2">Cod. Nav.</th>
					<th width = "1%" rowspan = "2"></th>
					<th width = "15%" colspan = "3"><s:property value="trimestreMes1" /></th>
					<th width = "1%" rowspan = "2"></th>
					<th width = "15%" colspan = "3"><s:property value="trimestreMes2" /></th>
					<th width = "1%" rowspan = "2"></th>
					<th width = "15%" colspan = "3"><s:property value="trimestreMes3" /></th>
				</tr>
				<tr>
					<th>C</th><th>NC</th><th>I</th>
					<th>C</th><th>NC</th><th>I</th>
					<th>C</th><th>NC</th><th>I</th>
				</tr>
				<s:iterator value="facturacionStore" status="offset">
					<!-- Color de las lineas de la tabla -->
					<s:if test="#offset.odd == true">
						<s:set name="trclass">impar</s:set>
					</s:if> 
					<s:else>
						<s:set name="trclass">par</s:set>
					</s:else>
					<!--  Lista -->
					<tr class="${trclass}">
						<td class="texto"><s:property value="idEmisor" /></td>
						<td class="texto"><s:property value="navision" /></td>
						<td> </td>
						<td class="numerico"><s:property value="enviadosCorporativos1" /></td>
						<td class="numerico"><s:property value="enviadosNoCorporativos1" /></td>
						<td class="numerico"><s:property value="enviadosInternacionales1" /></td>
						<td> </td>
						<td class="numerico"><s:property value="enviadosCorporativos2" /></td>
						<td class="numerico"><s:property value="enviadosNoCorporativos2" /></td>
						<td class="numerico"><s:property value="enviadosInternacionales2" /></td>
						<td> </td>
						<td class="numerico"><s:property value="enviadosCorporativos3" /></td>
						<td class="numerico"><s:property value="enviadosNoCorporativos3" /></td>
						<td class="numerico"><s:property value="enviadosInternacionales3" /></td>
					</tr>
				</s:iterator>
			</s:if>
			<s:else>
				<tr>
					<th width = "63%">Emisor</th>
					<th width = "7%">Cod. Nav.</th>
					<th width = "10%">Corp.</th>
					<th width = "10%">No Corp.</th>
					<th width = "10%">Intern.</th>
				</tr>
				<s:iterator value="facturacionStore" status="offset">
					<!-- Color de las lineas de la tabla -->
					<s:if test="#offset.odd == true">
						<s:set name="trclass">impar</s:set>
					</s:if> 
					<s:else>
						<s:set name="trclass">par</s:set>
					</s:else>
					<!--  Lista -->
					<tr class="${trclass}">
						<td class="texto"><s:property value="idEmisor" /></td>
						<td class="texto"><s:property value="navision" /></td>
						<td class="numerico"><s:property value="enviadosCorporativos1" /></td>
						<td class="numerico"><s:property value="enviadosNoCorporativos1" /></td>
						<td class="numerico"><s:property value="enviadosInternacionales1" /></td>
					</tr>
				</s:iterator>
			</s:else>
		</table>
		<s:url action="actionAdministracionFacturacionExcel" var="excelLink">
			<s:param name="factMonth"><s:property value="factMonth" /></s:param>
			<s:param name="factYear"><s:property value="factYear" /></s:param>
		</s:url>
		<a href="${excelLink}">Obtener resultados en hoja Excel</a>
	</div>
</div>
</body>
</html>