<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Editar texto alternativo de Avisos Junta</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/preferencias.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

	<div id="mainContainer">

		<s:include value="/includes/cabecera.jsp" />

		<div id="bodyContainer">
			<table class="generalBox">
				<tr>
					<td><h2>Texto alternativo para Avisos Junta</h2></td>
				</tr>
			</table>
			<br />
			<s:form action="actionAdministracionEditaAltText" method="post"
				theme="simple" enctype="multipart/form-data">
				<table id="tablaFormulario">
					<s:if test="!''.equals(mensajeEstado)">
						<tr class="titulin">
							<td colspan="6"><span class="errorMessage"> <s:property
										value="mensajeEstado" />
							</span></td>
						</tr>
					</s:if>
					<s:if test="%{hasFieldErrors ()}">
						<tr class="titulin">
							<td colspan="6"><s:iterator
									value="%{fieldErrors.get('editaEmisor.general')}">
									<span class="errorMessage"> <s:property />
									</span>
								</s:iterator></td>
						</tr>
					</s:if>
					<tr class="normal">
						<td class="izquierda" colspan="6">
							<p style="margin: 1em 5em;">Cuando se envía una notificación
								multimedia a un usuario que no es usuario de Avisos Junta, se le
								notifica con un SMS en caso de que no est&eacute; desactivado el env&iacute;o y cuyo texto puede personalizarse. Al texto
								introducido aquí se añadir&aacute; la URL con la descarga de la app.</p>
						</td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Desactivar env&iacute;o SMS:</td>
						<td class="izquierda" colspan="5"><s:checkbox
								name="formDisableInvitationSMS" theme="simple" /></td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Activar texto personalizado:</td>
						<td class="izquierda" colspan="5"><s:checkbox
								name="formAltTextEnable" theme="simple" /></td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Texto:</td>
						<td class="izquierda" colspan="5"><s:textfield
								name="formAltText" maxlength="135" size="135" theme="simple" />
						</td>
					</tr>
					<tr class="titulin">
						<td>&nbsp;</td>
					</tr>
					<tr class="normal">
						<td colspan="6" class="izquierda"><s:submit type="button"
								name="accion" value="Enviar" theme="simple" /> <s:submit
								type="button" name="accion" value="Resetear" theme="simple" />
						</td>
					</tr>
				</table>
			</s:form>
		</div>
	</div>
</body>
</html>
