<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Editar emisor</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/preferencias.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

	<div id="mainContainer">

		<s:include value="/includes/cabecera.jsp" />

		<div id="bodyContainer">

			<!-- Información / Titulín -->

			<table class="generalBox">
				<tr>
					<td><s:if test="'NUEVO'.equals(whatToDo)">
					Nuevo emisor
					</s:if> <s:else>
					Editando emisor <strong><s:property value="idEmisor" /></strong>
						</s:else></td>
				</tr>
			</table>
			<br />

			<!-- El formulario -->

			<s:form action="actionAdministracionEditaEmisor" method="post"
				theme="simple" enctype="multipart/form-data">
				<table id="tablaFormulario">
					<s:if test="!''.equals(mensajeEstado)">
						<tr class="titulin">
							<td colspan="6"><span class="errorMessage"> <s:property
										value="mensajeEstado" />
							</span></td>
						</tr>
					</s:if>
					<s:if test="%{hasFieldErrors ()}">
						<tr class="titulin">
							<td colspan="6"><s:iterator
									value="%{fieldErrors.get('editaEmisor.general')}">
									<span class="errorMessage"> <s:property />
									</span>
								</s:iterator></td>
						</tr>
					</s:if>

					<tr class="titulin">
						<td colspan="6" class="izquierda"><strong>General:</strong></td>
					</tr>
					<tr class="normal">
						<td class="izquierda" width="10%">Id:</td>
						<td class="izquierda" width="22%"><s:if
								test="'NUEVO'.equals(whatToDo)">
								<s:textfield name="idEmisor" maxlength="20" size="20"
									theme="simple" />
							</s:if> <s:else>
								<s:property value="idEmisor" />
								<s:hidden name="idEmisor" value="%{idEmisor}" />
							</s:else></td>
						<td class="izquierda" width="10%">Clave:</td>
						<td class="izquierda" width="25%"><s:textfield
								name="formClave" maxlength="20" size="20" theme="simple" /></td>
						<td class="izquierda" width="10%">Navision:</td>
						<td class="izquierda" width="23%"><s:textfield
								name="formNavision" maxlength="20" size="20" theme="simple" />
						</td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Descripci&oacute;n:</td>
						<td class="izquierda" colspan="5"><s:textfield
								name="formDescripcion" maxlength="100" size="80" theme="simple" />
						</td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Firma:</td>
						<td class="izquierda" colspan="5"><s:textfield
								name="formFirma" maxlength="100" size="80" theme="simple" /></td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Email administrador:</td>
						<td class="izquierda" colspan="5"><s:textfield
								name="formEmail" maxlength="100" size="80" theme="simple" /></td>
					</tr>
					<tr class="titulin">
						<td>&nbsp;</td>
					</tr>
					<tr class="titulin">
						<td colspan="6" class="izquierda"><strong>T&eacute;cnico:</strong></td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Tlfn:</td>
						<td class="izquierda"><s:textfield name="formTlfn"
								maxlength="20" size="20" theme="simple" /></td>
						<td class="izquierda" colspan="4">Ton:&nbsp;<s:textfield
								name="formTon" maxlength="2" size="2" theme="simple" />&nbsp;&nbsp;
							Npi:&nbsp;<s:textfield name="formNpi" maxlength="2" size="2"
								theme="simple" />
						</td>
					</tr>
					<tr class="normal">
						<td class="izquierda">IPs:</td>
						<td class="izquierda" colspan="5"><s:textarea name="formIp"
								cols="60" rows="5" theme="simple" /></td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Límite mensual:</td>
						<td class="izquierda" colspan="2"><s:textfield
								name="formLimite" maxlength="20" size="20" theme="simple" /></td>
						<td class="izquierda">Límite blando:</td>
						<td class="izquierda" colspan="2"><s:textfield
								name="formLimiteBlando" maxlength="20" size="20" theme="simple" />
						</td>
					</tr>
					<tr class="titulin">
						<td>&nbsp;</td>
					</tr>
					<tr class="titulin">
						<td colspan="6" class="izquierda"><strong>Avisos
								junta, texto personalizado:</strong></td>
					</tr>
					<!-- 				
				<tr class="normal">
					<td class="izquierda">Icono personalizado (256x256 pixels, png):</td>
					<td class="izquierda" colspan="4">
						<s:file name="formIcon" size="20" label="Examinar" />
					</td>
					<td class="derecha" colspan="1">
						<img src="<s:property value="uriIcon" />" width="64" height="64" />
					</td>
				</tr>
-->
					<tr class="normal">
						<td class="izquierda" colspan="6">
							<p style="margin: 1em 5em;">Cuando se envía una notificación multimedia a un usuario
								que no es usuario de Avisos Junta, se le notifica con un SMS
								cuyo texto puede personalizarse. Al texto introducido aquí se
								añadiá la URL con la descarga de la app.</p>
						</td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Activar:</td>
						<td class="izquierda" colspan="5"><s:checkbox
								name="formAltTextEnable" theme="simple" /></td>
					</tr>
					<tr class="normal">
						<td class="izquierda">Texto:</td>
						<td class="izquierda" colspan="5"><s:textfield
								name="formAltText" maxlength="135" size="135" theme="simple" />
						</td>
					</tr>
					<tr class="titulin">
						<td>&nbsp;</td>
					</tr>
					<tr class="normal">
						<td colspan="6" class="izquierda"><s:submit type="button"
								name="accion" value="Enviar" theme="simple" /> <s:submit
								type="button" name="accion" value="Resetear" theme="simple" />
						</td>
					</tr>
				</table>


				<!-- Refrescamos las paranoias que no están en el formulario -->
				<s:hidden name="numPaginaEmisor" value="%{numPaginaEmisor}" />
				<s:hidden name="whatToDo" value="%{whatToDo}" />
				<s:hidden name="activity" value="%{activity}" />
			</s:form>

			<!-- Navegación -->

			<table class="generalBox">
				<tr>
					<td><s:url action="actionAdministracionListaEmisores"
							var="backLink">
							<s:param name="numPaginaEmisores">
								<s:property value="numPaginaEmisores" />
							</s:param>
							<s:param name="activity">GestionEmisores</s:param>
						</s:url> <a href="${backLink}">Volver a la lista de emisores</a></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>