<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Panel de Administraci&oacute;n</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<s:if test="%{#session.objetoUsuario.esSuperAdministrador() || #session.objetoUsuario.esAdministrador()}" >
			<!-- Panel del administrador -->
			<table class="panelAdministracionTable">
				<tr>
					<th>Panel del control del administrador (<s:property value="#session.objetoUsuario.emisor.emisor" />)</th>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionListaUsuarios" var="listaUsuariosLink">
							<s:param name="idEmisor"><s:property value="#session.objetoUsuario.emisor.emisor" /></s:param>
							<s:param name="numPagina">0</s:param>
						</s:url>
						<a href="${listaUsuariosLink}">Gesti&oacute;n de los usuarios del emisor <s:property value="#session.objetoUsuario.emisor.emisor" /></a>
					</td>
				</tr>
				<tr>
					<td><a href="<s:url action='actionAdministracionCambioFirmaEmisor'/>">Cambio de la firma corporativa para <s:property value="#session.objetoUsuario.emisor.emisor" /></a></td>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionEstadisticasEmisor" var="estadisticasLink">
							<s:param name="idEmisor"><s:property value="#session.objetoUsuario.emisor.emisor" /></s:param>
						</s:url>
						<a href="${estadisticasLink}">Estad&iacute;sticas de <s:property value="#session.objetoUsuario.emisor.emisor" /></a>
					</td>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionEditaAltText" var="editaalttextLink">
						</s:url>
						<a href="${editaalttextLink}">Editar texto alternativo de Avisos Junta</a>
					</td>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionFuentesSuscripcion" var="fuentesLink">
						</s:url>
						<a href="${fuentesLink}">Gestión de Fuentes de Suscripción</a>
					</td>
				</tr>
			</table>
			<!-- Panel del superadministrador --> 
			<s:if test="%{#session.objetoUsuario.esSuperAdministrador()}">
				<table class="panelAdministracionTable">
					<tr>
						<th>Panel del control del superadministrador</th>
					</tr>
					<tr>
						<td>
							<s:url action="actionAdministracionEmisorUsuario" var="emisorUsuarioLink">
							</s:url>
							<a href="${emisorUsuarioLink}">Buscar el emisor al que pertenece un usuario</a>
						</td>
					</tr>
					<tr>
						<td>
							<s:url action="actionAdministracionListaEmisores" var="listaEmisoresLink">
								<s:param name="numPaginaEmisores">0</s:param>
								<s:param name="activity">GestionEmisores</s:param>
							</s:url>
							<a href="${listaEmisoresLink}">Gestión de emisores</a>
						</td>
					</tr>
					<tr>
						<td>
							<s:url action="actionAdministracionListaAlfanumericos" var="alfanumericosLink">
							</s:url>
							<a href="${alfanumericosLink}">Listado de alfanum&eacute;ricos</a>
						</td>
					</tr>
					<tr>
						<td>
							<s:url action="actionAdministracionListaEmisores" var="listaEmisoresEstadisticasLink">
								<s:param name="numPaginaEmisores">0</s:param>
								<s:param name="activity">Estadisticas</s:param>
							</s:url>
							<a href="${listaEmisoresEstadisticasLink}">Estadísticas de un emisor</a>
						</td>
					</tr>
					<tr>
						<td>
							<s:url action="actionAdministracionFacturacion" var="administracionFacturacionLink">
							</s:url>
							<a href="${administracionFacturacionLink}">Resumen de facturaci&oacute;n</a>
						</td>
					</tr>
				</table>
			</s:if>
		</s:if>
		<s:elseif test="%{#session.objetoUsuario.esFacturacion()}">
			<table class="panelAdministracionTable">
				<tr>
					<th>Panel del control de facturación</th>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionEmisorUsuario" var="emisorUsuarioLink">
						</s:url>
						<a href="${emisorUsuarioLink}">Buscar el emisor al que pertenece un usuario</a>
					</td>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionListaEmisores" var="listaEmisoresEstadisticasLink">
							<s:param name="numPaginaEmisores">0</s:param>
							<s:param name="activity">Estadisticas</s:param>
						</s:url>
						<a href="${listaEmisoresEstadisticasLink}">Estadísticas de un emisor</a>
					</td>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionFacturacion" var="administracionFacturacionLink">
						</s:url>
						<a href="${administracionFacturacionLink}">Resumen de facturaci&oacute;n</a>
					</td>
				</tr>
				<tr>
					<td>
						<s:url action="actionAdministracionListaAlfanumericos" var="alfanumericosLink">
						</s:url>
						<a href="${alfanumericosLink}">Listado de alfanum&eacute;ricos</a>
					</td>
				</tr>
			</table>
		</s:elseif>
		<s:else>
			No tiene permisos para acceder a esta secci&oacute;n.
		</s:else>	
	</div>
</div>
</body>
</html>