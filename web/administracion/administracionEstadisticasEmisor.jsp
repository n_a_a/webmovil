<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Estadísticas del Emisor <s:property value="#session.objetoUsuario.emisor.emisor" /></title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/administracion.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>

<div id="mainContainer">
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<h2>Estad&iacute;sticas del emisor <s:property value="idEmisor" /></h2>
		
		<table class="generalBox">
			<tr><td>L&iacute;mite mensual de mensajes para <s:property value="idEmisor" />: <s:property value="limiteMensual" />.</td></tr>
		</table>
		<s:form method="post" theme="simple">
		<s:hidden name="idEmisor" value="%{idEmisor}" />
		<table class="generalBox">
			<tr>
				<td>
					Seleccione un intervalo: <s:select list="intervalos" name="queIntervalo" />
					<s:submit type="button" name="accion" action="actionAdministracionEstadisticasEmisor" value="Consultar intervalo" theme="simple" />
				</td>
			</tr>
			<tr>
				<td>
					Seleccione mes: <s:select list="#{'01':'Enero', '02':'Febrero', '03':'Marzo', '04':'Abril', '05':'Mayo', '06':'Junio', 
													  '07':'Julio', '08':'Agosto', '09':'Septiembre', '10':'Octubre', '11':'Noviembre', '12':'Diciembre'}" name="mes" /> 
					Seleccione a&ntilde;o: <s:select list="anios" name="anio" /> 
					<s:submit type="button" name="accionConsultar" action="actionAdministracionEstadisticasEmisorConsultarFecha" value="Consultar fecha" theme="simple" />
				</td>
			</tr>
			<tr>
				<td>
					<p align="center" style="font-size:0.8em;">Pulse el bot&oacute;n una vez y espere. La consulta del mes en curso podr&iacute;a demorarse unos minutos. </p>
				</td>
			</tr>
		</table>
		</s:form>
		<br />
		<s:if test="title != null" >
			<table class="generalBox">
				<tr class="impar">
					<td colspan="2" class="texto">
						<h3>Nº de mensajes <s:property value="title" />:</h3>
					</td>
				</tr>
				<tr class="par"><td class="texto" width="80%">Enviados:</td><td class="numerico" width="20%"><s:property value="mensajesEnviados" /></td></tr>
				<tr class="impar"><td class="texto">Recibidos:</td><td class="numerico"><s:property value="mensajesRecibidos" /></td></tr>
				<tr class="par"><td class="texto">Enviados a Corporativos:</td><td class="numerico"><s:property value="mensajesEnviadosCorporativos" /></td></tr>
				<tr class="impar"><td class="texto">Pendientes de envío:</td><td class="numerico"><s:property value="mensajesPendientes" /></td></tr>
				<tr class="par"><td class="texto">Errores de envío:</td><td class="numerico"><s:property value="mensajesError" /></td></tr>
				<tr class="impar"><td class="texto">Con acuse de recibo:</td><td class="numerico"><s:property value="mensajesAcuseRecibo" /></td></tr>
				<tr class="par"><td class="texto">Con prioridad alta:</td><td class="numerico"><s:property value="mensajesPrioridadAlta" /></td></tr>
			</table>
			<br />
			<table class="generalBox">
				<tr class="impar">
					<td colspan="2" class="texto">
						<h3>Usuarios con m&aacute;s env&iacute;os <s:property value="title" />:</h3>
					</td>
				</tr>
				<s:iterator value="usuariosMasActivosStore" status="offset">
					<!-- Color de las lineas de la tabla -->
						<s:if test="#offset.odd == true">
							<s:set name="trclass">par</s:set>
						</s:if> 
						<s:else>
							<s:set name="trclass">impar</s:set>
						</s:else>
						<tr class="${trclass}">
							<td class="texto" width="80%">
								<s:property value="usuario" />
							</td>
							<td class="numerico"  width="20%">
								<s:property value="numEnviados" />
							</td>
						</tr>
				</s:iterator>
			</table>
		</s:if>
		<br />
	</div>
</div>
</body>
</html>