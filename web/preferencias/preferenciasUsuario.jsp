<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Preferencias de Usuario</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/preferencias.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>	

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<s:form action="actionPreferenciasUsuario" method="post" theme="simple">
			<table id="preferenciasTable">
					<tr class="preferenciasTableTr">
						<td class = "preferenciasTableTd">Firma personal (Máximo 80 carácteres):</td>
						<td class = "preferenciasTableTd"><s:textfield name="formFirma" maxlength="80" size="40" theme="simple" /></td>
					</tr>
					<tr class="preferenciasTableTr">
						<td class = "preferenciasTableTd">Nº de items por página en los listados:</td>
						<td class = "preferenciasTableTd"><s:textfield name="formMsgsPagina" size="3" theme="simple" /></td>
					</tr>
					<tr class="preferenciasTableTr">
						<td class="preferenciasTableTd">Notificaci&oacute;n por email al recibir un SMS:</td>
						<td class="preferenciasTableTd"><s:checkbox name="formRecibirEmail" theme="simple" /></td>
					</tr>
					<tr class="preferenciasTableTr">
						<td class = "preferenciasTableTd" colspan = "2" align = "center"><s:submit type="button" name="accion" value="Aceptar" theme="simple" /></td>
					</tr>
			</table>
		</s:form>
	</div>
</div>
</body>
</html>