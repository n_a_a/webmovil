<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Enviar mensajes</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/envios.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/jquery-ui.css?v=<s:property value="version" />" TYPE="text/css">
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery-ui.min.js"></script>
<script language="JavaScript" src="scripts/envios.js"
	type="text/javascript"></script>
<script>
	$(function() {
		$(document).tooltip();
	});
</script>
<s:head />
</head>
<body onload="javascript:cuenta_caracteres(document.forms[0]);">

	<div id="mainContainer">

		<s:include value="/includes/cabecera.jsp" />

		<div id="bodyContainer">

			<!-- Error general de envío -->
			<s:if test="%{hasFieldErrors ()}">
				<s:iterator value="%{fieldErrors.get('errorEnvio')}">
					<table class="generalBox">
						<tr>
							<td><span class="errorMessage"> <s:property />
							</span></td>
						</tr>
					</table>
				</s:iterator>
			</s:if>

			<s:if test="%{limiteExcedido}">
				<p>
					<b>Ha alcanzado el límite de mensajes que tiene permitido
						enviar al mes.</b><br> Mensajes enviados nacionales:&nbsp;<b><s:property
							value="smsEnviadosNacionales" />/ <s:property
							value="#session.objetoUsuario.maxNacional" /> </b>&nbsp;&nbsp;-&nbsp;&nbsp;Mensajes enviados internacionales:&nbsp;<b><s:property
							value="smsEnviadosInternacionales" />/ <s:property
							value="#session.objetoUsuario.maxInternacional" /> </b>
				</p>
			</s:if>
			<s:else>
				<p>
					Mensajes enviados nacionales:&nbsp;<b><s:property
							value="smsEnviadosNacionales" /> / <s:if
							test="totalNacionales != -1">
							<s:property value="#session.objetoUsuario.maxNacional" />
						</s:if> <s:else>
					ILIMITADO
				</s:else> </b>&nbsp;&nbsp;-&nbsp;&nbsp;Mensajes enviados internacionales:&nbsp;<b><s:property
							value="smsEnviadosInternacionales" /> / <s:if
							test="totalInternacionales != -1">
							<s:property value="#session.objetoUsuario.maxInternacional" />
						</s:if> <s:else>
					ILIMITADO
				</s:else> </b>
				</p>

				<s:form action="enviarSMS" theme="simple">
					<s:hidden name="smsEnviadosNacionales" />
					<s:hidden name="smsEnviadosInternacionales" />
					<s:hidden name="restoSmsNacionales" />
					<s:hidden name="restoSmsInternacionales" />
					<s:hidden name="totalNacionales" />
					<s:hidden name="totalInternacionales" />
					<table id="enviosTable">
						<s:if test="%{hasFieldErrors()}">
							<s:set name="errores"
								value="%{fieldErrors.get('envio.destinatario')}" />
							<s:iterator value="%{fieldErrors.get('envio.destinatario')}">
								<tr class="enviosTableTr">
									<td class="enviosTableTd"></td>
									<td class="enviosTableTd" colspan="2"><span
										class="errorMessage"><s:property /></span></td>
									<td class="enviosTableTd"></td>
								</tr>
							</s:iterator>
						</s:if>
						<s:if test="%{tienePermiso('per_solocorp')}">
							<tr class="enviosTableTr">
								<td width="10%" class="enviosTableTdRight">Destinatario:</td>
								<td width="20%" class="enviosTableTd"><s:textfield
										name="envio.destinatario"
										onkeyup="filtra_caracteres_corp(this)" /> <s:hidden
										name="envio.idContacto" /> <s:hidden name="envio.idGrupo" /></td>
								<td width="10%" class="enviosTableTd"><font class="v10Inegro">&nbsp;(Solo
										puede enviar mensajes a móviles coorporativos)</font></td>
								<td width="60%" class="enviosTableTd"><s:a
										href="actionAgendaContenidoDirecto.action"
										onclick="openAgendaWindow(); return false;">Acceso a la agenda</s:a></td>
							</tr>
							<s:hidden name="envio.tipoDestinatario" value="Nacional" />
						</s:if>
						<s:else>
							<tr class="enviosTableTr">
								<td width="10%" class="enviosTableTdRight">Destinatario:</td>
								<td width="20%" class="enviosTableTd"><s:select
										name="envio.tipoDestinatario" list="tiposDestinatario"
										onchange="muestra_texto(); limpia_form(this, enviarSMS_envio_destinatario); cuenta_caracteres(document.forms[0]);" style="width:100%;"/></td>
								<td width="10%" class="enviosTableTd"><s:textfield
										name="envio.destinatario"
										onkeyup="filtra_caracteres(this, enviarSMS_envio_tipoDestinatario)" />
									<s:hidden name="envio.idContacto" /> <s:hidden
										name="envio.idGrupo" /></td>
								<td width="60%" class="enviosTableTd"><s:a
										href="actionAgendaContenidoDirecto.action"
										onclick="openAgendaWindow(); return false;">Acceso a la agenda</s:a></td>
							</tr>
						</s:else>
						<tr class="enviosTableTr">
							<td class="enviosTableTd">&nbsp;</td>
							<td class="enviosTableTd" colspan="3" height="25">
								<div id="texto_nacional" style="display: block">Puede
									enviar a n&uacute;meros nacionales (9 cifras) o corporativos
									v&aacute;lidos (6 cifras)</div>
								<div id="texto_internacional" style="display: none">Los
									n&uacute;meros al extranjero deben estar en formato
									internacional.Ej: +34600123456</div>
							</td>
						</tr>
						<tr class="enviosTableTr">
							<td class="enviosTableTd">&nbsp;</td>
							<td class="enviosTableTd"><span class="nowrap">Caracteres / mensaje: <s:textfield
									name="contCaracteres" disabled="true"
									cssClass="enviosInputInfo" /></span>
							</td>
							<td class="enviosTableTdRight" colspan="2">Mensajes: <s:textfield
									name="contMensajes" disabled="true" cssClass="enviosInputInfo" />
							</td>
						</tr>
						<tr class="enviosTableTr">
							<td class="enviosTableTdRight" valign="top">Texto <a href="#"
								title="Si escribe más de 160 caracteres, el mensaje se segmentará. Puede ver el número de SMS que serán enviados en el indicador <em>Mensajes</em>."><img
									src="images/help.png" /></a>:</td>
							<td class="enviosTableTd" colspan="3"><s:textarea 
									name="envio.texto" cols="70" rows="7"
									onkeyup="javascript:cuenta_caracteres(document.forms[0]);" />
							</td>
						</tr>
						<tr class="enviosTableTr">
							<td class="enviosTableTdRight"><s:checkbox
									name="envio.conFirmaPersonal"
									onclick="javascript:cuenta_caracteres(document.forms[0]);" /></td>
							<td class="enviosTableTd">Firma personal:</td>
							<td class="enviosTableTd" colspan="2"><s:textfield
									name="envio.firmaPersonal"
									onkeyup="javascript:cuenta_caracteres(document.forms[0]);" /></td>
						</tr>
						<tr class="enviosTableTr">
							<s:if test="%{tienePermiso('per_firma')}">
								<td class="enviosTableTdRight"><s:checkbox
										name="envio.conFirmaCorporativa"
										onclick="javascript:cuenta_caracteres(document.forms[0]);" /></td>
							</s:if>
							<s:else>
								<td class="enviosTableTdRight"><s:checkbox disabled="true"
										name="envio.conFirmaCorporativa"
										onclick="javascript:cuenta_caracteres(document.forms[0]);" /></td>
							</s:else>
							<td class="enviosTableTd">Firma corporativa:</td>
							<td class="enviosTableTd" colspan="2"><s:textfield
									name="envio.firmaCorporativa" readonly="true"
									cssStyle="border:1px solid #ffffff" /></td>
						</tr>
						<s:if test="%{tienePermiso('per_recibo')}">
							<tr class="enviosTableTr">
								<td class="enviosTableTdRight"><s:checkbox
										name="envio.acuseRecibo" /></td>
								<td class="enviosTableTd" colspan="3">Acuse de recibo</td>
							</tr>
						</s:if>
						<s:if test="%{tienePermiso('per_prioridad')}">
							<tr class="enviosTableTr">
								<td class="enviosTableTdRight"><s:checkbox
										name="envio.prioridadAlta" /></td>
								<td class="enviosTableTd" colspan="3">Prioridad alta</td>
							</tr>
						</s:if>
						<s:if test="%{canSetAlfa}">
						<tr class="enviosTableTr">
							<td class="enviosTableTdRight"><s:checkbox
									name="envio.remitenteAlfanumerico" /></td>
							<td class="enviosTableTd" colspan="3">Remitente
								alfanum&eacute;rico (<s:property value="alfa" />)</td>
						</tr>
						</s:if>
						<tr class="enviosTableTr">
							<td class="enviosTableTdRight">Vigencia <a href="#"
								title="Indica al servidor que debe descartar el mensaje pasado este tiempo si no ha sido entregado al terminal."><img
									src="images/help.png" /></a>:
							</td>
							<td class="enviosTableTd" colspan="3"><s:select
									name="envio.vigencia" list="vigencias" /></td>
						</tr>
						<tr class="enviosTableTr">
							<td class="enviosTableTdRight">Tipo <a href="#"
								title="<strong>Estándar</strong> enviará el mensaje como notificación push a los destinatarios con la aplicación 'Avisos Junta' instalada (sin coste) o como SMS al resto. <br /><strong>Forzar SMS</strong> enviará SMS en cualquier caso.<br /> <strong>Sólo Avisos Junta</strong> enviará una notificación a la aplicación Avisos Junta (si el destinatario no tiene la aplicación instalada, no recibirá el mensaje y el proceso devolverá un error)"><img
									src="images/help.png" /></a>:
							</td>
							<td class="enviosTableTd" colspan="3"><s:select
									name="envio.tipo" list="#{'02':'Estándar', '00':'Forzar SMS', '01':'Sólo Avisos Junta', '03':'Avisos Junta Con Entrega Segura'}" /></td>
						</tr>
						<tr bgcolor="#FFFFFF">
							<td colspan="4" height="3"></td>
						</tr>
						<tr class="enviosTableTr" height="35" valign="middle">
							<td colspan="4" align="center"><s:submit type="button"
									value="Enviar" /> <s:submit type="button" value="Vista previa"
									action="enviarSMSPreview" /> <s:submit type="button"
									value="Cancelar" action="enviarSMSNuevo" /></td>
						</tr>
					</table>
				</s:form>

			</s:else>
		</div>
	</div>

</body>
</html>