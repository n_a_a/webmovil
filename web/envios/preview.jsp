<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WebMovil - Enviar mensajes</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css" />
<link rel="stylesheet" href="styles/envios.css?v=<s:property value="version" />" TYPE="text/css" />
</head>
<body>
<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />

	<div id="bodyContainer">
		<h2 id="titulo">Datos del mensaje a enviar</h2>
		
		<s:form action="enviarSMS" method="post" theme="simple">
			<s:hidden name="envio.tipoDestinatario" />
			<s:hidden name="envio.destinatario" />
			<s:hidden name="envio.texto" />
			<s:hidden name="envio.conFirmaPersonal" />
			<s:hidden name="envio.firmaPersonal" />
			<s:hidden name="envio.conFirmaCorporativa" />
			<s:hidden name="envio.firmaCorporativa" />
			<s:hidden name="envio.acuseRecibo" />
			<s:hidden name="envio.prioridadAlta" />
			<s:hidden name="envio.remitenteAlfanumerico" />
			<s:hidden name="envio.vigencia" />
			<s:hidden name="envio.idContacto"/>
			<s:hidden name="envio.idGrupo"/>
		<table id="enviosTable">
			<tr class="enviosTableTr">
				<td class="enviosTableTdRight">Destinatario:</td>
				<td class="enviosTableTd"><s:property value="envio.destinatario" /></td>
			</tr>
			<tr class="enviosTableTr">
				<td class="enviosTableTdRight" valign="top">Mensaje a enviar:</td>
				<td class="enviosTableTd">
					<s:textarea name="envio.mensaje" cols="68" rows="7" readonly="true" cssClass="enviosTextareaInfo"/>
				</td>
			</tr>
			<tr class="enviosTableTr">
				<td class="enviosTableTdRight">Vigencia:</td>
				<td class="enviosTableTd"><s:property value="envio.vigencia" /></td>
			</tr>
			<tr class="enviosTableTr">
				<td class="enviosTableTdRight">Acuse de recibo:</td>
				<td class="enviosTableTd">
					<s:if test="%{envio.acuseRecibo}">S&iacute;</s:if>
					<s:else>No</s:else>
				</td>
			</tr>
			<tr class="enviosTableTr">
				<td class="enviosTableTdRight">Prioridad alta:</td>
				<td class="enviosTableTd">
					<s:if test="%{envio.prioridadAlta}">S&iacute;</s:if>
					<s:else>No</s:else>
				</td>
			</tr>
			<tr class="enviosTableTr">
				<td class="enviosTableTdRight">Remitente alfanum&eacute;rico:</td>
				<td class="enviosTableTd">
					<s:if test="%{envio.remitenteAlfanumerico}">S&iacute;</s:if>
					<s:else>No</s:else>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
						<td colspan="2" height="3"></td>
					</tr>
			<tr class="enviosTableTr" height="35" valign="middle">
				<td colspan="2" align="center">
					<s:submit type="button" value="Enviar" /><s:submit type="button" value="Volver" action="enviarSMSInput" />
				</td>
			</tr>
		</table>
		</s:form>

	</div>
</div>
</body>
</html>