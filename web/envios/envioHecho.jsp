<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>WebMovil - Enviar mensajes</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css" />
<link rel="stylesheet" href="styles/envios.css?v=<s:property value="version" />" TYPE="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />

	<div id="bodyContainer">

		<p id="mensajeInfo">Su mensaje a <b><s:property value="envio.destinatario" /></b> ha sido enviado.</p>

		<s:form action="enviarSMSInput" method="post" theme="simple">
			<s:hidden name="envio.tipoDestinatario" />
			<s:hidden name="envio.destinatario" />
			<s:hidden name="envio.texto" />
			<s:hidden name="envio.conFirmaPersonal" />
			<s:hidden name="envio.firmaPersonal" />
			<s:hidden name="envio.conFirmaCorporativa" />
			<s:hidden name="envio.firmaCorporativa" />
			<s:hidden name="envio.acuseRecibo" />
			<s:hidden name="envio.prioridadAlta" />
			<s:hidden name="envio.remitenteAlfanumerico" />
			<s:hidden name="envio.vigencia" />
			<s:hidden name="envio.idContacto"/>
			<s:hidden name="envio.idGrupo"/>
			<p id="lineaBotones">
				<s:submit type="button" value="Volver" />
				<s:submit type="button" value="Nuevo mensaje" action="enviarSMSNuevo" />
			</p>
		</s:form>

	</div>
</div>
</body>
</html>