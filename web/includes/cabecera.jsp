<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="logosContainer">
	<div id="logoJunta">
		<span class="hidden">Junta de Andaluc&iacute;a</span>
	</div>
	<div id="logoWebMovil">
		<span class="hidden">Webmovil</span>
	</div>
	<p class="clear">&nbsp;</p>
</div>

<div id="infoBar">
	<div id="infoBarUsuario">
		<s:property value="#session.objetoUsuario.usuario" />
	</div>
	<div id="infoBarEmisor">
		<s:property value="#session.objetoUsuario.emisor.emisor" />
	</div>
	<p class="clear">&nbsp;</p>
</div>

<div id="menuBar">
	<s:if test="%{seccion == 'SMS'}">
		<div class="menuItemSelected">
			<a href="<s:url action='actionPanelSMS'/>">SMS</a>
		</div>
	</s:if>
	<s:else>
		<div class="menuItem">
			<a href="<s:url action='actionPanelSMS'/>">Sms</a>
		</div>
	</s:else>
	<s:if test="%{seccion == 'notificacionesjda'}">
		<div class="menuItemSelected">
			<a href="<s:url action='actionNotificacionesJDA'/>">AVISOS JUNTA</a>
		</div>
	</s:if>
	<s:else>
		<div class="menuItem">
			<a href="<s:url action='actionNotificacionesJDA'/>">Avisos Junta</a>
		</div>
	</s:else>
	<s:if test="%{seccion == 'agenda'}">
		<div class="menuItemSelected">
			<a href="<s:url action='actionAgendaContenidoCombo' />">AGENDA</a>
		</div>
	</s:if>
	<s:else>
		<div class="menuItem">
			<a href="<s:url action='actionAgendaContenidoCombo' />">Agenda</a>
		</div>
	</s:else>
	<s:if test="%{#session.objetoUsuario.esSuperAdministrador() || #session.objetoUsuario.esAdministrador()}">
		<s:if test="%{seccion == 'administracion'}">
			<div class="menuItemSelected">
				<a href="<s:url action='actionPanelAdministracion'/>">ADMINISTRACI&Oacute;N</a>
			</div>
		</s:if>
		<s:else>
			<div class="menuItem">
				<a href="<s:url action='actionPanelAdministracion'/>">Administraci&oacute;n</a>
			</div>
		</s:else>
	</s:if>
	<s:if test="%{#session.objetoUsuario.esFacturacion()}">
		<s:if test="%{seccion == 'administracion'}">
			<div class="menuItemSelected">
				<a href="<s:url action='actionPanelAdministracion'/>">FACTURACI&Oacute;N</a>
			</div>
		</s:if>
		<s:else>
			<div class="menuItem">
				<a href="<s:url action='actionPanelAdministracion'/>">Facturaci&oacute;n</a>
			</div>
		</s:else>
	</s:if>
	<s:if test="%{seccion == 'preferencias'}">
		<div class="menuItemSelected">
			<a href="<s:url action='actionPreferenciasUsuario'/>">PREFERENCIAS</a>
		</div>
	</s:if>
	<s:else>
		<div class="menuItem">
			<a href="<s:url action='actionPreferenciasUsuario'/>">Preferencias</a>
		</div>
	</s:else>
	<s:if test="%{seccion == 'ayuda'}">
		<div class="menuItemSelected">
			<a href="<s:url action='actionAyuda'/>">AYUDA</a>
		</div>
	</s:if>
	<s:else>
		<div class="menuItem">
			<a href="<s:url action='actionAyuda'/>">Ayuda</a>
		</div>
	</s:else>
	<div class="menuItem">
		<a href="<s:url action='salir' />">Salir</a>
	</div>
	<p class="clear">&nbsp;</p>
</div>