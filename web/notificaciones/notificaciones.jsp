<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Avisos Junta</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/notificaciones.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>	

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />
	
	<table class="notificacionesTable">
		<tr>
			<th>Avisos Junta</th>
		</tr>
		<tr>
			<td><a href="<s:url action='actionNotificacionesJDAComponer'/>">Componer notificación con adjuntos</a></td>
		</tr>
		<tr>
			<td><a href="<s:url action='actionNotificacionesJDAListaEnvios'/>">Consultar notificaciones enviadas</a></td>
		</tr>
		<tr>
			<td><a href="<s:url action='actionNotificacionesJDAInfoClientes'/>">Información sobre los destinatarios</a></td>
		</tr>
	</table>
</div>
</body>
</html>