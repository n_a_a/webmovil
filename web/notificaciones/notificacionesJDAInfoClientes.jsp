<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Avisos Junta - Información sobre los destinatarios</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/notificaciones.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/jquery-ui.css?v=<s:property value="version" />" TYPE="text/css">
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery-ui.min.js"></script>
<script src="https://maps.google.com/maps/api/js?v=3&sensor=false&libraries=places"></script>
<script>

	$(document).ready(
			function() {
				var latLng = new google.maps.LatLng(37.38826062499185,
						-5.995289855957026);

				var map = new google.maps.Map(document
						.getElementById('mapCanvas'), {
					zoom : 8,
					center : latLng,
					mapTypeId : google.maps.MapTypeId.ROADMAP
				});

				var marker = new google.maps.Marker({
					position : latLng,
					title : 'Localización',
					map : map,
					draggable : true
				});

				$(".thumb a").click(function(e) {
					var latLonStr = ($(this).attr("href"));
					var latLonArray = latLonStr.split(",");
					var lat = latLonArray[0];
					var lon = latLonArray[1];

					var location = new google.maps.LatLng(lat, lon);
					var bounds = new google.maps.LatLngBounds();

					/*
					marker.setPosition(location);
					map.fitBounds(bounds);
					map.setZoom(12);
					map.setCenter(location);
					*/
					
					marker.setPosition(location);
					map.setZoom (12);
					
					$("#mapCanvas").dialog({
						title : "Coordenadas " + lat + ", " + lon,
						height : 480,
						width : 640,
						modal : true
					});
										
					google.maps.event.trigger(map, 'resize');
					map.panTo (location);
					
					e.preventDefault();
					return false;
				});
			});
</script>
</head>
<body>
	<div id="mainContainer">

		<s:include value="/includes/cabecera.jsp" />

		<div id="bodyContainer">
			<table class="generalBox">
				<tr>
					<td><h3>Información sobre los destinatarios</h3></td>
				</tr>
			</table>
			<br />
			<table class="tablaFormulario">
				<s:form action="actionNotificacionesJDAInfoClientes" method="post"
					theme="simple" enctype="multipart/form-data">		
					<tr>
						<td width="20%" class="izquierda" valign="top"><strong>Teléfonos:</strong><br><small>Introduzca uno o varios números de destino separados por comas</small></td>
						<td width="80%" class="izquierda" valign="top"><s:textarea name="tlfnoDestino" cols="80" rows="7" /></td>
					</tr>
					<tr>
						<td class="izquierda">
							<s:submit type="button" name="accion" value="Enviar" />
						</td>
					</tr>
				</s:form>
			</table>
			<s:if test="numResultados > 0">
				<table class="tablaFormulario">
					<tr>
						<th>Teléfono:</th>
						<th>App instalada:</th>
						<!-- <th>Localización:</th> -->
						<!-- <th>Última actualización:</th> -->
					</tr>
					<s:iterator value="resultados" status="offset">
						<s:if test="#offset.odd == true"><s:set name="trclass">impar</s:set><s:set name="trclass2">par</s:set></s:if><s:else><s:set name="trclass">par</s:set><s:set name="trclass2">impar</s:set></s:else>
						<tr class="${trclass}">
							<s:if test="push == true">
								<td>
									<s:property value="tlfno" />
								</td>
								<td>
									SÍ
								</td>
								<!-- <td>
									<span class="thumb">
										<s:if test="lat != 0.0 && lon != 0.0">
											<a href="<s:property value="lat" />,<s:property value="lon" />">
												<s:property value="lat" />, <s:property value="lon" />
											</a>
										</s:if>
									</span>
									<s:else>
										No hay datos
									</s:else>
								</td> -->
								<!-- <td>
									<s:if test="ts == 0">
										No hay datos
									</s:if>
									<s:else>
										<s:property value="dateTimeString" />
									</s:else>
								</td> -->
							</s:if>
							<s:else>
								<td>
									<span style="color:red;"><s:property value="tlfno" /></span>
								</td>
								<td>
									<span style="color:red;">NO</span>
								</td>
								<!-- <td></td> -->
								<!-- <td></td> -->
							</s:else>						
						</tr>
					</s:iterator>
				</table>
			</s:if>
		</div>
	</div>
	<div id="mapCanvas" style="display: none;"></div>
</body>
</html>
