<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Avisos Junta - Componer</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/notificaciones.css?v=<s:property value="version" />" TYPE="text/css">
<script language="javascript" src="scripts/jquery.min.js"></script>
<script language="javascript" src="scripts/protoplasm/protoplasm.js"></script>
<script language="JavaScript" src="scripts/enviosNotificacion.js" type="text/javascript"></script>
<script>
	// transform() calls can be chained together
	
	Protoplasm.use('datepicker')
	.transform('.datepicker')
	.transform('.datetimepicker', { timePicker: true })
	.transform('.daterangepicker', { range: true, monthCount: 2 })
	.transform('.datetimepicker_es', { locale: 'es_ES', timePicker: true, use24hrs: true, dateTimeFormat: 'yyyy-MM-dd HH:mm:ss' });
	
	function setLineAccionValue (what) {
		document.getElementById ('lineAccionValue').value = what;
	}
	
	function setFileName (which, what) {
		document.getElementById ('adjuntos['+which+'].filename').value = what;
	}
	
	(function($) {
		$(document).ready(function() {
			$('#makethis a').click(function() {
				var tlfnoDestinoUrlEncoded = encodeURIComponent ($ ("#tlfnoDestino").val ());
				window.open ("actionNotificacionesJDAInfoClientesSimple.action?accion=Enviar&tlfnoDestino=" + tlfnoDestinoUrlEncoded, "Comprobación de destinatarios", "width=640, height=480, location=no, menubar=no, resizable=yes, scrollbars=yes");
				return false;
			});
		});
	})(jQuery);
</script>
</head>
<body>	

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<table class="generalBox">
			<tr>
				<td><h3>Avisos Junta - Componer</h3></td>
			</tr>
		</table>
		<br />
		
		<s:if test="someError != null">
			<table class="tablaFormulario">
				<tr>
					<td style="color:red; font-weight:bold;">
						<s:property value="someError" />
					</td>
				</tr>
			</table>
		</s:if>
		
		<s:form action="actionNotificacionesJDAComponer" method="post" enctype="multipart/form-data" theme="simple" >
			<input type="hidden" name="lineAccionValue" value="" id="lineAccionValue" />
			<s:hidden name="opcionesOn" value="%{opcionesOn}" />
			<s:hidden name="ackOn" value="%{ackOn}" />
			<s:hidden name="page" value="%{page}" />
			<s:hidden name="comesFrom" value="%{comesFrom}" />
			
			<table class="tablaFormulario">
				<tr>
					<td width="20%" class="izquierda" valign="top"><strong>Destino:</strong><br><small>Introduzca uno o varios números de destino separados por comas</small></td>
					<td class="izquierda" valign="top">
						<s:textarea name="tlfnoDestino" id="tlfnoDestino" cols="80" rows="5" theme="simple" />
					</td>
					<td width="25%" class="izquierda" valign="top">
						<span id="makethis" style="display: inline-block;"><a href="test">Comprobar</a></span><br />
						<span style="white-space: nowrap;"><s:a href="actionAgendaContenidoDirecto.action" onclick="openAgendaWindow(); return false;">Acceso a la agenda</s:a></span>
					</td>
				</tr>
				<tr>
					<td class="izquierda" valign="top"><strong>Texto:</strong></td>
					<td class="izquierda"><s:textarea name="texto" cols="80" rows="7" /></td>
					<td></td>
				</tr>
				<tr>
					<td class="derecha"></td>
					<td class="izquierda"><s:checkbox name="noSendSMS" id="noSendSMS"></s:checkbox>Si algún destinatario no tiene instalda la App, no enviar un SMS de aviso.</td>
					<td></td>
				</tr>
			</table> 
			<br />
			
			<!-- Adjuntos -->
			<table class="generalBox"><tr><td><h4>Adjuntos</h4></td></tr></table>
			<s:if test="numAdjuntos > 0 || opcionesOn == true || ackOn == true">
				<table class="tablaFormulario">
					<s:iterator value="adjuntos" status="offset" var="adjunto">
						<s:if test="#offset.odd == true"><s:set name="trclass">impar</s:set><s:set name="trclass2">par</s:set></s:if><s:else><s:set name="trclass">par</s:set><s:set name="trclass2">impar</s:set></s:else>
						<tr class="${trclass}">
							<!-- icon -->
							<td width="56" valign="top">
								<div class="iconfield">
									<img src="images/<s:property value="iconName" />" alt="<s:property value="type" />" title="<s:property value="typeName"/>" />
								</div>
							</td>
							<!-- body -->
							<td class="attachment_body izquierda">
								<s:if test="type.equals('p3s/url')">
									<table width="100%">
										<tr><td class="derecha" width="10%">URL:</td><td class="izquierda"><s:textfield value="%{#adjunto.url}" name="adjuntos[%{#adjunto.id}].url" size="80" /></td></tr>
										<tr><td class="derecha" width="10%">Desc.:</td><td class="izquierda"><s:textfield value="%{#adjunto.desc}" name="adjuntos[%{#adjunto.id}].desc" size="80" /></td></tr>
									</table>
								</s:if>
								<s:elseif test="type.equals('p3s/video')">
									<table width="100%">
										<tr><td class="derecha" width="10%">Título:</td><td class="izquierda"><s:textfield value="%{#adjunto.tit}" name="adjuntos[%{#adjunto.id}].tit" size="80" /></td></tr>
										<tr><td class="derecha" width="10%">URL:</td><td class="izquierda"><s:textfield value="%{#adjunto.url}" name="adjuntos[%{#adjunto.id}].url" size="80" /></td></tr>
									</table>
								</s:elseif>
								<s:elseif test="type.equals('p3s/loc')">
									<table width="100%">
										<tr>
											<td class="derecha" width="10%">Latitud:</td>
											<td class="izquierda" width="40%"><s:textfield value="%{#adjunto.lat}" name="adjuntos[%{#adjunto.id}].lat" id="adjuntos[%{#adjunto.id}].lat" size="20" /></td>
											<td class="derecha" width="10%">Desc.:</td>
											<td class="izquierda" width="40%"><s:textfield value="%{#adjunto.desc}" name="adjuntos[%{#adjunto.id}].desc" id="adjuntos[%{#adjunto.id}].desc" size="40" /></td>
										</tr>
										<tr>
											<td class="derecha" width="10%">Longitud:</td>
											<td class="izquierda" width="40%"><s:textfield value="%{#adjunto.lon}" name="adjuntos[%{#adjunto.id}].lon" id="adjuntos[%{#adjunto.id}].lon" size="20" /></td>
											<td width="50%" colspan="2" style="text-align:center"><a href="#" onclick="window.open ('includes/map.html?id=<s:property value="id" />', 'Seleccione localización', 'status=1,menubar=0,resizable=0,scrollbars=0,toolbar=0,width=750,height=580')">Seleccionar en un mapa</a></td>											
										</tr>
									</table>
								</s:elseif>
								<s:elseif test="type.equals('p3s/event')" >
									<table width="100%">
										<tr><td class="subtitler" colspan="4">Datos de la cita:</td></tr>
										<tr>
											<td class="derecha" width="10%">Título:</td>
											<td colspan="3" class="izquierda" width="90%"><s:textfield value="%{#adjunto.title}" name="adjuntos[%{#adjunto.id}].title" id="adjuntos[%{#adjunto.id}].title" size="80" /></td>
										</tr>
										<tr>
											<td class="derecha" width="10%">Desde:</td>
											<td class="izquierda" width="40%">
												<input type="text" value="<s:property value="start" />" name="adjuntos[<s:property value="id" />].start" size="20" class="datetimepicker_es"/>
											</td>
											<td class="derecha" width="10%">Hasta:</td>
											<td class="izquierda" width="40%">
												<input type="text" value="<s:property value="end" />" name="adjuntos[<s:property value="id" />].end" size="20" class="datetimepicker_es"/>
											</td>
										</tr>
										<tr>
											<td class="derecha" width="10%">URL:</td>
											<td colspan="3" class="izquierda" width="90%"><s:textfield value="%{#adjunto.url}" name="adjuntos[%{#adjunto.id}].url" id="adjuntos[%{#adjunto.id}].url" size="80" /></td>
										</tr>
										<!--
										<tr><td class="subtitler" colspan="4">Ubicación de la cita:</td></tr>
										<tr>
											<td class="derecha" width="10%">Latitud:</td>
											<td class="izquierda" width="40%"><s:textfield value="%{#adjunto.lat}" name="adjuntos[%{#adjunto.id}].lat" id="adjuntos[%{#adjunto.id}].lat" size="20" /></td>
											<td class="derecha" width="10%">Desc.:</td>
											<td class="izquierda" width="40%"><s:textfield value="%{#adjunto.desc}" name="adjuntos[%{#adjunto.id}].desc" id="adjuntos[%{#adjunto.id}].desc" size="40" /></td>
										</tr>
										<tr>
											<td class="derecha" width="10%">Longitud:</td>
											<td class="izquierda" width="40%"><s:textfield value="%{#adjunto.lon}" name="adjuntos[%{#adjunto.id}].lon" id="adjuntos[%{#adjunto.id}].lon" size="20" /></td>
											<td width="50%" colspan="2" style="text-align:center"><a href="#" onclick="window.open ('includes/map.html?id=<s:property value="id" />', 'Seleccione localización', 'status=1,menubar=0,resizable=0,scrollbars=0,toolbar=0,width=750,height=580')">Seleccionar en un mapa</a></td>											
										</tr>
										-->
									</table>
								</s:elseif>
								<s:elseif test="type.contains('image') || type.contains('application') || type.contains('audio')">
									<s:if test="alreadyOnServer">
										<table>
											<tr>
												<td>
													Archivo en servidor: <a href="<s:property value="uriToGet" />" target="_blank">Descargar</a>
												</td>
											</tr>
											<tr>
												<td>
													<s:if test="type.contains('image')">
														<div class="imgcontainer">
															<a href="<s:property value="uriToGet" />" target="_blank">
																<img src="<s:property value="uriToGet" />" />
															</a>
														</div>
													</s:if>
												</td>
											</tr>
										</table>
									</s:if>
									<s:elseif test="alreadyUploaded" >
										Archivo subido: <s:property value="justFilename" />.
									</s:elseif>
									<s:else>
										<s:file name="upload" label="Examinar..." onchange="setFileName ('%{#adjunto.id}', this.value);" size="80" accept="%{#adjunto.type}" />
										<s:if test="lastError != null" >
											<s:property value="lastError" />
										</s:if>
									</s:else>
									<s:hidden value="%{#adjunto.filename}" name="adjuntos[%{#adjunto.id}].filename" id="adjuntos[%{#adjunto.id}].filename" />
								</s:elseif>
								<s:if test="syntaxErrors!=null">
									<table class="tablaFormulario">
										<tr>
											<td style="color:red; font-weight:bold;">
												<s:iterator value="syntaxErrors">
													<s:property /><br/>
												</s:iterator>
											</td>
										</tr>
									</table>
								</s:if>
							</td>
							<!-- actions --> 
							<td width="10%" class="line_commands" >
								<s:submit type="image" src="images/icnjda-up.png" onclick="setLineAccionValue ('upAtt%{#adjunto.id}');" theme="simple" />
								<s:submit type="image" src="images/icnjda-dw.png" onclick="setLineAccionValue ('dwAtt%{#adjunto.id}');" theme="simple" />
								<s:submit type="image" src="images/icnjda-del.png" onclick="setLineAccionValue ('deAtt%{#adjunto.id}');" theme="simple" />
							</td>
						</tr>
					</s:iterator>
					<!-- Linea de opciones -->
					<s:if test="opcionesOn == true">
						<tr class="${trclass2}">
							<!-- icon -->
							<td width="56" valign="top">
								<div class="iconfield">
									<img src="images/ft-option.png" alt="p3s/option" title="Respuesta" />
								</div>
							</td>
							<!-- body -->
							<td class="attachment_body izquierda">
								<table width="100%">
									<tr><td class="subtitler" colspan="5">Opciones:</td></tr>
									<s:iterator value="opciones" status="offsetOp" var="opcion">
										<s:if test="#offsetOp.odd == true"><s:set name="trclass">impar</s:set></s:if><s:else><s:set name="trclass">par</s:set></s:else>
										<tr class="${trclass}">
											<td class="derecha" width="10%">Texto:</td><td class="izquierda" width="35%"><s:textfield value="%{#opcion.texto}" name="opciones[%{#offsetOp.index}].texto" size="40" /></td>
											<td class="derecha" width="5%">Url:</td><td class="izquierda" width="45%"><s:textfield value="%{#opcion.url}" name="opciones[%{#offsetOp.index}].url" size="40" /></td>
											<td class="izquierda" width="5%" class="line_commands">
												<s:submit type="image" src="images/icnjda-del.png" onclick="setLineAccionValue ('deOpc%{#offsetOp.index}');" theme="simple" />
											</td>
										</tr>
									</s:iterator>
									<tr>
										<td colspan="5"><s:submit type="button" name="accion" value="Nueva linea" /></td>
									</tr>
									<tr><td class="subtitler" colspan="5">Configuración:</td></tr>
									<tr>
										<td colspan="5">
											<s:checkbox name="respuestaFlagFeedback" label="Añadir cuadro de feedback"/>Añadir cuadro de feedback
										</td>
									</tr>
								</table>
								<s:if test="optionsSyntaxErrors!=null">
									<table class="tablaFormulario">
										<tr>
											<td style="color:red; font-weight:bold;">
												<s:iterator value="optionsSyntaxErrors">
													<s:property /><br/>
												</s:iterator>
											</td>
										</tr>
									</table>
								</s:if>							
							</td>
							<!-- actions --> 
							<td width="10%" class="line_commands" >
								<s:submit type="image" src="images/icnjda-del.png" onclick="setLineAccionValue ('deAttOp');" theme="simple" />
							</td>
						</tr>
					</s:if>
					<!-- Linea de opciones -->
					<s:if test="ackOn == true">
						<tr class="${trclass2}">
							<!-- icon -->
							<td width="56" valign="top">
								<div class="iconfield">
									<img src="images/ft-ack.png" alt="p3s/ack title="Confirmación de lectura" />
								</div>
							</td>
							<!-- body -->
							<td class="attachment_body izquierda">
								Este mensaje solicitará confirmación de lectura.
							</td>
							<!-- actions --> 
							<td width="10%" class="line_commands" >
								<s:submit type="image" src="images/icnjda-del.png" onclick="setLineAccionValue ('deAttAck');" theme="simple" />
							</td>
						</tr>
					</s:if>
				</table>
			</s:if>						
		
			<!-- Nuevo adjunto: Añade un nuevo adjunto al final -->
			<table class="tablaFormulario">
				<tr>
					<td width="20%" class="izquierda"><strong>Nuevo adjunto:</strong></td>
					<td width="80%" class="izquierda"> 
						<s:select name="nuevoTipo" list="listaTipos" multiple="false" listKey="key" listValue="value"></s:select>
						<s:submit type="button" name="accion" value="Aceptar" />
					</td>
				</tr>
			</table>
			
			<!-- Enviar/Cancelar -->
			<table class="tablaFormulario">
				<tr>
					<td class="derecha">
						<s:submit type="button" name="accion" value="Cancelar" />
						<s:submit type="button" name="accion" value="Enviar" />
					</td>
				</tr>
			</table>
		</s:form>
		<br />
	</div>
</div>
</body>
</html>