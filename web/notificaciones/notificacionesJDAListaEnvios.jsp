<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Avisos Junta - Lista de envíos</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/notificaciones.css?v=<s:property value="version" />" TYPE="text/css">
</head>
<body>	

<div id="mainContainer">

	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
	
		<table class="generalBox">
			<tr>
				<td><h3>Avisos Junta - Lista de envíos</h3></td>
			</tr>
		</table>
		<br />
		<table class="tablaFormulario">
			<tr>
				<th width="5%">ID</th>
				<th width="40%">Texto</th>
				<th width="20%">Adjuntos</th>
				<th width="10%">Tlfno</th>
				<th width="20%">Fecha / Hora</th>
				<th width="5%">E</th>
			</tr>
			<s:if test="numItems == 0">
				<tr class="${trclass}">
					<td colspan = 4>No hay registros.</td>
				</tr>
			</s:if>
			<s:else>
				<s:iterator value="notificaciones" status="offset" var="notificacion">
					<s:if test="#offset.odd == true"><s:set name="trclass">impar</s:set></s:if><s:else><s:set name="trclass">par</s:set></s:else>
					<s:url action="actionNotificacionesJDAVer"
						var="verLink">
						<s:param name="id"><s:property value="id" /></s:param>
						<s:param name="page"><s:property value="page" /></s:param>
						<s:param name="tlfn"><s:property value="tlfn" /></s:param>
					</s:url>
					<tr class="${trclass}">
						<td><a href="${verLink}"><s:property value="id" /></a></td>
						<td><a href="${verLink}"><s:property value="textoCut" /></a></td>
						<td>
							<s:iterator value="adjuntos" var="adjunto">
								<img src="images/<s:property value="iconName" />" alt="<s:property value="type" />" title="<s:property value="typeName"/>" />
							</s:iterator>
						</td>
						<td><s:property value="tlfn" /></td>
						<td><s:property value="dateStr" /></td>
						<td><s:property value="estado" /></td>
					</tr>
				</s:iterator>
			</s:else>
		</table>
		<table class="generalBox">
			<tr>
				<td width="32">
					<s:if test="page > 0">
						<s:url action="actionNotificacionesJDAListaEnvios"
							var="homeLink">
							<s:param name="accion">pgHo</s:param>
							<s:param name="page"><s:property value="page" /></s:param>
						</s:url>
						<a href="${homeLink}"><img src="images/icono-flecha-0.png" /></a>
					</s:if>
					<s:else>
						<img src="images/icono-flecha-0-g.png" />
					</s:else>
				</td>
				<td width="32">
					<s:if test="page > 0">
						<s:url action="actionNotificacionesJDAListaEnvios"
							var="prevLink">
							<s:param name="accion">pgUp</s:param>
							<s:param name="page"><s:property value="page" /></s:param>
						</s:url>
						<a href="${prevLink}"><img src="images/icono-flecha-1.png" /></a>
					</s:if>
					<s:else>
						<img src="images/icono-flecha-1-g.png" />
					</s:else>
				</td>
				<td width="32">
					<s:if test="page < maxPage">
						<s:url action="actionNotificacionesJDAListaEnvios"
							var="nextLink">
							<s:param name="accion">pgDw</s:param>
							<s:param name="page"><s:property value="page" /></s:param>
						</s:url>
						<a href="${nextLink}"><img src="images/icono-flecha-2.png" /></a>
					</s:if>
					<s:else>
						<img src="images/icono-flecha-2-g.png" />
					</s:else>
				</td>
				<td width="32">
					<s:if test="page < maxPage">
						<s:url action="actionNotificacionesJDAListaEnvios"
							var="lastLink">
							<s:param name="accion">pgEn</s:param>
							<s:param name="page"><s:property value="page" /></s:param>
						</s:url>
						<a href="${lastLink}"><img src="images/icono-flecha-3.png" /></a>
					</s:if>
					<s:else>
						<img src="images/icono-flecha-3-g.png" />
					</s:else>
				</td>
				<td>&nbsp;Página <s:property value="page + 1" /> de <s:property value="maxPage + 1" /></td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>