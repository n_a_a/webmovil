<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Agenda</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/agenda.css?v=<s:property value="version" />" TYPE="text/css">
<s:head />
</head>
<body>

<div id="mainContainer">
	
	<s:include value="/includes/cabecera.jsp" />
	
	<div id="bodyContainer">
		<table width="100%" height="100%">
			<tr height="100%">
				<td>
					<s:url action="actionAgendaContenido" var="frameGruposLink">
						<s:param name="tipoContactos">Grupos</s:param>
					</s:url>
					<iframe width="100%" height="100%" name="agendaCarpetas" src="${frameGruposLink}">
					</iframe>
				</td>
			
				<td>
					<s:url action="actionAgendaContenido" var="frameContactosLink">
						<s:param name="tipoContactos">Contactos</s:param>
					</s:url>
					<iframe width="100%" height="100%"  name="agendaCarpetas" src="${frameContactosLink}">
					</iframe>
				</td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>