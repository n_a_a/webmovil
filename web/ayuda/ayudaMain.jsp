<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebMovil - Ayuda</title>
<link rel="stylesheet" href="styles/webmovil.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/notificaciones.css?v=<s:property value="version" />" TYPE="text/css">
<link rel="stylesheet" href="styles/jquery-ui.css?v=<s:property value="version" />" TYPE="text/css">
<script src="scripts/jquery.min.js"></script>
<script src="scripts/jquery-ui.min.js"></script>
<script>
	$(function() {
		$(".accordion").accordion({
			heightStyle : "content",
			collapsible : true,
			active : false
		});
		$("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
		$("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
	});
</script>
<style>
.ui-tabs-vertical {
	position: relative;
	padding-left: 16em;
	/*background: none;*/
}

.ui-tabs-vertical .ui-tabs-nav {
	position: absolute;
	left: 0.25em;
	top: 0.25em;
	bottom: 0.25em;
	width: 15em;
	padding: 0.2em 0 0.2em 0.2em;
	background: #F3F7E3;
	font-family: "TeXGyreHerosCnRegular", arial, helvetica !important;
}

.ui-tabs-vertical .ui-tabs-nav li {
	right: 1px;
	width: 100%;
	border-color: #CCCCCC;
	border-right: none;
	border-bottom-width: 1px !important;
	-moz-border-radius: 4px 0px 0px 4px;
	-webkit-border-radius: 4px 0px 0px 4px;
	border-radius: 4px 0px 0px 4px;
	overflow: hidden;
	background: #E7EFC6;
	border-right: none;
}

.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-selected,.ui-tabs-vertical .ui-tabs-nav li.ui-state-active
	{
	border-right: 1px solid transparent;
	background: white;
}

.ui-tabs-vertical .ui-tabs-nav li a {
	float: right;
	width: 100%;
	text-align: right;
}

.ui-tabs-vertical>div { /*height: 35em;*/
	
}

.ui-tabs, .accordion {
	font-family: "TeXGyreHerosCnRegular", arial, helvetica !important;
	font-weight: lighter !important;
	font-size: 16px !important;
}
</style>
</head>
<body>
	<div id="mainContainer">

		<s:include value="/includes/cabecera.jsp" />

		<div id="bodyContainer">
			<table class="generalBox">
				<tr>
					<td><h2>Ayuda de Webmóvil</h2></td>
				</tr>
			</table>
			<br />

			<table class="generalBox">
				<tr>
					<td class="izquierda" style="text-align: left;">
						<div id="tabs">
							<ul>
								<li><a href="#tabs-1">Envío de SMS</a></li>
								<li><a href="#tabs-2">Envíos programados</a></li>
								<li><a href="#tabs-3">Consultar sus SMS</a></li>
								<li><a href="#tabs-4">Avisos Junta</a></li>
							</ul>
							<div id="tabs-1">
								<h2>Envío de SMS</h2>
								<div class="accordion">

									<h3>Introducción</h3>
									<div>
										<p>
											La principal funcionalidad de Webmóvil es el envío de SMS a
											través de su navegador web. La sección
											<code>Enviar Mensajes</code>
											del menú principal (que aparece como activa al entrar en la
											aplicación) contiene un formulario con todo lo que necesita
											para enviar SMS.
										</p>

										<p>
											Los SMS serán enviados por la plataforma de Servicios SMS de
											Sandetel (P3S) y recibidos en los terminales de destino. Con
											las opciones por defecto, si alguno de los terminales de
											destino tiene instalada la aplicación <a
												href="http://juntadeandalucia.es/organismos/empleoempresaycomercio/areas/tic-telecomunicaciones/serv-corporativos/paginas/avisos-junta.html">Avisos
												Junta</a> el envío se realizará como una notificación Push en
											vez de como un SMS.
										</p>

										<p>
											Es posible consultar si un conjunto de destinatarios tiene
											instalada la aplicación <a
												href="http://juntadeandalucia.es/organismos/empleoempresaycomercio/areas/tic-telecomunicaciones/serv-corporativos/paginas/avisos-junta.html">Avisos
												Junta</a> desde el propio Webmóvil, en la subsección <a
												href="http://localhost:8080/webmovil/actionNotificacionesJDAInfoClientes.action">Información
												sobre los destinatarios</a> dentro de la sección <a
												href="actionNotificacionesJDA.action">Avisos Junta</a>.
											Puede obtener más información sobre <strong>Avisos
												Junta</strong> en esta misma ayuda.
										</p>

										<p>La longitud de un SMS está limitada a 160 caracteres.
											Si necesita enviar textos más largos no tiene más que
											introducirlos normalmente y el sistema se encargará de
											segmentarlo. En todo momento puede consultar el contador de
											caracteres (que le indica cuántos caracteres restantes
											dispone) y el contador de mensajes (que le indica en cuántos
											mensajes se ha tenido que segmentar su texto). Tenga en
											cuenta que la facturación es por segmentos, no por mensajes
											completos. Un mensaje que tenga que ser dividido en dos
											segmentos (si, por ejemplo, ocupa 200 caracteres) se
											facturará como dos SMS enviados ya que, en la práctica, se
											envía dos SMS.</p>
									</div>

									<h3>Destinatarios</h3>
									<div>
										<p>Puede especificar un número de teléfono como
											destinatario de su mensaje, o hacer uso de la agenda, en cuyo
											caso podrá seleccionar todo un grupo de contactos como
											destinatario de mensajes.</p>
										<h4>Entrada directa</h4>
										<p>
											Puede introducir el número de teléfono del destinatario
											directamente en el campo de texto que aparece en la zona
											central de la fila etiquetada como
											<code>Destinatario</code>
											.
										</p>
										<p>
											En el caso que desée enviar un SMS a un destinatario fuera de
											España, deberá seleccionar
											<code>Internacional</code>
											en el cuadro desplegable e introducir el número de teléfono
											completo, incluyendo los prefijos necesarios.
										</p>
										<h4>A través de la agenda</h4>
										<p>
											Para acceder a la agenda, pulse en el enlace
											<code>Acceso a la agenda</code>
											. Se abrirá una ventana emergente con una vista de la agenda.
											Desde aquí puede:
										<ul>
											<li><strong>Enviar un mensaje a un sólo
													contacto</strong>:
												<ul>
													<li>Verá una lista de grupos. Navegue al grupo en el
														que se encuentra su contacto. Si su contacto está en el
														grupo raíz, omita este paso.</li>
													<li>Una vez que esté en el grupo en el que se
														encuentra el contacto al que desea enviar el mensaje,
														seleccione <code>Contactos</code> en el desplegable
														ubicado en la parte superior de la ventana y pulse sobre
														el botón <code>Ver</code>.
													</li>
													<li>Ahora verá una lista de contactos dentro del grupo
														actual. Pulse sobre el contacto al que desea enviar el
														mensaje. <em>Nota: aquí puede ver fácilmente si un
															destinatario tiene instalada la aplicación <strong>Avisos
																Junta</strong> o no.
													</em>
													</li>
													<li>La ventana de la agenda se cerrará y volverá al
														formulario de envío. Puede comprobar como en el cuadro de
														texto de la fila etiquetada como <code>Destinatario</code>
														aparece el nombre del contacto que acaba de seleccionar.
												</ul></li>
											<br />
											<li><strong>Enviar un mensaje a un grupo de
													contactos</strong>:
												<ul>
													<li>Verá una lista de grupos. Navegue al grupo que
														contiene al subgrupo al que desea enviar el mensaje. Si el
														subgrupo está en el grupo raíz, omita este paso.</li>
													<li>Una vez que pueda ver el grupo al que desea enviar
														su mensaje, pulse sobre el icono <img
														src="images/icono-enviar.png" /> que aparece junto al
														nombre del grupo.
													</li>
													<li>La ventana de la agenda se cerrará y volverá al
														formulario de envío. Puede comprobar como en el cuadro de
														texto de la fila etiquetada como <code>Destinatario</code>
														aparece el nombre del grupo que acaba de seleccionar.
												</ul></li>
											<br />
										</ul>
									</div>

									<h3>Texto del mensaje</h3>
									<div>
										<p>
											En el area de texto situada en la fila etiquetada como
											<code>Texto</code>
											es donde debe teclear el contenido de su mensaje. Puede ver
											cuántos caracteres le quedan en el indicador
											<code>Caracteres / Mensaje</code>
											situado justo encima.
										</p>
										<p>
											Si escribe más de 160 caracteres (incluyendo las firmas
											personal y del emisor, si están activas) su mensaje se
											segmentará. Recuerde que se facturarán tantos SMS como el
											número de segmentos en el que se divida su mensaje. Puede
											consultar el número de segmentos en el indicador
											<code>Mensajes</code>
											.
										</p>
									</div>

									<h3>Firmas</h3>
									<div>
										<p>Al texto del mensaje se puede añadir dos firmas que se
											concatenarán al final del mismo.</p>
										<ul>
											<li><strong>Firma personal</strong>: Puede editar esta
												firma directamente sobre el cuadro de texto (los cambios no
												serán persistentes) o en la sección <a
												href="actionPreferenciasUsuario.action">Preferencias</a> (en
												cuyo caso, los cambios sí serán persistentes). También puede
												elegir no incluirla desmarcando la casilla correspondiente.</li>
											<li><strong>Firma corporativa</strong>: No podrá editar
												este texto, ya que lo establece el administrador de su
												organismo emisor para todos los usuarios. Si su
												administrador le da permiso, puede desactivar esta firma.
												Por defecto, los usuarios se crean sin este permiso, por lo
												que es probable que deba solicitárselo a su administrador.</li>
										</ul>
									</div>

									<h3>Acuse de recibo</h3>
									<div>
										<p>
											Marque esta casilla si desea solicitar al terminar de destino
											que envíe un acuse de recibo. El envío del acuse de recibo
											depende totalmente del terminarl de destino, por lo que no
											recibir un acuse no implica que el mensaje no haya llegado.
											Puede consultar el estado de los mensajes enviados con acuse
											de recibo en la sección <a href="actionCarpetasLista.action">Carpetas</a>,
											dentro de su carpeta de enviados.
										</p>
									</div>

									<h3>Prioridad alta</h3>
									<div>
										<p>Marque esta casilla si desea que su mensaje tenga
											prioridad alta. En el caso de encolarse varios mensajes al
											mismo destinatario (por ejemplo, si el teléfono no es
											accesible por la red móvil), se dará salida primero a los que
											tengan prioridad alta.
									</div>

									<h3>Vigencia</h3>
									<div>
										<p>Seleccione cuánto tiempo debe permanecer el mensaje en
											el sistema si no puede entregarse inmediatamente.</p>
									</div>

									<h3>Tipo de envío</h3>
									<div>
										<p>Aquí puede seleccionar el tipo de envío que prefiere:</p>
										<ul>
											<li><strong>Estándar</strong> enviará el mensaje como
												notificación Push a los destinatarios que tengan
												instalada la aplicación <a
												href="http://juntadeandalucia.es/organismos/empleoempresaycomercio/areas/tic-telecomunicaciones/serv-corporativos/paginas/avisos-junta.html">Avisos
													Junta</a>, o como SMS al resto.</li>
											<li><strong>Forzar SMS</strong> enviará SMS
												en cualquier caso.</li>
											<li><strong>Sólo Avisos Junta</strong> enviará una
												notificación a la aplicación <a
												href="http://juntadeandalucia.es/organismos/empleoempresaycomercio/areas/tic-telecomunicaciones/serv-corporativos/paginas/avisos-junta.html">Avisos
													Junta</a> (si el destinatario no tiene la aplicación instalada,
												no recibirá el mensaje y el proceso devolverá un error).</li>
										</ul>
									</div>
								</div>
							</div>
							<div id="tabs-2">
								<h2>Envíos programados</h2>
								<div class="accordion">
									<h3>Introducción</h3>
									<div>
										<p>
											Webmóvil permite programar un envío masivo a una lista de
											destinatarios a una fecha y hora específicas. Podrá
											especificar la lista de destinatarios de forma manual
											(mediante un archivo de texto) o seleccionar un grupo de la
											agenda. Además, podrá configurar el envío de la misma forma
											que un envío normal (ver la sección <strong>Envío de
												SMS</strong> de esta ayuda para más detalles).
										</p>
										<p>Podrá consultar el estado de los envíos programados que
											no hayan sido ejecutados todavía, así como un históricos de
											los que ya lo fueron.</p>
									</div>
									<h3>Destinatarios</h3>
									<div>
										<p>Puede especificar la lista de destinatarios de diversas
											formas:</p>
										<h4>Archivo de lista de destinatarios</h4>
										<p>Puede subir un archivo de texto con una lista de
											destinatarios. Cada entrada del archivo recibirá un mensaje
											idéntico, salvo que escriba un mensaje personalizado para
											alguna entrada en concreto. El número de mensajes
											personalizados es ilimitado dentro de cada lista de
											destinatarios. Si lo desea, puede incluso enviar un mensaje
											diferente a cada entrada de la lista.</p>
										<p>
											El archivo de datos para realizar <b>envíos masivos</b> es un
											archivo de texto plano con un formato muy simple:
										</p>
										<ul>
											<li>Aparecerá una línea de texto en el archivo por cada
												número de teléfono al que queramos enviar el mensaje
												definido en el formulario.</li>
											<li>Alternativamente podemos especificar <b>mensajes
													personalizados</b> para los números que queramos de la lista.
												El sistema enviará esos mensajes personalizados en lugar del
												mensaje definido en el formulario.
											</li>
										</ul>
										<p>
											El formato general de cada linea del <b>archivo de datos</b>
											es el siguiente:
										</p>
										<div class="v10negro"
											style="margin-left: 3em; margin-right: 3em; margin-top: 1em; margin-bottom: 1em; border: 1px #006600 solid; padding: .5em .5em .5em .5em; background-color: #e7efc6;">
											<p align='center'>&lt;número de teléfono&gt; [&lt;mensaje
												personalizado&gt;]</p>
										</div>
										<p>
											En el caso de incluir un <b>mensaje personalizado</b>, éste
											debe separarse del número de teléfono por un espacio.
										</p>
										<p>Se debe tener en cuenta que:</p>
										<ul>
											<li>El texto no puede contener caracteres extraños
												(tildes, barras, parentesis, etc..).</li>
											<li>El texto debe contener 158 caracteres como máximo.</li>
										</ul>

										<p>
											A continuación se muestra un <b>ejemplo</b> de <b>archivo
												de datos</b>:
										</p>

										<div class="v10negro"
											style="margin-left: 3em; margin-right: 3em; margin-top: 1em; margin-bottom: 1em; border: 1px #006600 solid; padding: .5em .5em .5em .5em; background-color: #e7efc6;">
											<pre style="text-align: left">34699488499
34656666888
34669989898 Este es un mensaje personalizado.
34662229222
34610101010 Este es otro mensaje personalizado.
34633000222</pre>
										</div>
										<p>El ejemplo anterior enviará seis mensajes: cuatro con
											el texto introducido en el campo "Texto" del formulario y dos
											(uno al 34669989898 y otro al 34610101010) con los mensajes
											especificados a continuación del número.</p>

										<h4>Reutilizar archivos anteriores</h4>
										<p>
											Cada vez que sube un archivo de destinatarios, este se queda
											almacenado en el sistema. Para reutilizar algún archivo que
											haya utilizado anteriormente, en lugar de subirlo de nuevo
											puede seleccionarlo del desplegable
											<code>Utilizar fichero anterior</code>
											.
										<h4>Obtener la lista de un grupo de la agenda</h4>
										<p>
											Puede seleccionar un grupo de la agenda para realizar el
											envío masivo programado. Para ello pulse sobre el enlace
											<code>Acceso a la agenda</code>
											. Se abrirá una vista de la agenda en una ventana emergente.
										</p>
										<ul>
											<li>Verá una lista de grupos. Navegue al grupo que
												contiene al subgrupo al que desea enviar el mensaje. Si el
												subgrupo está en el grupo raíz, omita este paso.</li>
											<li>Una vez que pueda ver el grupo al que desea enviar
												su mensaje, pulse sobre el icono <img
												src="images/icono-enviar.png" /> que aparece junto al
												nombre del grupo.
											</li>
											<li>La ventana de la agenda se cerrará y volverá al
												formulario de envío. Puede comprobar como en el cuadro de
												texto de la fila etiquetada como <code>Destinatario</code>
												aparece el nombre del grupo que acaba de seleccionar.
										</ul>
									</div>

									<h3>Texto general</h3>
									<div>
										<p>El texto general es el texto que se enviará a los
											destinatarios de la lista para los que no haya definido un
											texto personalizado (o a todos, si envía a un grupo de la
											agenda).</p>
										<p>
											En el area de texto situada en la fila etiquetada como
											<code>Texto</code>
											es donde debe teclear el contenido de su mensaje. Puede ver
											cuántos caracteres le quedan en el indicador
											<code>Caracteres / Mensaje</code>
											situado justo encima.
										</p>
										<p>
											Si escribe más de 160 caracteres (incluyendo las firmas
											personal y del emisor, si están activas) su mensaje se
											segmentará. Recuerde que se facturarán tantos SMS como el
											número de segmentos en el que se divida su mensaje. Puede
											consultar el número de segmentos en el indicador
											<code>Mensajes</code>
											.
										</p>
									</div>

									<h3>Firmas</h3>
									<div>
										<p>Al texto del mensaje se puede añadir dos firmas que se
											concatenarán al final del mismo.</p>
										<ul>
											<li><strong>Firma personal</strong>: Puede editar esta
												firma directamente sobre el cuadro de texto (los cambios no
												serán persistentes) o en la sección <a
												href="actionPreferenciasUsuario.action">Preferencias</a> (en
												cuyo caso, los cambios sí serán persistentes). También puede
												elegir no incluirla desmarcando la casilla correspondiente.</li>
											<li><strong>Firma corporativa</strong>: No podrá editar
												este texto, ya que lo establece el administrador de su
												organismo emisor para todos los usuarios. Si su
												administrador le da permiso, puede desactivar esta firma.
												Por defecto, los usuarios se crean sin este permiso, por lo
												que es probable que deba solicitárselo a su administrador.</li>
										</ul>
									</div>

									<h3>Acuse de recibo</h3>
									<div>
										<p>
											Marque esta casilla si desea solicitar al terminar de destino
											que envíe un acuse de recibo. El envío del acuse de recibo
											depende totalmente del terminarl de destino, por lo que no
											recibir un acuse no implica que el mensaje no haya llegado.
											Puede consultar el estado de los mensajes enviados con acuse
											de recibo en la sección <a href="actionCarpetasLista.action">Carpetas</a>,
											dentro de su carpeta de enviados.
										</p>
									</div>

									<h3>Fecha y hora de envío</h3>
									<div>
										<p>
											Especifique la fecha y hora en la que quiere que se realice
											el envío masivo programado empleando los diversos
											desplegables de la fila etiquetada
											<code>Programar el</code>
											. Podrá especificar la hora con una precisión de quince
											minutos.
										</p>
									</div>

									<h3>Nombre para almacenar el envío</h3>
									<div>
										<p>Debe especificar un nombre para identificar el envío en
											el sistema. Le ayudará a localizarlo y en las vistas de
											envíos programados y el histórico de envíos. Debe rellenar
											este campo obligatoriamente.</p>
									</div>

									<h3>Tipo de envío</h3>
									<div>
										<p>Aquí puede seleccionar el tipo de envío que prefiere:</p>
										<ul>
											<li><strong>Estándar</strong> enviará el mensaje como
												notificación Push a los destinatarios que tengan
												instalada la aplicación <a
												href="http://juntadeandalucia.es/organismos/empleoempresaycomercio/areas/tic-telecomunicaciones/serv-corporativos/paginas/avisos-junta.html">Avisos
													Junta</a>, o como SMS al resto.</li>
											<li><strong>Forzar SMS</strong> enviará SMS
												en cualquier caso.</li>
											<li><strong>Sólo Avisos Junta</strong> enviará una
												notificación a la aplicación <a
												href="http://juntadeandalucia.es/organismos/empleoempresaycomercio/areas/tic-telecomunicaciones/serv-corporativos/paginas/avisos-junta.html">Avisos
													Junta</a> (si el destinatario no tiene la aplicación instalada,
												no recibirá el mensaje y el proceso devolverá un error).</li>
										</ul>
									</div>

									<h3>Consulta de envíos programados</h3>
									<div>
										<p>
											Puede consultar los envíos que tiene programados y que aún no
											han sido ejecutados pulsando en el enlace
											<code>Envíos programados</code>
											de la parte inferior del formulario.
										</p>
										<p>
											También puede consultar los envíos que ya han sido ejecutados
											(y ver el resultado de dichos envíos) pulsando en el enlace
											<code>Histórico de envíos</code>
											de la parte inferior del formulario.
										</p>
									</div>
								</div>
							</div>
							<div id="tabs-3">
								<h2>Consultar sus SMS</h2>
								<div class="accordion">
									<h3>Introducción</h3>
									<div>
										<p>
											En la sección
											<code>Carpetas</code>
											del menú principal podrá consultar todos los SMS que ha
											enviado, los que ha recibido (si su usuario tiene permiso
											para recibir mensajes), y crear si lo desea una serie de
											carpetas personalizadas donde mover mensajes para su
											almacenaje y posterior consulta.
										</p>
									</div>
									<h3>Mensajes enviados</h3>
									<div>
										<p>
											Para acceder a la lista de mensajes que ha enviado, pulse
											sobre la carpeta
											<code>Enviados</code>
											.
										</p>
										<p>
											En la vista de mensajes enviados verá una lista de los
											mensajes que ha enviado. Por defecto se muestran los mensajes
											enviados durante el mes en curso, pero puede cambiar el
											intervalo mostrado empleando el formulario
											<code>Seleccionar intervalo</code>
											situado sobre la lista.
										</p>
										<p>
											Si el número de mensajes en el intervalo excede el número de
											resultados que tiene configurado en la sección <a
												href="actionPreferenciasUsuario.action">Preferencias</a>
											para que se muestren de una vez, la lista se dividirá en
											páginas. Puede navegar por las diferentes páginas empleando
											el panel de navegación en la parte inferior de la lista,
											donde podrá avanzar o retroceder una página, o moverse a la
											primera o última páginas directamente.
										</p>
										<p>
											Además puede seleccionar un número de mensajes y borrarlos o
											moverlos a una carpeta personalizada marcando las casillas
											correspondientes y empleando los botones
											<code>Borrar seleccionados</code>
											y
											<code>Mover seleccionados a</code>
											, respectivamente.
										</p>

										<p>En la lista de mensajes puede ver el destinatario y el
											texto de cada mensaje. Tenga en cuenta que los mensajes que
											se tuvieron que segmentar aparecerán separados en segmentos,
											con una entrada para cada segmento. Además se muestra la
											fecha de envío y el estado:</p>
										<ul>
											<li>Si el mensaje fue enviado correctamente aparecerá un
												icono con una flecha verde: <img
												src="images/pic_enviado.gif" />
											</li>
											<li>Si el mensaje se envió marcando la opción <code>Acuse
													de recibo</code> y dicho acuse fue enviado por el terminal de
												destino, aparecerá un icono con una flecha azul: <img
												src="images/pic_recibido.gif" /></li>
											<li>Si ocurrió un error al enviar, aparecerá un sobre
												con un aspa: <img src="images/pic_error.gif" />
											</li>
											<li>Si el mensaje se envió a un destinatario como
												notificación push, aparecerá el icono <img
												src="images/push.png" />
											</li>
										</ul>

										<p>
											Si desea obtener los resultados en una hoja excel, pulse
											sobre el enlace
											<code>Obtener resultados en hoja Excel</code>
											, situado en la parte inferior de la página.
										</p>
									</div>
									<h3>Mensajes recibidos</h3>
									<div>
										<p>
											Para acceder a la lista de mensajes que ha enviado, pulse
											sobre la carpeta
											<code>Enviados</code>
											. Tenga en cuenta que para poder recibir SMS en la pasarela
											P3S su usuario debe tener permiso. Si su usuario no tiene
											permiso para recibir, solicítelo a su administrador.
										</p>
										<p>La funcionalidad de la carpeta recibidos es muy
											parecida a la de la carpeta enviados. Puede cambiar el
											intervalo, moverse por las diversas páginas de resultados,
											consultar datos sobre los mensajes recibidos, borrar o mover
											mensajes, u obtener los resultados en una hoja excel.
											Consulte la ayuda sobre la consulta de mensajes enviados para
											obtener más información.</p>
									</div>
									<h3>Almacenar mensajes en carpetas personalizadas</h3>
									<div>
										<p>El sistema le permite almacenar mensajes que haya
											recibido o enviado en carpetas personalizadas para su
											posterior consulta.</p>
										<h4>Crear una carpeta personalizada</h4>
										<p>
											En la vista de lista de carpetas (la que se muestra al entrar
											en la sección
											<code>Carpetas</code>
											) introduzca un nombre de carpeta en el cuadro de texto y
											pulse sobre el botón
											<code>Crear carpeta</code>
											. Una nueva carpeta con el nombre que le ha asignado
											aparecerá en la lista de carpetas.
										</p>
										<h4>Mover mensajes a una carpeta personalizada</h4>
										<p>
											Acceda a cualquier carpeta (Enviados, recibidos...), localice
											los mensajes que desée mover, márquelos, seleccione el nombre
											de la carpeta de destino en el desplegable, y pulse sobre el
											botón
											<code>Mover seleccionados a</code>
											.
									</div>
								</div>
							</div>
							<div id="tabs-4">
								<h2>Avisos junta</h2>
								<div class="accordion">
									<h3>Introducción</h3>
									<div>
										<p>
											<a
												href="http://juntadeandalucia.es/organismos/empleoempresaycomercio/areas/tic-telecomunicaciones/serv-corporativos/paginas/avisos-junta.html">Avisos
												Junta</a> es una aplicación para terminales móviles que permite
											a la ciudadanía recibir y gestionar de forma centralizada los
											mensajes de las distintas entidades de la Junta de Andalucía
											a través de un terminal iOS o Android.
										</p>
										<p>La aplicación puede recibir dos tipos de mensajes:</p>
										<ul>
											<li><strong>SMS</strong>: Al enviar un SMS a través de
												la P3S (empleando, por ejemplo y en nuestro caso, Webmóvil),
												si un destinatario tiene instalada la App, el sistema no
												enviará un SMS sino una notificación Push a dicha App. Por
												consiguiente, el usuario recibirá el mensaje en la App (y
												podrá gestionarlo desde allí) en lugar de un SMS.</li>
											<li><strong>Notificaciones con adjuntos</strong>: Este
												tipo de mensajes son específicos de <strong>Avisos
													Junta</strong>. Además del texto del mensaje, se puede adjuntar al
												mismo diversos contenidos:
												<ul>
													<li><strong>Imágenes</strong></li>
													<li><strong>Enlaces a videos</strong> (youtube)</li>
													<li><strong>Localizaciones geográficas</strong></li>
													<li><strong>Citas de calendario</strong> (con
														localización geográfica)</li>
													<li><strong>Documentos</strong></li>
													<li><strong>Audio</strong></li>
													<li><strong>Menús de opciones sencillos</strong> (debe
														de disponer de infraestructura para ello, como se detalla
														más adelante)</li>
												</ul></li>
										</ul>
									</div>

									<h3>Funcionalidades del sistema</h3>
									<div>
										<p>
											Además de la recepción de SMS y notificaciones con adjuntos,
											y de cara a los organismos emisores, el sistema de <strong>Avisos
												Junta</strong> proporciona una serie de funcionalidades muy
											interesantes y que detallamos a continuación:
										</p>
										<h4>Localización de terminales</h4>
										<p>Si el usuario lo habilita, la aplicación actualizará la
											localización del terminal de forma periódica y cada vez que
											se active. Esta característica nos brinda la posibilidad de:</p>
										<ul>
											<li>Enviar una notificación a los terminales ubicados en
												cierto área geográfica (aún no disponible desde webmóvil,
												tan solo a nivel de API)</li>
											<li>Conocer la ubicación de uno o más destinatarios y la
												fecha en la que se reportó dicha ubicación por última vez.</li>
										</ul>
										<h4>Especificar un tiempo de vida de las notificaciones</h4>
										<p>Puede especificarse una duración para las
											notificaciones enviadas, de forma que si la notificación no
											ha podido ser entregada a la App cuando se cumpla dicha
											duración, se reintente el envío como SMS.</p>
										<h4>Control de recepción / apertura / confirmación
											fidedignos</h4>
										<p>Puede consultar de un modo completamente fidedigno el
											estado de las notificaciones que envíe. Puede conocer:</p>
										<ul>
											<li>Si la App del usuario ha recibido y almacenado la
												notificación</li>
											<li>Si el usuario ha abierto la notificación</li>
											<li>Puede marcarse una confirmación de lectura en la
												notificación, de forma que el usuario pueda confirmar de
												forma específica en la App que ha leído y comprendido el
												mensaje.</li>
										</ul>
									</div>

									<h3>Las Notificaciones de Avisos Junta</h3>
									<div>
										<p>
											Las Notificaciones con adjuntos son las notificaciones
											propias del sistema <strong>Avisos Junta</strong>.
										</p>
										<p>
											Una Notificación de <strong>Avisos Junta</strong> se compone
											de un texto y una lista ordenada de contenidos adjuntos. Cada
											tipo de contenido adjunto puede aparecer un número ilimitado
											de veces, salvo por el adjunto de respuesta y el adjunto de
											confirmación
										</p>
										<p>
											Cuando la App de <strong>Avisos Junta</strong> recibe una
											notificación, mostrará todos sus contenidos adjuntos en el
											orden en el que fueron añadidos.
										</p>
									</div>

									<h3>Tipos de contenidos adjuntos</h3>
									<div>
										<p>Es posible añadir los siguientes tipos de adjuntos a
											las notificaciones:</p>
										<h4>URL</h4>
										<p>Contiene una URL (enlace) y un texto descriptivo de la
											misma.</p>
										<h4>Enlace a video</h4>
										<p>Contiene un enlace a un video de Youtube y un texto
											descriptivo.</p>
										<h4>Imágenes</h4>
										<p>El sistema admite imágenes png, jpg y gif. Las imágenes
											se suben al servidor y se accede desde ahí, no es necesario
											hospedarla en ningún sitio.</p>
										<h4>Documentos</h4>
										<p>El sistema admite documentos de tipo pdf, doc, docx y odt. 
											Los documentos se suben al servidor y son accesibles directamente desde la App.</p>
										<h4>Audio</h4>
										<p>El sistema admite audios de tipo mp3. 
											Los audios se suben al servidor y son accesibles directamente desde la App.</p>
										<h4>Localización geográfica</h4>
										<p>Una localización geográfica está formada por una
											longitud, una latitud, y un texto descriptivo. La App abren
											las localizaciones y muestran un marcador en un mapa en el
											punto especificado, permitiendo trazar rutas hasta dicho
											punto.</p>
										<h4>Cita de calendario</h4>
										<p>Una cita está compuesta por un título (que describe la
											cita), una fecha/hora de inicio y otra de finalización, una
											localización (longitud, latitud, descripción) y una URL
											opcional. La App es capaz de interpretar dichas citas y
											añadirlas a nuestros calendarios personales.
										<h4>Respuesta</h4>
										<p>
											Un adjunto de tipo respuesta sólo puede aparecer una vez por
											cada notificación, y está compuesto por un número de 1 a N
											opciones, cada opción siendo un par descripción / URL. La App
											le presentará al usuario un menú empleando las descripciones
											de cada opción. Si el usuario pulsa una de las opciones, se
											le redirigirá a la URL asociada. A dicha URL la aplicación le
											añade un parámetro GET
											<code>telefono</code>
											con el teléfono del usuario.
										</p>
										<p>
											Al crear este adjunto, puede activarse el "feedback". Si la
											App detecta que el adjunto solicita "feedback", mostrará al
											usuario un area de texto. El texto que introduzca el usuario
											será codificado en BASE64 e incluido en la llamada a la URL
											asociada a la opción que seleccione en un parémetro GET
											<code>feedback</code>
											.
										</p>
										<p>
											Este tipo de adjunto, por tanto, necesita cierta estructura
											por parte del organismo emisor que envía el mensaje. Por
											ejemplo, podría emplearse con un simple script que registrase
											confirmaciones o no confirmaciones de asistencia. Colocando
											en
											<code>http://www.ejemplo.org/</code>
											un script
											<code>confirmacion.php</code>
											que reciba un parametro
											<code>respuesta</code>
											que pueda valer si o no, y empleando el parametro "telefono"
											que añade la app, podría hacerse un registro de asistencia de
											forma muy sencilla.
										</p>
										<p>
											En este caso, crearíamos un adjunto de tipo respuesta
											indicando dos opciones "Asistiré" y "No asistiré" asociadas a
											las URL
											<code>http://www.ejemplo.org?respuesta=si</code>
											y
											<code>http://www.ejemplo.org?respuesta=no</code>
											, respectivamente.
										<p>
											Cuando la App reciba el mensaje, mostrará al usuario un menú
											con las opciones "Asistiré" y "No asistiré". Si el usuario
											selecciona, por ejemplo, "Asistiré", la App hará una llamada
											a la URL
											<code>http://www.ejemplo.org?respuesta=si&amp;telefono=XXXXXXXXX</code>
											donde
											<code>XXXXXXXXX</code>
											es el teléfono del usuario.
										</p>
										<p>Este sistema es a la vez simple y muy potente, ya que
											nos permite diseñar casi cualquier tipo de interacción
											sencilla con el usuario.</p>

										<h4>Confirmación de lectura</h4>
										<p>
											Si se incluye este adjunto, la app solicitará al usuario que
											confirme que ha leído y entendido el mensaje. Cuando el
											usuario lo haga, el mensaje se marcará con el estado especial
											"C" (ver más adelante la sección <strong>Consulta de
												Notificaciones enviadas</strong>).
										</p>
										<p>Con este método, tendremos la absoluta seguridad de que
											el destinatario ha recibido, leído y entendido el mensaje en
											cuestión, algo que era del todo imposible con los SMS.</p>
									</div>

									<h3>Composición y envío de Notificaciones</h3>
									<div>
										<p>
											La composición y envío de notificaciones se realiza desde la
											subsección <a href="actionNotificacionesJDAComponer.action">Componer
												notificación con adjuntos</a> de la sección <a
												href="actionNotificacionesJDA.action">Avisos Junta</a>.
										</p>
										<p>La vista nos muestra un formulario muy sencillo con los
											siguientes apartados:</p>
										<h4>Destino</h4>
										<p>En este area de texto podremos introducir o bien un
											número de teléfono o bien una lista de ellos separados por
											retornos de carro o comas si queremos enviar el mismo mensaje
											a varios destinatarios.</p>
										<p>
											Una vez introducida una lista de números de teléfono podemos
											obtener información sobre ellos directamente pulsando el
											enlace
											<code>Comprobar</code>
											. Este enlace abrirá una ventana emergente donde podremos
											para cada número si tiene instalada la App, y en caso
											afirmativo cuál fue la última localización geográfica
											registrada de su terminal y cuándo se notifico dicha
											localización. Esta vista es muy parecida a la que se describe
											más adelante en la sección <strong>Información sobre
												un grupo de destinatarios</strong>.
										<h4>Texto</h4>
										<p>En este area de texto podremos introducir un texto de
											hasta 4000 caracteres sin ninguna restricción sobre los
											caracteres que podemos emplear (recordemos que en los SMS no
											se pueden usar apenas caracteres extendidos).</p>
										<h4>Adjuntos</h4>
										<p>En esta zona aparecerán los adjuntos que vayamos
											añadiendo y será donde podremos editarlos, reordenarlos, o
											eliminarlos.</p>
										<p>
											Para añadir un nuevo adjunto, seleccione el tipo de adjunto
											en el desplegable etiquetado como
											<code>Nuevo Adjunto</code>
											y pulse
											<code>Aceptar</code>
											. Verá como un nuevo adjunto del tipo seleccionado se añade a
											la lista de adjuntos.
										</p>
										<p>Los adjuntos añadidos aparecerán vacíos. Tendremos que
											rellenar todos los campos de forma correcta.</p>
										<p>
											Para eliminar un adjunto de la lista, haremos click sobre el
											aspa de color rojo <img src="images/icnjda-del.png" /> que
											aparece en la parte derecha del adjunto.
										</p>
										<p>
											Podemos, como hemos dicho, reordenar los adjuntos en la
											lista. Para ello emplearemos las flechas hacia arriba y hacia
											abajo <img src="images/icnjda-up.png" /> <img
												src="images/icnjda-dw.png" /> que aparecen en la parte
											derecha de cada adjunto (salvo en los adjuntos de tipo
											respuesta, que siempre aparecerán al final).
										</p>
										<h5>Adjunto de tipo URL</h5>
										<p>Un adjunto de tipo URL está compuesto por una URL y una
											descripción de tipo URL. No olvide incluir el protocolo (http
											o https) en la URL. Webmóvil añadirá automáticamente http si
											usted no especifica protocolo.</p>
										<h5>Adjunto de tipo Cita</h5>
										<p>
											Un adjunto de tipo cita tiene dos apartados: <strong>datos
												de la cita</strong>, que está compuesto por un título, unas fechas y
											horas de inicio y de fin, y una URL opcional, y <strong>Ubicación
												de la cita</strong>, donde deberá especificar dónde se celebra la
											cita mediante longitud, latitud y una descripción. Para
											facilitar la tarea, puede pulsar sobre el enlace
											<code>Seleccionar en un mapa</code>
											, donde podrá seleccionar la ubicación en un mapa. Los tres
											campos (longitud, latitud, descripción) se rellenarán
											automáticamente según los datos suministrados por Google
											Maps.
										</p>
										<h5>Adjunto de tipo Localización</h5>
										<p>
											Al igual que la sección <em>ubicación</em> de una cita, una
											localización está compuesta por longitud, latitud y una
											descripción. Igualmente podrá hacer uso del mapa para
											seleccionar la ubicación pulsando sobre
											<code>Seleccionar en un mapa</code>
											.
										</p>
										<h5>Adjunto de tipo Enlace a video</h5>
										<p>Un enlace a video se compone de título y URL. La URL
											debe ser una URL de youtube completa, sin importar si es
											corta (https://youtu.be/...) o larga
											(https://www.youtube.com/...).</p>
										<h5>Adjunto de tipo Imagen (JPG, PNG, GIF)</h5>
										<p>Al crear un adjunto de cualquier tipo de imagen (JPG,
											PNG o GIF) aparecerá un formulario en el que puede
											seleccionar un archivo de su equipo que se subirá al
											servidor.</p>
										<h5>Adjunto de tipo Documento (PDF, DOC, DOCX, ODT)</h5>
										<p>Al igual que con las imágenes, al seleccionar un adjunto de tipo documento (PDF, DOC, DOCX, ODT) 
											aparecerá un formulario en el que puede
											seleccionar un archivo de su equipo que se subirá al
											servidor.</p>
										<h5>Adjunto de tipo Audio (MP3)</h5>
										<p>Al igual que con los documentos, al seleccionar un adjunto de tipo audio (MP3) 
											aparecerá un formulario en el que puede
											seleccionar un archivo de su equipo que se subirá al
											servidor.</p>
										<h5>Adjunto de tipo Respuesta</h5>
										<p>
											El adjunto de tipo Respuesta sólo puede añadirse una vez por
											cada Notificación. Un adjunto de tipo Respuesta se compone al
											menos por una opción. Puede añadir más opciones pulsando el
											botón
											<code>Nueva Linea</code>
											. Puede eliminar una opción pulsando sobre el aspa de color
											rojo <img src="images/icnjda-del.png" /> que aparece junto a
											cada linea.
										</p>
										<p>
											Cada linea está compuesta por un texto y una URL asociada.
											Consulte la sección <strong> tipos de contenidos
												adjuntos</strong> para más información sobre las respuestas.
										</p>
										<h5>Adjunto de tipo Confirmación de lectura</h5>
										<p>El adjunto de tipo Confirmación de lectura sólo puede
											aparecer una vez por cada Notificación. Si lo añade, la App
											solicitará al usuario una confirmación de que ha leído y
											entendido el mensaje cuando lo abra. Si el usuario confirma,
											el mensaje se actualizará en el servidor pasando a estado
											"confirmado".
										<h4>Envío</h4>
										<p>
											Cuando termine de componer la notificación y esté listo para
											enviarla, pulse el botón
											<code>Enviar</code>
										</p>
										.
									</div>

									<h3>Consulta de Notificaciones enviadas</h3>
									<div>
										<p>
											La consulta de Notificaciones enviadas se realiza desde la
											subsección <a
												href="actionNotificacionesJDAListaEnvios.action">Consultar
												notificaciones enviadas</a> de la sección <a
												href="actionNotificacionesJDA.action">Avisos Junta</a>.
										</p>
										<p>Accediendo a esta sección obtendrá una lista de todas
											las notificaciones que ha enviado a los diferentes usuarios.</p>
										<p>
											Puede navegar por la lista empleando los botones de
											paginación situados en la parte inferior. Puede controlar
											cuántos resultados se le ofrecen en cada página en la sección
											<a href="actionPreferenciasUsuario.action">Preferencias</a>.
										
										<p>Cada linea de la lista está formada por las siguientes
											columnas:</p>
										<ul>
											<li><strong>ID</strong>: Contiene el identificador de la
												Notificación en la P3S.</li>
											<li><strong>Texto</strong>: Contiene un resumen del
												texto incluido en la Notificación.</li>
											<li><strong>Adjuntos</strong>: Contiene una lista de
												iconos representando los diferentes adjuntos asociados a la
												Notificación.</li>
											<li><strong>Tlfno</strong>: Contiene el número de
												teléfono de destino.</li>
											<li><strong>Fecha/Hora</strong>: Contiene la fecha y
												hora en los que la notificación fue enviada.</li>
											<li><strong>E</strong>: Contiene el estado de la
												notificación.</li>
										</ul>
										<h4>Estados</h4>
										<p>Cada notificación puede estar en varios estados
											diferentes, que son:</p>
										<ul>
											<li><strong>X, No enviada</strong>: Ocurre cuando
												enviamos una notificación push a un usuario que no tiene
												instalada la App. El usuario recibirá un SMS de aviso en su
												lugar. Podrá ver la notificación en cuanto se instale la
												App; en ese momento la notificación pasará a estado <strong>R</strong>.</li>
											<li><strong>N, Nueva</strong>: El usuario de destino
												tiene la App instalada, le fue notificada la entrega
												mediante un push, pero la App aún no ha descargado la
												Notificación y sus adjuntos desde el servidor.</li>
											<li><strong>R, Recibida</strong>: La App ha descargado
												la Notificación y sus adjuntos del servidor. El usuario
												puede leerla en cualquier momento. Si un usuario elige
												marcar una notificación como "no leída" en la App, volverá a
												este estado.</li>
											<li><strong>A, Abierta</strong>: El usuario ha abierto
												la Notificación para visualizarla. En la App, la
												notificación se marca como "leída", aunque en este estado
												aún nada nos garantiza que, realmente, el usuario haya leído
												y comprendido la información contenida en la Notificación.</li>
											<li><strong>C, Confirmada</strong>: Si añade un adjunto
												de tipo <strong>Confirmación de lectura</strong> a su
												Notificación, cuando el usuario abra la Notificación en su
												terminal móvil se le solicitará que confirme que ha leído y
												comprendido la información suministrada. Cuando lo haga, la
												Notificación pasará a este estado.</li>
										</ul>
										<p>
											El proceso de enviar las Notificaciones incluyendo un adjunto
											de tipo <strong>Confirmación de lectura</strong> y poder
											consultar si la Notificación pasa a estado <strong>C</strong>
											puede ser muy interesante, especialmente cuando se trata de
											comunicar información importante.
										</p>
										<h4>Vista extendida / composición a partir de un mensaje
											enviado</h4>
										<p>Si hace click sobre el ID o el texto de alguna de las
											notificaciones que aparecen en la lista, se abrirá la misma
											en la vista de composición de Notificaciones, donde podrá
											examinar con detalle los adjuntos que se incluyeron o, si lo
											desea, podrá tomarla como base para componer una nueva
											Notificación.</p>
									</div>

									<h3>Información sobre un grupo de destinatarios</h3>
									<div>
										<p>
											Puede obtener información sobre un grupo de destinatarios
											accediendo a la subsección <a
												href="actionNotificacionesJDAInfoClientes.action">Información
												sobre los destinatarios</a> de la sección <a
												href="actionNotificacionesJDA.action">Avisos Junta</a>.
										</p>
										<p>
											En esta sección, puede introducir una lista de números de
											teléfono en el area de texto
											<code>Teléfonos</code>
											para obtener información sobre los mismos. Los teléfonos
											pueden estar separados por comas o saltos de linea.
										</p>
										<p>
											Una vez introducida la lista de números de teléfono pulse
											sobre el botón
											<code>Enviar</code>
											y obtendrá un listado con información sobre cada teléfono que
											haya introducido. El listado contiene la siguiente
											información:
										</p>
										<ul>
											<li><strong>App Instalada</strong>: indicará si el
												teléfono en cuestión tiene o no la App de Avisos Junta
												instalada.</li>
											<li><strong>Localización</strong>: si el usuario da su
												permiso para que la aplicación aporte periódicamente datos
												sobre la ubicación del teléfono, podrá ver aquí dicha
												ubicación en forma de latitud y longitud. Si hace click
												sobre este dato, obtendrá la ubicación en un mapa.</li>
											<li><strong>Última actualización</strong>: Contiene la
												fecha y hora de la última vez que la App actualizó la
												ubicación del teléfono en nuestro sistema.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>
